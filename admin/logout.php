<?php
require_once '../config/db.php';
require_once '../config/User.php';

$user = new User($pdo);
$user->logout();
header('Location: index.php');
