<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

//Load Composer's autoloader
require '../vendor/autoload.php';

$mail = new PHPMailer(true); // Passing `true` enables exceptions
try {
    //Server settings
    $mail->SMTPDebug = 2; // Enable verbose debug output
    $mail->isSMTP(); // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com'; // Specify main and backup SMTP servers
    $mail->SMTPAuth = true; // Enable SMTP authentication
    $mail->Username = 'test.villaalfredo@gmail.com'; // SMTP username
    $mail->Password = 'reservationsystem'; // SMTP password
    $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587; // TCP port to connect to

    //Recipients
    $mail->setFrom('test.villaalfredo@gmail.com', 'Villa Alfredo');
    $mail->addAddress('juliusdejon@gmail.com', 'Joe User'); // Add a recipient
    $mail->addAddress('juliusdejon@gmail.com'); // Name is optional
    $mail->addReplyTo('juliusdejon@gmail.com', 'Information');
    // $mail->addCC('cc@example.com');
    // $mail->addBCC('bcc@example.com');

    //Attachments
    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    //Content
    $mail->isHTML(true); // Set email format to HTML
    $mail->Subject = 'Thank you for Reserving a room at Villa Alredos Resort';
    $mail->Body = 'This is the HTML message body <b>in bold!</b>';
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    echo 'Message has been sent';
} catch (Exception $e) {
    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
}

        // Step 1 Reservation
    //     $mail = new PHPMailer(true); // Passing `true` enables exceptions
                //     //Server settings
                //     $mail->SMTPDebug = 0; // Enable verbose debug output
                //     $mail->isSMTP(); // Set mailer to use SMTP
                //     $mail->Host = 'smtp.gmail.com'; // Specify main and backup SMTP servers
                //     $mail->SMTPAuth = true; // Enable SMTP authentication
                //     $mail->Username = 'test.villaalfredo@gmail.com'; // SMTP username
                //     $mail->Password = 'reservationsystem'; // SMTP password
                //     $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
                //     $mail->Port = 587; // TCP port to connect to
                //     $mail->SMTPOptions = array(
                //         'ssl' => array(
                //             'verify_peer' => false,
                //             'verify_peer_name' => false,
                //             'allow_self_signed' => true,
                //         ),
                //     );
                //     //Recipients
                //     $mail->setFrom('test.villaalfredo@gmail.com', 'Villa Alfredo');
                //     $mail->addAddress($email_address, $first_name . ' ' . $last_name); // Add a recipient
                //     $baseurl = $_SERVER['SERVER_NAME'];
                //     $newContent = "
                //      Hi, " . $first_name . ' ' . $last_name . "<br /><br />
                //      The Reservation details you submitted is now pending.<br />
                //      To secure your reservation kindly pay 50% of the amount to our primary account below.<br/>
                //      Here&#39; your reservation details <a href='" . $baseurl . "/reservation/message.php?res_id=$encrypted'>" . $baseurl . "/reservation/message.php?res_id=$encrypted'</a>
                //      <br />
                //      <br />
                //      After you paid for the amount kindly upload it to the link above<br />
                //      <br />
                //      <br />
                //      <br />
                //      Note: You have 3 days starting from now to pay for your reservation or it will be Expired <br /> <br />

                //      Cancel Reservation? Call us or cancel it here <a href=''>" . $baseurl . "/reservation/cancel-reservations.php?res_id=$encrypted</a> <br/>
                //      <br />
                //      Regards, <br />
                //      Villa Alfredo&#39;s Resort
                //    ";

                //     //Content
                //     $mail->isHTML(true); // Set email format to HTML
                //     $mail->Subject = 'Reservation Request Step 1';
                //     $mail->Body = $newContent;
                //     $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';




        // Upload Receipt

          // $mail = new PHPMailer(true); // Passing `true` enables exceptions
            // //Server settings
            // $mail->SMTPDebug = 0; // Enable verbose debug output
            // $mail->isSMTP(); // Set mailer to use SMTP
            // $mail->Host = 'smtp.gmail.com'; // Specify main and backup SMTP servers
            // $mail->SMTPAuth = true; // Enable SMTP authentication
            // $mail->Username = 'test.villaalfredo@gmail.com'; // SMTP username
            // $mail->Password = 'reservationsystem'; // SMTP password
            // $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
            // $mail->Port = 587; // TCP port to connect to
            // $mail->SMTPOptions = array(
            //     'ssl' => array(
            //         'verify_peer' => false,
            //         'verify_peer_name' => false,
            //         'allow_self_signed' => true,
            //     ),
            // );

            // $baseurl = $_SERVER['SERVER_NAME'];

            // //Recipients
            // $mail->setFrom($email, $first_name . ' ' . $last_name);
            // $mail->addAddress('test.villaalfredo@gmail.com', $first_name . ' ' . $last_name); // Add a recipient
            // $newContent = "
            //         From $email <br />
            //         $first_name $last_name has uploaded a receipt <br />
            //         <img src='http://villaalfredos.x10host.com/admin/dist/img/receipts/$image' alt='Receipt' />

            //  ";

            // //Content
            // $mail->isHTML(true); // Set email format to HTML
            // $mail->Subject = 'Uploaded Receipt';
            // $mail->Body = $newContent;
            // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

            // if ($mail->send()) {

            //     // Send Email to Customer account
            //     $mail = new PHPMailer(true); // Passing `true` enables exceptions
            //     //Server settings
            //     $mail->SMTPDebug = 0; // Enable verbose debug output
            //     $mail->isSMTP(); // Set mailer to use SMTP
            //     $mail->Host = 'smtp.gmail.com'; // Specify main and backup SMTP servers
            //     $mail->SMTPAuth = true; // Enable SMTP authentication
            //     $mail->Username = 'test.villaalfredo@gmail.com'; // SMTP username
            //     $mail->Password = 'reservationsystem'; // SMTP password
            //     $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
            //     $mail->Port = 587; // TCP port to connect to
            //     $mail->SMTPOptions = array(
            //         'ssl' => array(
            //             'verify_peer' => false,
            //             'verify_peer_name' => false,
            //             'allow_self_signed' => true,
            //         ),
            //     );
            //     //Recipients
            //     $mail->setFrom('test.villaalfredo@gmail.com', 'Villa Alfredo');
            //     $mail->addAddress($email, $first_name . ' ' . $last_name); // Add a recipient
            //     $baseurl = $_SERVER['SERVER_NAME'];
            //     $newContent = "
            //      Hi, " . $first_name . ' ' . $last_name . "<br /><br />
            //      You have succesfully uploaded your Receipt <br />
            //      Wait for us to review the Receipt and we&#39;ll get back to you in a while
            //      <br />
            //      Here&#39; your reservation details <a href='" . $baseurl . "/reservation/message.php?res_id=$encrypted'>" . $baseurl . "/reservation/message.php?res_id=$encrypted'</a>
            //      <br />
            //      Regards, <br />
            //      Villa Alfredo&#39;s Resort
            //    ";

            //     //Content
            //     $mail->isHTML(true); // Set email format to HTML
            //     $mail->Subject = 'Payment on Review';
            //     $mail->Body = $newContent;
            //     $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
            //     if ($mail->send()) {

            //     } else {

            //     }

            // }