<?php
// --Mailing Requirements--
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;

require '../../src/Exception.php';
require '../../src/PHPMailer.php';
require '../../src/SMTP.php';

// --Mailing Requirments--

include '../../config/mysqli.php';

$image = (isset($_FILES['file']['name'])) ? $_FILES['file']['name'] : $_FILES['file']['name'] = "";
$image_2 = (isset($_FILES['file_2']['name'])) ? $_FILES['file_2']['name'] : $_FILES['file_2']['name'] = "";
$real_res_id = (isset($_POST['res_id'])) ? $_POST['res_id'] : $_POST['res_id'] = "";
$email = (isset($_POST['email'])) ? $_POST['email'] : $_POST['email'] = "";
$first_name = (isset($_POST['first_name'])) ? $_POST['first_name'] : $_POST['first_name'] = "";
$last_name = (isset($_POST['last_name'])) ? $_POST['last_name'] : $_POST['last_name'] = "";

$image = $_FILES['file']['name'];


        if ($image == '') {
            echo "<script>alert('Upload your receipt');</script>";
        } else {
        
            // upload
            $target_dir = "../../admin/dist/img/receipts/";
            $target_file = $target_dir . basename($_FILES["file"]["name"]);
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
            // Check file size
            // if ($_FILES["file"]["size"] > 500000) {
            //     echo "Sorry, your file is too large.";
            //     $uploadOk = 0;
            // }
            // // Allow certain file formats
            // if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            //     && $imageFileType != "gif") {
            //     echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            //     $uploadOk = 0;
            // }
        
            move_uploaded_file($_FILES["file"]["tmp_name"], $target_file);
        
            $sql = "UPDATE reservation SET receipt = '$image' WHERE client_reference_id='$real_res_id'";
        

        if ($mysqli->query($sql)) {

            echo '<div class="alert alert-success alert-dismissible">

          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

          Your Receipt has been Uploaded!

          </div>';

            //Send Email to the Email Account

            $encrypted = base64_encode($real_res_id);

            if ($mailing == true) {

                if ($deployedMailing == true) {

                    $from = "villa_alfredo_email@villaalfredo.x10host.com";
                    $to = "test.villaalfredo@gmail.com, $first_name.' '.$last_name";

                    $baseurl = $_SERVER['SERVER_NAME'];
                    $mailbody = "
                From $email <br />
                $first_name $last_name has uploaded a receipt <br />
                <img src='http://villaalfredos.x10host.com/admin/dist/img/receipts/$image' alt='Receipt' />

         ";
                    $subject = "Uploaded Receipt";

                    $headers = "From: $from" . "\r\n";
                    $headers .= "Reply-To: $from" . "\r\n";
                    $headers .= "MIME-Version: 1.0\r\n";
                    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
                    $resp = mail($to, $subject, $mailbody, $headers);
                    if ($resp) {

                        // Send Email to Customer account
                        $from = "villa_alfredo_email@villaalfredo.x10host.com";
                        $to = "$email, Villa Alfredo";

                        $baseurl = $_SERVER['SERVER_NAME'];
                        $mailbody = "
             Hi, " . $first_name . ' ' . $last_name . "<br /><br />
             You have succesfully uploaded your Receipt <br />
             Wait for us to review the Receipt and we&#39;ll get back to you in a while
             <br />
             Here&#39; your reservation details <a href='" . $baseurl . "/reservation/message.php?res_id=$encrypted'>" . $baseurl . "/reservation/message.php?res_id=$encrypted'</a>
             <br />
             Regards, <br />
             Villa Alfredo&#39;s Resort
           ";
                        $subject = "Payment on Review";

                        $headers = "From: $from" . "\r\n";
                        $headers .= "Reply-To: $from" . "\r\n";
                        $headers .= "MIME-Version: 1.0\r\n";
                        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

                        $resp = mail($to, $subject, $mailbody, $headers);

                        if ($resp) {

                        } else {
                            echo "<script>alert('cannot send email');</script>";
                        }
                    } else {
                        echo "<script>alert('cannot send email');</script>";
                    }
                } else {
                    // Upload Receipt

                    $mail = new PHPMailer(true); // Passing `true` enables exceptions
                    //Server settings
                    $mail->SMTPDebug = 0; // Enable verbose debug output
                    $mail->isSMTP(); // Set mailer to use SMTP
                    $mail->Host = 'smtp.gmail.com'; // Specify main and backup SMTP servers
                    $mail->SMTPAuth = true; // Enable SMTP authentication
                    $mail->Username = 'test.villaalfredo@gmail.com'; // SMTP username
                    $mail->Password = 'reservationsystem'; // SMTP password
                    $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
                    $mail->Port = 587; // TCP port to connect to
                    $mail->SMTPOptions = array(
                        'ssl' => array(
                            'verify_peer' => false,
                            'verify_peer_name' => false,
                            'allow_self_signed' => true,
                        ),
                    );

                    $baseurl = $_SERVER['SERVER_NAME'];

                    //Recipients
                    $mail->setFrom($email, $first_name . ' ' . $last_name);
                    $mail->addAddress('test.villaalfredo@gmail.com', $first_name . ' ' . $last_name); // Add a recipient
                    $newContent = "
                    From $email <br />
                    $first_name $last_name has uploaded a receipt <br />
                    <img src='http://villaalfredos.x10host.com/admin/dist/img/receipts/$image' alt='Receipt' />

             ";

                    //Content
                    $mail->isHTML(true); // Set email format to HTML
                    $mail->Subject = 'Uploaded Receipt';
                    $mail->Body = $newContent;
                    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

                    if ($mail->send()) {

                        // Send Email to Customer account
                        $mail = new PHPMailer(true); // Passing `true` enables exceptions
                        //Server settings
                        $mail->SMTPDebug = 0; // Enable verbose debug output
                        $mail->isSMTP(); // Set mailer to use SMTP
                        $mail->Host = 'smtp.gmail.com'; // Specify main and backup SMTP servers
                        $mail->SMTPAuth = true; // Enable SMTP authentication
                        $mail->Username = 'test.villaalfredo@gmail.com'; // SMTP username
                        $mail->Password = 'reservationsystem'; // SMTP password
                        $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
                        $mail->Port = 587; // TCP port to connect to
                        $mail->SMTPOptions = array(
                            'ssl' => array(
                                'verify_peer' => false,
                                'verify_peer_name' => false,
                                'allow_self_signed' => true,
                            ),
                        );
                        //Recipients
                        $mail->setFrom('test.villaalfredo@gmail.com', 'Villa Alfredo');
                        $mail->addAddress($email, $first_name . ' ' . $last_name); // Add a recipient
                        $baseurl = $_SERVER['SERVER_NAME'];
                        $newContent = "
                 Hi, " . $first_name . ' ' . $last_name . "<br /><br />
                 You have succesfully uploaded your Receipt <br />
                 Wait for us to review the Receipt and we&#39;ll get back to you in a while
                 <br />
                 Here&#39; your reservation details <a href='" . $baseurl . "/reservation/message.php?res_id=$encrypted'>" . $baseurl . "/reservation/message.php?res_id=$encrypted'</a>
                 <br />
                 Regards, <br />
                 Villa Alfredo&#39;s Resort
               ";

                        //Content
                        $mail->isHTML(true); // Set email format to HTML
                        $mail->Subject = 'Payment on Review';
                        $mail->Body = $newContent;
                        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
                        if ($mail->send()) {

                        } else {

                        }

                    }
                } //end of deployed Mailing
            } else {

                echo "<script>alert('Emailing is turned off please Report to the admin to receive an email');</script>";

            }
        } else {
            echo "<script>alert('error');</script>";
        }
    }
    
