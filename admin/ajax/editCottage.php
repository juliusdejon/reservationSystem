<?php
include '../../config/mysqli.php';
$cottage_type_id = $_POST['cottage_type_id'];
$cottage_name = $_POST['cottage_name'];
$cottage_price = $_POST['cottage_price'];
$cottage_info = $_POST['cottage_info'];

$image = (isset($_FILES['file']['name'])) ? $_FILES['file']['name'] : $_FILES['file']['name'] = "";

if ($image == '') {
    $sql = "UPDATE cottage_type SET cottage_name='$cottage_name',cottage_info = '$cottage_info',cottage_price='$cottage_price'
     WHERE cottage_type_id ='$cottage_type_id'
";
    if ($res = $mysqli->query($sql)) {
        echo '<div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        Successfully Updated!
        </div>';
        echo "<script type='text/javascript'>

        $('#loadedCottageImage').load('ajax/live/loadedCottageImage.php?cottage_type_id=" . $cottage_type_id . "');

        </script>";

    } else {
        echo "failed";
    }
} else {
    $newFileName = uniqid('uploaded-', true)
    . '.' . strtolower(pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION));

    $sql = "UPDATE cottage_type
SET cottage_name='$cottage_name',
cottage_info = '$cottage_info',
cottage_price='$cottage_price',
cottage_img = '$newFileName'
WHERE cottage_type_id ='$cottage_type_id'
";
    $res = $mysqli->query($sql);

    if ($res) {
// upload
        $target_dir = "../../img/";

        $target_file = $target_dir . $newFileName;
        $uploadOk = 1;

        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
// Check file size
        if ($_FILES["file"]["size"] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
// Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif") {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
        move_uploaded_file($_FILES["file"]["tmp_name"], $target_file);
        echo '<div class="alert alert-success alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
Successfully Updated!
</div>';
        echo "<script type='text/javascript'>

$('#loadedCottageImage').load('ajax/live/loadedCottageImage.php?cottage_type_id=" . $cottage_type_id . "');

</script>";

    }

}
