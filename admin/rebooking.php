<?php
include '../config/mysqli.php';
@$isDeleted = $_GET['success'];
@$isCancelled = $_GET['cancelled'];
@$isRebooked = $_GET['rebooking'];
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>Villa Alfredo Admin</title>

        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/adminlte.min.css">
        <!-- Semantic UI -->
        <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />




        <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap4.css">
    </head>

    <body class="hold-transition sidebar-mini">
        <div class="wrapper">

            <?php
include 'layout/navbar.php';
include 'layout/sidebar.php';
?>
                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <div class="content-header">
                        <div class="container-fluid">

                        </div>
                        <!-- /.container-fluid -->
                    </div>
                    <!-- /.content-header -->

                    <!-- Main content -->
                    <div class="content">
                        <?php if ($isDeleted == 'true') {
    echo "<div class='callout callout-success'>
                        <h5>Successfully Deleted!</h5>
                      </div>";
}?>
                        <?php if ($isDeleted == 'false') {
    echo "<div class='callout callout-info'>
                        <h5>Failed to Delete</h5>
                        <p>You are trying to delete a Booked or Cancelled reservation</p>
                      </div>";
}?>

                        <?php if ($isCancelled == 'true') {
    echo "<div class='callout callout-success'>
                        <h5>Successfully Cancelled a Reservation</h5>
                      </div>";
}?>
                        <?php if ($isCancelled == 'false') {
    echo "<div class='callout callout-info'>
                        <h5>Failed to Delete</h5>
                        <p>The Reservation Cannot be Cancelled</p>
                      </div>";
}?>

                        <?php if ($isRebooked == 'false') {
    echo "<div class='callout callout-info'>
                        <h5>The Selected Reservation cannot be Re-Book because of the ff.</h5>
                        <ul>
                        <li>The Booked Reservation is already checked in.</li>
                        </ul>
                      </div>";
}?>



                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Booked Reservation</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">

                                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                            <thead>
                                                <tr role="row">

                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending">Name</th>
                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Date</th>
                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Arrival Date</th>
                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Departure Date</th>
                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Reference Id</th>
                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Type</th>
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column descending" aria-sort="ascending">Status</th>
                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Assigned to</th>
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column descending" aria-sort="ascending">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
$sql = "SELECT * FROM customer JOIN reservation ON customer.client_reference_id=reservation.client_reference_id WHERE reservation_status='Booked'";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $client_reference_id = $rows['client_reference_id'];
    $first_name = $rows['first_name'];
    $last_name = $rows['last_name'];
    $arrival_date = new DateTime($arrival_date = $rows['arrival_date']);
    $departure_date = new DateTime($departure_date = $rows['departure_date']);
    $created_at = new DateTime($created_at = $rows['created_at']);
    $reservation_type = $rows['reservation_type'];
    $reservation_status = $rows['reservation_status'];
    $receipt = $rows['receipt'];
    $isRebook = $rows['isRebooked'];
    $assigned_to = $rows['assigned_to'];
    ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $first_name . ' ' . $last_name; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $created_at->format('F j, Y'); ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $arrival_date->format('F j, Y'); ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $departure_date->format('F j, Y'); ?>
                                                        </td>

                                                        <td>
                                                            <?php echo $client_reference_id; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $reservation_type; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $reservation_status; ?>
                                                        </td>
                                                        <td>
                                                        <?php echo  $assigned_to;?>
                                                        </td>
                                                        <td>
                                                            <div class='container'>

                                                                <input type="hidden" name="client_reference_id" value="<?php echo $client_reference_id; ?>" />
                                                                <a class='btn btn-info btn-sm' href="overview.php?ref_id=<?php echo $client_reference_id; ?>&view_by=reservation">Details <i class='fa fa-search'></i></a>
                                                                <?php
if ($reservation_status == 'Pending') {

    } else {
        ?>
        <?php if ($isRebook) {

        } else {?>
   <a class='btn bg-primary btn-sm' href="rebook_reservation.php?res_id=<?php echo $client_reference_id; ?>" onclick="return confirm('Are you sure you want to Rebook this item?');">Rebook <i class='fa fa-edit'></i></a>

        <?php
}
    }?>
                                                                <?php if ($user_role == 'frontdesk') {} else {
        ?>
                                                                <?php }?>
                                                            </div>

                                                            </div>
                                                        </td>
                                               
                                                    </tr>
                                                    <?php
}
?>
                                            </tbody>

                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.content -->
                </div>
                <!-- /.content-wrapper -->

                <!-- Main Footer -->
                <?php
include 'layout/footer.php';
?>
                    <!-- ./wrapper -->

                    <!-- REQUIRED SCRIPTS -->


                    <!-- jQuery -->
                    <script src="plugins/jquery/jquery.min.js"></script>
                    <!-- Downpayment Ajax -->
                    <script type="text/javascript" src="ajax/acceptDownpayment.js"></script>
                    <!-- Bootstrap 4 -->
                    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
                    <!-- DataTables -->
                    <script src="plugins/datatables/jquery.dataTables.js"></script>
                    <script src="plugins/datatables/dataTables.bootstrap4.js"></script>
                    <!-- AdminLTE App -->
                    <script src="dist/js/adminlte.min.js"></script>
                    <script>
                        $(function() {

                            $('#example1').DataTable({
                                "paging": true,
                                "lengthChange": false,
                                "searching": true,
                                "ordering": true,
                                "info": true,
                                "autoWidth": false,
                                "stripeClasses": ['odd-row', 'even-row'],
                                "order": [
                                    [4, "desc"]
                                ]
                            });
                        });
                    </script>
    </body>

    </html>
    <?php
if (isset($_POST['approve'])) {
    $client_reference_id = $_POST['client_reference_id'];

    $sql = "UPDATE reservation SET reservation_status ='Booked' WHERE client_reference_id='$client_reference_id'";
    if ($mysqli->query($sql)) {
        echo "<script>alert('Approved!');</script>";
    } else {
        echo "<script>alert('Denied!');</script>";
    }
}

if (isset($_POST['delete'])) {
    $client_reference_id = $_POST['client_reference_id'];
    $sql = "DELETE FROM reservation WHERE client_reference_id='$client_reference_id'";
    if ($mysqli->query($sql)) {
        $sql = "DELETE FROM reserved_rooms WHERE client_reference_id ='$client_reference_id'";
        if ($mysqli->query($sql)) {
            echo "<script>alert('Deleted!');</script>";
        }
    }
}