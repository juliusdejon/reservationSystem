<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>Villa Alfredo's Reservation System</title>

    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7CVarela+Round" rel="stylesheet">

    <!-- Bootstrap -->
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Owl Carousel -->
    <link type="text/css" rel="stylesheet" href="css/owl.carousel.css" />
    <link type="text/css" rel="stylesheet" href="css/owl.theme.default.css" />

    <!-- Magnific Popup -->
    <link type="text/css" rel="stylesheet" href="css/magnific-popup.css" />

    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="css/font-awesome.min.css">

    <!-- Custom stlylesheet -->
    <link type="text/css" rel="stylesheet" href="css/style.css" />

</head>

<body>
    <!-- Header -->
    <header id="home">
        <!-- Background Image -->
        <div class="bg-img" style="background-image: url('./img/background1.jpg');">
            <div class="overlay"></div>
        </div>
        <!-- /Background Image -->

        <!-- Nav -->
        <nav id="nav" class="navbar nav-transparent">
            <div class="container">

                <div class="navbar-header">
                    <!-- Logo -->
                    <div class="navbar-brand">
                        <a href="index.php">
                            <img class="logo" src="img/valogo-alt.png" alt="logo">
                            <img class="logo-alt" src="img/valogo-alt.png" alt="logo">
                        </a>
                    </div>
                    <!-- /Logo -->

                    <!-- Collapse nav button -->
                    <div class="nav-collapse">
                        <span></span>
                    </div>
                    <!-- /Collapse nav button -->
                </div>

                <!--  Main navigation  -->
                <ul class="main-nav nav navbar-nav navbar-right">
                    <li><a href="#home">Home</a></li>
                    <li><a href="#accomodation">Accomodation</a></li>
                    <li><a href="#day-tour-cottages">Cottages</a></li>
                    <li><a href="#contact">Contact</a></li>
                    <li><a href="./reservation/upload_receipt.php">Upload Receipt</a></li>
                    <li><a href="./reservation/index.php"><button class="secondary-btn">Book Now</button></a></li>
                </ul>
                <!-- /Main navigation -->

            </div>
        </nav>
        <!-- /Nav -->

        <!-- home wrapper -->
        <div class="home-wrapper">
            <div class="container">
                <div class="row">

                    <!-- home content -->
                    <div class="col-md-10 col-md-offset-1">
                        <div class="home-content">
                            <img src="img/valogo.png" class="main-logo" />
                            <h3 class="white-text">Welcome to your own Paradise</h3>
                            <p class="white-text">
                                Welcome to Villa Alfredo’s Resort, the most stylish garden resort, located in the heart of Pampanga, Philippines. It is just an hour and a half away from the busy city life of Manila.


                            </p>
                            <a href="./reservation/index.php" class="white-btn">Reserve now!</a>
                            <a href="#accomodation" class="main-btn">Learn More</a>
                        </div>
                    </div>
                    <!-- /home content -->

                </div>
            </div>
        </div>
        <!-- /home wrapper -->

    </header>
    <!-- /Header -->

    <!-- AVP -->
    <div id="avp" class="section sm-padding">

        <!-- Background Image -->
        <div class="bg-img-secondary" style="background-image: url('./img/colorfulParrot.jpg');">
            <div class="overlay-secondary"></div>
        </div>
        <!-- /Background Image -->

        <!-- Container -->
        <div class="container">

            <!-- Row -->
            <div class="avp-section">
                <div class="row">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-6">
                        <h1>Our AVP</h1>
                        <!-- <iframe width="100%" height="300" src="https://www.youtube.com/embed/f0a38zBXR8k?modestbranding=0&autohide=1&showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> -->
                    </div>
                </div>
            </div>
            <!-- /Row -->

        </div>
        <!-- /Container -->

    </div>
    <!-- /AVP -->


    <!-- Accomodation -->
    <div id="accomodation" class="">

        <!-- Container -->
        <div class="container-fluid">

            <!-- Row -->
            <div class="row">

                <!-- Section header -->
                <div class="section-header text-center">
                    <h2 class="title">Accomodations</h2>
                    <p class="subtitle">Our 90 room resort offers 3 spacious room types: Hotel type, villas and cottages. All rooms are perfectly set amidst the well-landscaped tropical garden. Most rooms offers direct pool or garden view which adds to the relaxing ambience
                        of the resort.</p>
                </div>
                <!-- /Section header -->

                <!-- Room -->
                <div class="col-md-3 col-xs-6 room">
                    <img class="img-responsive" src="./img/family-villa.jpg" alt="">
                    <div class="room-content">
                        <h3>Family Villa</h3>
                        <div>PHP8,000.00/per night</div>
                    </div>
                </div>
                <!-- /Room -->

                <!-- Room -->
                <div class="col-md-3 col-xs-6 room">
                    <img class="img-responsive" src="./img/bella-rooms.jpg" alt="">
                    <div class="room-content">
                        <h3>Bella Rooms</h3>
                        <div>PHP8,000.00 - PHP10,000.00/per night </div>
                    </div>
                </div>
                <!-- /Room -->

                <!-- Room -->
                <div class="col-md-3 col-xs-6 room">
                    <img class="img-responsive" src="./img/ana-villa.jpg" alt="">
                    <div class="room-content">
                        <h3>Ana Villa</h3>
                        <div>PHP2,000.00/per night</div>
                    </div>
                </div>
                <!-- /Room -->

                <!-- Room -->
                <div class="col-md-3 col-xs-6 room">
                    <img class="img-responsive" src="./img/hotel-rooms.jpg" alt="">
                    <div class="room-content">
                        <h3>Hotel Rooms</h3>
                        <div>PHP2,500.00/per night</div>
                    </div>
                </div>
                <!-- /Room -->
                <!-- Room -->
                <div class="col-md-3 col-xs-6 room">
                    <img class="img-responsive" src="./img/cottage-rooms.jpg" alt="">
                    <div class="room-content">
                        <h3>Cottage Rooms</h3>
                        <div>PHP3,500.00 - PHP4,500.00/per night</div>
                    </div>
                </div>
                <!-- /Room -->
                <!-- Room -->
                <div class="col-md-3 col-xs-6 room">
                    <img class="img-responsive" src="./img/nic-villa.jpg" alt="">
                    <div class="room-content">
                        <h3>Nic Villa</h3>
                        <div>PHP4,500.00 - PHP5,000.00/per night</div>
                    </div>
                </div>
                <!-- /Room -->
                <!-- Room -->
                <div class="col-md-3 col-xs-6 room">
                    <img class="img-responsive" src="./img/raf-villa.jpg" alt="">
                    <div class="room-content">
                        <h3>Raf Villa</h3>
                        <div>PHP8,000.00/per night</div>
                    </div>
                </div>
                <!-- /Room -->
                <!-- Room -->
                <div class="col-md-3 col-xs-6 room">
                    <img class="img-responsive" src="./img/lyn-villa.jpg" alt="">
                    <div class="room-content">
                        <h3>Lyn Villa</h3>
                        <div>PHP 3,500.00 - PHP5,000.00/per night</div>
                    </div>
                </div>
                <!-- /Room -->


            </div>
            <!-- /Row -->

        </div>
        <!-- /Container -->

    </div>
    <!-- /Accomodation -->



    <!-- Day Tour Cottages -->
    <div id="day-tour-cottages" class="section sm-padding bg-grey">

        <!-- Container -->
        <div class="container">

            <!-- Row -->
            <div class="row">

                <!-- why choose us content -->
                <div class="col-md-6">
                    <div class="section-header">
                        <div class="h2">Day Tour Cottages</div>
                    </div>
                    <div class="feature">
                        <p>Open Cottages, Gazebo, Tents, Tables, Kubo, Pavillion, Wave Open, Concrete Umbrella,Function halls</p>
                    </div>
                    <div class="more">
                        <button class="main-btn">More</button>
                    </div>

                </div>
                <!-- /day tour cottages content -->

                <!-- About slider -->
                <div class="col-md-6">
                    <div id="about-slider" class="owl-carousel owl-theme">
                        <img class="img-responsive" src="./img/open-cottage.jpg" alt="">
                        <img class="img-responsive" src="./img/pavillion.jpg" alt="">
                        <img class="img-responsive" src="./img/function-hall-2-elevated.jpg" alt="">
                        <img class="img-responsive" src="./img/double-cottage.jpg" alt="">
                        <img class="img-responsive" src="./img/open-alain.jpg" alt="">
                    </div>
                </div>
                <!-- /About slider -->

            </div>
            <!-- /Row -->

        </div>
        <!-- /Container -->

    </div>
    <!-- /Day Tour Cottages -->


    <!-- Footer -->
    <footer id="contact" class="sm-padding bg-dark">

        <!-- Container -->
        <div class="container">

            <!-- Row -->
            <div class="row">
                <div class="col-md-4">
                    <div>
                        <div>Telephone Numbers:</div>
                        <div>Manila Phone Office: +63 02 5844840</div>
                        <div>Resort Phone Office: +63 045 455-1397</div>
                        <div>Resort Phone Office: +63 045 455-0789</div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div>
                        <div>Mobile Numbers: </div>
                        <div>Globe: 0927.9338858</div>
                        <div>Smart: 0920.6009904</div>
                        <div>Sun: 0925.8301640</div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div>Address: Villa Alfredos' Resort, Purok 1 Barangay Baliti, City of San Fernando, Pampanga Philippines</div>

                </div>
            </div>

            <!-- /Row -->
            <div class="row">
                <div class="col-md-5">
                    <div>Email Address: villaalfredosresort@yahoo.com </div>
                    <div>Email Address: villaalfredos@yahoo.com</div>
                </div>
            </div>
            <!-- Row -->
            <div class="row">
                <div class="footer-copyright">
                    <p>Copyright
                        <script>
                            const d = new Date;
                            document.write(d.getFullYear());
                        </script>. All Rights Reserved</a>
                    </p>
                </div>
            </div>
            <!-- /Row -->
        </div>
        <!-- /Container -->

    </footer>
    <!-- /Footer -->

    <!-- Back to top -->
    <div id="back-to-top"></div>
    <!-- /Back to top -->

    <!-- Preloader -->
    <div id="preloader">
        <div class="preloader">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
    <!-- /Preloader -->

    <!-- jQuery Plugins -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="js/jquery.magnific-popup.js"></script>
    <script type="text/javascript" src="js/main.js"></script>

</body>

</html
