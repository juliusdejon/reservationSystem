<?php 
require_once '../config/mysqli.php';

$sql = "DELETE FROM customer";
$mysqli->query($sql);

$sql = "DELETE FROM customer_extras";
$mysqli->query($sql);

$sql ="DELETE FROM occupied_cottages";
$mysqli->query($sql);

$sql =" DELETE FROM reservation";
$mysqli->query($sql);

$sql = "DELETE FROM reserved_rooms";
$mysqli->query($sql);

$sql= "DELETE FROM transaction";
$mysqli->query($sql);

header('Location: dashboard.php');