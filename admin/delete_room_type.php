<?php
include '../config/mysqli.php';
$room_id = $_GET['room_id'];

$sql = "SELECT COUNT(*) as checked_in FROM
reserved_rooms JOIN transaction ON transaction.client_reference_id=reserved_rooms.client_reference_id
WHERE room_type_id='$room_id' AND reservation_status='Checked In' OR reservation_status='Booked'";
$res = $mysqli->query($sql);
$rows = mysqli_fetch_assoc($res);
$checked_in_count = $rows['checked_in'];

$sql = "SELECT COUNT(*) as totalRooms FROM rooms WHERE room_type_id='$room_id'";
$res = $mysqli->query($sql);
$rows = mysqli_fetch_assoc($res);
$totalRooms = $rows['totalRooms'];

$availableRooms = $totalRooms - $checked_in_count;
if ($totalRooms == 0) {
    $sql = "UPDATE room_type SET isDeleted='1' WHERE room_id='$room_id'";
    $mysqli->query($sql);

    header('Location: manage_rooms.php?room_id=' . $room_id . '&deleted=true');
} else {
    if ($availableRooms == 0) {
        header('Location: manage_rooms.php?room_id=' . $room_id . '&deleted=false');
    }
    header('Location: manage_rooms.php?room_id=' . $room_id . '&deleted=false');
}
