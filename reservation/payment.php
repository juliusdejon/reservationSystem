<?php

include 'functions.php';

if ((empty($_SESSION['arrivalDate'])) || (empty($_SESSION['departureDate']))) {

    header('Location: index.php?dates=false');
}
if (empty($_SESSION['rooms'])) {

    header('Location: select-room.php?rooms=false');
}

// $arrival_date = date_create_from_format('F j, Y', $_SESSION['arrivalDate']);
// $arrival_date = $arrival_date->format('Y-m-d');

// $departure_date = date_create_from_format('F j, Y', $_SESSION['departureDate']);
// $departure_date = $departure_date->format('Y-m-d');
$arrival_date = $_SESSION['arrivalDate'];
$departure_date = $_SESSION['departureDate'];
$departure = strtotime($departure_date);
$arrival = strtotime($arrival_date);
$datediff = $departure - $arrival;

$noofNights = round($datediff / (60 * 60 * 24));

$rooms = $_SESSION['rooms'];
$rooms = array_chunk($rooms, 2);

// $adult = $_SESSION['adult'];
// $adult = array_chunk($adult, 2);

// $child = $_SESSION['child'];
// $child = array_chunk($child, 2);

$_SESSION['arrivalDate'] = $arrival_date;
$_SESSION['departureDate'] = $departure_date;

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>Villa Alfredo's Reservation System</title>

    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7CVarela+Round" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <!-- Bootstrap -->
    <link type="text/css" rel="stylesheet" href="../css/bootstrap.min.css" />
    <!-- Form Semantic -->
    <!-- <link type="text/css" rel="stylesheet" href="../css/form.min.css" /> -->
    <!-- Semantic -->

    <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />
    <!-- Initialize Semantic UI and JQuery to load Calendar Datepicker-->
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/semantic.min.js"></script>


    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="../css/font-awesome.min.css">

    <!-- Icons -->
    <link rel="stylesheet" href="../css/icon.min.css">

    <!-- Custom stlylesheet -->
    <link type="text/css" rel="stylesheet" href="../css/style.css" />
    <link type="text/css" rel="stylesheet" href="../css/styles.css" />

</head>

<body>
    <!-- Header -->
    <header>
        <!-- Nav -->
        <nav id="nav" class="navbar">
            <div class="container">

                <div class="navbar-header">
                    <!-- Logo -->
                    <div class="navbar-brand">
                        <a href="../index.php">
                            <img class="logo" src="../img/valogo-alt.png" alt="logo">
                            <img class="logo-alt" src="../img/valogo-alt.png" alt="logo">
                        </a>
                    </div>
                    <!-- /Logo -->

                    <!-- Collapse nav button -->
                    <div class="nav-collapse">
                        <span></span>
                    </div>
                    <!-- /Collapse nav button -->
                </div>

                <!--  Main navigation  -->
                <ul class="main-nav nav navbar-nav navbar-right">
                    <li><a href="../#home">Home</a></li>
                    <li><a href="../#accomodation">Accomodation</a></li>
                    <li><a href="../#day-tour-cottages">Cottages</a></li>
                    <li><a href="../#contact">Contact</a></li>
                    <li><a href="index.php"><button class="secondary-btn">Book Now</button></a></li>
                </ul>
                <!-- /Main navigation -->

            </div>
        </nav>
        <!-- /Nav -->
    </header>
    <section class="xs-padding">
        <div class="ui segment container">
            <div class="row">
                <div class="col-md-9">
                    <div class="ui stackable mini steps">
                        <div class="completed step">
                            <i class="calendar alternate icon"></i>
                            <div class="content">
                                <div class="title">Select Dates</div>
                                <div class="description">Choose Date of Stay</div>
                            </div>
                        </div>
                        <div class="completed step">
                            <i class="bed icon"></i>
                            <div class="content">
                                <div class="title">Select Room</div>
                                <div class="description">Choose Rooms</div>
                            </div>
                        </div>
                        <!-- <div class="completed step">
                                <i class="tag icon"></i>
                                <div class="content">
                                    <div class="title">Amenities</div>
                                    <div class="description">Extras & Amenities</div>
                                </div>
                            </div> -->
                        <div class="active step">
                            <i class="credit card icon"></i>
                            <div class="content">
                                <div class="title">Payment</div>
                                <div class="description">Verify reservation details</div>
                            </div>
                        </div>
                    </div>
                    <form action="" name="payment" onsubmit="return validateForm()" method="post" class="ui form">
                        <h3>Step 3</h3>
                        <h4 class="ui dividing header">Contact Information</h4>
                        <div class="fields">
                            <div class="eight wide field">
                                <label>First Name</label>
                                    <input type="text" name="first_name" class="letters-only" placeholder="First Name">
                                </div>
                                <div class="eight wide field">
                                <label>Last Name</label>
                                    <input type="text" name="last_name" class="letters-only" placeholder="Last Name">
                            </div>
                        </div>
                        <div class="fields">
                        <div class="eight wide field">
                            <label>Email Address</label>
                                    <input type="email" name="email" placeholder="Email Address">
                        </div>
                        <div class="eight wide field">
                        <label>Contact No</label>
                                    <input type="text" name="contact" class="numbers-only" placeholder="Contact Number" data-mask="0000-000-0000" >
                        </div>
                        </div>
                        <div class="field">
                        <label>Additional Details</label>
                        <textarea name="additional_details" class="form-control alpha-numeric-only"></textarea>
                        </div>
                        <!-- <div class="fields">
                        <div class="eight wide field">
                            <label>No of Adults/Teen/Above 3 feet</label>
                                    <input type='text' onkeypress='validate(event)' name="no_of_adults" placeholder="0" id="no_of_adults" maxlength="3">
                        </div>
                        <div class="eight wide field">
                        <label>No. of Kids below 3 Feet</label>
                                    <input type='text' onkeypress='validate(event)' name="no_of_kids" placeholder="0" id="no_of_kids" maxlength="3">
                        </div>
                        </div> -->
                        <h4 class="ui dividing header">Payment Method</h4>

                        <div class="grouped fields">
                            <div class="field">
                                <div class="ui large radio checkbox">
                                    <input type="radio" name="method" checked="" tabindex="0" class="hidden" value="bank">
                                    <label>Deposit thru bank</label>
                                </div>
                            </div>
                            <div class="field">
                                <div class="ui large radio checkbox">
                                    <input type="radio" name="method" tabindex="0" class="hidden" value="paypal">
        <input type="hidden" name="cmd" value="_xclick" />
		<input type="hidden" name="no_note" value="1" />
		<input type="hidden" name="lc" value="UK" />
		<input type="hidden" name="currency_code" value="PHP" />
		<input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest" />
                                    <label>Paypal</label>
                                </div>
                            </div>

                        </div>
                        <input type="submit" name="pay" class="fluids ui teal button" value="Book Now" />
                    </form>
                </div>
                <div class="col-md-3">
                <span class="reservation">Your Reservation</span>
                        <div class="reservation-content">
                            <div class="ui clearing divider"></div>
                            <div class="reservation-title">Room Selections: </div>
                           <?php
$entrance = [];
$totalChild = [];
$totalAdult = [];
$grandTotal = [];
for ($i = 0; $i < count($rooms); $i++) {
    echo "<div class='ui segment'>";

    $room_id = $rooms[$i][0];
    $room_qty = $rooms[$i][1];

    $sql1 = "SELECT * FROM room_type WHERE room_id ='$room_id' AND isDeleted=0";
    $res1 = $mysqli->query($sql1);
    while ($row1 = mysqli_fetch_assoc($res1)) {
        $room_price = $row1['room_price'];
        $roomTotal = $room_price * $room_qty * $noofNights;
        echo "<div>" . $row1['room_name'] . " x " . $room_qty . " = ₱ " . number_format($roomTotal, 2) . "</div>";

        array_push($grandTotal, $roomTotal);
    }
    // for ($j = 0; $j < count($adult); $j++) {
    //     if ($room_id == $adult[$j][0]) {
            # Uncomment this if you want to set a default adult of 1 person per room
            // if($adult[$j][1] == 0) {
            //     $adult[$j][1] = 1;
            // }
    //         $no_of_adults = $adult[$j][1] * 250;
    //         array_push($entrance, $no_of_adults);
    //         array_push($totalAdult, $adult[$j][1]);
    //         echo "<div>Adult x " . $adult[$j][1] . " = ₱ " . number_format($no_of_adults, 2) . "</div>";
    //     }
    // }
    // for ($j = 0; $j < count($child); $j++) {
    //     if ($room_id == $child[$j][0]) {
    //         $no_of_child = $child[$j][1] * 150;

    //         array_push($entrance, $no_of_child);
    //         array_push($totalChild, $child[$j][1]);
    //         echo "<div>Child x " . $child[$j][1] . " = ₱" . number_format($no_of_child, 2) . "</div>";
    //     }
    // }

    echo "</div>";
    // $_SESSION['totalAdult'] = $totalAdult;
    // $_SESSION['totalChild'] = $totalChild;
}?>



                            <div class="reservation-title">Arrival Date:</div>
                            <?php echo $arrival_date; ?>
                            <div class="ui hidden divider"></div>
                            <div class="reservation-title">Departure Date</div>
                            <?php echo $departure_date; ?>
                            <div class="ui hidden divider"></div>
                            <div class="reservation-title">Room Nights</div>
                            <?php echo $noofNights; ?>
                            <!-- <div class="reservation-title">No of Persons</div>
                            Adult x <span id="label-quantity-adult">₱ 0.00</span> = <span id="label-total-adult">₱ 0.00</span><br />
                            Kids x <span id="label-quantity-kids">₱ 0.00</span> = <span id="label-total-kids">₱ 0.00</span><br /> -->
                            <div class="ui clearing divider"></div>
                            <div class="reservation-title">Total Amount</div>
                            <?php $grandTotal = array_sum($grandTotal) + array_sum($entrance);
$grandTotal = intval($grandTotal);?>
                            <input type="hidden" id="inputTotal" value="<?php echo $grandTotal; ?>" />
                            <span id="totalAmount">
                            <?php
echo "₱ " . number_format($grandTotal, 2);
// echo number_format(array_sum($grandTotal), 2);
$_SESSION['downpayment'] = $grandTotal / 2;
?></span>
<div class"ui label tiny">
    </div>
                            <div class="ui clearing divider"></div>
                    </div>
                </div>
    </section>
    </div>

    <!-- Back to top -->
    <div id="back-to-top"></div>
    <!-- /Back to top -->

    <!-- Preloader -->
    <div id="preloader">
        <div class="preloader">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
    <script>
        $('.ui.radio.checkbox')
            .checkbox();
    </script>
    <!-- jQuery Plugins -->
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/jquery.mask.js"></script>

    <script type="text/javascript" src="../js/main.js"></script>
    <!-- <script>
    function validate(evt) {
  var theEvent = evt || window.event;

  // Handle paste
  if (theEvent.type === 'paste') {
      key = event.clipboardData.getData('text/plain');
  } else {
  // Handle key press
      var key = theEvent.keyCode || theEvent.which;
      key = String.fromCharCode(key);
  }
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}
    </script> -->
    <script>


    Number.prototype.format = function(n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};



    $('#no_of_adults').keydown(function() {
    // computation entrance Fee
    var quantity = $('#no_of_adults').val();
    var compute = ($('#no_of_adults').val() * 250) + ($('#no_of_kids').val() * 150) + parseInt($('#inputTotal').val());
    var entrance = ($('#no_of_adults').val() * 250 );
    $('#label-quantity-adult').text(quantity);


    $('#label-total-adult').text('₱ ' + entrance.format(2)  );
    $('#totalAmount').text('₱ ' + compute.format(2));
    });
    $('#no_of_kids').keydown(function() {
    // computation entrance Fee
    var quantity = $('#no_of_kids').val();
    var compute = ($('#no_of_adults').val() * 250) + ($('#no_of_kids').val() * 150) + parseInt($('#inputTotal').val());
    var entrance = ($('#no_of_kids').val() * 150 );
    $('#label-quantity-kids').text(quantity);

    $('#label-total-kids').text('₱ ' + entrance.format(2));
    $('#totalAmount').text('₱ ' + compute.format(2));
    });
</script>
    <script>

    // Validation Payment form
    function validateForm() {
    var first_name = document.forms["payment"]["first_name"].value;
    var last_name = document.forms["payment"]["last_name"].value;
    var email = document.forms["payment"]["email"].value;
    var contact = document.forms["payment"]["contact"].value;
    var no_of_adults = document.forms["payment"]["no_of_adults"].value;

    if (first_name == "") {
        alert("Enter First Name");
        return false;
    }
    if (last_name == "") {
        alert("Enter Last Name");
        return false;
    }
    if (email == "") {
        alert("Enter Email");
        return false;
    }
    if (contact == "") {
        alert("Enter Contact number");
        return false;
    }

    if (no_of_adults == "") {
        alert("Enter No of Guests");
        return false;
    }

}
    </script>
    <!-- JQuery Validation source -->
    <script type="text/javascript" src="../admin/plugins/jquery/jquery-key-restrictions.min.js"></script>
    <!-- Actual Validation calling class -->
    <script type="text/javascript">
    $(document).ready(function () {
        $(".letters-only").lettersOnly();
        $(".numbers-only").numbersOnly();
        $(".alpha-numeric-only").alphaNumericOnly();
    });
    </script>
</body>

</html
