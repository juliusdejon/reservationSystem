<?php include 'reservationAdminFunctions.php'; // Get the rang of dates function date_range($first, $last, $step, $output_format) { $dates = array(); $current = strtotime($first); $last = strtotime($last); while ($current <= $last) { $dates[] = date($output_format, $current); $current = strtotime($step, $current); } return $dates; } ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Villa Alfredo Admin</title>
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
    <!-- Semantic UI -->
    <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />
    <!-- Initialize Semantic UI and JQuery to load Calendar Datepicker-->
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/semantic.min.js"></script>
    <!-- Calendar -->
    <link type="text/css" rel="stylesheet" href="../css/calendar.min.css" />
    <script src="../js/calendar.min.js"></script>
    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    <!-- Icons -->
    <link rel="stylesheet" href="../css/icon.min.css"> </head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <?php include 'layout/navbar.php';include 'layout/sidebar.php';?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h2><i class="fa fa-bed"></i> Walk In Reservation</h2>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            <!-- Main content -->
            <div class="content">
                <form action="" name="customerForm" method="post" class="ui form" onsubmit="return validateForm();">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="ui segment">
                                <h4 class="ui dividing header">Contact Information</h4>
                                <div class="fields">
                                    <div class="eight wide field"> <label>First Name</label> <input type="text" name="first_name" class="letters-only" placeholder="First Name"> </div>
                                    <div class="eight wide field"> <label>Last Name</label> <input type="text" name="last_name" class="letters-only" placeholder="Last Name"> </div>
                                </div>
                                <div class="fields">
                                    <div class="eight wide field"> <label>Email Address</label> <input type="email" required name="email" placeholder="Email Address"> </div>
                                    <div class="eight wide field"> <label>Contact No</label> <input type="text" name="contact" class="numbers-only" placeholder="Contact Number" data-mask="0000-000-0000"> </div>
                                </div>
                                <div class="fields">
                                    <div class="six wide field"> <label>No of Adults/Teen/Above 3 feet</label> <input type='number'min="0" max="100" name="no_of_adults" class="numbers-only" placeholder="0" id="no_of_adults" maxlength="3"> </div>
                                    <div class="six wide field"> <label>No. of Kids below 3 Feet</label> <input type='number' min="0" max="100"name="no_of_kids" class="numbers-only" placeholder="0" id="no_of_kids" maxlength="3"> </div>
                                    <div class="four wide field"> <label>No. of Senior/Pwd</label> <input type='number'min="0" max="100" name="no_of_senior_pwd" class="numbers-only" placeholder="0" id="no_of_senior_pwd" maxlength="2"> </div>
                                </div>
                                <h4 class="ui dividing header">Select Dates</h4>
                                <div class="ui form">
                                    <div class="two fields">
                                        <div class="field"> <label>Arrival Date</label>
                                            <div class="ui calendar" id="rangestart">
                                                <div class="ui input left icon"> <i class="calendar icon"></i> <input type="text" placeholder="Start" id="start" name="arrivalDate" autocomplete="off"> </div>
                                            </div>
                                        </div>
                                        <div class="field"> <label>Departure Date</label>
                                            <div class="ui calendar" id="rangeend">
                                                <div class="ui input left icon"> <i class="calendar icon"></i> <input type="text" placeholder="End" id="end" name="departureDate" autocomplete="off"> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="reservation-content">
                                <div class="ui clearing divider"></div> <button class="ui button teal fluid huge" name="submitBooking">Submit Booking <i class="fa fa-arrow-right"></i></button> </div>
                            </div>
                    </div>
                    <div class="ui segment col-md-9">
                        <h4>Available Rooms</h4>
                        <div id="available-rooms"> </div>
                    </div>
            </div>
            </form>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <!-- Main Footer -->
        <?php include 'layout/footer.php';?>
        <!-- ./wrapper -->
        <!-- REQUIRED SCRIPTS -->

        <script>
            const today = new Date();
            $('#rangestart').calendar({
                minDate: today,
                type: 'date',
                endCalendar: $('#rangeend'),
                onChange: function(date, text, mode) {
                    $("#available-rooms").html('<img src="ajax/loading-spinner.gif" width="100px">');
                    var start = $("#start").val();
                    var end = $("#end").val();
                    $.post("ajax/searchAvailableRooms.php", {
                        start: start,
                        end: end,
                    }, function(data) {
                        $("#available-rooms").html(data);
                    });
                },
            });
            $('#rangeend').calendar({
                type: 'date',
                startCalendar: $('#rangestart'),
                onChange: function(date, text, mode) {
                    $("#available-rooms").html('<img src="ajax/loading-spinner.gif" width="100px">');
                    var start = $("#start").val();
                    var end = $("#end").val();
                    var jqxhr = $.post("ajax/searchAvailableRooms.php", {
                        start: start,
                        end: end,
                    }, function(data) {
                        $("#available-rooms").html(data);
                    });
                },
                onHidden: function() {
                    $.post("ajax/searchAvailableRooms.php", {
                        start: start,
                        end: end,
                    }, function(data) {
                        $("#available-rooms").html(data);
                    });
                },
            });
        </script>
        <script>
             //Validation Payment
             function validateForm() {
                var first_name = document.forms["customerForm"]["first_name"].value;
                var last_name = document.forms["customerForm"]["last_name"].value;
                var email = document.forms["customerForm"]["email"].value;
                var contact = document.forms["customerForm"]["contact"].value;
                var no_of_adults = document.forms["customerForm"]["no_of_adults"].value;
                var no_of_kids = document.forms["customerForm"]["no_of_kids"].value;
                var arrivalDate = document.forms["customerForm"]["arrivalDate"].value;
                var departureDate = document.forms["customerForm"]["departureDate"].value;
                 if (first_name == "") {
                      alert("Enter First Name");
                      return false;
                       }
                if (last_name == "") {
                     alert("Enter Last Name");
                      return false;
                      }
                if (no_of_adults == "") {
                     alert("No. Of Adults is Required!"); 
                     return false;
                     }
                if(arrivalDate == "") {
                    alert("Select Arrival Date");
                    return false;
                }
                if(departureDate =="") {
                    alert("Select Departure Date");
                    return false;
                }
                if($email =="") {
                    alert("Email is required");
                    return false;
                }
                if(arrivalDate == departureDate) {
                    alert("Enter range of Dates");
                    return false;
                }

                }
        </script>
        <script type="text/javascript" src="../js/jquery.mask.js"></script>
        <!-- jQuery -->
        <script src=" plugins/jquery/jquery.min.js "></script>
        <!-- JQuery Validation source -->
        <script type="text/javascript" src="plugins/jquery/jquery-key-restrictions.min.js"></script>
        <!-- Actual Validation calling class -->
        <script type="text/javascript">
        $(document).ready(function () {
            $(".letters-only").lettersOnly();
            $(".numbers-only").numbersOnly();
            $(".alpha-numeric-only").alphaNumericOnly();
        });
        </script>
        <!-- Bootstrap 4 -->
        <script src="plugins/bootstrap/js/bootstrap.bundle.min.js "></script>
        <!-- AdminLTE App -->
        <script src="dist/js/adminlte.min.js "></script>
</body>

</html>