<?php
include 'database.php';

// timezone
date_default_timezone_set('Asia/Manila');

$today = date('Y-m-d H:i:s');


$mysqli = new mysqli($host, $user, $pass, $db);

// Update Expired Reservation

$sql = "SELECT * FROM reservation WHERE reservation_status='Pending'";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $client_reference_id = $rows['client_reference_id'];
    $expires_at = $rows['expires_at'];
    $reservation_status = $rows['reservation_status'];

    if (strtotime($today) > strtotime($expires_at)) {
        $sql = "UPDATE reservation SET reservation_status='Expired' WHERE reservation_status='Pending' AND client_reference_id ='$client_reference_id'";
        $mysqli->query($sql);
    }
}
