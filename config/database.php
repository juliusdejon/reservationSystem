<?php
date_default_timezone_set('Asia/Manila');
// Development == XAMPP Sever

// Production == Deploy in the Website at http://villaalfredos.x10host.com
$production = true;

// Mailing == false = system will not send email to customer.
$mailing = true;

// DeployedMailing == false = use test.villaalfredo@gmail.com to test on development.
// Note: Testing Paypal on Development won't work because Paypal needs a site eg. villaalfredos.x10host.com
// and not a localhost
// Create email on deployed site, same as this -> villa_alfredo_email@villaalfredo.x10host.com 
$deployedMailing = true;

if ($production == true) {

    $host = 'localhost';

    $db = 'villaal3_alfredo';

    $user = 'villaal3_user';

    $pass = 'admin';

} else {

    $host = 'localhost';

    $db = 'villa_alfredo_v1';

    $user = 'root';

    $pass = '';

}
