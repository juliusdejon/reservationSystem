<?php
include '../config/mysqli.php';

if (isset($_POST['check_in'])) {
    $client_reference_id = $_POST['client_reference_id'];
    $Adults = $_POST['no-of-adults'];
    $kids = $_POST['no-of-kids'];
    $roomKey = $_POST['roomKey'];
    $extra_adult = $_POST['extra-adults'];
    $extra_kids = $_POST['extra-kids'];
    $senior_pwd = (int) $_POST['no-of-senior-pwd'];
    $deposits = $_POST['deposits'];
    $input_payment = $_POST['input_payment'] ? $_POST['input_payment'] : 0;
    $balance = $_POST['balance'];
    $depositTotalInput = $_POST['depositTotalInput'];

    $depositArr = explode(',', $deposits);

    $mysqli->query($room_key);
    for ($i = 0; $i < count($depositArr); $i++) {
        $sql = "INSERT INTO customer_extras (client_reference_id,extras_id) VALUES('$client_reference_id','$depositArr[$i]')";
        $mysqli->query($sql);
    }
    for ($i = 0; $i < $roomKey; $i++) {
        $sql = "INSERT INTO customer_extras (client_reference_id,extras_id) VALUES('$client_reference_id','7')";
        $mysqli->query($sql);

    }
    $total = $_POST['subtotal'];
    $sql = "SELECT * FROM transaction WHERE client_reference_id ='$client_reference_id'";
    $res = $mysqli->query($sql);
    $rows = mysqli_fetch_assoc($res);
    $downpayment_db = $rows['downpayment'];
    $transaction_id = $rows['transaction_id'];
    $downpayment = $downpayment_db + $input_payment - $depositTotalInput;
    $balance = (int) $balance - (int) $input_payment;

    $sql = "UPDATE reservation SET reservation_status='Checked In' WHERE client_reference_id = '$client_reference_id'";
    $mysqli->query($sql);
    $Adults = $Adults + $extra_adult;
    $kids = $kids + $extra_kids ;
    $sql = "UPDATE customer SET no_of_adults ='$Adults', no_of_kids='$kids', no_of_senior_pwd='$senior_pwd' WHERE client_reference_id = '$client_reference_id'";
    $res = $mysqli->query($sql);

    if ($res) {
        $date = date("Y-m-d H:i:s");
        $sql = "INSERT INTO payments (transaction_fk, check_in_payment) VALUES('$transaction_id','$input_payment')";
        $mysqli->query($sql);
        $sql = "UPDATE transaction SET balance ='$balance', total = '$total', transaction_status='checked_in',check_in_date='$date'
        WHERE client_reference_id ='$client_reference_id'";
        $res = $mysqli->query($sql);
        if ($res) {
            header('Location: check_in_receipt.php?res_id=' . $client_reference_id);
        } else {
            echo "<script>alert('Failed to check-In');</script>";
        }
    } else {
        echo "<script>alert('Error');</script>";
    }
}
