<?php
include '../../config/mysqli.php';
$arrival_date = $_POST['start'];
$departure_date = $_POST['end'];
$client_reference_id = $_POST['ref_id'];

$arrival_date = date_create_from_format('F j, Y', $arrival_date);
$arrival_date = $arrival_date->format('Y-m-d');

$departure_date = date_create_from_format('F j, Y', $departure_date);
$departure_date = $departure_date->format('Y-m-d');

// Get the rang of dates
function date_range($first, $last, $step, $output_format)
{
    $dates = array();
    $current = strtotime($first);
    $last = strtotime($last);
    while ($current <= $last) {
        $dates[] = date($output_format, $current);
        $current = strtotime($step, $current);
    }
    return $dates;
}

?>


<table border="1" align="center">
  <th style="padding:6px; color: #424242;">Rooms</th>
  <th style="padding:6px; color: #424242;">Quantity</th>
  <th style="padding:6px; color: #424242;">Price </th>
  <th style=" color: #424242;">Availability</th>
  <tbody
  <?php

// Select room type eg. Ana Villa, Mike Hotel
$res = $mysqli->query('SELECT room_id FROM room_type');
while ($rows = mysqli_fetch_assoc($res)) {
    $roomType = $rows['room_id'];

    // Get the Input dates provided by the customer at the front end eg. he clicked July 24 - 26
    $range = date_range($arrival_date, $departure_date, "+1 day", "Y-m-d");
    // Slice it to July 24 - 25
    $reserved_period = array_slice($range, 0, -1);

    // Here you are selecting 'RESERVED ROOMS!!' AND ONLY WITH A STATUS THAT HAS BEEN CONFIRMED AND BOOKED REFLECTED by the given Dates on the site
    $s = "SELECT * FROM reserved_rooms JOIN customer ON customer.client_reference_id = reserved_rooms.client_reference_id
        JOIN reservation ON reserved_rooms.client_reference_id = reservation.client_reference_id
        WHERE reserved_rooms.room_type_id = '$roomType' AND reservation_status='Booked'  OR reservation_status='Checked In'";

    $r = $mysqli->query($s);

    $reserved_rooms = [];

    while ($ro = mysqli_fetch_assoc($r)) {
        $arrival_date_db = $ro['arrival_date'];
        $departure_date_db = $ro['departure_date'];
        // Get the Database Arrival Date and Departure Date of Ana Villa Room type that has been Reserved
        $rangeDb = date_range($arrival_date_db, $departure_date_db, "+1 day", "Y-m-d");

        $roomNotAvailable = 'not existing';
        foreach ($reserved_period as $day) {
            // Check the reserved period above an Array of dates
            // eg. $reservation_period = [2018-07-25,2018-07-25] // These dates will be compared at the Database
            // rangeDb
            if (in_array($day, $rangeDb)) {
                $roomNotAvailable = 'existing';
            }
        }
        if ($roomNotAvailable == 'existing') {
            array_push($reserved_rooms, 1);
        }

    }

    $reserved_rooms = array_sum($reserved_rooms);
    // Available Rooms = Total Rooms - Reserved Rooms IN Check In Date
    $sqlAvailableRoom = "SELECT COUNT(*) as TotalRoom FROM rooms WHERE room_type_id='$roomType'";
    $resAvailableRoom = $mysqli->query($sqlAvailableRoom);
    $result = mysqli_fetch_assoc($resAvailableRoom);

    $result = $result['TotalRoom'] - $reserved_rooms;

    $sql = "SELECT * FROM reserved_rooms JOIN room_type ON reserved_rooms.room_type_id=room_type.room_id WHERE client_reference_id='$client_reference_id'
 GROUP BY room_type_id";
    $res = $mysqli->query($sql);
    while ($rows = mysqli_fetch_assoc($res)) {
        $room_id = $rows['room_id'];
        ?>
      <tr>
        <td><?php echo $rows['room_name']; ?></td>
        <td align="center">x
          <?php
$sql1 = "SELECT COUNT(*) as totalRoom FROM reserved_rooms WHERE room_type_id ='$room_id' AND client_reference_id ='$client_reference_id'";
        $res1 = $mysqli->query($sql1);
        $row = mysqli_fetch_assoc($res1);
        $countedRoom = $row['totalRoom'];
        echo $countedRoom;

        ?>
      </td>
      <td align="center">₱ <?php echo $rows['room_price'] * $countedRoom; ?></td>
      <td align="center"><?php if ($result >= 1) {echo 'Yes';} else {
            echo 'No Available on selected dates';
        }?></td>
      </tr>
    <?php
}

}
?>
</tbody>
</table>