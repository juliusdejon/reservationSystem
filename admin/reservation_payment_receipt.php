<?php
$res_id = $_GET['res_id'];

?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>Villa Alfredo Admin</title>

        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/adminlte.min.css">
        <!-- Semantic UI -->
        <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />

        <script>
            function resizeIframe(obj) {
                obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
            }
        </script>

    </head>

    <body class="hold-transition sidebar-mini">
        <div class="wrapper">

            <?php
include 'layout/navbar.php';
include 'layout/sidebar.php';
?>
                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <div class="content-header">
                        <div class="container-fluid">
                            <div class="row mb-2">
                                <div class="col-sm-6">
                                    </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->
                    </div>
                    <!-- /.content-header -->

                    <!-- Main content -->
                    <div class="content">


                    <div class="ui segment container">
                        <button class="ui teal button" onclick="printFrame('receipt')">Print Reservation Details</button>
                        <iframe id="receipt" class="ui container fluid" src="reservation-details.php?res_id=<?php echo $res_id; ?>" frameborder="0" allow="autoplay" scrolling="no" onload="resizeIframe(this)"></iframe>

                    </div>


                </div>
                <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Main Footer -->
        <?php
include 'layout/footer.php';
?>
            <!-- ./wrapper -->
            <!--pull me -->

            <!-- REQUIRED SCRIPTS -->
            <script>
              function lessDeposit(id,price) {
              var status = $('.check' + id)[0].checked;
                if(status == true) {
                  var dep = $("#depositTotalInput").val();

                  var less = parseInt(dep) - parseInt(price);
                  $("#depositTotalInput").val(less);

                  var formattedDep = (less).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                  $("#depositTotalInputTxt").text(formattedDep);


                  var bal = $("#balanceTotal").val();
                  var newBal = parseInt(bal) - price;
                  $("#balanceTotal").val(newBal);
                  var formattedBal = (newBal).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                  $("#balanceTotalTxt").text(formattedBal);

                } else {
                  var dep = $("#depositTotalInput").val();
                  var less = parseInt(dep) + parseInt(price);
                  $("#depositTotalInput").val(less);

                  var formattedDep = (less).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                  $("#depositTotalInputTxt").text(formattedDep);

                  var bal = $("#balanceTotal").val();
                  var newBal = parseInt(bal) + price;
                  $("#balanceTotal").val(newBal);
                  var formattedBal = (newBal).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                  $("#balanceTotalTxt").text(formattedBal);

                }

              }
              </script>
               <script>
                        // Validation Payment form
                        function validateForm() {
                            var balance = document.forms["checkoutForm"]["balance"].value;
                            var amount = document.forms["checkoutForm"]["amount"].value;

                            if(amount == '') {
                              alert('Enter amount');
                              return false;
                            }
                            if(balance > amount) {
                              alert('Insufficient Amount given');
                              return false;
                            }

                        }
                    </script>
              <script>

              function printFrame(id) {
                        var frm = document.getElementById(id).contentWindow;
                        frm.focus();// focus on contentWindow is needed on some ie versions
                        frm.print();
                        return false;
              }
              </script>
            <!-- jQuery -->
            <script src="plugins/jquery/jquery.min.js"></script>
            <!-- Bootstrap 4 -->
            <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
            <!-- AdminLTE App -->
            <script src="dist/js/adminlte.min.js"></script>
    </body>

    </html>