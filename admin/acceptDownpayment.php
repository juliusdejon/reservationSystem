<?php
include '../config/mysqli.php';
use PHPMailer\PHPMailer\PHPMailer;

require '../src/Exception.php';
require '../src/PHPMailer.php';
require '../src/SMTP.php';

$client_reference_id = $_POST['client_reference_id'];
$amount = $_POST['amount'];
$requiredPayment = $_POST['requiredPayment'];
$requiredTotal = $_POST['requiredTotal'];

$user = $_POST['user'];

if (empty($_POST['amount'])) {
    echo "<script>alert('Insufficient fund');</script";
    header('Location: reservations.php?insufficent='.$client_reference_id);
}
if (isset($_POST['amount'])) {
    if($amount < $requiredPayment) {
        echo "<script>alert('Insufficient fund');</script";
        header('Location: reservations.php?insufficent='.$client_reference_id);
    }
    else if ($amount > $requiredTotal) {
        header('Location: reservations.php?excess='.$client_reference_id);
    }
    else {
    $check = "SELECT * FROM transaction WHERE client_reference_id ='$client_reference_id'";
    $result = $mysqli->query($check);       
    $result = mysqli_num_rows($result);
    if($result >= 1) {
        $sql = "UPDATE transaction SET downpayment='$amount' WHERE client_reference_id='$client_reference_id'";
    } else {
        $sql = "INSERT INTO transaction (client_reference_id,downpayment) VALUES ('$client_reference_id', '$amount') ";
    }
        $res = $mysqli->query($sql);
        if ($res) {
            $sql = "UPDATE reservation SET reservation_status='Booked', assigned_to='$user' WHERE client_reference_id='$client_reference_id'";
            $res = $mysqli->query($sql);
            if ($res) {
                header('Location: reservations.php');

                $sql = "SELECT * FROM customer WHERE client_reference_id='$client_reference_id'";
                $res = $mysqli->query($sql);
                while ($rows = mysqli_fetch_assoc($res)) {
                    $email_address = $rows['email_address'];
                    $first_name = $rows['first_name'];
                    $last_name = $rows['last_name'];
                    $arrival_date = $rows['arrival_date'];
                    $departure_date = $rows['departure_date'];
                    $additional_details = $rows['additional_details'];
                    $no_of_adults = $rows['no_of_adults'];
                    $no_of_kids = $rows['no_of_kids'];
                
                }

                if ($mailing == true) {
                    $encrypted = base64_encode($client_reference_id);
                    if ($deployedMailing == true) {

                        $from = "villa_alfredo_email@villaalfredo.x10host.com";
                        $to = "$email_address, $first_name.' '.$last_name";

                        $baseurl = $_SERVER['SERVER_NAME'];
                        $amount = number_format($amount,2);
                        $mailbody = "
                        <div id=':14u' class='ii gt'><div id=':14t' class='a3s aXjCH '><div id='m_-852930379096079456page-wrapper' style='background-color:#ffffff;padding:0px'><div class='adM'>
    </div><table style='font-family:'Open Sans',sans-serif;font-size:12px;margin:0px' width='100%' border='0' cellspacing='0'>
    <tbody>
    <tr>
    <td>
    <p style='font-size:20px'><span class='il'>Villa</span> <span class='il'>Alfredo</span> Resort</p>
    </td>
    </tr>
    <tr>
    <td class='m_-852930379096079456box-grey' style='background-color:#eeeeee;padding:15px 15px 15px 15px;border-bottom-left-radius:0;border-bottom-right-radius:0' valign='top'>
    <table width='100%' border='0' cellpadding='0'>
    <tbody>
    <tr valign='top'>
    <td width='65%'><span style='font-size:12px'><strong>Thank you, ".$first_name." Your booking is now Approved!.</strong></span></td>
    <td style='font-size:12px' width='35%'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
    <tbody>
    <tr>
    <td align='right'><strong>Reference Number:</strong>".$client_reference_id."</td>
    </tr>
    </tbody>
    </table>
    </td>
    </tr>
    </tbody>
    </table>
    </td>
    </tr>
    <tr>
    <td class='m_-852930379096079456box-grey' style='background-color:#f8f8f8;padding:0 15px 15px 15px;border-top-left-radius:0;border-top-right-radius:0' valign='top'>
    <table width='100%' border='0' cellpadding='0'>
    <tbody>
    <tr valign='top'>
    <td width='65%'>
    <ul style='padding:15px 0 0 15px;margin:0'>
    <li style='line-height:20px;padding:0;margin:0'>For booking enquires, cancellations or amendments please contact us directly at <a href='mailto:test.villaalfredo@gmail.com' target='_blank'>test.villaalfredo@gmail.com</a> or (045) 455 1397.</li>
    </ul>
    </td>
    <td style='font-size:12px' width='35%'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
    <tbody>
    <tr>
    <td align='right'>&nbsp;</td>
    </tr>
    </tbody>
    </table>
    </td>
    </tr>
    </tbody>
    </table>
    </td>
    </tr>
    <tr>
    <td>
    <div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Your Booking</strong></div>
    <table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
    <tbody>
    <tr>
    <td style='padding:1px 10px 1px 20px' valign='top' width='20%'><strong>Guest:</strong></td>
    <td style='padding:1px 20px' width='80%'>".$first_name." ".$last_name." </td>
    </tr>
    <tr>
    <td style='padding:1px 10px 1px 20px' valign='top' width='20%'><strong>Details:</strong></td>
    <td style='padding:1px 0px 1px 20px' width='80%'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                                <tbody><tr>
                                    <td width='75%'>
                                    ";
                        $sqlMail = "SELECT * FROM reserved_rooms
                        JOIN customer ON
                        reserved_rooms.client_reference_id = customer.client_reference_id
                        JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
                        WHERE customer.client_reference_id ='$client_reference_id'
                        GROUP BY reserved_rooms.room_type_id";
                         $resMail = $mysqli->query($sqlMail);
                         $total = 0;
                         $subtotal = [];
                         while ($rows = mysqli_fetch_assoc($resMail)) {
                            $room_type_id = $rows['room_type_id'];
                            $sql1 = "SELECT COUNT(*) as quantity FROM reserved_rooms
                          JOIN customer ON
                          reserved_rooms.client_reference_id = customer.client_reference_id
                          JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
                          WHERE customer.client_reference_id ='$client_reference_id' AND room_type_id ='$room_type_id'";
                            $res1 = $mysqli->query($sql1);
                            $row1 = mysqli_fetch_assoc($res1);
                            $total = $row1['quantity'] * $rows['room_price'];
    
                            $mailbody .= "
                                            ".$rows['room_name'] ." x
                                            " . $row1['quantity'] . ",
                                        ";
    
                            $count++;
                            array_push($subtotal, $total);
                        }
                        
                        $departure = strtotime($departure_date);
                        $arrival = strtotime($arrival_date);
                        $datediff = $departure - $arrival;

                        $noofNights = round($datediff / (60 * 60 * 24));

                        function multiplyDays($total)
                        {
                            global $noofNights;
                            return ($total * $noofNights);
                        }
                        $grandTotal = array_sum(array_map("multiplyDays", $subtotal));
                        $totalAdult = $no_of_adults * 250;
                        $totalKid = $no_of_kids * 150;
                        $grandTotal = $grandTotal + $totalAdult + $totalKid;
                        $grandestTotal = number_format($grandTotal, 2);
                        $down = number_format($grandTotal / 2, 2);
    
    
                        $mailbody .="Adult x ".$no_of_adults.",
                        Child x ".$no_of_kids."";
    
                        $mailbody .="-PHP ".$grandestTotal."</td>
                            
                                </tr>
                            </tbody></table></td>
    </tr>
    <tr>
    <td style='padding:1px 10px 1px 20px' width='20%'><strong>Check-in:</strong></td>
    <td style='padding:1px 20px' width='80%'>".$arrival_date." from 14:00</td>
    </tr>
    <tr>
    <td style='padding:1px 10px 1px 20px' width='20%'><strong>Check-out:</strong></td>
    <td style='padding:1px 20px' width='80%'>".$departure_date." until 12:00</td>
    </tr>
    </tbody>
    </table>
    </td>
    </tr>
    
    <tr>
    <td>
    <div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Additional Details</strong></div>
    <table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
    <tbody>
    <tr>
    <td><table width='100%' border='0' cellspacing='0' cellpadding='0'>
    <tbody><tr>
    <td width='20%' style='padding:1px 10px 1px 20px'><strong>Additional comments:</strong></td>
    <td width='80%' style='padding:1px 20px'>".$additional_details."</td>
    </tr>
    </tbody></table></td>
    </tr>
    </tbody>
    </table>
    </td>
    </tr>
    
    <tr>
    <td>
    <div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Payment Details</strong></div>
    <table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
    <tbody>
    <tr>
    <td style='padding:1px 20px' width='50%'><strong>Booking value:</strong></td>
    
    </tr>
    
    <tr>
    <td style='padding:1px 20px'><strong>Downpayment: ".$down."</strong></td>
    <td style='padding:1px 15px;vertical-align:top' align='right'>PHP ".$amount."</td>
    </tr>
    
    <tr>
    <td colspan='2'></td>
    </tr>
    </tbody>
    </table>
    </td>
    </tr>
    
    <tr>
    <td>
    <div style='font-size:14px;margin-top:20px'><strong>Booking Policies</strong></div>
    <table style='margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
    <tbody><tr>
    <td><table width='100%' border='0' cellpadding='0' cellspacing='0'>
                                <tbody><tr>
                                    <td style='border-bottom:2px solid #eeeeee;border-top:2px solid #eeeeee;padding:10px 20px!important'>
                                    <span style='font-size:11px'>
                                        <strong>Cancellation:</strong> If cancelled, modified or in case of no-show, no penalty   will be charged.<br>
                                       <strong>Other policies:  </strong>Please call 63 045 455-0789 or send an email to <a href='mailto:test.villaalfredo@gmail.com' target='_blank'>test.villaalfredo@gmail.com</a><br> Hotel requires an 'Incidental Deposit' at check-in, which is fully refundable at check-out.<br>
                                    </span>
                                    </td>
                                </tr>
    
                            </tbody></table></td>
    </tr>
    </tbody></table></td></tr></tbody>
    </table>
    
    
    
    <table><tbody><tr>
    <td>
    <div style='font-size:14px;margin-top:20px'><strong>Villa Alfredos</strong></div>
    </td>
    </tr>
    </tbody>
    </table>
    </td>
    </tr>
    </tbody>
    </table>
    </td>
    </tr>
    
    </tbody></table><div class='yj6qo'></div><div class='adL'>
    </div></div></div></div>";
    
    
                        $mailbody .="<br>
                        Check your reservation details here <a href='" . $baseurl . "/reservation/message.php?res_id=$encrypted'>" . $baseurl . "/reservation/message.php?res_id=$encrypted'</a>
                        <br/>
                        <h3>House Rules</h3>
                        <ol>
                        <li>  No hard liquor </li>
                        <li> No eating near the pool. </li>
                        <li> No firearms allowed. </li>
                        <li> No littering. </li>
                        <li> No picking of fruits and flowers. </li>
                        <li> Wearing sando, t-shirt & pants will not be allowed to swim. </li>
                        <li> Videoke & swimming are allowed until 12:00 o’clock midnight only. </li>
                        <li> Loud music is not allowed between 12 o’clock midnight up to 8 o’clock in the morning. </li>
                        <li> Management provides 24 hours security, we are not responsible for item lost inside the car, please lock your vehicle and secure your entire valuable. </li>
                        <li> Please pay particular attention to your entire valuable and secure them at all times. 11. No running, diving, shoving or pushing within the pool area. </li>
                        <li> Right Conduct. We reserve the right to evict/eject any guest or group of guest should we find them engaging in fights, or generally disrupting the peace at  the resort </li>
                        </ol>
                        <i>Not allowed: Radio, Component, Speaker, DVD Player, Magic Sing, Electric Stove, Gas Stove, Butane Stove. </i>
                        ";
                        $subject = "We have received your payment";

                        $headers = "From: $from" . "\r\n";
                        $headers .= "Reply-To: $from" . "\r\n";
                        $headers .= "MIME-Version: 1.0\r\n";
                        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
                        $resp = mail($to, $subject, $mailbody, $headers);
                    } else {
                        $mail = new PHPMailer(true); // Passing `true` enables exceptions
                        //Server settings
                        $mail->SMTPDebug = 0; // Enable verbose debug output
                        $mail->isSMTP(); // Set mailer to use SMTP
                        $mail->Host = 'smtp.gmail.com'; // Specify main and backup SMTP servers
                        $mail->SMTPAuth = true; // Enable SMTP authentication
                        $mail->Username = 'test.villaalfredo@gmail.com'; // SMTP username
                        $mail->Password = 'reservationsystem'; // SMTP password
                        $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
                        $mail->Port = 587; // TCP port to connect to
                        $mail->SMTPOptions = array(
                            'ssl' => array(
                                'verify_peer' => false,
                                'verify_peer_name' => false,
                                'allow_self_signed' => true,
                            ),
                        );
                        //Recipients
                        $mail->setFrom('test.villaalfredo@gmail.com', 'Villa Alfredo');
                        $mail->addAddress($email_address, $first_name . ' ' . $last_name); // Add a recipient
                        $baseurl = $_SERVER['SERVER_NAME'];
                        $amount = number_format($amount,2);
                        $mailbody = "
                        <div id=':14u' class='ii gt'><div id=':14t' class='a3s aXjCH '><div id='m_-852930379096079456page-wrapper' style='background-color:#ffffff;padding:0px'><div class='adM'>
    </div><table style='font-family:'Open Sans',sans-serif;font-size:12px;margin:0px' width='100%' border='0' cellspacing='0'>
    <tbody>
    <tr>
    <td>
    <p style='font-size:20px'><span class='il'>Villa</span> <span class='il'>Alfredo</span> Resort</p>
    </td>
    </tr>
    <tr>
    <td class='m_-852930379096079456box-grey' style='background-color:#eeeeee;padding:15px 15px 15px 15px;border-bottom-left-radius:0;border-bottom-right-radius:0' valign='top'>
    <table width='100%' border='0' cellpadding='0'>
    <tbody>
    <tr valign='top'>
    <td width='65%'><span style='font-size:12px'><strong>Thank you, ".$first_name." Your booking is now Approved!.</strong></span></td>
    <td style='font-size:12px' width='35%'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
    <tbody>
    <tr>
    <td align='right'><strong>Reference Number:</strong>".$client_reference_id."</td>
    </tr>
    </tbody>
    </table>
    </td>
    </tr>
    </tbody>
    </table>
    </td>
    </tr>
    <tr>
    <td class='m_-852930379096079456box-grey' style='background-color:#f8f8f8;padding:0 15px 15px 15px;border-top-left-radius:0;border-top-right-radius:0' valign='top'>
    <table width='100%' border='0' cellpadding='0'>
    <tbody>
    <tr valign='top'>
    <td width='65%'>
    <ul style='padding:15px 0 0 15px;margin:0'>
    <li style='line-height:20px;padding:0;margin:0'>For booking enquires, cancellations or amendments please contact us directly at <a href='mailto:test.villaalfredo@gmail.com' target='_blank'>test.villaalfredo@gmail.com</a> or (045) 455 1397.</li>
    </ul>
    </td>
    <td style='font-size:12px' width='35%'>
    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
    <tbody>
    <tr>
    <td align='right'>&nbsp;</td>
    </tr>
    </tbody>
    </table>
    </td>
    </tr>
    </tbody>
    </table>
    </td>
    </tr>
    <tr>
    <td>
    <div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Your Booking</strong></div>
    <table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
    <tbody>
    <tr>
    <td style='padding:1px 10px 1px 20px' valign='top' width='20%'><strong>Guest:</strong></td>
    <td style='padding:1px 20px' width='80%'>".$first_name." ".$last_name." </td>
    </tr>
    <tr>
    <td style='padding:1px 10px 1px 20px' valign='top' width='20%'><strong>Details:</strong></td>
    <td style='padding:1px 0px 1px 20px' width='80%'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                                <tbody><tr>
                                    <td width='75%'>
                                    ";
                        $sqlMail = "SELECT * FROM reserved_rooms
                        JOIN customer ON
                        reserved_rooms.client_reference_id = customer.client_reference_id
                        JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
                        WHERE customer.client_reference_id ='$client_reference_id'
                        GROUP BY reserved_rooms.room_type_id";
                         $resMail = $mysqli->query($sqlMail);
                         $total = 0;
                         $subtotal = [];
                         while ($rows = mysqli_fetch_assoc($resMail)) {
                            $room_type_id = $rows['room_type_id'];
                            $sql1 = "SELECT COUNT(*) as quantity FROM reserved_rooms
                          JOIN customer ON
                          reserved_rooms.client_reference_id = customer.client_reference_id
                          JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
                          WHERE customer.client_reference_id ='$client_reference_id' AND room_type_id ='$room_type_id'";
                            $res1 = $mysqli->query($sql1);
                            $row1 = mysqli_fetch_assoc($res1);
                            $total = $row1['quantity'] * $rows['room_price'];
    
                            $mailbody .= "
                                            ".$rows['room_name'] ." x
                                            " . $row1['quantity'] . ",
                                        ";
    
                            $count++;
                            array_push($subtotal, $total);
                        }
                        
                        $departure = strtotime($departure_date);
                        $arrival = strtotime($arrival_date);
                        $datediff = $departure - $arrival;

                        $noofNights = round($datediff / (60 * 60 * 24));

                        function multiplyDays($total)
                        {
                            global $noofNights;
                            return ($total * $noofNights);
                        }
                        $grandTotal = array_sum(array_map("multiplyDays", $subtotal));
                        $totalAdult = $no_of_adults * 250;
                        $totalKid = $no_of_kids * 150;
                        $grandTotal = $grandTotal + $totalAdult + $totalKid;
                        $grandestTotal = number_format($grandTotal, 2);
                        $down = number_format($grandTotal / 2, 2);
    
    
                        $mailbody .="Adult x ".$no_of_adults.",
                        Child x ".$no_of_kids."";
    
                        $mailbody .="-PHP ".$grandestTotal."</td>
                            
                                </tr>
                            </tbody></table></td>
    </tr>
    <tr>
    <td style='padding:1px 10px 1px 20px' width='20%'><strong>Check-in:</strong></td>
    <td style='padding:1px 20px' width='80%'>".$arrival_date." from 14:00</td>
    </tr>
    <tr>
    <td style='padding:1px 10px 1px 20px' width='20%'><strong>Check-out:</strong></td>
    <td style='padding:1px 20px' width='80%'>".$departure_date." until 12:00</td>
    </tr>
    </tbody>
    </table>
    </td>
    </tr>
    
    <tr>
    <td>
    <div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Additional Details</strong></div>
    <table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
    <tbody>
    <tr>
    <td><table width='100%' border='0' cellspacing='0' cellpadding='0'>
    <tbody><tr>
    <td width='20%' style='padding:1px 10px 1px 20px'><strong>Additional comments:</strong></td>
    <td width='80%' style='padding:1px 20px'>".$additional_details."</td>
    </tr>
    </tbody></table></td>
    </tr>
    </tbody>
    </table>
    </td>
    </tr>
    
    <tr>
    <td>
    <div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Payment Details</strong></div>
    <table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
    <tbody>
    <tr>
    <td style='padding:1px 20px' width='50%'><strong>Booking value:</strong></td>
    
    </tr>
    
    <tr>
    <td style='padding:1px 20px'><strong>Downpayment: ".$down."</strong></td>
    <td style='padding:1px 15px;vertical-align:top' align='right'>PHP ".$amount."</td>
    </tr>
    
    <tr>
    <td colspan='2'></td>
    </tr>
    </tbody>
    </table>
    </td>
    </tr>
    
    <tr>
    <td>
    <div style='font-size:14px;margin-top:20px'><strong>Booking Policies</strong></div>
    <table style='margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
    <tbody><tr>
    <td><table width='100%' border='0' cellpadding='0' cellspacing='0'>
                                <tbody><tr>
                                    <td style='border-bottom:2px solid #eeeeee;border-top:2px solid #eeeeee;padding:10px 20px!important'>
                                    <span style='font-size:11px'>
                                        <strong>Cancellation:</strong> If cancelled, modified or in case of no-show, no penalty   will be charged.<br>
                                       <strong>Other policies:  </strong>Please call 63 045 455-0789 or send an email to <a href='mailto:test.villaalfredo@gmail.com' target='_blank'>test.villaalfredo@gmail.com</a><br> Hotel requires an 'Incidental Deposit' at check-in, which is fully refundable at check-out.<br>
                                    </span>
                                    </td>
                                </tr>
    
                            </tbody></table></td>
    </tr>
    </tbody></table></td></tr></tbody>
    </table>
    
    
    
    <table><tbody><tr>
    <td>
    <div style='font-size:14px;margin-top:20px'><strong>Villa Alfredos</strong></div>
    </td>
    </tr>
    </tbody>
    </table>
    </td>
    </tr>
    </tbody>
    </table>
    </td>
    </tr>
    
    </tbody></table><div class='yj6qo'></div><div class='adL'>
    </div></div></div></div>";
    
    
                        $mailbody .="<br>
                        Check your reservation details here <a href='" . $baseurl . "/reservation/message.php?res_id=$encrypted'>" . $baseurl . "/reservation/message.php?res_id=$encrypted'</a>
                        <br/>
                        <h3>House Rules</h3>
                        <ol>
                        <li>  No hard liquor </li>
                        <li> No eating near the pool. </li>
                        <li> No firearms allowed. </li>
                        <li> No littering. </li>
                        <li> No picking of fruits and flowers. </li>
                        <li> Wearing sando, t-shirt & pants will not be allowed to swim. </li>
                        <li> Videoke & swimming are allowed until 12:00 o’clock midnight only. </li>
                        <li> Loud music is not allowed between 12 o’clock midnight up to 8 o’clock in the morning. </li>
                        <li> Management provides 24 hours security, we are not responsible for item lost inside the car, please lock your vehicle and secure your entire valuable. </li>
                        <li> Please pay particular attention to your entire valuable and secure them at all times. 11. No running, diving, shoving or pushing within the pool area. </li>
                        <li> Right Conduct. We reserve the right to evict/eject any guest or group of guest should we find them engaging in fights, or generally disrupting the peace at  the resort </li>
                        </ol>
                        <i>Not allowed: Radio, Component, Speaker, DVD Player, Magic Sing, Electric Stove, Gas Stove, Butane Stove. </i>
                        ";
                        //Content
                        $mail->isHTML(true); // Set email format to HTML
                        $mail->Subject = 'We have received your payment';
                        $mail->Body = $mailbody;
                        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
                        $mail->send();
                }
            }
        }
    }
}
}
