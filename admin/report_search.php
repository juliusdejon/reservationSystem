<?php
include "../config/mysqli.php";

$start = $_GET['start'];
$yesterday = strtotime('-1 day', strtotime($start));
$yesterday = date("Y-m-d", $yesterday);
$end = $_GET['end'];
$type = $_GET['type'];
@$print = $_GET['print'];
?>
<!-- Font Awesome Icons -->
<link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
        <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
         <!-- Semantic UI -->
  <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />

<div class="row">
<div class="col-md-6">
           <div class="logo">
                <img src="../img/valogo.png" width="200px">
            </div>
</div>
<div class="col-md-6 mt-3">
                <h3>Villa Alfredo's Resort Report</h3>

                <br>

                <span>Purok 1 Barangay Baliti, City of San Fernando</span>
                <span>,Pampanga Philippines</span>

                <br>

                <span>Manila Phone Office: +63 02 5844840</span>
                <span>/Resort Phone Office: +63 045 455-1397</span>
                <br>
                <span>villaalfredosresort@yahoo.com</span>
                <span>/villaalfredos@yahoo.com</span>
</div>
</div><center>From: <?php echo $start . ' To  ' . $end; ?></center>
</div>
<hr />
<!-- Sales -->
<?php if ($type == 'Sales' and $start =='' || $end=='' ) {

?>
<h1>All Reports on Revenue</h1>
<!-- <h2>Room Reservation</h2> -->
<table class="ui striped table">
<thead>
<tr>
  <th>Reference ID</th>
  <th>Name</th>
  <th>Date</th>
  <th>Type</th>
  <th>Total</th>
</tr>
</thead>
<tbody>
<?php
$sql = "SELECT * FROM transaction JOIN reservation ON transaction.client_reference_id = reservation.client_reference_id JOIN customer ON transaction.client_reference_id = customer.client_reference_id WHERE (reservation_status='Booked' OR reservation_status='Checked In' OR reservation_status='Done')";
$res = $mysqli->query($sql);
$report_total = [];
$down = [];
$final_total =[];
while ($rows = mysqli_fetch_assoc($res)) {
  $balance = $rows['balance'];
  $total = $rows['total'] - $balance;
  $downpayment= $rows['downpayment'];
    ?>
<tr>
    <td><?php echo $rows['client_reference_id']; ?></td>
    <td><?php echo $rows['first_name'] . ' ' . $rows['last_name']; ?></td>
    <td><?php echo $rows['arrival_date']; ?></td>
    <td><?php echo $rows['reservation_type'];?></td>
    <td>₱ <?php echo number_format($total,2);?></td>
</tr>
<?php
array_push($report_total, $total);
array_push($down, $downpayment);
}

?>
<tr>
<td></td>
<td></td>
<td></td>
<td></td>
<td>
<h1>Grand Total</h1>
<?php 
$report_total = array_sum($report_total);
array_push($final_total,$report_total);
echo '₱ '.number_format($report_total,2);
?>
</td>
</tr>
</tbody>
</table>
<h2>Daytour/Overnight</h2>
<table class="ui striped table">
<thead>
<tr>
  <th>Reference ID</th>
  <th>Name</th>
  <th>Date</th>
  <th>Type</th>
  <th>Total</th>
</tr>
</thead>
<tbody>
<?php
$sql = "SELECT * FROM transaction JOIN customer ON transaction.client_reference_id = customer.client_reference_id WHERE transaction_type='Daytour' OR transaction_type='Overnight'";
$res = $mysqli->query($sql);
$report_total = [];
$down = [];
while ($rows = mysqli_fetch_assoc($res)) {
  $balance = $rows['balance'];
  $total = $rows['total'] - $balance;
  $downpayment= $rows['downpayment'];
    ?>
<tr>
    <td><?php echo $rows['client_reference_id']; ?></td>
    <td><?php echo $rows['first_name'] . ' ' . $rows['last_name']; ?></td>
    <td><?php echo $rows['arrival_date']; ?></td>
    <td><?php echo $rows['reservation_type'];?></td>
    <td>₱ <?php echo number_format($total,2);?></td>
</tr>
<?php
array_push($report_total, $total);
array_push($down, $downpayment);
}

?>
<tr>
<td></td>
<td></td>
<td></td>
<td></td>
<td>
<h1>Grand Total</h1>
<?php 
$report_total = array_sum($report_total);
array_push($final_total,$report_total);
echo '₱ '.number_format($report_total,2);
?>
</td>
</tr>
</tbody>
</table>
<table class="ui table">
<tbody>
<tr>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td>
<h1> Grand Total</h1>
<?php
$final_total = array_sum($final_total);
echo '₱ '.number_format($final_total,2);
?>
</tr>
</tbody>
</table>
<?php
} 

else {
if($type=='Sales') {
?>
<h1>Reports on Revenue</h1>
<!-- <h2>Room Reservation</h2> -->
<table class="ui striped table">
<thead>
<tr>
  <th>Reference ID</th>
  <th>Name</th>
  <th>Date</th>
  <th>Type</th>
  <th>Total</th>
</tr>
</thead>
<tbody>
<?php
$sql = "SELECT * FROM transaction JOIN customer ON transaction.client_reference_id = customer.client_reference_id
  JOIN reservation ON transaction.client_reference_id = reservation.client_reference_id
  WHERE arrival_date BETWEEN '$start' AND '$end' OR arrival_date BETWEEN '$yesterday' AND '$start' AND (reservation_status='Booked' OR reservation_status='Checked In' OR reservation_status='Done')";

   
$res = $mysqli->query($sql);
$report_total = [];
$down = [];
$final_total =[];
while ($rows = mysqli_fetch_assoc($res)) {
  $balance = $rows['balance'];
  $total = $rows['total'] - $balance;
  $downpayment= $rows['downpayment'];
    ?>
<tr>
    <td><?php echo $rows['client_reference_id']; ?></td>
    <td><?php echo $rows['first_name'] . ' ' . $rows['last_name']; ?></td>
    <td><?php echo $rows['arrival_date']; ?></td>
    <td><?php echo $rows['reservation_type'];?></td>
    <td>₱ <?php echo number_format($total,2);?></td>
</tr>
<?php
array_push($report_total, $total);
array_push($down, $downpayment);
}

?>
<tr>
<td></td>
<td></td>
<td></td>
<td></td>
<td>
<h1>Grand Total</h1>
<?php 
$report_total = array_sum($report_total);
array_push($final_total,$report_total);
echo '₱ '.number_format($report_total,2);
?>
</td>
</tr>
</tbody>
</table>
<h2>Daytour/Overnight</h2>
<table class="ui striped table">
<thead>
<tr>
  <th>Reference ID</th>
  <th>Name</th>
  <th>Date</th>
  <th>Type</th>
  <th>Total</th>
</tr>
</thead>
<tbody>
<?php
$sql = "SELECT * FROM transaction JOIN customer ON transaction.client_reference_id = customer.client_reference_id WHERE arrival_date BETWEEN '$start' AND '$end' AND  transaction_type='Daytour' OR transaction_type='Overnight'";
$res = $mysqli->query($sql);
$report_total = [];
$down = [];
while ($rows = mysqli_fetch_assoc($res)) {
  $balance = $rows['balance'];
  $total = $rows['total'] - $balance;
  $downpayment= $rows['downpayment'];
    ?>
<tr>
    <td><?php echo $rows['client_reference_id']; ?></td>
    <td><?php echo $rows['first_name'] . ' ' . $rows['last_name']; ?></td>
    <td><?php echo $rows['arrival_date']; ?></td>
    <td><?php echo $rows['transaction_type'];?></td>
    <td>₱ <?php echo number_format($total,2);?></td>
</tr>
<?php
array_push($report_total, $total);
array_push($down, $downpayment);
}

?>
<tr>
<td></td>
<td></td>
<td></td>
<td></td>
<td>
<h1>Grand Total</h1>
<?php 
$report_total = array_sum($report_total);
array_push($final_total,$report_total);
echo '₱ '.number_format($report_total,2);
?>
</td>
</tr>
</tbody>
</table>
<table class="ui table">
<tbody>
<tr>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td>
<h1> Grand Total</h1>
<?php
$final_total = array_sum($final_total);
echo '₱ '.number_format($final_total,2);
?>
</tr>
</tbody>
</table>
<?php
} else {

}
}

?>
<!-- Confirmed Booking -->
<?php if ($type == 'Booked' and $start =='' || $end=='' ) {

    ?>
    <h1>All Reports on Confirmed Booking/Booked Reservation</h1>
<table class="ui striped table">
  <thead>
    <tr>
      <th>Reference ID</th>
      <th>Name</th>
      <th>Arrival Date</th>
      <th>Departure Date</th>
      <th>Date Reserved</th>
    </tr>
  </thead>
  <tbody>
  <?php
    $sql = "SELECT * FROM reservation JOIN customer ON customer.client_reference_id = reservation.client_reference_id
    WHERE reservation_status='Booked'";
    $res = $mysqli->query($sql);
    while ($rows = mysqli_fetch_assoc($res)) {
        ?>
    <tr>
        <td><?php echo $rows['client_reference_id']; ?></td>
        <td><?php echo $rows['first_name'] . ' ' . $rows['last_name']; ?></td>
        <td><?php echo $rows['arrival_date']; ?></td>
        <td><?php echo $rows['departure_date']; ?></td>
        <td><?php echo $rows['created_at']; ?></td>
    </tr>
  <?php
}
    ?>
  </tbody>
</table>
<?php
} 

else {
  if($type=='Booked') {
?>
  <h1>Reports on Confirmed Booking/Booked Reservation</h1>
  <table class="ui striped table">
    <thead>
      <tr>
        <th>Reference ID</th>
        <th>Name</th>
        <th>Arrival Date</th>
        <th>Departure Date</th>
        <th>Date Reserved</th>
      </tr>
    </thead>
    <tbody>
    <?php
      $sql = "SELECT * FROM reservation JOIN customer ON customer.client_reference_id = reservation.client_reference_id
      WHERE reservation_status='Booked' AND created_at BETWEEN '$start' AND '$end' OR created_at BETWEEN '$yesterday' AND '$start'";
      $res = $mysqli->query($sql);
      while ($rows = mysqli_fetch_assoc($res)) {
          ?>
      <tr>
          <td><?php echo $rows['client_reference_id']; ?></td>
          <td><?php echo $rows['first_name'] . ' ' . $rows['last_name']; ?></td>
          <td><?php echo $rows['arrival_date']; ?></td>
          <td><?php echo $rows['departure_date']; ?></td>
          <td><?php echo $rows['created_at']; ?></td>
      </tr>
    <?php
  }
      ?>
    </tbody>
  </table>

<?php
} else {

}
}

if ($type == 'Expired') {

    ?>
  <h1>Reports on Expired Reservation</h1>
<table class="ui striped table">
<thead>
  <tr>
    <th>Reference ID</th>
    <th>Name</th>
    <th>Arrival Date</th>
    <th>Departure Date</th>
    <th>Date Reserved</th>
    <th>Expired Date</th>
  </tr>
</thead>
<tbody>
<?php
$sql = "SELECT * FROM reservation JOIN customer ON customer.client_reference_id = reservation.client_reference_id
      WHERE reservation_status='Expired' AND created_at BETWEEN '$start' AND '$end' OR created_at BETWEEN '$yesterday' AND '$start'";
    $res = $mysqli->query($sql);
    while ($rows = mysqli_fetch_assoc($res)) {
        ?>
  <tr>
      <td><?php echo $rows['client_reference_id']; ?></td>
      <td><?php echo $rows['first_name'] . ' ' . $rows['last_name']; ?></td>
      <td><?php echo $rows['arrival_date']; ?></td>
      <td><?php echo $rows['departure_date']; ?></td>
      <td><?php echo $rows['created_at']; ?></td>
      <td><?php echo $rows['expires_at']; ?></td>
  </tr>
<?php
}
    ?>
</tbody>
</table>
<?php
}
?>

<!-- Arrival Departure Check In Checkout -->
<?php if ($type == 'ArrivalDeparture' and $start =='' || $end=='' ) {

?>
<h1>All Reports on Check In / Check Out</h1>
<table class="ui striped table">
<thead>
<tr>
  <th>Reference ID</th>
  <th>Name</th>
  <th>Pax</th>
  <th>Checked In Date</th>
  <th>Checked Out Date</th>
  <th>Date Created</th>
</tr>
</thead>
<tbody>
<?php

$sql = "SELECT * FROM reservation JOIN customer ON customer.client_reference_id = reservation.client_reference_id
JOIN transaction ON customer.client_reference_id = transaction.client_reference_id";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    ?>
<tr>
    <td><?php echo $rows['client_reference_id']; ?></td>
    <td><?php echo $rows['first_name'] . ' ' . $rows['last_name']; ?></td>
    <td><?php echo $rows['no_of_adults']; + $rows['no_of_kids']; + $rows['no_of_senior_pwd'];?></td>
    <td><?php echo $rows['check_in_date']; ?></td>
    <td><?php echo $rows['check_out_date']; ?></td>
    <td><?php echo $rows['created_at']; ?></td>
</tr>
<?php
}
?>
</tbody>
</table>
<?php
} 

else {
if($type=='ArrivalDeparture') {
?>
<h1>Reports on Confirmed CheckIn/Checkout</h1>
<table class="ui striped table">
<thead>
  <tr>
  <th>Reference ID</th>
  <th>Name</th>
  <th>Pax</th>
  <th>Checked In Date</th>
  <th>Checked Out Date</th>
  <th>Date Created</th>
  </tr>
</thead>
<tbody>
<?php
$sql = "SELECT * FROM transaction JOIN customer ON customer.client_reference_id = transaction.client_reference_id
JOIN reservation ON customer.client_reference_id = reservation.client_reference_id WHERE check_in_date BETWEEN '$start' AND '$end' OR check_out_date BETWEEN '$start' AND '$end'";
  $res = $mysqli->query($sql);
  while ($rows = mysqli_fetch_assoc($res)) {
      ?>
  <tr>
  <td><?php echo $rows['client_reference_id']; ?></td>
    <td><?php echo $rows['first_name'] . ' ' . $rows['last_name']; ?></td>
    <td><?php echo $rows['no_of_adults']; + $rows['no_of_kids']; + $rows['no_of_senior_pwd'];?></td>
    <td><?php echo $rows['check_in_date']; ?></td>
    <td><?php echo $rows['check_out_date']; ?></td>
    <td><?php echo $rows['created_at']; ?></td>
  </tr>
<?php
}
  ?>
</tbody>
</table>

<?php
} else {

}
}

if ($type == 'Expired') {

?>
<h1>Reports on Expired Reservation</h1>
<table class="ui striped table">
<thead>
<tr>
<th>Reference ID</th>
<th>Name</th>
<th>Arrival Date</th>
<th>Departure Date</th>
<th>Date Reserved</th>
<th>Expired Date</th>
</tr>
</thead>
<tbody>
<?php
$sql = "SELECT * FROM reservation JOIN customer ON customer.client_reference_id = reservation.client_reference_id
  WHERE reservation_status='Expired' AND created_at BETWEEN '$start' AND '$end' OR created_at BETWEEN '$yesterday' AND '$start'";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    ?>
<tr>
  <td><?php echo $rows['client_reference_id']; ?></td>
  <td><?php echo $rows['first_name'] . ' ' . $rows['last_name']; ?></td>
  <td><?php echo $rows['arrival_date']; ?></td>
  <td><?php echo $rows['departure_date']; ?></td>
  <td><?php echo $rows['created_at']; ?></td>
  <td><?php echo $rows['expires_at']; ?></td>
</tr>
<?php
}
?>
</tbody>
</table>
<?php
}
?>



<!-- Cancelled Booking -->
<?php if ($type == 'Cancelled' and $start =='' || $end=='' ) {

?>
<h1>All Reports on Cancelled Booking</h1>
<table class="ui striped table">
<thead>
<tr>
  <th>Reference ID</th>
  <th>Name</th>
  <th>Arrival Date</th>
  <th>Departure Date</th>
  <th>Date Reserved</th>
</tr>
</thead>
<tbody>
<?php
$sql = "SELECT * FROM reservation JOIN customer ON customer.client_reference_id = reservation.client_reference_id
WHERE reservation_status='Cancelled'";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    ?>
<tr>
    <td><?php echo $rows['client_reference_id']; ?></td>
    <td><?php echo $rows['first_name'] . ' ' . $rows['last_name']; ?></td>
    <td><?php echo $rows['arrival_date']; ?></td>
    <td><?php echo $rows['departure_date']; ?></td>
    <td><?php echo $rows['created_at']; ?></td>
</tr>
<?php
}
?>
</tbody>
</table>
<?php
} 

else {
if($type=='Cancelled') {
?>
<h1>Reports on Cancelled Reservation</h1>
<table class="ui striped table">
<thead>
  <tr>
    <th>Reference ID</th>
    <th>Name</th>
    <th>Arrival Date</th>
    <th>Departure Date</th>
    <th>Date Reserved</th>
  </tr>
</thead>
<tbody>
<?php
  $sql = "SELECT * FROM reservation JOIN customer ON customer.client_reference_id = reservation.client_reference_id
  WHERE reservation_status='Booked' AND arrival_date BETWEEN '$start' AND '$end' OR arrival_date BETWEEN '$yesterday' AND '$start'";
  $res = $mysqli->query($sql);
  while ($rows = mysqli_fetch_assoc($res)) {
      ?>
  <tr>
      <td><?php echo $rows['client_reference_id']; ?></td>
      <td><?php echo $rows['first_name'] . ' ' . $rows['last_name']; ?></td>
      <td><?php echo $rows['arrival_date']; ?></td>
      <td><?php echo $rows['departure_date']; ?></td>
      <td><?php echo $rows['created_at']; ?></td>
  </tr>
<?php
}
  ?>
</tbody>
</table>

<?php
} else {

}
}

if ($type == 'Expired') {

?>
<h1>Reports on Expired Reservation</h1>
<table class="ui striped table">
<thead>
<tr>
<th>Reference ID</th>
<th>Name</th>
<th>Arrival Date</th>
<th>Departure Date</th>
<th>Date Reserved</th>
<th>Expired Date</th>
</tr>
</thead>
<tbody>
<?php
$sql = "SELECT * FROM reservation JOIN customer ON customer.client_reference_id = reservation.client_reference_id
  WHERE reservation_status='Expired' AND created_at BETWEEN '$start' AND '$end' OR created_at BETWEEN '$yesterday' AND '$start'";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    ?>
<tr>
  <td><?php echo $rows['client_reference_id']; ?></td>
  <td><?php echo $rows['first_name'] . ' ' . $rows['last_name']; ?></td>
  <td><?php echo $rows['arrival_date']; ?></td>
  <td><?php echo $rows['departure_date']; ?></td>
  <td><?php echo $rows['created_at']; ?></td>
  <td><?php echo $rows['expires_at']; ?></td>
</tr>
<?php
}
?>
</tbody>
</table>
<?php
}

?>
<!-- Extensions-->
<?php
if ($type == 'Extensions' and $start =='' || $end=='' ) { 
  ?>
  <h1>All Reports on Extension/Adjustments</h1>
  <table class="ui striped table">
  <thead>
  <tr>
  <th>Reference ID</th>
  <th>Name</th>
  <th>Arrival Date</th>
  <th>Departure Date</th>
  <th>Date Reserved</th>
  <th>Expired Date</th>
  </tr>
  </thead>
  <tbody>
  <?php
  $sql = "SELECT * FROM reservation JOIN customer ON customer.client_reference_id = reservation.client_reference_id
          JOIN transaction ON customer.client_reference_id = transaction.client_reference_id
          WHERE transaction_type='Adjustment' ";
  $res = $mysqli->query($sql);
  while ($rows = mysqli_fetch_assoc($res)) {
      ?>
  <tr>
    <td><?php echo $rows['client_reference_id']; ?></td>
    <td><?php echo $rows['first_name'] . ' ' . $rows['last_name']; ?></td>
    <td><?php echo $rows['arrival_date']; ?></td>
    <td><?php echo $rows['departure_date']; ?></td>
    <td><?php echo $rows['created_at']; ?></td>
    <td><?php echo $rows['expires_at']; ?></td>
  </tr>
  <?php
  }
  ?>
  </tbody>
  </table>
  <?php

  } else if($type == 'Extensions') {
  ?>
  <h1>Reports on Extension/Adjustments</h1>
  <table class="ui striped table">
  <thead>
  <tr>
  <th>Reference ID</th>
  <th>Name</th>
  <th>Arrival Date</th>
  <th>Departure Date</th>
  <th>Date Reserved</th>
  <th>Expired Date</th>
  </tr>
  </thead>
  <tbody>
  <?php
  $sql = "SELECT * FROM reservation JOIN customer ON customer.client_reference_id = reservation.client_reference_id
          JOIN transaction ON customer.client_reference_id = transaction.client_reference_id
          WHERE transaction_type='Adjustment'  AND created_at BETWEEN '$start' AND '$end' OR created_at BETWEEN '$yesterday' AND '$start'";
  $res = $mysqli->query($sql);
  while ($rows = mysqli_fetch_assoc($res)) {
      ?>
  <tr>
    <td><?php echo $rows['client_reference_id']; ?></td>
    <td><?php echo $rows['first_name'] . ' ' . $rows['last_name']; ?></td>
    <td><?php echo $rows['arrival_date']; ?></td>
    <td><?php echo $rows['departure_date']; ?></td>
    <td><?php echo $rows['created_at']; ?></td>
    <td><?php echo $rows['expires_at']; ?></td>
  </tr>
  <?php
  }
  ?>
  </tbody>
  </table>
<?php
  }else {

  }


?>

<!-- Rebooking-->
<?php
if ($type == 'Rebooking' and $start =='' || $end=='' ) { 
  ?>
  <h1>All Reports on Rebooking</h1>
  <table class="ui striped table">
  <thead>
  <tr>
  <th>Reference ID</th>
  <th>Name</th>
  <th>Arrival Date</th>
  <th>Departure Date</th>
  <th>Date Reserved</th>
  <th>Expired Date</th>
  </tr>
  </thead>
  <tbody>
  <?php
  $sql = "SELECT * FROM reservation JOIN customer ON customer.client_reference_id = reservation.client_reference_id
          WHERE isRebooked ='1' ";
  $res = $mysqli->query($sql);
  while ($rows = mysqli_fetch_assoc($res)) {
      ?>
  <tr>
    <td><?php echo $rows['client_reference_id']; ?></td>
    <td><?php echo $rows['first_name'] . ' ' . $rows['last_name']; ?></td>
    <td><?php echo $rows['arrival_date']; ?></td>
    <td><?php echo $rows['departure_date']; ?></td>
    <td><?php echo $rows['created_at']; ?></td>
    <td><?php echo $rows['expires_at']; ?></td>
  </tr>
  <?php
  }
  ?>
  </tbody>
  </table>
  <?php

  } else if($type=='Rebooking'){
  ?>
  <h1>Reports on Rebooking</h1>
  <table class="ui striped table">
  <thead>
  <tr>
  <th>Reference ID</th>
  <th>Name</th>
  <th>Arrival Date</th>
  <th>Departure Date</th>
  <th>Date Reserved</th>
  <th>Expired Date</th>
  </tr>
  </thead>
  <tbody>
  <?php
  $sql = "SELECT * FROM reservation JOIN customer ON customer.client_reference_id = reservation.client_reference_id
          WHERE isRebooked='1'  AND created_at BETWEEN '$start' AND '$end' OR created_at BETWEEN '$yesterday' AND '$start'";
  $res = $mysqli->query($sql);
  while ($rows = mysqli_fetch_assoc($res)) {
      ?>
  <tr>
    <td><?php echo $rows['client_reference_id']; ?></td>
    <td><?php echo $rows['first_name'] . ' ' . $rows['last_name']; ?></td>
    <td><?php echo $rows['arrival_date']; ?></td>
    <td><?php echo $rows['departure_date']; ?></td>
    <td><?php echo $rows['created_at']; ?></td>
    <td><?php echo $rows['expires_at']; ?></td>
  </tr>
  <?php
  }
  ?>
  </tbody>
  </table>
<?php
  } else {

  }


?>

<!-- Damages-->
<?php
if ($type == 'Damages' and $start =='' || $end=='' ) { 
  ?>
  <h1>All Reports on Damages</h1>
  <table class="ui striped table">
  <thead>
  <tr>
  <th>Reference ID</th>
  <th>Name</th>
  <th>Arrival Date</th>
  <th>Departure Date</th>
  <th>Damages</th>
  <th>Total</th>
  </tr>
  </thead>
  <tbody>
  <?php
  $sql = "SELECT * FROM  extras JOIN customer_extras on extras.extras_id = customer_extras.extras_id
          JOIN customer ON customer.client_reference_id = customer_extras.client_reference_id
          WHERE extras_category='Damages'OR extras_category='Charges' ";
  $res = $mysqli->query($sql);
  while ($rows = mysqli_fetch_assoc($res)) {
      ?>
  <tr>
    <td><?php echo $rows['client_reference_id']; ?></td>
    <td><?php echo $rows['first_name'] . ' ' . $rows['last_name']; ?></td>
    <td><?php echo $rows['arrival_date']; ?></td>
    <td><?php echo $rows['departure_date']; ?></td>
    <td><?php echo $rows['extras_name']; ?></td>
    <td>₱ <?php echo number_format($rows['extras_price'],2); ?></td>
  </tr>
  <?php
  }
  ?>
  </tbody>
  </table>
  <?php

  } else if($type == 'Damages') {
  ?>
  <h1>Reports on Damages</h1>
  <table class="ui striped table">
  <thead>
  <tr>
  <th>Reference ID</th>
  <th>Name</th>
  <th>Arrival Date</th>
  <th>Departure Date</th>
  <th>Damages</th>
  <th>Total</th>
  </tr>
  </thead>
  <tbody>
  <?php
  $sql = "SELECT * FROM  extras JOIN customer_extras on extras.extras_id = customer_extras.extras_id
  JOIN customer ON customer.client_reference_id = customer_extras.client_reference_id
  WHERE extras_category='Damages'OR extras_category='Charges'  AND arrival_date BETWEEN '$start' AND '$end' OR arrival_date BETWEEN '$yesterday' AND '$start'";
  $res = $mysqli->query($sql);
  while ($rows = mysqli_fetch_assoc($res)) {
      ?>
  <tr>
    <td><?php echo $rows['client_reference_id']; ?></td>
    <td><?php echo $rows['first_name'] . ' ' . $rows['last_name']; ?></td>
    <td><?php echo $rows['arrival_date']; ?></td>
    <td><?php echo $rows['departure_date']; ?></td>
    <td><?php echo $rows['extras_name']; ?></td>
    <td>₱ <?php echo number_format($rows['extras_price'],2); ?></td>
  </tr>
  <?php
  }
  ?>
  </tbody>
  </table>
<?php
  }else {

  }


?>


<!-- Occupancy-->
<?php
if ($type == 'Occupancy' and $start =='' || $end=='' ) { 
  ?>
  <h1>Occupancy Reports All Rooms</h1>
  <table class="ui striped table">
  <thead>
  <tr>
  <th>Room</th>
  <th>Name</th>
  <th>Arrival Date</th>
  <th>Departure Date</th>
  <th>Type</th>
  </tr>
  </thead>
  <tbody>
  <?php
  $sql = "SELECT * FROM `reserved_rooms`
   JOIN customer ON reserved_rooms.client_reference_id = customer.client_reference_id
   JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
   JOIN reservation ON reserved_rooms.client_reference_id = reservation.client_reference_id";
  $res = $mysqli->query($sql);
  while ($rows = mysqli_fetch_assoc($res)) {
      ?>
  <tr>
    <td><?php echo $rows['room_name']; ?></td>
    <td><?php echo $rows['first_name'] . ' ' . $rows['last_name']; ?></td>
    <td><?php echo $rows['arrival_date']; ?></td>
    <td><?php echo $rows['departure_date']; ?></td>
    <td><?php echo $rows['reservation_type'];?></td>
  </tr>
  <?php
  }
  ?>
  </tbody>
  </table>
  <?php

  } else if($type == 'Occupancy') {
  ?>
  <h1>Occupancy Report </h1>
  <table class="ui striped table">
  <thead>
  <tr>
  <th>Room</th>
  <th>Name</th>
  <th>Arrival Date</th>
  <th>Departure Date</th>
  <th>Type</th>
  </tr>
  </thead>
  <tbody>
  <?php
  $sql = "SELECT * FROM `reserved_rooms`
   JOIN customer ON reserved_rooms.client_reference_id = customer.client_reference_id
   JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
   JOIN reservation ON reserved_rooms.client_reference_id = reservation.client_reference_id
   WHERE arrival_date BETWEEN '$start' AND '$end' OR arrival_date BETWEEN '$yesterday' AND '$start'";
  $res = $mysqli->query($sql);
  while ($rows = mysqli_fetch_assoc($res)) {
      ?>
  <tr>
    <td><?php echo $rows['room_name']; ?></td>
    <td><?php echo $rows['first_name'] . ' ' . $rows['last_name']; ?></td>
    <td><?php echo $rows['arrival_date']; ?></td>
    <td><?php echo $rows['departure_date']; ?></td>
    <td><?php echo $rows['reservation_type'];?></td>
  </tr>
  <?php
  }
  ?>
  </tbody>
  </table>
<?php
  }else {

  }


?>


<?php
if (isset($print)) {
    ?>
  <p style="text-align:right; font-size:20px; margin-top:50px;"id="report-footer">Printed by: _________________</p>
  <script>
  window.print();
  </script>
  <?php
}

?>

