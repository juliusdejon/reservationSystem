<?php

include '../../config/mysqli.php';

if (isset($_POST['variable'])) {
    $var = $_POST['variable'];
    if (!empty($var)) {
        $sql =
            "SELECT * FROM customer INNER JOIN reservation
             JOIN reserved_rooms ON customer.client_reference_id = reserved_rooms.client_reference_id
             JOIN room_type ON room_type.room_id = reserved_rooms.room_type_id
             WHERE customer.client_reference_id='$var'";
        $res = $mysqli->query($sql);
        while ($rows = mysqli_fetch_assoc($res)) {
            $client_reference_id = $rows['client_reference_id'];
            $first_name = $rows['first_name'];
            $last_name = $rows['last_name'];
            $email_address = $rows['email_address'];
            $arrival_date = $rows['arrival_date'];
            $departure_date = $rows['departure_date'];
            $created_at = $rows['created_at'];
            $no_of_adults = $rows['no_of_adults'];
            $no_of_kids = $rows['no_of_kids'];
            $reservation_status = $rows['reservation_status'];

        }
        if ($var == @$client_reference_id) {?>
    <div class="card">
                <div class="row">
                    <div class="col-md-6">


            <?php

            if ($reservation_status == 'Expired') {

            } else {?>
                 <form role="form" id="formReceipt" action="ajax/uploadReceipt.php" method="post" enctype="multipart/form-data">
                                        <div class="card-body">
                                            <div class="form-group">
                                                <div class="div-image">
                                                    <?php
@$sql = "SELECT * FROM reservation JOIN customer on reservation.client_reference_id=customer.client_reference_id WHERE reservation.client_reference_id ='$var'";
                $result = $mysqli->query($sql);
                $row = mysqli_fetch_assoc($result);
                $receipt = $row['receipt'];
                // $receipt_2 = $row['receipt_2'];
                $email = $row['email_address'];
                $first_name = $row['first_name'];
                $last_name = $row['last_name'];
                if ($receipt != '') {
                    echo '<img width="100" height="100" src="../admin/dist/img/receipts/' . $receipt . '" alt="Default Receipt" id="blah">';
                } else {
                    echo "<img width='100' height='100' src='../img/default.png' alt='Default Profile Pic' id='blah'>";

                }

                ?>
                                                </div>
                                                <div class="input-group">
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" name="file" onchange="readURL(this);" value="<?php echo $receipt; ?>">
                                                    </div>
                                                    <div class="input-group-append">
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="input-group">
                                               
                                                </div>

                                        </div>
                                        <!-- /.card-body -->

                                        <div class="card-footer">
                                            <input type="hidden" name="res_id" value="<?php echo $var; ?>" />
                                            <input type="hidden" name="email" value= "<?php echo $email; ?>" />
                                            <input type="hidden" name="first_name" value= "<?php echo $first_name; ?>" />
                                            <input type="hidden" name="last_name" value= "<?php echo $last_name; ?>" />
                                            <div class="row">
                                            <div class="col-md-6">
                                            </div>
                                            </div>
                                            <button type="submit" id="button" class="ui red button" name="submit">Upload</button>
                                        </div>
                                    </form>
<?php }?>
            </div>
            <div class="col-md-6">
            <h4 class="lead">
                            <?php echo $first_name . ' ' . $last_name; ?>
                        </h4>
                Arrival Date:
                <strong><?php echo $arrival_date; ?></strong><br>
                Departure Date:
                <strong><?php echo $departure_date; ?></strong><br/>
                No of Adults:
                <?php echo $no_of_adults; ?>
                <br />
                No of Kids:
                <?php echo $no_of_kids; ?>
                <br />
                No of Nights:
                <?php
$room_total = [];
            $departure = strtotime($departure_date);
            $arrival = strtotime($arrival_date);
            $datediff = $departure - $arrival;
            $noofNights = round($datediff / (60 * 60 * 24));
            echo $noofNights;
            ?><br />
                <?php
$sql = "SELECT * FROM reserved_rooms
           JOIN room_type ON reserved_rooms.room_type_id=room_type.room_id
           WHERE reserved_rooms.client_reference_id='$var'
           GROUP BY room_id";
            $res = $mysqli->query($sql);
            echo "<table border='1'>";
            echo "<tr>";
            echo "<th>Room Name</th>";
            echo "<th>Quantity</th>";
            echo "<th>Price</th>";
            echo "<th>Total Amount</th>";
            
            
            $total = [];
            while ($rows = mysqli_fetch_assoc($res)) {
                echo "<tr>";
                $room_type_id = $rows['room_type_id'];
                $room_name = $rows['room_name'];
                $room_price = $rows['room_price'];
                $room_info = $rows['room_info'];
                $room_img = $rows['room_img'];
                
                

                $sql1 = "SELECT COUNT(*) as quantity FROM reserved_rooms
                     JOIN customer ON
                     reserved_rooms.client_reference_id = customer.client_reference_id
                     JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
                     WHERE customer.client_reference_id ='$client_reference_id' AND room_type_id ='$room_type_id'";
                $res1 = $mysqli->query($sql1);
                $row1 = mysqli_fetch_assoc($res1);
                $quantity = $row1['quantity'];

                array_push($total, $room_price * $quantity);
                echo "<td>" . $room_name . "</td>";
                echo "<td>" . $quantity . "</td>";
                echo "<td>₱ " . number_format($room_price, 2) ."</td>";
                echo "<td>₱ " . number_format($room_price * $quantity, 2) ."</td>";
                echo "</tr>";
            }
            echo "</table>";
            ?>
            <h2>Total Amount Due: ₱ <?php echo number_format(array_sum($total),2);?></h2>
         </div>
        </div>
                    <!-- <hr /> -->

                    <!-- /.row
                    <div class="row">
                        <?php
$sql = "SELECT * FROM reserved_rooms
            JOIN room_type ON reserved_rooms.room_type_id=room_type.room_id
            WHERE reserved_rooms.client_reference_id='$var'
            GROUP BY room_id";
            $res = $mysqli->query($sql);
            while ($rows = mysqli_fetch_assoc($res)) {
                $room_type_id = $rows['room_type_id'];
                $room_name = $rows['room_name'];
                $room_price = $rows['room_price'];
                $room_info = $rows['room_info'];
                $room_img = $rows['room_img'];
                ?>

                            <div class="col-md-3">
                            <div class="ui card">
  <div class="image">
  <img src='../img/<?php echo $room_img; ?>' alt="Card image cap">
  </div>
  <div class="content">
    <a class="header"><?php echo $room_name; ?></a>
    <div class="description">
        <?php echo $room_info; ?>
    </div>
  </div>
  <div class="extra content">
    <a>
    <i class="fa fa-bed"></i>
      Price
                                        <?php echo $room_price; ?>
                                        <?php
$sql1 = "SELECT COUNT(*) as quantity FROM reserved_rooms
     JOIN customer ON
     reserved_rooms.client_reference_id = customer.client_reference_id
     JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
     WHERE customer.client_reference_id ='$client_reference_id' AND room_type_id ='$room_type_id'";
                $res1 = $mysqli->query($sql1);
                $row1 = mysqli_fetch_assoc($res1);
                $quantity = $row1['quantity'];
                ?>
                                            Qty:
                                            <?php echo $quantity; ?>
                                            <?php
$subtotal = $quantity * $room_price;
                array_push($room_total, $subtotal);
                ?>
    </a>
  </div>
</div>


                            </div>

                            <?php
}
            ?>


                    </div>
</div> -->
                    <div class="row">
                        <!-- accepted payments column -->
                        <div class="col-6">

                        </div>

                    </div>
                    <!-- /.row -->

                    <!-- this row will not appear when printing -->
                    <div class="row no-print">
                        <div class="col-12">
                            <input type="hidden" name="client_reference_id" value="<?php echo $client_reference_id; ?>" />
                        </div>
                    </div>
</div>
    </div>
    <?php
} else {
            echo "<div class='bs-example' data-example-id='simple-jumbotron'><div class='jumbotron'><h3>Reference ID Not Found.</h3><p></p></div></div>";
        }
        ?>



        <?php
}
    ?>
            <script>
                let deposits = [];
                let depositTotal = [];
            </script>

            <?php
}
?>



                <!-- jQuery -->
                <script src="../admin/plugins/jquery/jquery.min.js"></script>
                <script>
                    function addDeposit(id, price) {

                        var status = $('.check' + id)[0].checked;
                        if (status == true) {
                            deposits.push(id);
                            depositTotal.push(price);
                        } else {
                            for (var i = deposits.length - 1; i >= 0; i--) {
                                if (deposits[i] === id) {
                                    deposits.splice(i, 1);
                                    depositTotal.splice(i, 1);
                                }
                            }


                        }
                        var sum = 0;
                        for (var i = 0; i < depositTotal.length; i++) {
                            sum += depositTotal[i]
                        }
                        $("#depositTotalLabel").text(sum);
                        $("#depositTotalInput").val(sum);

                        $('#depositHiddenInput').val(deposits);

                        var depositTotalInput = $("#depositTotalInput").val();
                        // computation entrance Fee
                        var entrance = ($('#no-of-adult').val() * 250) + ($('input#no-of-kids').val() * 150);
                        $('#entrance').text(entrance);
                        $('#entranceTotal').val(entrance);
                        // computation subtotal
                        console.log(depositTotalInput);
                        var subtotal = entrance + parseInt($("#roomTotal").val()) + parseInt(depositTotalInput);
                        console.log(subtotal);
                        $("#subtotal").text(subtotal);
                        $("#subTotal").val(subtotal);

                        // computated downpayment
                        var downpayment = $("#downpaymentTotal").val();
                        $("#downpayment").text(downpayment);
                        $("#downpaymentTotal").val(downpayment);

                        // computation balance
                        var balance = subtotal - downpayment;
                        $("#balance").text(balance);
                        $("#balanceTotal").val(balance);
                    }
                </script>
                <script src="../admin/ajax/computations.js"></script>
                <script src="ajax/uploadReceipt.js"></script>