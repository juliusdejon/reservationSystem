<?php
include '../config/mysqli.php';
$room_id = $_GET['room_id'];
$room_number = $_GET['room_number'];

$sql = "SELECT COUNT(*) as checked_in FROM reserved_rooms JOIN transaction ON transaction.client_reference_id=reserved_rooms.client_reference_id WHERE room_type_id='$room_id'";
$res = $mysqli->query($sql);
$rows = mysqli_fetch_assoc($res);
$checked_in_count = $rows['checked_in'];

$sql = "SELECT COUNT(*) as totalRooms FROM rooms WHERE room_type_id='$room_id'";
$res = $mysqli->query($sql);
$rows = mysqli_fetch_assoc($res);
$totalRooms = $rows['totalRooms'];

$availableRooms = $totalRooms - $checked_in_count;
if ($availableRooms == 0) {
    header('Location: view_room.php?room_id=' . $room_id . '&deleted=false');
} else {
    $sql = "DELETE FROM rooms WHERE room_number ='$room_number'";
    $mysqli->query($sql);

    header('Location: view_room.php?room_id=' . $room_id . '&deleted=true');
}
