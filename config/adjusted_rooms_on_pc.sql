-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 27, 2018 at 03:26 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `villa_alfredo_v1`
--

-- --------------------------------------------------------

--
-- Table structure for table `adjusted_rooms`
--

CREATE TABLE `adjusted_rooms` (
  `adjustment_id` int(11) NOT NULL,
  `client_reference_id` varchar(255) NOT NULL,
  `adjusted_room_type_id` int(11) NOT NULL,
  `consumed_days` int(11) NOT NULL,
  `new_adjusted_room_id` int(11) NOT NULL,
  `adjusted_start_date` date NOT NULL,
  `adjusted_end_date` date NOT NULL,
  `rate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adjusted_rooms`
--
ALTER TABLE `adjusted_rooms`
  ADD PRIMARY KEY (`adjustment_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adjusted_rooms`
--
ALTER TABLE `adjusted_rooms`
  MODIFY `adjustment_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
