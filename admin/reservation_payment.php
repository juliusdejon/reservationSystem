    <?php
include '../config/mysqli.php';
$ref_id = $_GET['res_id'];
include 'reservationAdminFunctions.php';
// Get the rang of dates
function date_range($first, $last, $step, $output_format)
{
    $dates = array();
    $current = strtotime($first);
    $last = strtotime($last);
    while ($current <= $last) {
        $dates[] = date($output_format, $current);
        $current = strtotime($step, $current);
    }
    return $dates;
}

$sql =
    "SELECT * FROM customer INNER JOIN reservation
 JOIN reserved_rooms ON customer.client_reference_id = reserved_rooms.client_reference_id
 JOIN room_type ON room_type.room_id = reserved_rooms.room_type_id
 WHERE customer.client_reference_id='$ref_id'";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $first_name = $rows['first_name'];
    $last_name = $rows['last_name'];
    $email_address = $rows['email_address'];
    $arrival_date = $rows['arrival_date'];
    $departure_date = $rows['departure_date'];
    $created_at = $rows['created_at'];
    $no_of_adults = $rows['no_of_adults'];
    $no_of_kids = $rows['no_of_kids'];
    $no_of_senior_pwd = $rows['no_of_senior_pwd'];
}

$room_total = [];

$departure = strtotime($departure_date);
$arrival = strtotime($arrival_date);
$datediff = $departure - $arrival;
$noofNights = round($datediff / (60 * 60 * 24));

?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>Villa Alfredo Admin</title>

        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/adminlte.min.css">
        <!-- Semantic UI -->
        <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />

        <!-- Initialize Semantic UI and JQuery to load Calendar Datepicker-->
        <script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" src="../js/semantic.min.js"></script>

        <!-- Calendar -->
        <link type="text/css" rel="stylesheet" href="../css/calendar.min.css" />
        <script src="../js/calendar.min.js"></script>



        <!-- Font Awesome Icon -->
        <link rel="stylesheet" href="../css/font-awesome.min.css">

        <!-- Icons -->
        <link rel="stylesheet" href="../css/icon.min.css">




    </head>

    <body class="hold-transition sidebar-mini">
        <div class="wrapper">

            <?php
include 'layout/navbar.php';
include 'layout/sidebar.php';
?>
                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <div class="content-header">
                        <div class="container-fluid">
                            <div class="row mb-2">
                                <div class="col-sm-6">
                                    <h2><i class="fa fa-bed"></i> Room Payment   </h2>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->
                    </div>
                    <!-- /.content-header -->

                    <!-- Main content -->
                    <div class="content">

                            <div class="row">
                                <div class="col-md-9">
                                    <div class="ui segment">

                                        <h4 class="ui dividing header">Reserved Rooms </h4>

                                        <div class="row">
                                        <div class="col-md-3">

                                        </div>

                                        </div>

                                        <div class="row">
                        <?php
$sql = "SELECT * FROM reserved_rooms
           JOIN room_type ON reserved_rooms.room_type_id=room_type.room_id
           WHERE reserved_rooms.client_reference_id='$ref_id'
           GROUP BY room_id";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $room_type_id = $rows['room_type_id'];
    $room_name = $rows['room_name'];
    $room_price = $rows['room_price'];
    $room_info = $rows['room_info'];
    $room_img = $rows['room_img'];
    ?>

                            <div class="col-md-3">
                            <div class="ui card">
  <div class="image">
  <img src='../img/<?php echo $room_img; ?>' alt="Card image cap">
  </div>
  <div class="content">
    <a class="header"><?php echo $room_name; ?></a>
    <div class="description">
        <?php echo $room_info; ?>
    </div>
  </div>
  <div class="extra content">
    <a>
    <i class="fa fa-bed"></i>
      Price
                                        <?php echo $room_price; ?>
                                        <?php
$sql1 = "SELECT COUNT(*) as quantity FROM reserved_rooms
     JOIN customer ON
     reserved_rooms.client_reference_id = customer.client_reference_id
     JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
     WHERE customer.client_reference_id ='$ref_id' AND room_type_id ='$room_type_id'";
    $res1 = $mysqli->query($sql1);
    $row1 = mysqli_fetch_assoc($res1);
    $quantity = $row1['quantity'];
    ?>
                                            Qty:
                                            <?php echo $quantity; ?>
                                            <?php
$subtotal = $quantity * $room_price;
    array_push($room_total, $subtotal);
    ?>
    </a>
  </div>
</div>


                            </div>

                            <?php
}
?>


                    </div>
                    <div class="row">

            <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content container">
    <div class="modal-body">
    <h4 class="ui dividing header">Extras</h4>
    <form action="ajax/addExtras.php" method="post">
    <input type="hidden" name="client_reference_id" value="<?php echo $_GET['res_id']; ?>" />
    <span id="result"></span>
                    <div id="list">

  <div class="row" id="item">
    <div class="form-group col-md-6" >
        <label for="inputEmail3" class="control-label">Item</label>

        <select id="select" class="form-control" name="item[]">
        <option>---</option>
        <?php
$sql = "SELECT * FROM extras WHERE extras_category='Rental'";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $extras_name = $rows['extras_name'];
    $extras_id = $rows['extras_id'];
    $extras_price = $rows['extras_price'];

    echo "<option value='$extras_id'>$extras_name - ₱$extras_price</option>";
}
?>
        </select>


    </div>
<div class="form-group col-md-6">
          <label for="inputEmail3" class="col-lg-4 control-label">Qty</label>

<input type="number" min="0" max ="30"maxlength="2" class="form-control col-sm-4 numbers-only" name="quantity[]" value="0">
</div>
</div>

</div>


<div>

<button class="ui button teal tiny" type="button" id="add">More Item <i class="fa fa-plus"></i></button>
</div>
<input type="submit" name="addExtras" class="ui button teal pull-right" value="Add Items">
</form>

    </div>


</div>


  </div>

</div>

                    <div class="col-md-12">

<button type="button" class="ui button teal" data-toggle="modal" data-target=".bd-example-modal-lg">Add Extras <i class="fa fa-plus"></i></button>

                    <div class="form-group col-md-6" >
<?php
$extrasTotal = [];

$sql = "SELECT * FROM customer_extras
JOIN extras ON customer_extras.extras_id = extras.extras_id
WHERE client_reference_id = '$ref_id' AND extras_category='Rental'
GROUP BY customer_extras.extras_id";

$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $extras_id = $rows['extras_id'];
    $extras_name = $rows['extras_name'];
    $extras_price = $rows['extras_price'];
    $sqlCount = "SELECT COUNT(*) as extras_qty FROM customer_extras WHERE extras_id='$extras_id' AND client_reference_id='$ref_id'";
    $resCount = $mysqli->query($sqlCount);
    $rowCount = mysqli_fetch_array($resCount);
    $extras_qty = $rowCount['extras_qty'];

    $extras = $extras_price * $extras_qty;
    array_push($extrasTotal, $extras);
    ?>
    <div class="row">
<input type="text" class="form-control col-md-6" disabled value="<?php echo $extras_name; ?>-<?php echo $extras_qty; ?>"/>
<a type="button" class="ui button red tiny delete-row" href="delete_extras_walk_in.php?id=<?php echo $extras_id; ?>&res_id=<?php echo $ref_id; ?>"><i class="fa fa-close"></i></a>
</div>
<?php
}
?>
</div>
                    <!-- <h4 class="ui dividing header">Function Room</h4>
                    To be Added -->
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-md-6">

</div>
<div class="col-md-6">

<p class="lead">Amount</p>
<form action="reservation_payment_feedback.php" name="walkInPaymentForm" method="post" onsubmit="return validateForm();">
                                                    <table class="table">
                                                        <tbody>
                                                        <tr>
                                                                <th>Room Total:</th>
                                                                <td>₱
                                                                <?php
$room_total_final = array_sum($room_total);
$room_total_times = $room_total_final * $noofNights;
echo number_format($room_total_times, 2);?>

                                                                </td>
                                                    </tr>

                                                            <tr>
                                                                <th>Entrance Fees:</th>
                                                                <td>
                                                                <?php $entranceTotal = ($no_of_adults * 250) + ($no_of_kids * 150) + ($no_of_senior_pwd * 200);?>
                                                                    <input type="hidden" id="entrance" value="<?php echo $entranceTotal; ?>" />
                                                                    <span id="entranceText">₱
                                                                    <?php echo number_format($entranceTotal, 2); ?>
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                               <tr>
                                                                    <th>Extras</th>
                                                                    <td>₱
                                                                        <?php
$extrasInput = array_sum($extrasTotal);
echo number_format($extrasInput, 2);?>
                                                                    </td>
                                                                </tr>
                                                            <tr>
                                                                <th>Total</th>
                                                                <td>
                                                                <?php
$grandTotal = $entranceTotal + $room_total_times + $extrasInput;?>
                                                                    <span id="totalText">₱ <?php echo number_format($grandTotal, 2); ?></span>
                                                                    <input type="hidden" name="total" id="total" value="<?php echo $grandTotal; ?>" >
                                                                </td>
                                                            </tr>


                                                            <tr>
                                                                <th>Downpayment</th>
                                                                <td>
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend"> <span class="input-group-text">Php</span></div>
                                                                    <input type="number" min="0" class="col-md-8 form-control numbers-only" name="downpayment" placeholder="Payment in Peso" required    />
                                                                    </div>
                                                                </td>
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                <input type="hidden" name="ref_id" value="<?php echo $_GET['res_id']; ?>" />

                                                    <button type="submit" name="pay" class="ui button pull-right teal"> Submit Booking <i class="fa fa-arrow-right"></i></button>
</form>
                                                </div>


</div>




                                        <div class="ui form">
                                            <div class="two fields">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="ui segment">
                                    <h5>Customer </h5>
                                    <h4 class="lead">
                                        <?php echo $first_name . ' ' . $last_name; ?>
                                        </h4>
                                    <div class="reservation-title">Arrival Date:</div>
                                    <?php echo $arrival_date; ?>
                            <div class="ui hidden divider"></div>
                            <div class="reservation-title">Departure Date</div>
                            <?php echo $departure_date; ?>
                            <div class="ui hidden divider"></div>
                            <div class="reservation-title">Room Nights</div>
                            <?php echo $noofNights; ?>
                            <div class="ui hidden divider"></div>
                            <div class="reservation-title">Room Selections: </div>
                            <?php
$room_total_side = [];
$sql = "SELECT * FROM reserved_rooms
           JOIN room_type ON reserved_rooms.room_type_id=room_type.room_id
           WHERE reserved_rooms.client_reference_id='$ref_id'
           GROUP BY room_id";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $room_type_id = $rows['room_type_id'];
    $room_name = $rows['room_name'];
    $room_price = $rows['room_price'];
    $room_info = $rows['room_info'];
    $room_img = $rows['room_img'];
    ?>

<?php echo $room_name; ?>
                                        <?php
$sql1 = "SELECT COUNT(*) as quantity FROM reserved_rooms
     JOIN customer ON
     reserved_rooms.client_reference_id = customer.client_reference_id
     JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
     WHERE customer.client_reference_id ='$ref_id' AND room_type_id ='$room_type_id'";
    $res1 = $mysqli->query($sql1);
    $row1 = mysqli_fetch_assoc($res1);
    $quantity = $row1['quantity'];
    ?>
                                          x
                                            <?php echo $quantity; ?>
                                            <?php
$subtotal = $quantity * $room_price;
    array_push($room_total_side, $subtotal);
}
?>
                           =  ₱ <?php echo number_format(array_sum($room_total_side), 2); ?>
                            <div class="ui hidden divider"></div>
                            <div class="reservation-title">No of Persons</div>
                            Adult x <?php echo $no_of_adults; ?> = <span id="label-total-adult">₱ <?php echo number_format($no_of_adults * 250, 2); ?></span><br />
                            Kids x <?php echo $no_of_kids; ?>= <span id="label-total-kids">₱ <?php echo number_format($no_of_kids * 150, 2); ?></span><br />
                            Senior/Pwd x <?php echo $no_of_senior_pwd; ?>    = <span id="label-total-kids">₱ <?php echo number_format($no_of_senior_pwd * 200, 2); ?> </span><br />
                            <div class="ui clearing divider"></div>
                            <span id="totalAmount">
                            </span>

                                    </div>
                                </div>
                            </div>


                    </div>

                    <!-- /.content -->
                </div>
                <!-- /.content-wrapper -->

                <!-- Main Footer -->
                <?php
include 'layout/footer.php';
?>
                    <!-- ./wrapper -->

                    <!-- REQUIRED SCRIPTS -->
                    <script>
                        $(document).ready(function() {
                            var a = document.getElementById("item");
                            $("#add").click(function() {
                                $(a).clone().appendTo('#list').find("input[type='number']").val("");
                            });
                        });
                    </script>
                    <script type="text/javascript" src="../js/jquery.mask.js"></script>
                    <script>
                        // Validation Payment form
                        function validateForm() {
                            var downpayment = document.forms["walkInPaymentForm"]["downpayment"].value;
                            var total = document.forms["walkInPaymentForm"]["total"].value;
                            var computed_total = parseInt(total) / 2;
                            if (parseInt(downpayment) < computed_total ) {
                                alert("Please enter 50% amount of your total");
                                return false;
                            }
                            if(parseInt(downpayment) > parseInt(total)) {
                                alert("Amount entered is higher than his Total");
                                return false;
                            }
                      
                        }
                    </script>
                    <!-- jQuery -->
                    <script src=" plugins/jquery/jquery.min.js "></script>
                    <!-- JQuery Validation source -->
                    <script type="text/javascript" src="plugins/jquery/jquery-key-restrictions.min.js"></script>
                    <!-- Actual Validation calling class -->
                    <script type="text/javascript">
                    $(document).ready(function () {
                        $(".letters-only").lettersOnly();
                        $(".numbers-only").numbersOnly();
                        $(".alpha-numeric-only").alphaNumericOnly();
                    });
                    </script>
                    <script src="ajax/addExtras.js" type="text/javascript"> </script>
                    <!-- Bootstrap 4 -->
                    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js "></script>
                    <!-- AdminLTE App -->
                    <script src="dist/js/adminlte.min.js "></script>
    </body>

    </html>