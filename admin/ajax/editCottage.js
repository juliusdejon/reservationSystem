$("#editCottageForm").submit(function(e) {
    e.preventDefault();
    $("#button").html('<img src="ajax/loading-spinner.gif" width="25px">')
    var form = $(this);
    var post_url = form.attr("action");
    $.ajax({
        type: "post",
        url: post_url,
        data: new FormData(this),
        cache: false,
        processData: false,
        contentType: false,
        success: function(info) {
            $("#result").html(info);
            $("#button").text('Update');
        }
    });
});