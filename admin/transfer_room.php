<?php
$rebook_id = $_GET['res_id'];
$room_id = $_GET['room_id'];
@$no_room = $_GET['no_room'];
@$invalid_date = $_GET['invalid_date'];
include '../config/mysqli.php';

$sql_identifier = "SELECT * FROM reservation
        JOIN customer on reservation.client_reference_id = customer.client_reference_id
        WHERE reservation.client_reference_id = '$rebook_id'";
$res_identifier = $mysqli->query($sql_identifier);
while ($rows_identifier = $res_identifier->fetch_assoc()) {
    $reservation_status = $rows_identifier['reservation_status'];
    $first_name = $rows_identifier['first_name'];
    $last_name = $rows_identifier['last_name'];
    $email = $rows_identifier['email_address'];
    $contact = $rows_identifier['contact_number'];
    $no_of_kids = $rows_identifier['no_of_kids'];
    $no_of_adults = $rows_identifier['no_of_adults'];
    $arrival_date = $rows_identifier['arrival_date'];
    $departure_date = $rows_identifier['departure_date'];
}
// if ($reservation_status != 'Booked') {
//     header('Location: rebooking.php?rebooking=false');
// }

// Get the rang of dates
function date_range($first, $last, $step, $output_format)
{
    $dates = array();
    $current = strtotime($first);
    $last = strtotime($last);
    while ($current <= $last) {
        $dates[] = date($output_format, $current);
        $current = strtotime($step, $current);
    }
    return $dates;
}

// Visited the Adjustment Page, Insert all the old booking

// $sql = "SELECT * FROM reserved_rooms JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id WHERE client_reference_id='$rebook_id' GROUP BY room_type_id";
// $res = $mysqli->query($sql);
// while ($rows  = mysqli_fetch_assoc($res)) {
//     $room_id = $rows['room_type_id'];
//     $sql_count = "SELECT COUNT(*) as quantity FROM reserved_rooms WHERE room_type_id='$room_id' AND client_reference_id ='$rebook_id'";

//     $res_count = $mysqli->query($sql_count);
//     $rows_count = mysqli_fetch_assoc($res_count);
//     $quantity = $rows_count['quantity'];
//     $mysqli->query("INSERT INTO adjusted_rooms (client_reference_id, room_type_id, adjusted_room_quantity,adjusted_start_date,adjusted_end_date) VALUES ('$rebook_id','$room_id','$quantity', '$check_in_date','$date_now')");

// }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Villa Alfredo Admin</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Semantic UI -->
  <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />

         <!-- Initialize Semantic UI and JQuery to load Calendar Datepicker-->
         <script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" src="../js/semantic.min.js"></script>

        <!-- Calendar -->
        <link type="text/css" rel="stylesheet" href="../css/calendar.min.css" />
        <script src="../js/calendar.min.js"></script>

        <!-- Icons -->
        <link rel="stylesheet" href="../css/icon.min.css">



</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

<?php
include 'layout/navbar.php';
include 'layout/sidebar.php';
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Upgrade/Downgrade of Room: <?php echo $rebook_id; ?></h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
    <div class="row">


                            <div class="col-md-8">
                            <form action="transfer_post.php" name="transferForm" method="post" onsubmit="return validateForm();">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Transfer Form </h3>
                                    </div>
                                    <!-- /.card-header -->

                                    <div class="card-body">
                                          <input type="hidden" name="ref_id" id="ref_id" value="<?php echo $rebook_id; ?>">
                                            <div class="row">
                                                    <div class="ui form">
                                                        <div class="two fields">
      Date Now: <?php echo $date_now = date('Y-m-d'); ?> <br/>
      Checkout Date: <?php echo $departure_date; ?>
      <input type="hidden" name="date_now" value="<?php echo $date_now; ?>" />
      <input type="hidden" name="old_room" value="<?php echo $room_id; ?>" />
      <input type="hidden" name="client_reference_id" value="<?php echo $rebook_id; ?>" />
<?php
$sql_trans = "SELECT * FROM transaction WHERE client_reference_id ='$rebook_id'";
$res_trans = $mysqli->query($sql_trans);
$row_trans = mysqli_fetch_assoc($res_trans);
$check_in_date = $row_trans['check_in_date'];
$check_in_date = substr($check_in_date, 0, 10);

?>
<input type="hidden" name="check_in_date" value="<?php echo $check_in_date; ?>" />
                                                        </div>
<label>From</label>
                                                        <div class="row">
                        <?php
$sql = "SELECT * FROM reserved_rooms
           JOIN room_type ON reserved_rooms.room_type_id=room_type.room_id
           WHERE reserved_rooms.client_reference_id='$rebook_id' AND room_id='$room_id'
           GROUP BY room_id
           ";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $room_type_id = $rows['room_type_id'];
    $room_name = $rows['room_name'];
    $room_price = $rows['room_price'];
    $room_info = $rows['room_info'];
    $room_img = $rows['room_img'];
    ?>

                            <div class="col-md-6">
                            <div class="ui card">
  <div class="image">
  <img src='../img/<?php echo $room_img; ?>' alt="Card image cap">
  </div>
  <div class="content">
    <a class="header"><?php echo $room_name; ?></a>
    <div class="description">
    </div>
  </div>
  <div class="extra content">
    <a>
    <i class="fa fa-bed"></i>
      Price
                                        <?php echo $room_price; ?>
    </a>
  </div>
</div>


                            </div>

                            <?php
}
?>


                    </div>
                    <input type="submit" name="transfer" class="ui fluid teal button" value="Transfer Room" />
                    <hr/>
                    <label>To</label>
                    <table class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                            <thead>
                                                <tr role="row">

                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending">Room Name</th>
                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Room Description</th>
                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Room Price</th>
                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Available</th>
                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Quantity</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                        <?php
// Select room type eg. Ana Villa, Mike Hotel
$res = $mysqli->query('SELECT * FROM room_type WHERE isDeleted=0');
while ($rows = mysqli_fetch_assoc($res)) {
    $roomType = $rows['room_id'];
    $room_name = $rows['room_name'];
    $room_info = $rows['room_info'];
    $room_price = $rows['room_price'];
    // Get the Input dates provided by the customer at the front end eg. he clicked July 24 - 26
    $range = date_range($date_now, $departure_date, "+1 day", "Y-m-d");
    // Slice it to July 24 - 25
    $reserved_period = array_slice($range, 0, -1);

    // Here you are selecting 'RESERVED ROOMS!!' AND ONLY WITH A STATUS THAT HAS BEEN CONFIRMED AND BOOKED REFLECTED by the given Dates on the site
    $s = "SELECT * FROM reserved_rooms JOIN customer ON customer.client_reference_id = reserved_rooms.client_reference_id
        JOIN reservation ON reserved_rooms.client_reference_id = reservation.client_reference_id
        WHERE reserved_rooms.room_type_id = '$roomType' AND (reservation_status='Booked'  OR reservation_status='Checked In')";

    $r = $mysqli->query($s);
    $reserved_rooms = 0;
    $my_selected_rooms = [];
    while ($ro = mysqli_fetch_assoc($r)) {
        $arrival_date_db = $ro['arrival_date'];
        $departure_date_db = $ro['departure_date'];
        // -1 to the Departure date. July 26 becomes July 25... so it will not consider 26 as still Reserved date.
        $departure_date_db = date('Y-m-d', (strtotime('-1 day', strtotime($departure_date_db))));
        // Get the Database Arrival Date and Departure Date of Ana Villa Room type that has been Reserved
        $rangeDb = date_range($arrival_date_db, $departure_date_db, "+1 day", "Y-m-d");

        $roomNotAvailable = 'not existing';
        foreach ($reserved_period as $day) {
            // Check the reserved period above an Array of dates
            // eg. $reservation_period = [2018-07-25,2018-07-25] // These dates will be compared at the Database
            // rangeDb
            if (in_array($day, $rangeDb)) {
                $roomNotAvailable = 'existing';
            }
        }
        if ($roomNotAvailable == 'existing') {
            // array_push($reserved_rooms, 1);
            $reserved_rooms++;
        }
    }

    // $reserved_rooms = array_sum($reserved_rooms);

    // Available Rooms = Total Rooms - Reserved Rooms IN Check In Date
    $sqlAvailableRoom = "SELECT COUNT(*) as TotalRoom FROM rooms WHERE room_type_id='$roomType'";
    $resAvailableRoom = $mysqli->query($sqlAvailableRoom);
    $result = mysqli_fetch_assoc($resAvailableRoom);
    $result = $result['TotalRoom'] - $reserved_rooms;

    ?>
      <tr>
        <td><?php echo $room_name; ?></td>
        <td><?php echo $room_info; ?></td>
        <td>₱<?php echo number_format($room_price, 2); ?></td>
        <td>
        <?php
if ($result <= 0) {
        echo "0";
    } else {
        echo $result;
    }?>
        </td>
        <td>
          <?php

    if ($result <= 0) {
        echo "<a class='ui red right ribbon label'>Not Available</a>";
    } else {
        ?>
      <input type="radio" class="ui teal"  name='room_to_transfer' value="<?php echo $roomType; ?>" />
  <?php

    }

    ?>
                          </td>
      </tr>


<?php }?>


</tbody>
</table>


                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                &nbsp;
                                                            </div>
                                                        </div>
                                        </div>
                                        </div>

                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                </div>


<div class="card">
<div id="available-rooms">


</div>
</div>


                            </div>

      </div>
</div>
                        </div>
                    </div>
                    </form>

    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
<?php
include 'layout/footer.php';
?>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- Arrival Date -->



<script>
                        const today = new Date();
                        $('#rangestart').calendar({
                            type: 'date',
                            endCalendar: $('#rangeend'),
                            onChange: function (date, text, mode) {
                              $("#available-rooms").html('<img src="ajax/loading-spinner.gif" width="100px">')
                              var start = $("#start").val();
                              var end = $("#end").val();
                              var ref_id = $("#ref_id").val();
                              $.post( "ajax/getAvailableRooms.php", {start:start ,end: end, ref_id: ref_id},function(data) {
                              $( "#available-rooms").html(data);
                                });

                          },
                        });
                        $('#rangeend').calendar({
                            type: 'date',
                            startCalendar: $('#rangestart'),
                            onChange: function (date, text, mode) {
                              $("#available-rooms").html('<img src="ajax/loading-spinner.gif" width="100px">')
                              var start = $("#start").val();
                              var end = $("#end").val();
                              var ref_id = $("#ref_id").val();
                              var jqxhr =$.post( "ajax/getAvailableRooms.php", {start:start ,end: end,ref_id:ref_id},function(data) {
                              $("#available-rooms").html(data);
                            });
                          },
                          onHidden: function () {
                                $.post( "ajax/getAvailableRooms.php", {start:start ,end: end,ref_id:ref_id},function(data) {
                              $("#available-rooms").html(data);
                            });
                            },
                        });
                    </script>



<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>

<script>

                        // Validation Payment form
                        function validateForm() {
                            var room = document.forms["transferForm"]["room_to_transfer"].value;
                            if(room == 0) {
                              alert('No room Selected');
                              return false;
                            }

                        }
                    </script>


<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
     <!-- DataTables -->
     <script src="plugins/datatables/jquery.dataTables.js"></script>
      <script src="plugins/datatables/dataTables.bootstrap4.js"></script>

                             <script>
                        $(function() {
                            $('#example1').DataTable({
                                "paging": true,
                                "lengthChange": false,
                                "searching": true,
                                "ordering": true,
                                "info": true,
                                "autoWidth": false,
                                "pageLength": 3,
                                "stripeClasses": ['odd-row', 'even-row'],
                            });
                        });
                    </script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
</body>
</html>

