<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

//Load Composer's autoloader
require '../vendor/autoload.php';

$mail = new PHPMailer(true); // Passing `true` enables exceptions
try {
    //Server settings
    $mail->SMTPDebug = 2; // Enable verbose debug output
    $mail->isSMTP(); // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com'; // Specify main and backup SMTP servers
    $mail->SMTPAuth = true; // Enable SMTP authentication
    $mail->Username = 'test.villaalfredo@gmail.com'; // SMTP username
    $mail->Password = 'reservationsystem'; // SMTP password
    $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587; // TCP port to connect to
    $mail->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true,
        ),
    );
    //Recipients
    $mail->setFrom('test.villaalfredo@gmail.com', 'Villa Alfredo');
    $mail->addAddress('juliusdejon@gmail.com', 'Julius Dejon'); // Add a recipient
    // $mail->addAddress('ellen@example.com'); // Name is optional
    // $mail->addReplyTo('info@example.com', 'Information');
    // $mail->addCC('cc@example.com');
    // $mail->addBCC('bcc@example.com');

    //Attachments
    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    $htmlContent = '<body style="width:100% !important; margin:0 !important; padding:0 !important; -webkit-text-size-adjust:none; -ms-text-size-adjust:none; background-color:#FFFFFF;">
    <table cellpadding="0" cellspacing="0" border="0" id="backgroundTable" style="height:auto !important; margin:0; padding:0; width:100% !important; background-color:#FFFFFF; color:#222222;">
        <tr>
            <td>
             <div id="tablewrap" style="width:100% !important; max-width:600px !important; text-align:center !important; margin-top:0 !important; margin-right: auto !important; margin-bottom:0 !important; margin-left: auto !important;">
                  <table id="contenttable" width="600" align="center" cellpadding="0" cellspacing="0" border="0" style="background-color:#FFFFFF; text-align:center !important; margin-top:0 !important; margin-right: auto !important; margin-bottom:0 !important; margin-left: auto !important; border:none; width: 100% !important; max-width:600px !important;">
                <tr>
                    <td width="100%">
                        <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td width="100%" bgcolor="#ffffff" style="text-align:center;"><a href="#"><img src="https://villaalfredos.com/assets/images/valogo.png" alt="Main banner image and link" style="display:inline-block; max-width:100% !important; width:100% !important; height:auto !important;border-bottom-right-radius:8px;border-bottom-left-radius:8px;" border="0"></a>
                                </td>
                            </tr>
                       </table>
                       <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="25" width="100%">
                            <tr>
                                <td width="100%" bgcolor="#ffffff" style="text-align:left;">
                                    <p style="color:#222222; font-family:Arial, Helvetica, sans-serif; font-size:15px; line-height:19px; margin-top:0; margin-bottom:20px; padding:0; font-weight:normal;">
                                        Dear Jane Smith,
                                    </p>
                                    <p style="color:#222222; font-family:Arial, Helvetica, sans-serif; font-size:15px; line-height:19px; margin-top:0; margin-bottom:20px; padding:0; font-weight:normal;">
                                       Thank you for reserving a room with us! You have 3 days to Deposit to secure your reservation slot. After you deposit kindly upload your Receipt to this page, <a style="color:#2489B3; font-weight:bold; text-decoration:underline;" href="#">Villa Alfredo Site</a>!
                                    </p>
                                        <table border="0" cellspacing="0" cellpadding="0" width="100%" class="emailwrapto100pc">
                                          <tr>
                                            <td class="emailcolsplit" align="left" valign="top" width="58%">
                                                <p style="color:#222222; font-family:Arial, Helvetica, sans-serif; font-size:15px; line-height:19px; margin-top:0; margin-bottom:20px; padding:0; font-weight:normal;">
                                                    Swine pork salami, beef ribs doner pastrami fatback. Bacon ipsum dolor sit amet leberkas pork chop bacon doner venison hamburger pastrami bresaola short ribs filet mignon ham prosciutto salami tri-tip. Turducken pork pancetta meatloaf venison. Spare ribs tongue drumstick pastrami frankfurter brisket shoulder. Ground round capicola filet mignon chicken cow boudin venison bacon.                                            </p>
                                            </td>
                                            <td class="emailcolsplit" align="left" valign="top" width="42%" style="padding-left:17px;max-width:231px;">
                                                <a href="#"><img src="https://villaalfredos.com/assets/images/20-2000x1125-800x450.jpg" alt="Supporting image 1" style="display:block; width:100% !important; height:auto !important;border-radius:8px;" border="0"></a>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td class="emailcolsplit" align="left" valign="top" width="58%">
                                                <p style="color:#222222; font-family:Arial, Helvetica, sans-serif; font-size:15px; line-height:19px; margin-top:0; margin-bottom:20px; padding:0; font-weight:normal;">
                                                    Ham hock pork chop jerky, turducken kielbasa meatball sausage drumstick bacon fatback corned beef meatloaf pork belly rump. Boudin cow hamburger ball tip chuck rump meatball. Andouille tenderloin flank, strip steak pork loin biltong swine turducken rump pork belly ball tip jowl. Hamburger tail short loin jerky doner venison rump ham ground round pork chop turkey fatback prosciutto.
                                                </p>
                                            </td>
                                            <td class="emailcolsplit" align="left" valign="top" width="42%" style="padding-left:17px;max-width:231px;">
                                                <a href="#"><img src="https://villaalfredos.com/assets/images/shutterstock-2000x1334.jpg" alt="Supporting image 2" style="display:block; width:100% !important; height:auto !important;border-radius:8px;" border="0"></a>
                                            </td>
                                          </tr>
                                        </table>

                                </td>
                            </tr>
                       </table>
                       <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                              <td width="100%" bgcolor="#ffffff" style="text-align:center;"><a style="font-weight:bold; text-decoration:none;" href="#"><div style="display:block; max-width:100% !important; width:93% !important; height:auto !important;background-color:#2489B3;padding-top:15px;padding-right:15px;padding-bottom:15px;padding-left:15px;border-radius:8px;color:#ffffff;font-size:24px;font-family:Arial, Helvetica, sans-serif;">Call-To-Action Image or Text!</div></a></td>
                            </tr>
                       </table>
                       <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="25" width="100%">
                            <tr>
                                <td width="100%" bgcolor="#ffffff" style="text-align:left;">
                                    <p style="color:#222222; font-family:Arial, Helvetica, sans-serif; font-size:11px; line-height:14px; margin-top:0; margin-bottom:15px; padding:0; font-weight:normal;">
                                        Email not displaying correctly? <a style="color:#2489B3; font-weight:bold; text-decoration:underline;" href="#">View it in your browser.</a>
                                    </p>
                                    <p style="color:#222222; font-family:Arial, Helvetica, sans-serif; font-size:11px; line-height:14px; margin-top:0; margin-bottom:15px; padding:0; font-weight:normal;">
                                        Copyright 2013 Your Company. All Rights Reserved.<br>
                                        If you no longer wish to receive emails from us, you may <a style="color:#2489B3; font-weight:normal; text-decoration:underline;" href="#">unsubscribe</a>.
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            </div>
            </td>
        </tr>
    </table>
    </body>';
    $newContent = "    <html lang='en'>
    <head>
        <meta charset='UTF-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1.0'>
        <meta http-equiv='X-UA-Compatible' content='ie=edge'>
        <title>Document</title>
    </head>
    <body>
    Hi, Julius<br><br>
    The Reservation details you submitted is now pending.<br>
    To secure your reservation kindly pay 50% of the amount to our primary account below.<br>
    Here's your reservation details <a href='>www.facebook.com</a>
    </body>
    </html>";

    //Content
    $mail->isHTML(true); // Set email format to HTML
    $mail->Subject = 'Reservation Request Step 1';
    $mail->Body = $newContent;
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    echo 'Message has been sent';
} catch (Exception $e) {
    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
}
