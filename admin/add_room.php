<?php
include '../config/mysqli.php';

$room_id = $_POST['room_id'];
$room_number = $_POST['room_number'];

$sql = "SELECT room_number FROM rooms WHERE room_type_id='$room_id' AND room_number='$room_number'";
$res = $mysqli->query($sql);
$row = mysqli_fetch_assoc($res);
$room_number_db = $row['room_number'];

if ($room_number == '') {
    header('Location: view_room.php?room_id=' . $room_id . '&added=empty');
} else {
    if ($room_number == $room_number_db) {
        header('Location: view_room.php?room_id=' . $room_id . '&added=exists');
    } else {
        $sql = "INSERT INTO rooms (room_type_id,room_number) VALUES('$room_id','$room_number')";
        if ($mysqli->query($sql)) {
            header('Location: view_room.php?room_id=' . $room_id . '&added=true');

            echo "<script>
              window.location.href('view_rooms.php?<?php echo $room_id;?>')
              if ( window.history.replaceState ) {
                  window.history.replaceState( null, null, window.location.href('view_rooms.php?<?php echo $room_id;?>') );
              }
          </script>";
        }
    }
}
