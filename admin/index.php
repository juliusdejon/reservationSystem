<?php
session_start();
// Redirect if someone has Session
if (isset($_SESSION['id'])) {
    header('Location: dashboard.php');
}
?>
<!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Villa Alfredo's Admin| Log in</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <!-- Theme style -->
        <link rel="stylesheet" href="./dist/css/adminlte.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="./plugins/iCheck/square/blue.css">
        
        
        <script src="dist/js/plugins/jquery/jquery.min.js"></script>

    </head>

    <body class="hold-transition login-page">

        <div class="login-box">
            <div class="login-logo">
                <a href="./index2.html"><b>Villa Alfredo's </b>admin</a>
            </div>
            <!-- /.login-logo -->
            <div class="card">
                <div class="card-body login-card-body">
                    <p class="login-box-msg">Sign in to start your session</p>

                    <form action="ajax/login.php" method="post" id="login">
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" name="username" placeholder="Username">
                            <span class="fa fa-envelope form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" name="password" placeholder="Password">
                            <span class="fa fa-lock form-control-feedback"></span>
                        </div>
                        <div class="row">
                            <div class="col-8">
                            </div>
                            <!-- /.col -->
                            <div class="col-4">
                                <button type="submit" id="button" name="login" class="btn btn-primary btn-block btn-flat">Sign In</button>
                            </div>

                            <!-- /.col -->
                        </div>
                        <div class="form-group-has-feedback" id="result">

                        </div </form>
                </div>
                <!-- /.login-card-body -->


            </div>
        </div>
        <!-- /.login-box -->
        <!-- jQuery -->
        <script src="./plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="ajax/login.js"></script>
        <!-- Bootstrap 4 -->
        <script src="./plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

    </body>

    </html>
