
<?php
include '../config/mysqli.php';
include 'daytourFunctions.php';

@$success = $_GET['feedback'];
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>Villa Alfredo Admin</title>

        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
        <!-- Data table -->
        <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap4.css">

        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/adminlte.min.css">
        <!-- Semantic UI -->
        <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />
        <!-- Initialize Semantic UI and JQuery to load Calendar Datepicker-->
        <script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" src="../js/semantic.min.js"></script>

        <!-- Calendar -->
        <link type="text/css" rel="stylesheet" href="../css/calendar.min.css" />
        <script src="../js/calendar.min.js"></script>

        <!-- Icons -->
        <link rel="stylesheet" href="../css/icon.min.css">


    </head>

    <body class="hold-transition sidebar-mini">
        <div class="wrapper">

            <?php
include 'layout/navbar.php';
include 'layout/sidebar.php';
?>
                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <div class="content-header">
                        <div class="container-fluid">

                        </div>
                        <!-- /.container-fluid -->
                    </div>
                    <!-- /.content-header -->

                    <!-- Main content -->
                    <div class="content">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Walk In Day Tour/Overnight</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <form action="" name="customerForm" method="post" onsubmit="return validateForm();">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label>First Name</label>
                                                            <input type="text" name="first_name" class="form-control letters-only">
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label>Last Name</label>
                                                            <input type="text" name="last_name" class="form-control letters-only">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label>Contact Number</label>
                                                            <input type="text" name="contact" data-mask="0000-000-0000" class="form-control numbers-only">
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label>Email Address</label>
                                                            <input type="text" name="email" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="ui form">
                                                        <div class="row">
                                                        <div class="col-md-6">
                                                            Type
                                                            <select class="ui select fluid" name="type">
                                                                    <option value="Daytour">Daytour</option>
                                                                    <option value="Overnight">Overnight</option>
                                                                </select>
                                                        </div>
                                                        <div class="col-md-6">
                                                            Arrival Date
                                                            <?php $now = date('Y-m-d');?>
                                                            <input type="hidden" placeholder="Start" name="arrival_date" autocomplete="off" value="<?php echo $now; ?>">
                                                            <input disabled type="text" placeholder="Start" value="<?php echo $now; ?>">
                                                        </div>
</div>
                                                        <div class="row mt-4">
                                                            <div class="col-md-3">
                                                                No. of Adults
                                                                <input type="number" name="no_of_adults" class="numbers-only" min="0" max="100" />
                                                            </div>
                                                            <div class="col-md-3">
                                                                No. of Kids <input type="number" name="no_of_kids" class="number-only" min="0" max="100"/>
                                                            </div>

                                                            <div class="col-md-6">
                                                                No. of Senior/Pwd <input type="number" name="no_of_senior_pwd" class="numbers-only" min="0" max="100"/>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>

                                                <button type="submit" name="add_cottage" id="button" class="ui fluid teal button">Add Cottages</button>

                                        </form>
                                        </div>

                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <div id="result"></div>

                            </div>
                        </div>
                    </div>
                    <!-- /.content -->
                </div>
                <!-- /.content-wrapper -->

                <!-- Main Footer -->
                <?php
include 'layout/footer.php';
?>
                    <!-- ./wrapper -->

                    <!-- REQUIRED SCRIPTS -->

                    <script>
                        const today = new Date();
                        $('#rangestart').calendar({
                            minDate: today,
                            type: 'date',
                        });
                    </script>
                    <script type="text/javascript" src="../js/jquery.mask.js"></script>

                    <!-- jQuery -->
                    <script src="plugins/jquery/jquery.min.js"></script>
                    <!-- Ajax -->
                    <script src="ajax/walkIn.js"></script>
                    <!-- Bootstrap 4 -->
                    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
                    <!-- DataTables -->
                    <script src="plugins/datatables/jquery.dataTables.js"></script>
                    <script src="plugins/datatables/dataTables.bootstrap4.js"></script>
                    <!-- AdminLTE App -->
                    <script src="dist/js/adminlte.min.js"></script>

                    <script>
                        // Validation Payment form
                        function validateForm() {
                            var first_name = document.forms["customerForm"]["first_name"].value;
                            var last_name = document.forms["customerForm"]["last_name"].value;
                            var email = document.forms["customerForm"]["email"].value;
                            var contact = document.forms["customerForm"]["contact"].value;
                            var no_of_adults = document.forms["customerForm"]["no_of_adults"].value;
                            var no_of_kids = document.forms["customerForm"]["no_of_kids"].value;

                            if (first_name == "") {
                                alert("Enter First Name");
                                return false;
                            }
                            if (last_name == "") {
                                alert("Enter Last Name");
                                return false;
                            }

                            if (no_of_adults == "") {
                                alert("No. Of Adults is Required!");
                                return false;
                            }


                        }
                    </script>
                    <!-- jQuery -->
                    <script src="plugins/jquery/jquery.min.js "></script>
                    <!-- JQuery Validation source -->
                    <script type="text/javascript" src="plugins/jquery/jquery-key-restrictions.min.js"></script>
                    <!-- Actual Validation calling class -->
                    <script type="text/javascript">
                    $(document).ready(function () {
                        $(".letters-only").lettersOnly();
                        $(".numbers-only").numbersOnly();
                        $(".alpha-numeric-only").alphaNumericOnly();
                    });
                    </script>
                    <!-- Bootstrap 4 -->
                    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js "></script>
                    <!-- AdminLTE App -->
                    <script src="dist/js/adminlte.min.js "></script>

    </body>

    </html>