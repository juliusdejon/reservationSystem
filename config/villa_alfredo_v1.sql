-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 08, 2018 at 01:20 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `villa_alfredo_v1`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `user_id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  `mobile_number` varchar(20) NOT NULL,
  `user_role` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`user_id`, `first_name`, `last_name`, `username`, `password`, `email_address`, `mobile_number`, `user_role`) VALUES
(1, 'Villa', 'Alfredo', 'admin', 'admin', 'admin@gmail.com', '09321321', 'admin'),
(2, 'John', 'Doe', 'frontdesk', 'frontdesk', 'frontdesk@gmail.com', '095512712184', 'frontdesk');

-- --------------------------------------------------------

--
-- Table structure for table `cottages`
--

CREATE TABLE `cottages` (
  `cottage_type_id` int(11) NOT NULL,
  `cottage_number` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cottages`
--

INSERT INTO `cottages` (`cottage_type_id`, `cottage_number`) VALUES
(6, ' Concrete Cottage Big 1'),
(6, ' Concrete Cottage Big 2'),
(7, ' BC Open Cottage 1'),
(7, ' BC Open Cottage 2'),
(8, 'Wave Open Cottage 1'),
(8, 'Wave Open Cottage 2'),
(9, 'Dex Open Cottage 1'),
(9, 'Dex Open Cottage 2'),
(10, 'Dex Open Alphabet Cottage 1'),
(10, 'Dex Open Alphabet Cottage 2'),
(11, 'Function Hall I 1'),
(11, 'Function Hall I 2'),
(12, 'Function Hall II 1'),
(12, 'Function Hall II 2'),
(13, 'Function Hall 2 Elevated 1'),
(13, 'Function Hall 2 Elevated 2'),
(14, 'Gazeebo Cottage 1'),
(14, 'Gazeebo Cottage 2'),
(15, 'Kubo Open Cottage 1'),
(15, 'Kubo Open Cottage 2'),
(16, 'Lyn Open Cottage 1'),
(16, 'Lyn Open Cottage 2'),
(17, 'Alain Open Cottage 1'),
(17, 'Alain Open Cottage 2'),
(18, 'Pavilion Open 1'),
(18, 'Pavilion Open 2'),
(19, 'Tent Big 1'),
(19, 'Tent Big 2'),
(20, 'Tent Ella 1'),
(20, 'Tent Ella 2'),
(21, 'Tent Mike 1'),
(21, 'Tent Mike 2'),
(22, 'Concrete Umbrella 1'),
(22, 'Concrete Umbrella 2');

-- --------------------------------------------------------

--
-- Table structure for table `cottage_type`
--

CREATE TABLE `cottage_type` (
  `cottage_type_id` int(11) NOT NULL,
  `cottage_name` varchar(100) NOT NULL,
  `cottage_price` float NOT NULL,
  `cottage_info` text NOT NULL,
  `cottage_img` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cottage_type`
--

INSERT INTO `cottage_type` (`cottage_type_id`, `cottage_name`, `cottage_price`, `cottage_info`, `cottage_img`) VALUES
(6, 'Concrete Cottage Big', 1500, 'good for 20pax â€¢ tables & chairs \r\noptional videoke (PHP500)', 'uploaded-5b66561b829be1.42900154.jpg'),
(7, 'BC Open Cottage', 1000, 'Good for 20pax â€¢ tables & chairs \r\noptional videoke (PHP500)', 'tnbc-open-img1-600x337.jpg'),
(8, 'Wave Open Cottage', 1000, 'Good for 10pax â€¢ tables & chairs \r\noptional videoke (PHP500)', 'Wave Open Cottage.jpg'),
(9, 'Dex Open Cottage', 2500, 'Good for 30pax â€¢ tables & chairs \r\nfree videoke', 'Dex Open Cottage.jpg'),
(10, 'Dex Open Alphabet Cottage', 1000, 'Good for 20pax â€¢ tables & chairs \r\noptional videoke (PHP500)', 'uploaded-5b77bd5fc92320.20958326.jpg'),
(11, 'Function Hall 1', 25000, 'Air-conditioned room â€¢ good for 150pax \r\ntables & chairs â€¢ optional videoke (PHP500)', 'Function Hall 1.jpg'),
(12, 'Function Hall 2', 15000, 'Covered area â€¢ good for 250pax \r\ntables & chairs â€¢ optional videoke (PHP500)', 'Function Hall 2.jpg'),
(13, 'Function Hall 2 Elevated', 10000, 'Air-conditioned room â€¢ good for 80pax \r\ntables & chairs â€¢ optional videoke (PHP500)', 'uploaded-5b77bf693e9ed1.31345804.jpg'),
(14, 'Gazeebo Cottage', 2500, 'Good for 40pax â€¢ tables & chairs \r\noptional videoke (PHP500)', 'uploaded-5b77bd6ea53670.20559337.jpg'),
(15, 'Kubo Open Cottage', 700, 'Good for 10pax â€¢ tables & chairs \r\noptional videoke (PHP500)', 'Kubo Open Cottage.jpg'),
(16, 'Lyn Open Cottage', 1500, 'Good for 20pax â€¢ tables & chairs \r\noptional videoke (PHP500)', 'Lyn Open Cottage.jpg'),
(17, 'Alain Open Cottage', 700, 'Good for 10pax â€¢ tables & chairs \r\noptional videoke (PHP500)', 'Alain Open Cottage.jpg'),
(18, 'Pavilion Open', 8000, 'Good for 100pax â€¢ tables & chairs \r\noptional videoke (PHP500)', 'Pavilion Open.jpg'),
(19, 'Tent Big ', 1000, 'Good for 10pax â€¢ tables & chairs \r\nvideoke not allowed', 'Tent Big.jpg'),
(20, 'Tent Ella', 700, 'Good for 10pax â€¢ tables & chairs \r\nvideoke not allowed', 'Tent Ella.jpg'),
(21, 'Tent Mike', 700, 'Good for 10pax â€¢ tables & chairs \r\nvideoke not allowed', 'Tent Mike.jpg'),
(22, 'Concrete Umbrella', 700, 'Good for 10pax â€¢ tables & chairs \r\nvideoke not allowed', 'Concrete Umbrella.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `client_reference_id` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `contact_number` varchar(255) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `arrival_date` date NOT NULL,
  `departure_date` date NOT NULL,
  `additional_details` text NOT NULL,
  `no_of_adults` tinyint(4) NOT NULL,
  `no_of_kids` tinyint(4) NOT NULL,
  `no_of_senior_pwd` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer_corkages`
--

CREATE TABLE `customer_corkages` (
  `client_reference_id` varchar(30) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer_extras`
--

CREATE TABLE `customer_extras` (
  `client_reference_id` varchar(255) NOT NULL,
  `extras_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `extras`
--

CREATE TABLE `extras` (
  `extras_id` int(11) NOT NULL,
  `extras_name` varchar(255) NOT NULL,
  `extras_category` varchar(255) NOT NULL,
  `extras_price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `extras`
--

INSERT INTO `extras` (`extras_id`, `extras_name`, `extras_category`, `extras_price`) VALUES
(1, 'Unlimited Videoke', 'Rental', 500),
(2, 'Gas Stove', 'Rental', 400),
(3, 'Mattress', 'Rental', 300),
(4, 'San Mig, Pale Pilsen per case', 'Corkage', 100),
(5, 'Red Horse per case', 'Corkage', 200),
(6, 'Hard Liquer per Bottle', 'Corkage', 150),
(7, 'Room Key', 'Deposit', 100),
(8, 'Songbook', 'Deposit', 200),
(9, 'TV Remote Control ', 'Deposit', 300),
(10, 'Basketball Court rental', 'Deposit', 1000),
(11, 'Towel ', 'Rental', 300),
(12, 'Aviary', 'Rental', 30),
(13, 'Wall Climbing', 'Rental', 150),
(14, 'Lost Key', 'Damages', 100);

-- --------------------------------------------------------

--
-- Table structure for table `occupied_cottages`
--

CREATE TABLE `occupied_cottages` (
  `client_reference_id` varchar(255) NOT NULL,
  `cottage_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE `reservation` (
  `client_reference_id` varchar(255) NOT NULL,
  `receipt` text NOT NULL,
  `receipt_2` text NOT NULL,
  `amount_deposited` int(11) NOT NULL,
  `txnid` varchar(20) CHARACTER SET utf8 NOT NULL,
  `reservation_type` varchar(50) NOT NULL,
  `reservation_status` varchar(255) NOT NULL,
  `isRebooked` tinyint(4) NOT NULL,
  `assigned_to` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `expires_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reserved_rooms`
--

CREATE TABLE `reserved_rooms` (
  `client_reference_id` varchar(255) NOT NULL,
  `room_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `room_type_id` int(11) NOT NULL,
  `room_number` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`room_type_id`, `room_number`) VALUES
(32, 'Mike Hotel Room 2'),
(32, 'Mike Hotel Room 3'),
(33, 'Raf Villa 1'),
(33, 'Raf Villa 2'),
(33, 'Raf Villa 3'),
(32, 'Mike Hotel Room 1'),
(34, 'Family Villa 1'),
(39, 'Alain Hotel Room 1'),
(40, 'Ariane Hotel Room 1'),
(40, 'Ariane Hotel Room 2'),
(40, 'Ariane Hotel Room 3'),
(41, 'Gabby Hotel Room 1'),
(41, 'Gabby Hotel Room 2'),
(41, 'Gabby Hotel Room 3'),
(42, 'Lyn Villa 1'),
(42, 'Lyn Villa 2'),
(42, 'Lyn Villa 3'),
(42, 'Lyn Villa 4'),
(42, 'Lyn Villa 5'),
(42, 'Lyn Villa 6'),
(44, 'Nic Villa 1'),
(44, 'Nic Villa 2'),
(44, 'Nic Villa 3'),
(45, 'Nic Villa 4'),
(45, 'Nic Villa 5'),
(46, 'Cottage Villa 1'),
(47, 'Cottage Villa 2'),
(48, 'Cottage Villa 3'),
(49, 'Cabana 1'),
(49, 'Cabana 2'),
(49, 'Cabana 3'),
(49, 'Cabana 4'),
(49, 'Cabana 5'),
(49, 'Cabana 6'),
(49, 'Cabana 7'),
(49, 'Cabana 8'),
(49, 'Cabana 9'),
(49, 'Cabana 10'),
(31, 'Ana Villa 1'),
(31, 'Ana Villa 2'),
(31, 'Ana Villa 3'),
(31, 'Ana Villa 4'),
(31, 'Ana Villa 5'),
(31, 'Ana Villa 6'),
(31, 'Ana Villa 7'),
(31, 'Ana Villa 8'),
(31, 'Ana Villa 9'),
(31, 'Ana Villa 10'),
(31, 'Ana Villa 11'),
(31, 'Ana Villa 12'),
(31, 'Ana Villa 14'),
(32, 'Mike Hotel Room 4'),
(32, 'Mike Hotel Room  5'),
(32, 'Mike Hotel Room  6'),
(32, 'Mike Hotel Room 7'),
(32, 'Mike Hotel Room  8'),
(32, 'Mike Hotel Room  9'),
(34, 'Family Villa 2'),
(34, 'Family Villa  3'),
(34, 'Family Villa 4'),
(34, 'Family Villa  5'),
(34, 'Family Villa  6'),
(34, 'Family Villa  7'),
(34, 'Family Villa  8'),
(34, 'Family Villa 9'),
(34, 'Family Villa 10'),
(34, 'Family Villa 12'),
(34, 'Family Villa 11'),
(35, 'Bella Room 2'),
(35, 'Bella Room 3'),
(35, 'Bella Room 4'),
(35, 'Bella Room 5'),
(35, 'Bella Room 6'),
(35, 'Bella Room 7'),
(35, 'Bella Room 8'),
(35, 'Bella Room 9'),
(35, 'Bella Room 10'),
(35, 'Bella Room 11'),
(35, 'Bella Room 12'),
(35, 'Bella Room 13'),
(35, 'Bella Room 14'),
(36, 'Bella Room Two 1'),
(36, 'Bella Room Two 2'),
(36, 'Bella Room Two 3'),
(36, 'Bella Room Two 4'),
(36, 'Bella Room Two 5'),
(36, 'Bella Room Two 6'),
(36, 'Bella Room Two 7'),
(36, 'Bella Room Two 8'),
(36, 'Bella Room Two 9'),
(36, 'Bella Room Two 10'),
(36, 'Bella Room Two 11'),
(37, 'Ella Hotel Room 1'),
(37, 'Ella Hotel Room 2'),
(37, 'Ella Hotel Room 3'),
(37, 'Ella Hotel Room 4'),
(37, 'Ella Hotel Room 5'),
(37, 'Ella Hotel Room 6'),
(38, 'Trina Hotel Room 1'),
(38, 'Trina Hotel Room 2'),
(38, 'Trina Hotel Room 3'),
(38, 'Trina Hotel Room 4'),
(38, 'Trina Hotel Room 5'),
(38, 'Trina Hotel Room 6'),
(39, 'Alain Hotel Room 2'),
(39, 'Alain Hotel Room 3'),
(39, 'Alain Hotel Room 4'),
(39, 'Alain Hotel Room 5'),
(39, 'Alain Hotel Room 6'),
(40, 'Ariane Hotel Room 4'),
(40, 'Ariane Hotel Room 5'),
(40, 'Ariane Hotel Room 6'),
(40, 'Ariane Hotel Room 7'),
(40, 'Ariane Hotel Room 8'),
(40, 'Ariane Hotel Room 9'),
(40, 'Ariane Hotel Room 10'),
(41, 'Gabby Hotel Room 4'),
(41, 'Gabby Hotel Room 5'),
(41, 'Gabby Hotel Room 6'),
(41, 'Gabby Hotel Room 7'),
(41, 'Gabby Hotel Room 8'),
(43, 'Lyn Hotel 7'),
(43, 'Lyn Hotel 8'),
(43, 'Lyn Hotel 9'),
(43, 'Lyn Hotel 10'),
(43, 'Lyn Hotel 11'),
(43, 'Lyn Hotel 12'),
(44, 'Nic Villa 4'),
(44, 'Nic Villa 5'),
(46, 'Cottage Villa 2'),
(46, 'Cottage Villa 3'),
(46, 'Cottage Villa 4'),
(46, 'Cottage Villa 5'),
(46, 'Cottage Villa 6'),
(49, 'Cabana 11'),
(49, 'Cabana 12'),
(49, 'Cabana 14'),
(49, 'Cabana 15'),
(49, 'Cabana 16'),
(49, 'Cabana 17'),
(49, 'Cabana 18'),
(49, 'Cabana 19');

-- --------------------------------------------------------

--
-- Table structure for table `room_type`
--

CREATE TABLE `room_type` (
  `room_id` int(11) NOT NULL,
  `room_name` varchar(100) NOT NULL,
  `room_type` varchar(100) NOT NULL,
  `room_price` float NOT NULL,
  `room_info` text NOT NULL,
  `room_adults_capacity` tinyint(4) NOT NULL,
  `room_child_capacity` tinyint(4) NOT NULL,
  `room_hourly_rate` float NOT NULL,
  `room_img` text NOT NULL,
  `isDeleted` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room_type`
--

INSERT INTO `room_type` (`room_id`, `room_name`, `room_type`, `room_price`, `room_info`, `room_adults_capacity`, `room_child_capacity`, `room_hourly_rate`, `room_img`, `isDeleted`) VALUES
(31, 'Ana Villa', 'Villa', 2000, 'Air-conditioned room | 1 Double size bed ', 2, 2, 200, 'uploaded-5b60676635a560.71332798.jpg', 0),
(32, 'Mike Hotel Room', 'Hotel', 2500, ' Air-conditioned room | 2 Double size bed ', 4, 4, 200, 'mike-hotel.jpg', 0),
(33, 'Raf Villa', 'Villa', 8000, 'Air-conditioned room | 2 Queen size bed ', 4, 4, 600, 'uploaded-5b52d8dbc589f2.83982681.jpg', 0),
(34, 'Family Villa', 'Villa', 8000, 'Air conditioned | 1 room with 6 double size beds ', 6, 6, 500, 'uploaded-5b52d9f0886eb9.25283629.jpg', 0),
(35, 'Bella Room 1', 'Villa', 8000, 'Air-conditioned room | 2 Queen size bed \r\n ', 4, 4, 800, 'uploaded-5b52da63781d23.32860499.jpg', 0),
(36, 'Bella Room 2', 'Villa', 10000, 'Air-conditioned room | Two double size bed \r\n\r\n', 4, 4, 800, 'uploaded-5b52dac76660b3.62858782.jpg', 0),
(37, 'Ella Hotel Room', 'Hotel', 2500, 'Air-conditioned room | 2 Double size bed', 4, 4, 200, 'uploaded-5b52db3a6919d7.66193177.jpg', 0),
(38, 'Trina Hotel Room', 'Hotel', 2500, 'Air-conditioned room  | 2 Double size bed ', 4, 4, 200, 'uploaded-5b52db938ba870.33266590.jpg', 0),
(39, 'Alain Hotel Room', 'Hotel', 2500, 'Air-conditioned room | 2 double size bed ', 4, 4, 200, 'uploaded-5b52dbcb79f857.22167122.jpg', 0),
(40, 'Ariane Hotel Room', 'Hotel', 2500, 'Air-conditioned room  | 2 double size bed ', 4, 4, 200, 'uploaded-5b52dbfdda9f97.04459746.jpg', 0),
(41, 'Gabby Hotel Room', 'Hotel', 2500, 'Air-conditioned room | 2 double size bed  ', 4, 4, 200, 'uploaded-5b52dc343bee41.49834986.jpg', 0),
(42, 'Lyn Villa 1-6', 'Villa', 5000, 'Air conditioned 2 bed rooms |  2 Queen size bed ', 4, 4, 500, 'uploaded-5b52dc6e082084.07395425.jpg', 0),
(43, 'Lyn Hotel 7-12', 'Hotel', 3500, 'Air conditioned room | 2 Double size bed ', 4, 4, 300, 'uploaded-5b52dc91da8ae2.43806720.jpg', 0),
(44, 'Nic Villa 1, 2 & 3', 'Villa', 4500, 'Air-conditioned room | 4 Queen size bed', 8, 8, 300, 'uploaded-5b52dcbd932585.99230948.jpg', 0),
(45, 'Nic Villa 4 & 5', 'Villa', 5000, 'Air-conditioned room | 4 queen size bed', 8, 8, 300, 'uploaded-5b52dcef5dad50.99642935.jpg', 0),
(46, 'Cottage Villa 1', 'Villa', 4500, 'Air-conditioned room | 2 King size bed', 4, 4, 300, 'uploaded-5b52dd3eed7a34.73403556.jpg', 0),
(47, 'Cottage Villa 2', 'Villa', 4500, 'Air-conditioned room | 2 Queen size bed', 4, 4, 300, 'uploaded-5b52dd6f8e40b0.60258908.jpg', 0),
(48, 'Cottage Villa 3', 'Villa', 4500, 'Air-conditioned room | 2 Queen size bed ', 4, 4, 300, 'uploaded-5b52dd9aecb606.87977185.jpg', 0),
(49, 'Cabana', 'Hotel', 4000, 'Air-conditioned room | 2 Double size bed', 4, 4, 300, 'uploaded-5b52ddcdad63c6.53235870.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `transaction_id` int(11) NOT NULL,
  `client_reference_id` varchar(255) NOT NULL,
  `downpayment` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `transaction_type` varchar(255) NOT NULL,
  `transaction_status` varchar(255) NOT NULL,
  `check_in_date` datetime NOT NULL,
  `check_out_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `cottages`
--
ALTER TABLE `cottages`
  ADD KEY `room_type_id` (`cottage_type_id`);

--
-- Indexes for table `cottage_type`
--
ALTER TABLE `cottage_type`
  ADD PRIMARY KEY (`cottage_type_id`);

--
-- Indexes for table `extras`
--
ALTER TABLE `extras`
  ADD PRIMARY KEY (`extras_id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD KEY `room_type_id` (`room_type_id`);

--
-- Indexes for table `room_type`
--
ALTER TABLE `room_type`
  ADD PRIMARY KEY (`room_id`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`transaction_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cottage_type`
--
ALTER TABLE `cottage_type`
  MODIFY `cottage_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `extras`
--
ALTER TABLE `extras`
  MODIFY `extras_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `room_type`
--
ALTER TABLE `room_type`
  MODIFY `room_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
