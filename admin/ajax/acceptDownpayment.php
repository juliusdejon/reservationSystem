<?php

include '../../config/mysqli.php';
use PHPMailer\PHPMailer\PHPMailer;

require '../../src/Exception.php';
require '../../src/PHPMailer.php';
require '../../src/SMTP.php';

$client_reference_id = $_POST['client_reference_id'];
$amount = $_POST['amount'];

if (empty($_POST['amount'])) {
    echo '<div class="alert alert-info alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h5><i class="icon fa fa-info"></i> No amount given</h5>
</div>';
    return;
}
if (isset($_POST['amount'])) {
    $check = "SELECT * FROM transaction WHERE client_reference_id ='$client_reference_id'";
    $result = $mysqli->query($check);
    $result = mysqli_num_rows($result);
    if ($result == 1) {
        $update = "UPDATE transaction SET downpayment='$amount' WHERE client_reference_id ='$client_reference_id'";
        $updateRes = $mysqli->query($update);
        if ($updateRes) {
            $sql = "UPDATE reservation SET reservation_status='Booked' WHERE client_reference_id='$client_reference_id'";
            $res = $mysqli->query($sql);
            echo " <div class='callout callout-success'>
                  <h5>Downpayment amount has been updated to</h5>
                  <p>₱ $amount</p>
                </div>";
        }
    } else {
        $sql = "INSERT INTO transaction (client_reference_id,downpayment) VALUES ('$client_reference_id', '$amount') ";
        $res = $mysqli->query($sql);
        if ($res) {
            $sql = "UPDATE reservation SET reservation_status='Booked' WHERE client_reference_id='$client_reference_id'";
            $res = $mysqli->query($sql);
            if ($res) {
                echo " <div class='callout callout-success'>
                <h5>Successfully Booked</h5>
              </div>";

                $sql = "SELECT * FROM customer WHERE client_reference_id='$client_reference_id'";
                $res = $mysqli->query($sql);
                while ($rows = mysqli_fetch_assoc($res)) {
                    $email_address = $rows['email_address'];
                    $first_name = $rows['first_name'];
                    $last_name = $rows['last_name'];
                }

                if ($mailing == true) {
                    $encrypted = base64_encode($client_reference_id);
                    if ($deployedMailing == true) {

                        $from = "villa_alfredo_email@villaalfredo.x10host.com";
                        $to = "$email_address, $first_name.' '.$last_name";

                        $baseurl = $_SERVER['SERVER_NAME'];
                        $mailbody = "
                    From Villa Alfredo Resort <br />
                    Your Payment of " . $amount . " is Approved!  <br />
                    See you on your setted dates <br/>
                    Check your reservation details here <a href='" . $baseurl . "/reservation/message.php?res_id=$encrypted'>" . $baseurl . "/reservation/message.php?res_id=$encrypted'</a>
                    <br/>
                    <h3>House Rules</h3>
                    <ol>
                    <li>  No hard liquor </li>
                    <li> No eating near the pool. </li>
                    <li> No firearms allowed. </li>
                    <li> No littering. </li>
                    <li> No picking of fruits and flowers. </li>
                    <li> Wearing sando, t-shirt & pants will not be allowed to swim. </li>
                    <li> Videoke & swimming are allowed until 12:00 o’clock midnight only. </li>
                    <li> Loud music is not allowed between 12 o’clock midnight up to 8 o’clock in the morning. </li>
                    <li> Management provides 24 hours security, we are not responsible for item lost inside the car, please lock your vehicle and secure your entire valuable. </li>
                    <li> Please pay particular attention to your entire valuable and secure them at all times. 11. No running, diving, shoving or pushing within the pool area. </li>
                    <li> Right Conduct. We reserve the right to evict/eject any guest or group of guest should we find them engaging in fights, or generally disrupting the peace at  the resort </li>
                    </ol>
                    <i>Not allowed: Radio, Component, Speaker, DVD Player, Magic Sing, Electric Stove, Gas Stove, Butane Stove. </i>
                    ";
                        $subject = "Reservation Complete";

                        $headers = "From: $from" . "\r\n";
                        $headers .= "Reply-To: $from" . "\r\n";
                        $headers .= "MIME-Version: 1.0\r\n";
                        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
                        $resp = mail($to, $subject, $mailbody, $headers);
                    } else {
                        $mail = new PHPMailer(true); // Passing `true` enables exceptions
                        //Server settings
                        $mail->SMTPDebug = 0; // Enable verbose debug output
                        $mail->isSMTP(); // Set mailer to use SMTP
                        $mail->Host = 'smtp.gmail.com'; // Specify main and backup SMTP servers
                        $mail->SMTPAuth = true; // Enable SMTP authentication
                        $mail->Username = 'test.villaalfredo@gmail.com'; // SMTP username
                        $mail->Password = 'reservationsystem'; // SMTP password
                        $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
                        $mail->Port = 587; // TCP port to connect to
                        $mail->SMTPOptions = array(
                            'ssl' => array(
                                'verify_peer' => false,
                                'verify_peer_name' => false,
                                'allow_self_signed' => true,
                            ),
                        );
                        //Recipients
                        $mail->setFrom('test.villaalfredo@gmail.com', 'Villa Alfredo');
                        $mail->addAddress($email_address, $first_name . ' ' . $last_name); // Add a recipient
                        $baseurl = $_SERVER['SERVER_NAME'];
                        $mailbody = "
                    From Villa Alfredo Resort <br />
                    Your Payment of " . $amount . " is Approved!  <br />
                    See you on your setted dates <br/>
                    Check your reservation details here <a href='" . $baseurl . "/reservation/message.php?res_id=$encrypted'>" . $baseurl . "/reservation/message.php?res_id=$encrypted'</a>
                    ";

                        //Content
                        $mail->isHTML(true); // Set email format to HTML
                        $mail->Subject = 'Reservation Complete';
                        $mail->Body = $mailbody;
                        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
                        $mail->send();
                    }
                }
            }
        }
    }
}
