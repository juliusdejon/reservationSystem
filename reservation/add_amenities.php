<?php
// Requires functions.php
include 'functions.php';
// Redirect if Dates are Not Set

if ((empty($_SESSION['arrivalDate'])) || (empty($_SESSION['departureDate']))) {
    header('Location: index.php?dates=false');
}
$arrival_date = date_create_from_format('F j, Y', $_SESSION['arrivalDate']);
$arrival_date = $arrival_date->format('Y-m-d');

$departure_date = date_create_from_format('F j, Y', $_SESSION['departureDate']);
$departure_date = $departure_date->format('Y-m-d');
$departure = strtotime($departure_date);
$arrival = strtotime($arrival_date);
$datediff = $departure - $arrival;

$noofNights = round($datediff / (60 * 60 * 24));
// Get the rang of dates
function date_range($first, $last, $step, $output_format)
{
    $dates = array();
    $current = strtotime($first);
    $last = strtotime($last);
    while ($current <= $last) {
        $dates[] = date($output_format, $current);
        $current = strtotime($step, $current);
    }
    return $dates;
}

?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

        <title>Villa Alfredo's Reservation System</title>

        <!-- Google font -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7CVarela+Round" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

        <!-- Bootstrap -->
        <link type="text/css" rel="stylesheet" href="../css/bootstrap.min.css" />

        <!-- Semantic -->
        <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />
        <!-- Initialize Semantic UI and JQuery to load Calendar Datepicker-->
        <script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" src="../js/semantic.min.js"></script>

        <!-- Owl Carousel -->
        <link type="text/css" rel="stylesheet" href="../css/owl.carousel.css" />
        <link type="text/css" rel="stylesheet" href="../css/owl.theme.default.css" />

        <!-- Magnific Popup -->
        <link type="text/css" rel="stylesheet" href="../css/magnific-popup.css" />

        <!-- Font Awesome Icon -->
        <link rel="stylesheet" href="../css/font-awesome.min.css">

        <!-- Icons -->
        <link rel="stylesheet" href="../css/icon.min.css">

        <!-- Custom stlylesheet -->
        <link type="text/css" rel="stylesheet" href="../css/style.css" />


    </head>

    <body>
        <!-- Header -->
        <header>
            <!-- Nav -->
            <nav id="nav" class="navbar">
                <div class="container">

                    <div class="navbar-header">
                        <!-- Logo -->
                        <div class="navbar-brand">
                            <a href="index.html">
                                <img class="logo" src="../img/valogo-alt.png" alt="logo">
                                <img class="logo-alt" src="../img/valogo-alt.png" alt="logo">
                            </a>
                        </div>
                        <!-- /Logo -->

                        <!-- Collapse nav button -->
                        <div class="nav-collapse">
                            <span></span>
                        </div>
                        <!-- /Collapse nav button -->
                    </div>

                    <!--  Main navigation  -->
                    <ul class="main-nav nav navbar-nav navbar-right">
                        <li><a href="../index.php">Home</a></li>
                        <li><a href="../index.php#accomodation">Accomodation</a></li>
                        <li><a href="#day-tour-cottages">Cottages</a></li>
                        <li><a href="#contact">Contact</a></li>
                        <li><a href="./index.php"><button class="secondary-btn">Book Now</button></a></li>
                    </ul>
                    <!-- /Main navigation -->

                </div>
            </nav>
            <!-- /Nav -->
        </header>
        <section class="xs-padding">
            <div class="ui segment container">
                <div class="row">
                    <div class="col-md-9">
                        <div class="ui stackable mini steps">
                            <div class="completed step">
                                <i class="calendar alternate icon"></i>
                                <div class="content">
                                    <div class="title">Select Dates</div>
                                    <div class="description">Choose Date of Stay</div>
                                </div>
                            </div>
                            <div class="completed step">
                                <i class="bed icon"></i>
                                <div class="content">
                                    <div class="title">Select Room</div>
                                    <div class="description">Choose Rooms</div>
                                </div>
                            </div>
                            <div class="active step">
                                <i class="tag icon"></i>
                                <div class="content">
                                    <div class="title">Amenities</div>
                                    <div class="description">Extras & Amenities</div>
                                </div>
                            </div>
                            <div class="disabled step">
                                <i class="credit card icon"></i>
                                <div class="content">
                                    <div class="title">Payment</div>
                                    <div class="description">Verify reservation details</div>
                                </div>
                            </div>
                        </div>
                        <?php
$rooms = $_SESSION['rooms'];
$rooms = array_chunk($rooms, 2);

?>
                        <form action="" method="post">
                            <div class="ui relaxed divided items">
                                <?php
for ($i = 0; $i < count($rooms); $i++) {
    $sql = "SELECT * FROM room_type WHERE room_id=" . $rooms[$i][0];
    $res = $mysqli->query($sql);
    while ($rows = mysqli_fetch_assoc($res)) {
        $room_name = $rows['room_name'];
        $room_price = $rows['room_price'];
        $room_img = $rows['room_img'];
        $room_capacity = $rows['room_capacity'];    
  
        $room_info = $rows['room_info'];
        ?>
                                <div class="item">
                                    <div class="ui medium image">
                                        <img src="../img/<?php echo $room_img; ?>">
                                    </div>
                                    <div class="content">
                                        <a class="header">

                                            <?php echo $rows['room_name']; ?>
                                        </a>
                                        <div class="meta">

                                        </div>
                                        <div class="description">
                                            <?php echo $rows['room_info']; ?>
                                        </div>
                                        <table class="ui very basic collapsing celled table">
                                            <thead>
                                                <tr>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <h4 class="ui image header">
                                                            <i class="fa fa-user"></i>
                                                            <div class="content">
                                                                Capacity
                                                                <div class="sub header">
                                                                </div>
                                                            </div>
                                                        </h4>
                                                    </td>
                                                    <td>
                                                        <a class="ui teal circular label">
                                                            <?php echo $room_capacity; ?>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <div class="extra">

                                            <div class="ui input right floated">
                                                <button class="ui teal button" disabled >
    <?php echo $rooms[$i][1];?>
    </button>
    <?php

     
        ?>
</select>
                                            </div>

                                            <div class="ui tiny blue label right floated right pointing">

                                                Quantity
                                            </div>
                                            <div class="ui-label">₱
                                                <?php echo number_format($rows['room_price'], 2); ?>/night</div>
                                        </div>

                                    </div>
                                </div>
                       <?php }
}
?>


<h3>Extras eg.(Videoke, Gas Stove, Extra Mattress)</h3>

<form action="" method="post">
  <input type="hidden" name="client_reference_id" value="<?php echo $client_ref_id; ?>">
<div id="list">

  <div class="row" id="item">
    <div class="form-group col-md-6" >
        <label for="inputEmail3" class="control-label">Item</label>

        <select id="select" class="form-control" name="item[]">
        <option>---</option>
        <?php
$sql = "SELECT * FROM extras WHERE extras_category='Rental'";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
$extras_name = $rows['extras_name'];
$extras_id = $rows['extras_id'];
$extras_price = $rows['extras_price'];

echo "<option value='$extras_id'>$extras_name - ₱$extras_price</option>";
}
?>
        </select>


    </div>
<div class="form-group col-md-6">
          <label for="inputEmail3" class="col-sm-4 control-label">Qty</label>

<input type="number" min="1" class="form-control col-sm-4" name="quantity[]" value="1">
</div>
</div>

</div>


<div>

<button class="ui button teal tiny" type="button" id="add">More Item <i class="fa fa-plus"></i></button>
</div>



                            </div>

                    </div>
                    <div class="col-md-3">
                        <span class="reservation">Your Reservation</span>
                        <div class="reservation-content">
                            <div class="ui clearing divider"></div>
                            <div class="reservation-title">Room Selections: </div>
                            <div id="room-selected"></div>
                            <div class="reservation-title">Arrival Date:</div>
                            <?php echo $_SESSION['arrivalDate']; ?>
                            <div class="ui hidden divider"></div>
                            <div class="reservation-title">Departure Date</div>
                            <?php echo $_SESSION['departureDate']; ?>
                            <div class="ui hidden divider"></div>
                            <div class="reservation-title">Room Nights</div>
                            <?php echo $noofNights; ?>
                            <div class="ui clearing divider"></div>
                            <div class="reservation-title">Room Total</div>
                            <div class="ui clearing divider"></div>
                            <?php

?>
                        </div>
                        <input type="submit" name="addAmenities" class="fluid huge ui teal button" value="Proceed" />
                        </form>
                    </div>
        </section>
        </div>
        <script>
                                                                $(document).ready(function() {
                                                                    var a = document.getElementById("item");
                                                                    $("#add").click(function() {
                                                                        $(a).clone().appendTo('#list');
                                                                    });
                                                                });
                                                            </script>

        <!-- Back to top -->
        <div id="back-to-top"></div>
        <!-- /Back to top -->

        <!-- Preloader -->
        <div id="preloader">
            <div class="preloader">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>

        <!-- jQuery Plugins -->
        <script type="text/javascript" src="../js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../js/main.js"></script>

    </body>

    </html