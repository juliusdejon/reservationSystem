<?php
include '../config/mysqli.php';
$cottage_type_id = $_GET['cottage_id'];
@$deleted = $_GET['deleted'];
@$added = $_GET['added'];
$sql = "SELECT * FROM cottage_type WHERE cottage_type_id = '$cottage_type_id'";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $cottage_type_id;
    $cottage_name = $rows['cottage_name'];
    $cottage_img = $rows['cottage_img'];
    $cottage_info = $rows['cottage_info'];
    $cottage_price = $rows['cottage_price'];
}
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>Villa Alfredo Admin</title>

        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/adminlte.min.css">
        <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap4.css">
        <!-- Semantic UI -->
        <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />
        <!--Data table-->
        <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap4.css">
    </head>

    <body class="hold-transition sidebar-mini">
        <div class="wrapper">

            <?php
include 'layout/navbar.php';
include 'layout/sidebar.php';
?>
                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <div class="content-header">
                        <div class="container-fluid">
                            <div class="row mb-2">
                                <div class="col-sm-6">
                                    <h3 class="m-0 text-dark">
                                        <?php if ($deleted == 'true') {
    echo '<div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        Successfully Deleted Cottage!
                                        </div>';
} else {

}?>
                                    <?php if ($deleted == 'false') {
    echo '<div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        Unable to delete. Checkout first the customer staying at the Cottage.
                                        </div>';
} else {

}?>
                      <?php if ($added == 'true') {
    echo '<div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        Successfully Added Cottage!
                                        </div>';
} else if ($added == 'empty') {
    echo '<div class="alert alert-info alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    Please Enter a Cottage number
    </div>';
} else if ($added == 'exists') {
    echo '<div class="alert alert-info alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    Cottage Number Already Exists
    </div>';
} else {

}
?>
                                    <a href="manage_cottages.php" class="btn btn-sm btn-primary">Back </a>
                                       <?php echo $cottage_name; ?>
                                    </h3>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->
                    </div>
                    <!-- /.content-header -->
                    <script type="text/javascript">
                        function readURL(input) {
                            if (input.files && input.files[0]) {
                                var reader = new FileReader();

                                reader.onload = function(e) {
                                    $('#blah').attr('src', e.target.result);
                                }

                                reader.readAsDataURL(input.files[0]);
                            }
                        }
                    </script>
                    <!-- Main content -->
                    <div class="content">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card">
                                <div id="loadedCottageImage">
                                    <div class="card-header no-border" style="background: url(../img/<?php echo $cottage_img; ?>);
                                        height: 200px;
                                        background-repeat: no-repeat;
                                        width: 100%;
                                        background-size: cover;
                                        background-position: center center;">
                                </div>

                                        <div class="card-tools">

                                        </div>
                                    </div>
                                    <div class="card-body p-0">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card card-teal">
                                <a data-toggle="collapse" href="#edit" role="button" aria-expanded="false" aria-controls="collapseExample">
                                    <div class="card-header ui fluid button">
                                     <h3 class="card-title">Edit Cottage Information <i class="fa fa-arrow-down"></i></h3>
                                    </div>
                                    </a>
                                    <form action="ajax/editCottage.php" id="editCottageForm" role="form" method="post" enctype="multipart/form-data">
                                        <div class="card-body collapse" id="edit">
                                <div id="result"></div>
                                <input type="hidden" name="cottage_type_id" value="<?php echo $cottage_type_id ?>"/>
                                            <div class="form-group">
                                                <div class="div-image">
                                                    <?php
$image = '<img width="100" height="100" src="../img/' . $cottage_img . '" alt="Default Profile Pic" id="blah">';
echo $image;
?>
                                                </div>
                                                <label for="exampleInputFile">Image</label>
                                                <div class="input-group">
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" name="file" onchange="readURL(this);">
                                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                                    </div>
                                                    <div class="input-group-append">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Cottage Name</label>
                                                    <input type="text" name="cottage_name" class="form-control" id="exampleInputEmail1" placeholder="eg. Family Villa" value="<?php echo $cottage_name; ?>">
                                                </div>
                                                </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Cottage Info</label>
                                                    <textarea class="form-control" rows="1" placeholder="Enter ..." name="cottage_info"><?php echo $cottage_info; ?></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Price</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">₱</span>
                                                        </div>
                                                        <input type="number" name="cottage_price" value="<?php echo $cottage_price; ?>" class="form-control">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">.00</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="submit" id="button" class="ui button teal fluid" name="submit">Update</button>

                                        </div>
                                        <!-- /.card-body -->

                                    </form>

                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                    <div class="col-md-6">
                                <div class="card card-teal">
                                    <div class="card-header">
                                        <h3 class="card-title">Add Cottage</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <!-- form start -->
                                    <form role="form" action="add_cottage.php" method="post">
                                        <div class="card-body">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Cottage Number</label>
                                                    <input type="text" name="cottage_number" class="form-control" id="exampleInputEmail1" placeholder="101">
                                                    <input type="hidden" name="cottage_type_id" value="<?php echo $cottage_type_id; ?>" />
                                                </div>

                                            </div>
                                        </div>
                                        <!-- /.card-body -->

                                        <div class="card-footer">
                                            <button type="submit" class="btn btn-teal" name="submit">Add</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header no-border">
                                <small class="lead text-center">Cottage</small>

                                    <div class="card-tools">
                                    </div>
                                </div>
                                <div class="card-body p-0">
                                <table id="example1" class="display p-0 m-0" style="width:100%" role="grid" aria-describedby="example1_info">
                                        <thead>
                                            <tr>
                                                <th>Cottage Number</th>
                                            <th> Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
$res = $mysqli->query("SELECT * FROM cottages WHERE cottage_type_id='$cottage_type_id'");
while ($rows = mysqli_fetch_assoc($res)) {
    ?>
                                                <tr>
                                        <td><?php echo $rows['cottage_number']; ?></td>
                                                    <td>
                                                        <div>

                                                            <a href="delete_cottage.php?cottage_number=<?php echo $rows['cottage_number']; ?>&cottage_type_id=<?php echo $cottage_type_id; ?>" class="text-muted">
                                                                <button class="btn btn-sm bg-danger text-muted"><i class="fa fa-trash"></i></button>
                                                            </a>
                                                    </td>
                                                </tr>
                                                <?php
}
?>


                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- /.content -->
                </div>
                <!-- /.content-wrapper -->

                <!-- Main Footer -->
                <?php
include 'layout/footer.php';
?>
                    <!-- ./wrapper -->

                    <!-- REQUIRED SCRIPTS -->
                    <!-- jQuery -->
                    <script src="plugins/jquery/jquery.min.js"></script>
                    <!-- AJAX -->
                    <script src="ajax/editCottage.js"></script>
                    <!-- Data tables-->
                    <script src="plugins/datatables/jquery.dataTables.js"></script>
                    <script src="plugins/datatables/dataTables.bootstrap4.js"></script>
                    <!-- Bootstrap 4 -->
                    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
                    <!-- AdminLTE App -->
                    <script src="dist/js/adminlte.min.js"></script>
                    <script>
                        $(function() {

                            $('#example1').DataTable({
                                "paging": true,
                                "pageLength": 5,

                            });
                        });
                    </script>
    </body>
    </body>

    </html>

    <?php
