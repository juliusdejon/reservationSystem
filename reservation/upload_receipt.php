<?php

include 'functions.php';
include '../config/mysqli.php';

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>Villa Alfredo's Reservation System</title>

    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7CVarela+Round" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <!-- Bootstrap -->
    <link type="text/css" rel="stylesheet" href="../css/bootstrap.min.css" />
    <!-- Custom stlylesheet -->
    <link type="text/css" rel="stylesheet" href="../css/style.css" />
    <link type="text/css" rel="stylesheet" href="../css/styles.css" />

    <!-- Semantic -->
    <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />
    <!-- Initialize Semantic UI and JQuery to load Calendar Datepicker-->
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/semantic.min.js"></script>


    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="../css/font-awesome.min.css">

    <!-- Icons -->
    <link rel="stylesheet" href="../css/icon.min.css">


    <script>
  function resizeIframe(obj) {
    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
  }
</script>
        <script language="javascript" type="text/javascript">
function removeSpaces(string) {
 return string.split(' ').join('');
}
</script>
     <script type="text/javascript">
                    function readURL(input) {
                        if (input.files && input.files[0]) {
                            var reader = new FileReader();

                            reader.onload = function(e) {
                                $('#blah').attr('src', e.target.result);
                            }

                            reader.readAsDataURL(input.files[0]);
                        }


                    }
                    function previewFile() {
  var preview = document.querySelector('#receipt_2');
  var file    = document.querySelector('#receipt_two').files[0];
  var reader  = new FileReader();

  reader.addEventListener("load", function () {
    preview.src = reader.result;
  }, false);

  if (file) {
    reader.readAsDataURL(file);
  }
}
                </script>
</head>

<body>
    <!-- Header -->
    <header>
        <!-- Nav -->
        <nav id="nav" class="navbar">
            <div class="container">

                <div class="navbar-header">
                    <!-- Logo -->
                    <div class="navbar-brand">
                        <a href="../index.php">
                            <img class="logo" src="../img/valogo-alt.png" alt="logo">
                            <img class="logo-alt" src="../img/valogo-alt.png" alt="logo">
                        </a>
                    </div>
                    <!-- /Logo -->

                    <!-- Collapse nav button -->
                    <div class="nav-collapse">
                        <span></span>
                    </div>
                    <!-- /Collapse nav button -->
                </div>

                <!--  Main navigation  -->
                <ul class="main-nav nav navbar-nav navbar-right">
                    <li><a href="../#home">Home</a></li>
                    <li><a href="../#accomodation">Accomodation</a></li>
                    <li><a href="../#day-tour-cottages">Cottages</a></li>
                    <li><a href="../#contact">Contact</a></li>
                    <li><a href="index.php"><button class="secondary-btn">Book Now</button></a></li>
                </ul>
                <!-- /Main navigation -->

            </div>
        </nav>
        <!-- /Nav -->
    </header>
        <div class="ui segment container">
            <div id="result"></div>
            <div class="row container">
                Reference Id:
                <div class="ui mini icon input">
                    <input type="text"
                            placeholder="JD12082018F857I"
                            id="reference_id"
                            name="reference_id"
                            autofocus
                            onkeydown="this.value=removeSpaces(this.value);"/>
                    <i class="search icon"></i>
                </div>
            </div>
        </div>

    <div class="row ui segment container">
                <div class="col-md-12">
                    <div id="check-in-Form"></div>
                </div>
            </div>
    </div>

    <!-- Back to top -->
    <div id="back-to-top"></div>
    <!-- /Back to top -->

    <!-- Preloader -->
    <div id="preloader">
        <div class="preloader">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
    <script>
        $('.ui.radio.checkbox')
            .checkbox();
    </script>
    <!-- jQuery Plugins -->
    <script src="../admin/plugins/jquery/jquery.min.js"></script>
    <!-- Live Search CheckIn Form-->
    <script src="ajax/checkIn.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/main.js"></script>
    <script src="ajax/uploadReceipt.js"></script>
<script>

  function printFrame(id) {
            var frm = document.getElementById(id).contentWindow;
            frm.focus();// focus on contentWindow is needed on some ie versions
            frm.print();
            return false;
}
  </script>

</body>

</html
