<?php

include '../../config/mysqli.php';

if (isset($_POST['variable'])) {
    $var = $_POST['variable'];
    if (!empty($var)) {
        $sql =
            "SELECT * FROM customer INNER JOIN reservation
             JOIN reserved_rooms ON customer.client_reference_id = reserved_rooms.client_reference_id
             JOIN room_type ON room_type.room_id = reserved_rooms.room_type_id
             JOIN transaction ON transaction.client_reference_id = customer.client_reference_id
             WHERE customer.client_reference_id='$var' AND transaction_status!='checked_in'";
        $res = $mysqli->query($sql);
        while ($rows = mysqli_fetch_assoc($res)) {
            $client_reference_id = $rows['client_reference_id'];
            $first_name = $rows['first_name'];
            $last_name = $rows['last_name'];
            $email_address = $rows['email_address'];
            $arrival_date = $rows['arrival_date'];
            $departure_date = $rows['departure_date'];
            $created_at = $rows['created_at'];
            $no_of_adults = $rows['no_of_adults'];
            $no_of_kids = $rows['no_of_kids'];
            $no_of_senior_pwd = $rows['no_of_senior_pwd'];
        }
        if ($var == @$client_reference_id) {?>
    <form name="checkForm"  action="check_in_feedback.php"method="post" onsubmit="return validateForm();">
    <div class="card">
            <div class=" p-3 mb-3">
                <!-- title row -->
                <div class="row">
                    <div class="col-12">
                        <h4 class="lead">
                            Reservation Details :
                            <?php echo $client_reference_id; ?>
                            <small class="float-right">Date: <?php echo $created_at; ?></small>
                        </h4>
                        <h2 class="lead">
                            <?php echo $first_name . ' ' . $last_name; ?>
                        </h2>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- info row -->
                <div class="row -info">
                    <div class="col-sm-2 -col">
                        Arrival Date:
                        <strong><?php echo $arrival_date; ?></strong><br> Departure Date:
                        <strong><?php echo $departure_date; ?></strong>
                    </div>
                    <!-- /.col -->
                    <div class="col-md-2 -col">
                        <div class="col-6">
                            <!-- No of Adults: -->
                            <!-- <input type="text" class="form-control" disabled value="<?php echo $no_of_adults; ?>" /> -->
                            <input type="hidden" min="0" name="no-of-adults" class="form-control form-control-sm numbers-only" id="no-of-adult" maxlength="2" value="<?php echo $no_of_adults; ?>" />
                        </div>
                        <div class="col-6">
                            Adults:
                             <input type="text" name="extra-adults" id="extra-adults" maxlength="2" class="form-control" value="0"/>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-md-2 -col">
                        <div class="col-6">
                            <!-- No of Kids: -->
                            <!-- <input type="text" class="form-control" disabled value="<?php echo $no_of_kids; ?>" /> -->
                            <input type="hidden" name="no-of-kids" id="no-of-kids" value="<?php echo $no_of_kids; ?>" />
                        </div>
                        <div class="col-6">
                            Kids:
                            <input type="text" min="0" name="extra-kids" class="form-control form-control-sm numbers-only" id="extra-kids" maxlength="2" value="0" />
                        </div>
                    </div>
                    <div class="col-md-2 -col">
                        <div class="col-6">
                            <small>No. of Senior/PWD</small>
                            <input type="text" min="0" name="no-of-senior-pwd" class="form-control form-control-sm numbers-only" id="no-of-senior-pwd" maxlength="1" value="<?php echo $no_of_senior_pwd; ?>" />
                        </div>
                    </div>
                    <!-- /.col -->


                    <div class="col-md-4">
                        <div class="col-6">
                            <div>Deposits</div>
                            <?php $s = "SELECT * FROM extras WHERE extras_category ='Deposit'";
            $r = $mysqli->query($s);
            while ($rws = mysqli_fetch_assoc($r)) {
                $id = $rws['extras_id'];
                $price = $rws['extras_price'];
                if ($id == 7) {
                    ?>
                        <div class="ui disabled checkbox">
                                <input type="checkbox" checked class="check<?php echo $id; ?>" disabled>
                                <label><?php echo $rws['extras_name'] . "- ₱ " . $rws['extras_price']; ?></label>
                            </div>
                <?php

                } else {?>

                            <div class="ui checkbox">
                                <input type="checkbox" class="check<?php echo $id; ?>" onclick="addDeposit(<?php echo $id; ?>,<?php echo $price; ?> )">
                                <label><?php echo $rws['extras_name'] . "- ₱ " . $rws['extras_price']; ?></label>
                            </div>
                            <?php
}
            }
            ?>

                        </div>
                    </div>



                </div>
                <!-- /.row -->
                <strong>Rates</strong>
                <br>
                Adults = ₱ 250.00 <br>
                Kids = ₱ 150.00 <br>
                Senior/Pwd = ₱ 200.00<br><br>
                No of Nights:
                <?php
$room_total = [];
            $departure = strtotime($departure_date);
            $arrival = strtotime($arrival_date);
            $datediff = $departure - $arrival;
            $noofNights = round($datediff / (60 * 60 * 24));
            echo $noofNights;
            ?>
                    <input type="hidden" value="<?php echo $noofNights; ?>" id="no-of-nights" />
                    <hr />

                    <!-- /.row -->
                    <div class="row">
                        <?php
                       $count_room_key = "SELECT COUNT(*) as totalRoomKey FROM reserved_rooms WHERE client_reference_id='$client_reference_id'";
                       $res_room_key = $mysqli->query($count_room_key);
                       $row_room_key = mysqli_fetch_assoc($res_room_key);
                       $totalRoomKey = $row_room_key['totalRoomKey'];

$sql = "SELECT * FROM reserved_rooms
                                JOIN room_type ON reserved_rooms.room_type_id=room_type.room_id
                                WHERE reserved_rooms.client_reference_id='$var'
                                GROUP BY room_id";
            $res = $mysqli->query($sql);
            while ($rows = mysqli_fetch_assoc($res)) {
                $room_type_id = $rows['room_type_id'];
                $room_name = $rows['room_name'];
                $room_price = $rows['room_price'];
                $room_info = $rows['room_info'];
                $room_img = $rows['room_img'];
                ?>
                            <div class="col-md-3">
                                <div class="card">
                                    <img class="card-img-top" src='../img/<?php echo $room_img; ?>' alt="Card image cap">
                                    <div class="card-body">
                                        <h6>
                                            <?php echo $room_name; ?>
                                        </h6>
                                        <p class="card-text">
                                            <?php echo $room_info; ?>
                                        </p>
                                        Price
                                        <?php echo $room_price; ?>
                                        <?php
$sql1 = "SELECT COUNT(*) as quantity FROM reserved_rooms
                                        JOIN customer ON
                                        reserved_rooms.client_reference_id = customer.client_reference_id
                                        JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
                                        WHERE customer.client_reference_id ='$client_reference_id' AND room_type_id ='$room_type_id'";
                $res1 = $mysqli->query($sql1);
                $row1 = mysqli_fetch_assoc($res1);
                $quantity = $row1['quantity'];
                ?>
                                            Qty:
                                            <?php echo $quantity; ?>
                                            <?php
$subtotal = $quantity * $room_price;
                array_push($room_total, $subtotal);
                ?>
                                    </div>
                                </div>
                            </div>
                            <?php
}
            ?>
                                <!-- /.col -->
                                <div class="col-md-6">
                                    <p class="lead">Amount</p>

                                    <div class="table-responsive">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <th>Room Price: </th>
                                                    <td>
                                                        <?php $room_total = array_sum($room_total);
            $room_total = intval($room_total) * $noofNights;
            echo "₱ " . number_format($room_total, 2);
            ?>
                                                    </td>
                                                    <input type="hidden" name="room_total" id="roomTotal" value="<?php echo intval($room_total); ?>" />
                                                </tr>
                                                <tr>
                                                    <th>Entrance Fees:</th>
                                                    <td>
                                                        <span id="entrance"></span>
                                                        <input type="hidden" name="entrance" id="entranceTotal" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Extras:</th>
                                                    <td>
                                                        <?php
$totalExtras = [];
            $sql = "SELECT * FROM customer_extras
                                                    JOIN extras ON customer_extras.extras_id = extras.extras_id
                                                    WHERE client_reference_id = '$var' AND (extras_category='Rental' OR extras_category='Corkage' OR extras_category='Charges')
                                                    GROUP BY customer_extras.extras_id";
            $res = $mysqli->query($sql);
            while ($rows = mysqli_fetch_assoc($res)) {
                $extras_id = $rows['extras_id'];
                $extras_name = $rows['extras_name'];
                $extras_price = $rows['extras_price'];
                $sqlCount = "SELECT COUNT(*) as extras_qty FROM customer_extras WHERE extras_id='$extras_id' AND client_reference_id='$var'";
                $resCount = $mysqli->query($sqlCount);
                $rowCount = mysqli_fetch_array($resCount);
                $extras_qty = $rowCount['extras_qty'];
                array_push($totalExtras, $extras_qty * $extras_price);
            }
            $totalExtras = array_sum($totalExtras);
            ?>
                                                            <span id="extras">₱ <?php echo number_format($totalExtras, 2); ?></span>
                                                            <input type="hidden" id="extrasHiddenInput" value="<?php echo $totalExtras; ?>" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Deposit:</th>
                                                    <td>
                                                        <span id="depositTotalLabel">₱ <?php echo $totalRoomKey * 100;?></span>
                                                        <input type="hidden" name="depositTotalInput" id="depositTotalInput" value="0" />
                                                        <input type="hidden" name="deposits" id="depositHiddenInput">
                                                        <input type="hidden" name="totalRoomKey" id="roomKey" value="<?php echo $totalRoomKey * 100;?>" />
                                                        <input type="hidden" name="roomKey" value="<?php echo $totalRoomKey;?>" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Total Amount Due</th>
                                                    <td>
                                                        <span id="subtotal"><?php echo (int)$totalRoomKey * 100;?></span>
                                                        <input type="hidden" name="subtotal" id="subTotal" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th style="width:50%">Downpayment</th>
                                                    <td>
            <?php
$sql = "SELECT downpayment from transaction WHERE transaction.client_reference_id = '$client_reference_id'";
            $res = $mysqli->query($sql);
            $rowDown = mysqli_fetch_assoc($res);
            $downpayment = $rowDown['downpayment'];
            $downpayment = intval($downpayment);
            ?>
                                                            <span id="downpayment">
                                                        <?php echo "₱ " . number_format($downpayment, 2); ?>
                                                            </span>
                                                            <input type="hidden" name="downpayment" id="downpaymentTotal" value="<?php echo $downpayment; ?>" />

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Balance </th>
                                                    <td>
                                                        <?php
            $no_of_adults = $no_of_adults * 250;
            $no_of_kids = $no_of_kids * 150;
            $entrance = $no_of_adults + $no_of_kids;
            $balance = $room_total + $totalExtras + $entrance - $downpayment;

            $balance = intval($balance);
            if ($downpayment > $room_total + $totalExtras + $entrance) {
                $new_balance = 0;
            }
            ?>
                                                            <span id="balance"><?php echo "₱ " . number_format($new_balance, 2); ?></span>
                                                            <input type="hidden" name="balance" id="balanceTotal" value="<?php echo $balance; ?> " />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Initial Payment</th>
                                                    <td><input type="number" class="form-control form-control-md" name="input_payment" min="0" oninput="validity.valid||(value='');"/></td>
                                                </tr>

                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                <!-- /.col -->
                    </div>
                    <div class="row">
                        <!-- accepted payments column -->
                        <div class="col-6">

                        </div>

                    </div>
                    <!-- /.row -->

                    <!-- this row will not appear when printing -->
                    <div class="row no-print">
                        <div class="col-12">
                            <input type="hidden" name="client_reference_id" value="<?php echo $client_reference_id; ?>" />
                            <button type="submit" onsubmit="return validateForm();" name="check_in" class="btn btn-success float-right"><i class="fa fa-arrow-right"></i>
                  Check In
                  </button>
                        </div>
                    </div>
            </div
    </div>
    </form>
    <?php
} else {
            echo "<div class='bs-example' data-example-id='simple-jumbotron'><div class='jumbotron'><h3>Reference ID Not Found.</h3><p></p></div></div>";
        }
        ?>



        <?php
}
    ?>
            <script>
                let deposits = [];
                let depositTotal = [];
            </script>

            <?php
}
?>



                <!-- jQuery -->
                <script src="plugins/jquery/jquery.min.js"></script>

                <!--
                    Computation not getting at Db
                    <script>
                function validateSenior() {
                var seniorInput = $('#no-of-senior-pwd').val();
                var adultInput = $('#no-of-adult').val();
                  if(seniorInput > adultInput) {
                    $('#no-of-senior-pwd').val(adultInput);
                  }
                }
                </script> -->
                <script>
                    Number.prototype.format = function(n, x) {
                        var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
                        return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
                    };

                    // Validation Payment form
                    function validateForm() {
                        var balance = document.forms["checkForm"]["balance"].value;
                        var input_payment = document.forms["checkForm"]["input_payment"].value;
                        var no_of_adults = document.forms["checkForm"]["no_of_adults"].value;

                        if (parseInt(input_payment) < parseInt(balance)) {
                            alert("Please Settle your Balance");
                            return false;
                        }

                        if (no_of_adults == "" || no_of_adult == 0) {
                            alert("an Adult is Required");
                            return false;
                        }
                 
                    }
                    function addDeposit(id, price) {

                        var status = $('.check' + id)[0].checked;
                        if (status == true) {
                            deposits.push(id);
                            depositTotal.push(price);
                        } else {
                            for (var i = deposits.length - 1; i >= 0; i--) {
                                if (deposits[i] === id) {
                                    deposits.splice(i, 1);
                                    depositTotal.splice(i, 1);
                                }
                            }


                        }
                        var sum = parseInt($('#roomKey').val());
                        for (var i = 0; i < depositTotal.length; i++) {
                            sum += depositTotal[i]
                        }
                        $("#depositTotalLabel").text('₱ ' + sum.format(2));
                        $("#depositTotalInput").val(sum);

                        $('#depositHiddenInput').val(deposits);

                        var depositTotalInput = $("#depositTotalInput").val();
                        // computation entrance Fee
                        var discount = 250 * 0.20;
                        var seniorComputation = $('#no-of-senior-pwd').val() * (250 - discount);
                        var entrance = ($('#no-of-adult').val() * 250 + $('#extra-adults').val() * 250) + ($('input#no-of-kids').val() * 150 + $('#extra-kids').val() * 150) + seniorComputation;
                        $('#entrance').text('₱ ' + entrance.format(2));
                        $('#entranceTotal').val(entrance);

                        // computation with Extras

                        var extras = $('#extrasHiddenInput').val();
                        // computation subtotal
                        console.log(depositTotalInput);
                        var subtotal = entrance + parseInt($("#roomTotal").val()) + parseInt(depositTotalInput) + parseInt(extras);
                        console.log(subtotal);
                        $("#subtotal").text('₱ ' + subtotal.format(2));
                        $("#subTotal").val(subtotal);

                        // computated downpayment
                        var downpayment = $("#downpaymentTotal").val();

                        // computation balance
                        if(downpayment > subtotal) {
                        var balance = 0
                        } else {
                        var balance = subtotal - downpayment;
                        }
                        $("#balance").text('₱ ' + balance.format(2));
                        $("#balanceTotal").val(balance);
                    }
                </script>
                <script src="ajax/computations.js"></script>
                <!-- JQuery Validation source -->
                <script type="text/javascript" src="plugins/jquery/jquery-key-restrictions.min.js"></script>
                <!-- Actual Validation calling class -->
                <script type="text/javascript">
                    $(document).ready(function() {
                        $(".letters-only").lettersOnly();
                        $(".numbers-only").numbersOnly();
                        $(".alpha-numeric-only").alphaNumericOnly();
                    });
                </script>

