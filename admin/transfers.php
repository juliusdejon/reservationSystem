<?php
$rebook_id = $_GET['res_id'];
@$no_room = $_GET['no_room'];
@$invalid_date = $_GET['invalid_date'];
include '../config/mysqli.php';

$sql_identifier = "SELECT * FROM reservation
        JOIN customer on reservation.client_reference_id = customer.client_reference_id
        WHERE reservation.client_reference_id = '$rebook_id'";
$res_identifier = $mysqli->query($sql_identifier);
while ($rows_identifier = $res_identifier->fetch_assoc()) {
    $reservation_status = $rows_identifier['reservation_status'];
    $first_name = $rows_identifier['first_name'];
    $last_name = $rows_identifier['last_name'];
    $email = $rows_identifier['email_address'];
    $contact = $rows_identifier['contact_number'];
    $no_of_kids = $rows_identifier['no_of_kids'];
    $no_of_adults = $rows_identifier['no_of_adults'];
    $arrival_date = $rows_identifier['arrival_date'];
    $departure_date = $rows_identifier['departure_date'];
}
// if ($reservation_status != 'Booked') {
//     header('Location: rebooking.php?rebooking=false');
// }

// Get the rang of dates
function date_range($first, $last, $step, $output_format)
{
    $dates = array();
    $current = strtotime($first);
    $last = strtotime($last);
    while ($current <= $last) {
        $dates[] = date($output_format, $current);
        $current = strtotime($step, $current);
    }
    return $dates;
}

// Visited the Adjustment Page, Insert all the old booking

// $sql = "SELECT * FROM reserved_rooms JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id WHERE client_reference_id='$rebook_id' GROUP BY room_type_id";
// $res = $mysqli->query($sql);
// while ($rows  = mysqli_fetch_assoc($res)) {
//     $room_id = $rows['room_type_id'];
//     $sql_count = "SELECT COUNT(*) as quantity FROM reserved_rooms WHERE room_type_id='$room_id' AND client_reference_id ='$rebook_id'";

//     $res_count = $mysqli->query($sql_count);
//     $rows_count = mysqli_fetch_assoc($res_count);
//     $quantity = $rows_count['quantity'];
//     $mysqli->query("INSERT INTO adjusted_rooms (client_reference_id, room_type_id, adjusted_room_quantity,adjusted_start_date,adjusted_end_date) VALUES ('$rebook_id','$room_id','$quantity', '$check_in_date','$date_now')");

// }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Villa Alfredo Admin</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Semantic UI -->
  <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />

         <!-- Initialize Semantic UI and JQuery to load Calendar Datepicker-->
         <script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" src="../js/semantic.min.js"></script>

        <!-- Calendar -->
        <link type="text/css" rel="stylesheet" href="../css/calendar.min.css" />
        <script src="../js/calendar.min.js"></script>

        <!-- Icons -->
        <link rel="stylesheet" href="../css/icon.min.css">



</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

<?php
include 'layout/navbar.php';
include 'layout/sidebar.php';
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Upgrade/Downgrade of Room: <?php echo $rebook_id; ?></h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
    <div class="row">


                            <div class="col-md-8">
                            <form action="adjustment_post.php" name="rebookForm" method="post" onsubmit="return validateForm();">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Transfer Form </h3>
                                    </div>
                                    <!-- /.card-header -->

                                    <div class="card-body">
                                          <input type="hidden" name="ref_id" id="ref_id" value="<?php echo $rebook_id; ?>">
                                            <div class="row">
                                                    <div class="ui form">
                                                        <div class="two fields">
                                                          <div class="field">
                                                            <label>Arrival Date</label>
                                                            <div class="ui calendar" id="rangestart">
                                                                <div class="ui input left icon">
                                                                    <i class="calendar icon"></i>
                                                                    <input type="text" disabled id="start" placeholder="Start" name="arrival_date_post" autocomplete="off" value="<?php echo $arrival_date; ?>">
                                                                </div>
                                                            </div>
                                                          </div>
                                                          <div class="field">
                                                            <label>Departure Date</label>
                                                            <div class="ui calendar" id="rangeend">
                                                                <div class="ui input left icon">
                                                                    <i class="calendar icon"></i>
                                                                    <input type="text" id="end" placeholder="End" name="departure_date_post" autocomplete="off" value="<?php echo $departure_date; ?>">
                                                                </div>
                                                            </div>
                                                          </div>
                                                        </div>

                                                        <div class="row">
                        <?php
$sql = "SELECT * FROM reserved_rooms
           JOIN room_type ON reserved_rooms.room_type_id=room_type.room_id
           WHERE reserved_rooms.client_reference_id='$rebook_id'
           ";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $room_type_id = $rows['room_type_id'];
    $room_name = $rows['room_name'];
    $room_price = $rows['room_price'];
    $room_info = $rows['room_info'];
    $room_img = $rows['room_img'];
    ?>

                            <div class="col-md-6">
                            <div class="ui card">
  <div class="image">
  <img src='../img/<?php echo $room_img; ?>' alt="Card image cap">
  </div>
  <div class="content">
    <a class="header"><?php echo $room_name; ?></a>
    <div class="description">
    </div>
  </div>
  <div class="extra content">
    <a>
    <i class="fa fa-bed"></i>
      Price
                                        <?php echo $room_price; ?>
      <a href='transfer_room.php?room_id=<?php echo $room_type_id; ?>&res_id=<?php echo $rebook_id; ?>' class="ui tiny fuild teal button"> Transfer</a>
    </a>
  </div>
</div>


                            </div>

                            <?php
}
?>


                    </div>

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                &nbsp;
                                                            </div>
                                                        </div>
                                        </div>

                                        </div>

                                        </div>

                                    </div>
                                    <!-- /.card-body -->
                                </div>


<div class="card">

</div>


                            </div>

      </div>
</div>
                        </div>
                    </div>
                    </form>

    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
<?php
include 'layout/footer.php';
?>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- Arrival Date -->



<script>
                        const today = new Date();
                        $('#rangestart').calendar({
                            type: 'date',
                            endCalendar: $('#rangeend'),
                            onChange: function (date, text, mode) {
                              $("#available-rooms").html('<img src="ajax/loading-spinner.gif" width="100px">')
                              var start = $("#start").val();
                              var end = $("#end").val();
                              var ref_id = $("#ref_id").val();
                              $.post( "ajax/getAvailableRooms.php", {start:start ,end: end, ref_id: ref_id},function(data) {
                              $( "#available-rooms").html(data);
                                });

                          },
                        });
                        $('#rangeend').calendar({
                            type: 'date',
                            startCalendar: $('#rangestart'),
                            onChange: function (date, text, mode) {
                              $("#available-rooms").html('<img src="ajax/loading-spinner.gif" width="100px">')
                              var start = $("#start").val();
                              var end = $("#end").val();
                              var ref_id = $("#ref_id").val();
                              var jqxhr =$.post( "ajax/getAvailableRooms.php", {start:start ,end: end,ref_id:ref_id},function(data) {
                              $("#available-rooms").html(data);
                            });
                          },
                          onHidden: function () {
                                $.post( "ajax/getAvailableRooms.php", {start:start ,end: end,ref_id:ref_id},function(data) {
                              $("#available-rooms").html(data);
                            });
                            },
                        });
                    </script>



<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>

<script>

                        // Validation Payment form
                        function validateForm() {
                            var continuex = document.forms["rebookForm"]["continuex"].value;
                            if (continuex == 'no') {
                                alert("Unable to Transfer Dates.The Rooms Reserved has no Available Rooms.");
                                return false;
                            }

                        }
                    </script>


<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
     <!-- DataTables -->
     <script src="plugins/datatables/jquery.dataTables.js"></script>
      <script src="plugins/datatables/dataTables.bootstrap4.js"></script>

                             <script>
                        $(function() {
                            $('#example1').DataTable({
                                "paging": true,
                                "lengthChange": false,
                                "searching": true,
                                "ordering": true,
                                "info": true,
                                "autoWidth": false,
                                "pageLength": 3,
                                "stripeClasses": ['odd-row', 'even-row'],
                            });
                        });
                    </script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
</body>
</html>

