<script src="plugins/jquery/jquery.min.js"></script>
<?php
include '../../config/mysqli.php';
$arrival_date = $_POST['start'];
$departure_date = $_POST['end'];

$arrival_date = date_create_from_format('F j, Y', $arrival_date);
$arrival_date = $arrival_date->format('Y-m-d');

$departure_date = date_create_from_format('F j, Y', $departure_date);
$departure_date = $departure_date->format('Y-m-d');

// Get the rang of dates
function date_range($first, $last, $step, $output_format)
{
    $dates = array();
    $current = strtotime($first);
    $last = strtotime($last);
    while ($current <= $last) {
        $dates[] = date($output_format, $current);
        $current = strtotime($step, $current);
    }
    return $dates;
}
?>
<h5>Range: <?php echo $arrival_date; ?> to <?php echo $departure_date; ?></h5>
<table class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                            <thead>
                                                <tr role="row">

                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending">Room Name</th>
                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Room Description</th>
                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Room Price</th>
                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Available</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                        <?php
// Select room type eg. Ana Villa, Mike Hotel
$res = $mysqli->query('SELECT * FROM room_type WHERE isDeleted=0');
while ($rows = mysqli_fetch_assoc($res)) {
    $roomType = $rows['room_id'];
    $room_name = $rows['room_name'];
    $room_info = $rows['room_info'];
    $room_price = $rows['room_price'];
    // Get the Input dates provided by the customer at the front end eg. he clicked July 24 - 26
    $range = date_range($arrival_date, $departure_date, "+1 day", "Y-m-d");
    // Slice it to July 24 - 25
    $reserved_period = array_slice($range, 0, -1);

    // Here you are selecting 'RESERVED ROOMS!!' AND ONLY WITH A STATUS THAT HAS BEEN CONFIRMED AND BOOKED REFLECTED by the given Dates on the site
    $s = "SELECT * FROM reserved_rooms JOIN customer ON customer.client_reference_id = reserved_rooms.client_reference_id
        JOIN reservation ON reserved_rooms.client_reference_id = reservation.client_reference_id
        WHERE reserved_rooms.room_type_id = '$roomType' AND (reservation_status='Booked'  OR reservation_status='Checked In')";

    $r = $mysqli->query($s);
    $reserved_rooms = 0;
    $my_selected_rooms = [];
    while ($ro = mysqli_fetch_assoc($r)) {
        $arrival_date_db = $ro['arrival_date'];
        $departure_date_db = $ro['departure_date'];
        // -1 to the Departure date. July 26 becomes July 25... so it will not consider 26 as still Reserved date.
        $departure_date_db = date('Y-m-d', (strtotime('-1 day', strtotime($departure_date_db))));
        // Get the Database Arrival Date and Departure Date of Ana Villa Room type that has been Reserved
        $rangeDb = date_range($arrival_date_db, $departure_date_db, "+1 day", "Y-m-d");

        $roomNotAvailable = 'not existing';
        foreach ($reserved_period as $day) {
            // Check the reserved period above an Array of dates
            // eg. $reservation_period = [2018-07-25,2018-07-25] // These dates will be compared at the Database
            // rangeDb
            if (in_array($day, $rangeDb)) {
                $roomNotAvailable = 'existing';
            }
        }
        if ($roomNotAvailable == 'existing') {
            // array_push($reserved_rooms, 1);
            $reserved_rooms++;
        }
    }

    // $reserved_rooms = array_sum($reserved_rooms);

    // Available Rooms = Total Rooms - Reserved Rooms IN Check In Date
    $sqlAvailableRoom = "SELECT COUNT(*) as TotalRoom FROM rooms WHERE room_type_id='$roomType'";
    $resAvailableRoom = $mysqli->query($sqlAvailableRoom);
    $result = mysqli_fetch_assoc($resAvailableRoom);
    $result = $result['TotalRoom'] - $reserved_rooms;

    ?>
      <tr>
        <td><?php echo $room_name; ?></td>
        <td><?php echo $room_info; ?></td>
        <td align="center">₱<?php echo number_format($room_price, 2); ?></td>
        <td align="center">

        <?php

    if ($result <= 0) {
        echo "<a class='ui red right ribbon label'>Not Available</a>";
    } else {
        echo $result;
    }
    ?>
                          </td>
      </tr>

<?php }?>


</tbody>
</table>