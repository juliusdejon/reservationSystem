<?php
include '../config/mysqli.php';
$room_id = $_GET['room_id'];
@$deleted = $_GET['deleted'];
@$added = $_GET['added'];
$sql = "SELECT * FROM room_type WHERE room_id = '$room_id'";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $room_id;
    $room_name = $rows['room_name'];
    $room_img = $rows['room_img'];
    $room_info = $rows['room_info'];
    $room_type = $rows['room_type'];
    $room_price = $rows['room_price'];
    $room_adults_capacity = $rows['room_adults_capacity'];
    $room_child_capacity = $rows['room_child_capacity'];
}
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>Villa Alfredo Admin</title>

        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/adminlte.min.css">
        <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap4.css">
        <!-- Semantic UI -->
        <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />
        <!--Data table-->
        <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap4.css">
    </head>

    <body class="hold-transition sidebar-mini">
        <div class="wrapper">

            <?php
include 'layout/navbar.php';
include 'layout/sidebar.php';
?>
                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <div class="content-header">
                        <div class="container-fluid">
                            <div class="row mb-2">
                                <div class="col-sm-6">
                                    <h3 class="m-0 text-dark">
                                        <?php if ($deleted == 'true') {
    echo '<div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        Successfully Deleted Room!
                                        </div>';
} else {

}?>
                                    <?php if ($deleted == 'false') {
    echo '<div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        Unable to delete. Checkout first the customer staying at the Room.
                                        </div>';
} else {

}?>
                      <?php if ($added == 'true') {
    echo '<div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        Successfully Added Room!
                                        </div>';
} else if ($added == 'empty') {
    echo '<div class="alert alert-info alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    Please Enter a Room number
    </div>';
} else if ($added == 'exists') {
    echo '<div class="alert alert-info alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    Room Number Already Exists
    </div>';
} else {

}
?>
                                    <a href="manage_rooms.php" class="btn btn-sm btn-primary">Back </a>
                                       <?php echo $room_name; ?>
                                    </h3>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->
                    </div>
                    <!-- /.content-header -->
                    <script type="text/javascript">
                        function readURL(input) {
                            if (input.files && input.files[0]) {
                                var reader = new FileReader();

                                reader.onload = function(e) {
                                    $('#blah').attr('src', e.target.result);
                                }

                                reader.readAsDataURL(input.files[0]);
                            }
                        }
                    </script>
                    <!-- Main content -->
                    <div class="content">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card">
                                <div id="loadedRoomImage">
                                    <div class="card-header no-border" style="background: url(../img/<?php echo $room_img; ?>);
                                        height: 200px;
                                        background-repeat: no-repeat;
                                        width: 100%;
                                        background-size: cover;
                                        background-position: center center;">
                                </div>

                                        <div class="card-tools">

                                        </div>
                                    </div>
                                    <div class="card-body p-0">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card card-teal">
                                <a data-toggle="collapse" href="#edit" role="button" aria-expanded="false" aria-controls="collapseExample">
                                    <div class="card-header ui fluid button">
                                     <h3 class="card-title">Edit Room Information <i class="fa fa-arrow-down"></i></h3>
                                    </div>
                                    </a>
                                    <form action="ajax/editRoom.php" id="editRoomForm" role="form" method="post" enctype="multipart/form-data">
                                        <div class="card-body collapse" id="edit">
                                <div id="result"></div>
                                <input type="hidden" name="room_id" value="<?php echo $room_id ?>"/>
                                            <div class="form-group">
                                                <div class="div-image">
                                                    <?php
$image = '<img width="100" height="100" src="../img/' . $room_img . '" alt="Default Profile Pic" id="blah">';
echo $image;
?>
                                                </div>
                                                <label for="exampleInputFile">Image</label>
                                                <div class="input-group">
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" name="file" onchange="readURL(this);">
                                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                                    </div>
                                                    <div class="input-group-append">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Room Name</label>
                                                    <input type="text" maxlength="20" name="room_name" class="form-control alpha-numeric-only" id="exampleInputEmail1" placeholder="eg. Family Villa" value="<?php echo $room_name; ?>">
                                                </div>
                                                </div>
                                                <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Type</label>
                                                    <input type="text" maxlength="10" name="room_type" class="form-control letters-only" id="exampleInputPassword1" placeholder="eg. Villa" value="<?php echo $room_type; ?>" />
                                                </div>
                                                </div>
                                                </div>
                                                <div class="row">
                                                <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Adult Capacity</label>
                                                    <input type="text" maxlength="2" name="adult_capacity" value="<?php echo $room_adults_capacity; ?>" placeholder="0" class="form-control numbers-only">
                                                </div>
                                                </div>
                                                <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Child Capacity</label>
                                                    <input type="text" maxlength="2" name="child_capacity" value="<?php echo $room_child_capacity; ?>" placeholder="0" class="form-control numbers-only">
                                                </div>
                                                </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Room Info</label>
                                                    <textarea class="form-control alpha-numeric-only" rows="1" placeholder="Enter ..." name="room_info"><?php echo $room_info; ?></textarea>

                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Price</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">₱</span>
                                                        </div>
                                                        <input type="text" maxlength="5" name="room_price" value="<?php echo $room_price; ?>" class="form-control numbers-only">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">.00</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="submit" id="button" class="ui button teal fluid" name="submit">Update</button>

                                        </div>
                                        <!-- /.card-body -->

                                    </form>

                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                    <div class="col-md-6">
                                <div class="card card-teal">
                                    <div class="card-header">
                                        <h3 class="card-title">Add Rooms</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <!-- form start -->
                                    <form role="form" action="add_room.php" method="post">
                                        <div class="card-body">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Room Number</label>
                                                    <input type="text"  maxlength="20" name="room_number" class="form-control alpha-numeric-only" id="exampleInputEmail1" placeholder="101">
                                                    <input type="hidden" name="room_id" value="<?php echo $room_id; ?>" />
                                                </div>

                                            </div>
                                        </div>
                                        <!-- /.card-body -->

                                        <div class="card-footer">
                                            <button type="submit" class="btn btn-teal" name="submit">Add</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header no-border">
                                <small class="lead text-center">Rooms</small>

                                    <div class="card-tools">
                                    </div>
                                </div>
                                <div class="card-body p-0">
                                <table id="example1" class="display p-0 m-0" style="width:100%" role="grid" aria-describedby="example1_info">
                                        <thead>
                                            <tr>
                                                <th>Room Number</th>
                                            <th> Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
$res = $mysqli->query("SELECT * FROM rooms WHERE room_type_id='$room_id'");
while ($rows = mysqli_fetch_assoc($res)) {
    ?>
                                                <tr>
                                        <td><?php echo $rows['room_number']; ?></td>
                                                    <td>
                                                        <div>

                                                            <a href="delete_room.php?room_number=<?php echo $rows['room_number']; ?>&room_id=<?php echo $room_id; ?>" class="text-muted">
                                                                <button class="btn btn-sm bg-danger text-muted"><i class="fa fa-trash"></i></button>
                                                            </a>
                                                    </td>
                                                </tr>
                                                <?php
}
?>


                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- /.content -->
                </div>
                <!-- /.content-wrapper -->

                <!-- Main Footer -->
                <?php
include 'layout/footer.php';
?>
                    <!-- ./wrapper -->

                    <!-- REQUIRED SCRIPTS -->
                    <!-- jQuery -->
                    <script src="plugins/jquery/jquery.min.js"></script>
                    <!-- JQuery Validation source -->
                       <script type="text/javascript" src="plugins/jquery/jquery-key-restrictions.min.js"></script>
                    <!-- Actual Validation calling class -->
                    <script type="text/javascript">
                    $(document).ready(function () {
                        $(".letters-only").lettersOnly();
                        $(".numbers-only").numbersOnly();
                        $(".alpha-numeric-only").alphaNumericOnly();
                    });
                    </script>
                    <!-- AJAX -->
                    <script src="ajax/editRoom.js"></script>
                    <!-- Data tables-->
                    <script src="plugins/datatables/jquery.dataTables.js"></script>
                    <script src="plugins/datatables/dataTables.bootstrap4.js"></script>
                    <!-- Bootstrap 4 -->
                    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
                    <!-- AdminLTE App -->
                    <script src="dist/js/adminlte.min.js"></script>
                    <script>
                        $(function() {

                            $('#example1').DataTable({
                                "paging": true,
                                "pageLength": 5,

                            });
                        });
                    </script>
    </body>
    </body>

    </html>

    <?php
