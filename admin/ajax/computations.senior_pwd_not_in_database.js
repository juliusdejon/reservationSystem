








Number.prototype.format = function(n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};


$(document).ready(function() {
    // computation entrance Fee

    // less the Senior/pwd 
    var seniorInput = $('#no-of-senior-pwd').val();
    var adultInput = $('#no-of-adult').val();


    var entrance = (adultInput * 250) + ($('input#no-of-kids').val() * 150);
    $('#entrance').text('₱ ' + entrance.format(2));
    $('#entranceTotal').val(entrance);

    // deposit
    var depositTotalInput = $("#depositTotalInput").val();


    // computation subtotal
    var subtotal = entrance + parseInt($("#roomTotal").val()) + parseInt(depositTotalInput);
    $("#subtotal").text('₱ ' + subtotal.format(2));
    $("#subTotal").val(subtotal);

    // computated downpayment 
    var downpayment = $("#downpaymentTotal").val();
    $("#downpayment").text('₱ ' + downpayment.format(2));
    $("#downpaymentTotal").val(downpayment);

    // computation balance
    var balance = subtotal - downpayment;
    $("#balance").text('₱ ' + balance.format(2));
    $("#balanceTotal").val(balance);

});


$('#no-of-senior-pwd').keydown(function() {
 // computation entrance Fee

    // less the Senior/pwd 
    var seniorInput = $('#no-of-senior-pwd').val();
    var adultInput = $('#no-of-adult').val();
    let adultComputation = adultInput * 250;
    const discount = (250 * 0.20);
    remainingAdult = adultInput - seniorInput;
    if(seniorInput <= 0 ) {
     adultComputation = (adultInput * 250);
    } else {
    adultComputation = (seniorInput * (250 - discount)) + (remainingAdult * 250);
    }
    
var entrance = adultComputation + ($('input#no-of-kids').val() * 150);
    $('#entrance').text('₱ ' + entrance.format(2));
    $('#entranceTotal').val(entrance);

    // deposit
    var depositTotalInput = $("#depositTotalInput").val();


    // computation subtotal
    var subtotal = entrance + parseInt($("#roomTotal").val()) + parseInt(depositTotalInput);
    $("#subtotal").text('₱ ' + subtotal.format(2));
    $("#subTotal").val(subtotal);

    // computated downpayment 
    var downpayment = $("#downpaymentTotal").val();
    $("#downpayment").text('₱ ' + downpayment.format(2));
    $("#downpaymentTotal").val(downpayment);

    // computation balance
    var balance = subtotal - downpayment;
    $("#balance").text('₱ ' + balance.format(2));
    $("#balanceTotal").val(balance);

});


$('#no-of-adult').keydown(function() {
    // computation entrance Fee
    // less the Senior/pwd 
    var seniorInput = $('#no-of-senior-pwd').val();
    var adultInput = $('#no-of-adult').val();
    let adultComputation = adultInput * 250;
    const discount = (250 * 0.20);
    remainingAdult = adultInput - seniorInput;
    if(seniorInput <= 0 ) {
     adultComputation = (adultInput * 250);
    } else {
    adultComputation = (seniorInput * (250 - discount)) + (remainingAdult * 250);
    }
    
    var entrance = adultComputation + ($('input#no-of-kids').val() * 150);
    $('#entrance').text('₱ ' + entrance.format(2));
    $('#entranceTotal').val(entrance);

    // deposit
    var depositTotalInput = $("#depositTotalInput").val();


    // computation subtotal
    var subtotal = entrance + parseInt($("#roomTotal").val()) + parseInt(depositTotalInput);
    $("#subtotal").text('₱ ' + subtotal.format(2));
    $("#subTotal").val(subtotal);

    // computated downpayment 
    var downpayment = $("#downpaymentTotal").val();
    $("#downpayment").text('₱ ' + downpayment.format(2));
    $("#downpaymentTotal").val(downpayment);

    // computation balance
    var balance = subtotal - parseInt($("#downpaymentTotal").val());
    $("#balance").text('₱ ' + balance.format(2));
    $("#balanceTotal").val(balance);
});

$('#no-of-kids').keydown(function() {
    // computation entrance Fee
    // less the Senior/pwd 
    var seniorInput = $('#no-of-senior-pwd').val();
    var adultInput = $('#no-of-adult').val();
    let adultComputation = adultInput * 250;
    const discount = (250 * 0.20);
    remainingAdult = adultInput - seniorInput;
    if(seniorInput <= 0 ) {
     adultComputation = (adultInput * 250);
    } else {
    adultComputation = (seniorInput * (250 - discount)) + (remainingAdult * 250);
    }
    
    var entrance = adultComputation + ($('input#no-of-kids').val() * 150);
    $('#entranceTotal').val(entrance);

    // deposit
    var depositTotalInput = $("#depositTotalInput").val();


    // computation subtotal
    var subtotal = entrance + parseInt($("#roomTotal").val()) + parseInt(depositTotalInput);
    $("#subtotal").text('₱ ' + subtotal.format(2));
    $("#subTotal").val(subtotal);


    // computated downpayment 
    var downpayment = $("#downpaymentTotal").val();
    $("#downpayment").text('₱ ' + downpayment.format(2));
    $("#downpaymentTotal").val(downpayment);

    // computation balance
    var balance = subtotal - downpayment;
    $("#balance").text('₱ ' + balance.format(2));
    $("#balanceTotal").val(balance);

});


$('#no-of-adult').change(function() {
    // computation entrance Fee
    var countNights = $("#no-of-nights").val();
    // less the Senior/pwd 
    var seniorInput = $('#no-of-senior-pwd').val();
    var adultInput = $('#no-of-adult').val();
    let adultComputation = adultInput * 250;
    const discount = (250 * 0.20);
    remainingAdult = adultInput - seniorInput;
    if(seniorInput <= 0 ) {
     adultComputation = (adultInput * 250);
    } else {
    adultComputation = (seniorInput * (250 - discount)) + (remainingAdult * 250);
    }
        
    var entrance = adultComputation + ($('input#no-of-kids').val() * 150);
    $('#entrance').text('₱ ' + entrance.format(2));
    $('#entranceTotal').val(entrance);
    // deposit
    var depositTotalInput = $("#depositTotalInput").val();


    // computation subtotal
    var subtotal = entrance + parseInt($("#roomTotal").val()) + parseInt(depositTotalInput);
    $("#subtotal").text('₱ ' + subtotal.format(2));
    $("#subTotal").val(subtotal);

    // computated downpayment 
    var downpayment = $("#downpaymentTotal").val();
    $("#downpayment").text('₱ ' + downpayment.format(2));
    $("#downpaymentTotal").val(downpayment);

    // computation balance
    var balance = subtotal - parseInt($("#downpaymentTotal").val());
    $("#balance").text('₱ ' + balance.format(2));
    $("#balanceTotal").val(balance);
});

$('#no-of-kids').change(function() {
    // computation entrance Fee
    // less the Senior/pwd 
    var seniorInput = $('#no-of-senior-pwd').val();
    var adultInput = $('#no-of-adult').val();
    let adultComputation = adultInput * 250;
    const discount = (250 * 0.20);
    remainingAdult = adultInput - seniorInput;
    if(seniorInput <= 0 ) {
     adultComputation = (adultInput * 250);
    } else {
    adultComputation = (seniorInput * (250 - discount)) + (remainingAdult * 250);
    }
        
    var entrance = adultComputation + ($('input#no-of-kids').val() * 150);
    $('#entrance').text('₱ ' + entrance.format(2));
    $('#entranceTotal').val(entrance);

    // deposit
    var depositTotalInput = $("#depositTotalInput").val();


    // computation subtotal
    var subtotal = entrance + parseInt($("#roomTotal").val()) + parseInt(depositTotalInput);
    $("#subtotal").text('₱ ' + subtotal.format(2));
    $("#subTotal").val(subtotal);


    // computated downpayment 
    var downpayment = $("#downpaymentTotal").val();
    $("#downpayment").text('₱ ' + downpayment.format(2));
    $("#downpaymentTotal").val(downpayment);

    // computation balance
    var balance = subtotal - downpayment;
    $("#balance").text('₱ ' + balance.format(2));
    $("#balanceTotal").val(balance);

});