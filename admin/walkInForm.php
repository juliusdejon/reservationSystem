<?php
include '../config/mysqli.php';

if (isset($_POST['compute'])) {

    $client_reference_id = $_POST['client_reference_id'];
    $downpayment = $_POST['downpayment'];
    $total = $_POST['total'];
    $balance = $total - $downpayment;
    $cottages = [];
    $hasSelectedCottage = [];
    $corkages = $_POST['corkages'];
    $sql = "SELECT * FROM cottage_type";
    $res = $mysqli->query($sql);
    while ($rows = mysqli_fetch_assoc($res)) {
        $cottageType = $rows['cottage_type_id'];
        $_POST['cottage_id' . $cottageType];

        $isSelected = $_POST['quantity' . $cottageType];
        array_push($hasSelectedCottage, $isSelected);
        if ($isSelected != 0) {
            array_push($cottages, $cottageType);
            array_push($cottages, $isSelected);
        }
    }
    $count = 0;
    for ($i = 0; $i < count($hasSelectedCottage); $i++) {
        if ($hasSelectedCottage[$i] != 0) {

        } else {
            $count++;
        }
    }

    $cottages = array_chunk($cottages, 2);

    $sql = "UPDATE transaction SET downpayment='$downpayment', balance='$balance',total ='$total' WHERE client_reference_id='$client_reference_id'";
    $res = $mysqli->query($sql);
    // Update Transaction
    if ($res) {
        if($corkages == 0 || $corkages == "" || $corkages == "") {

        } else {
            $mysqli->query("INSERT INTO customer_corkages (client_reference_id, price) VALUES('$client_reference_id','$corkages')");

        }

        if ($count == count($hasSelectedCottage)) {

            header('Location: overview_daytour.php?ref_id=' . $client_reference_id);
        } else {

            for ($i = 0; $i < count($cottages); $i++) {
                $cottage_id = $cottages[$i][0];
                $cottage_qty = $cottages[$i][1];
                for ($j = 0; $j < $cottage_qty; $j++) {
                    $sql1 = "INSERT INTO occupied_cottages (cottage_id,client_reference_id) VALUES('$cottage_id','$client_reference_id')";
                    $mysqli->query($sql1);
                }

            }
            header('Location: overview_daytour.php?ref_id=' . $client_reference_id);
        }
    } else {
        echo "<script>alert('Something Went Wrong creating the transaction');</script>";
        // header('Location: avail_cottages.php?res_id='.$client_reference_id);
    }

}
