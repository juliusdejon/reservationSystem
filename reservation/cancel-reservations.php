<?php

include 'functions.php';
include '../config/mysqli.php';

$res_id = $_GET['res_id'];

$real_res_id = base64_decode($res_id);
$checkSQL = "SELECT client_reference_id FROM customer WHERE client_reference_id='$real_res_id'";
$resSQL = $mysqli->query($checkSQL);
$rowSQL = mysqli_fetch_assoc($resSQL);
$client_reference_id_db = $rowSQL['client_reference_id'];

if ($client_reference_id_db) {
} else {
    header('Location: index.php');
}
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

        <title>Villa Alfredo's Reservation System</title>

        <!-- Google font -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7CVarela+Round" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

        <!-- Bootstrap -->
        <link type="text/css" rel="stylesheet" href="../css/bootstrap.min.css" />

        <!-- Semantic -->
        <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />
        <!-- Initialize Semantic UI and JQuery to load Calendar Datepicker-->
        <script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" src="../js/semantic.min.js"></script>


        <!-- Font Awesome Icon -->
        <link rel="stylesheet" href="../css/font-awesome.min.css">

        <!-- Icons -->
        <link rel="stylesheet" href="../css/icon.min.css">

        <!-- Custom stlylesheet -->
        <link type="text/css" rel="stylesheet" href="../css/style.css" />
        <link type="text/css" rel="stylesheet" href="../css/styles.css" />
        <script>
            function resizeIframe(obj) {
                obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
            }
        </script>
        <script type="text/javascript">
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#blah').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }
        </script>
    </head>

    <body>
        <!-- Header -->
        <header>
            <!-- Nav -->
            <nav id="nav" class="navbar">
                <div class="container">

                    <div class="navbar-header">
                        <!-- Logo -->
                        <div class="navbar-brand">
                            <a href="index.html">
                                <img class="logo" src="../img/valogo-alt.png" alt="logo">
                                <img class="logo-alt" src="../img/valogo-alt.png" alt="logo">
                            </a>
                        </div>
                        <!-- /Logo -->

                        <!-- Collapse nav button -->
                        <div class="nav-collapse">
                            <span></span>
                        </div>
                        <!-- /Collapse nav button -->
                    </div>

                    <!--  Main navigation  -->
                    <ul class="main-nav nav navbar-nav navbar-right">
                        <li><a href="../#home">Home</a></li>
                        <li><a href="../#accomodation">Accomodation</a></li>
                        <li><a href="../#day-tour-cottages">Cottages</a></li>
                        <li><a href="photogallery.php">Photo Gallery</a></li>
                        <li><a href="#contact">Contact</a></li>
                        <li><a href="index.php"><button class="secondary-btn">Book Now</button></a></li>
                    </ul>
                    <!-- /Main navigation -->

                </div>
            </nav>
            <!-- /Nav -->
        </header>
        <section class="xs-padding">
            <div class="ui message container">
                <div id="result"></div>

                <div class="row">
                    <div class="col-md-8">
                        <div class="ui red header">
                            Cancel Reservation Form
                        </div>
                        <div>
                            <label> Are you sure you  want to cancel your reservation?</label>
                            <div class="row">
                                <div class="col-md-3">
                                    Reference Id:
                                </div>
                                <div class="col-md-6">
                                    <input type="text" disabled class="form-control form-input-sm" value="<?php echo $client_reference_id_db; ?>" />
                            </div>
                            <div class="col-md-3">
                      <a  href="update-reservation.php?res_id=<?php echo $real_res_id; ?>" class="ui button fluid red">Yes</a>
          </div>
                                </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="ui segment container">
                <button class="ui teal button" onclick="printFrame('receipt')">Print Reservation Details</button>
                <iframe id="receipt" class="ui container fluid" src="reservation-details.php?res_id=<?php echo $res_id; ?>" frameborder="0" allow="autoplay" scrolling="no" onload="resizeIframe(this)"></iframe>

            </div>
        </section>
        </div>

        <!-- Back to top -->
        <div id="back-to-top"></div>
        <!-- /Back to top -->

        <!-- Preloader -->
        <div id="preloader">
            <div class="preloader">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <script>
            $('.ui.radio.checkbox')
                .checkbox();
        </script>
        <!-- jQuery Plugins -->
        <script type="text/javascript" src="../js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../js/main.js"></script>
        <script src="ajax/uploadReceipt.js"></script>
        <script>
            function printFrame(id) {
                var frm = document.getElementById(id).contentWindow;
                frm.focus(); // focus on contentWindow is needed on some ie versions
                frm.print();
                return false;
            }
        </script>

    </body>

    </html