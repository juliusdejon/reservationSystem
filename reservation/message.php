<?php

include 'functions.php';
include '../config/mysqli.php';

$res_id = $_GET['res_id'];
@$paypal = $_GET['paypal'];

$real_res_id = base64_decode($res_id);
$checkSQL = "SELECT customer.client_reference_id,reservation_status FROM customer JOIN reservation
             ON customer.client_reference_id = reservation.client_reference_id
             WHERE customer.client_reference_id='$real_res_id' AND reservation.reservation_status='Pending' OR reservation.reservation_status='Booked' ";
$resSQL = $mysqli->query($checkSQL);
$rowSQL = mysqli_fetch_assoc($resSQL);
$client_reference_id_db = $rowSQL['client_reference_id'];

if ($res_id == '') {
    header('Location: index.php');
}
if ($client_reference_id_db) {

} else {
    header('Location: index.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>Villa Alfredo's Reservation System</title>

    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7CVarela+Round" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <!-- Bootstrap -->
    <link type="text/css" rel="stylesheet" href="../css/bootstrap.min.css" />

    <!-- Semantic -->
    <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />
    <!-- Initialize Semantic UI and JQuery to load Calendar Datepicker-->
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/semantic.min.js"></script>


    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="../css/font-awesome.min.css">

    <!-- Icons -->
    <link rel="stylesheet" href="../css/icon.min.css">

    <!-- Custom stlylesheet -->
    <link type="text/css" rel="stylesheet" href="../css/style.css" />
    <link type="text/css" rel="stylesheet" href="../css/styles.css" />
    <script>
  function resizeIframe(obj) {
    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
  }
</script>
     <script type="text/javascript">
                    function readURL(input) {
                        if (input.files && input.files[0]) {
                            var reader = new FileReader();

                            reader.onload = function(e) {
                                $('#blah').attr('src', e.target.result);
                            }

                            reader.readAsDataURL(input.files[0]);
                        }
                    }
                </script>
</head>

<body>
    <!-- Header -->
    <header>
        <!-- Nav -->
        <nav id="nav" class="navbar">
            <div class="container">

                <div class="navbar-header">
                    <!-- Logo -->
                    <div class="navbar-brand">
                        <a href="../index.php">
                            <img class="logo" src="../img/valogo-alt.png" alt="logo">
                            <img class="logo-alt" src="../img/valogo-alt.png" alt="logo">
                        </a>
                    </div>
                    <!-- /Logo -->

                    <!-- Collapse nav button -->
                    <div class="nav-collapse">
                        <span></span>
                    </div>
                    <!-- /Collapse nav button -->
                </div>

                <!--  Main navigation  -->
                <ul class="main-nav nav navbar-nav navbar-right">
                    <li><a href="../#home">Home</a></li>
                    <li><a href="../#accomodation">Accomodation</a></li>
                    <li><a href="../#day-tour-cottages">Cottages</a></li>
                    <li><a href="photogallery.php">Photo Gallery</a></li>
                    <li><a href="#contact">Contact</a></li>
                    <li><a href="index.php"><button class="secondary-btn">Book Now</button></a></li>
                </ul>
                <!-- /Main navigation -->

            </div>
        </nav>
        <!-- /Nav -->
    </header>
    <section class="xs-padding">
        <div class="ui message container">
        <div id="result"></div>

            <div class="row">
                <div class="col-md-8">
                <?php if ($paypal == true) {
    ?>

  <?php
include 'paypalConfig.php';

    $real_res_id = base64_decode($res_id);

    $sql = "SELECT * FROM customer WHERE client_reference_id='$real_res_id'";

    $res = $mysqli->query($sql);

    while ($rows = mysqli_fetch_assoc($res)) {
        $client_reference_id = $rows['client_reference_id'];
        $no_of_adults = $rows['no_of_adults'];
        $no_of_kids = $rows['no_of_kids'];
        $arrival_date = $rows['arrival_date'];
        $departure_date = $rows['departure_date'];
        $departure = strtotime($departure_date);
        $arrival = strtotime($arrival_date);
        $datediff = $departure - $arrival;
        $noofNights = round($datediff / (60 * 60 * 24));
    }

    $sql = "SELECT * FROM reserved_rooms
    JOIN customer ON
    reserved_rooms.client_reference_id = customer.client_reference_id
    JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
    WHERE customer.client_reference_id ='$real_res_id'
    GROUP BY reserved_rooms.room_type_id";

    $res = $mysqli->query($sql);
    $total = 0;
    $subtotal = [];
    while ($rows = mysqli_fetch_assoc($res)) {
        $room_type_id = $rows['room_type_id'];
        $sql1 = "SELECT COUNT(*) as quantity FROM reserved_rooms
  JOIN customer ON
  reserved_rooms.client_reference_id = customer.client_reference_id
  JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
  WHERE customer.client_reference_id ='$real_res_id' AND room_type_id ='$room_type_id'";
        $res1 = $mysqli->query($sql1);
        $row1 = mysqli_fetch_assoc($res1);
        ?>
              <?php $total = $row1['quantity'] * $rows['room_price'];?>
    <?php
array_push($subtotal, $total);

    }?>

    
<?php

$extrasTotal = [];
$sql = "SELECT * FROM customer_extras
JOIN extras ON customer_extras.extras_id = extras.extras_id
WHERE client_reference_id = '$real_res_id' AND extras_category='Rental'
GROUP BY customer_extras.extras_id";

$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $extras_id = $rows['extras_id'];
    $extras_name = $rows['extras_name'];
    $extras_price = $rows['extras_price'];
    $sqlCount = "SELECT COUNT(*) as extras_qty FROM customer_extras WHERE extras_id='$extras_id'";
    $resCount = $mysqli->query($sqlCount);
    $rowCount = mysqli_fetch_array($resCount);
    $extras_qty = $rowCount['extras_qty'];
    ?>

<?php
 array_push($extrasTotal, $extras_qty * $extras_price);
}

$extrasTotal = array_sum($extrasTotal);
    $grandTotal = array_sum($subtotal);
    $totalKid = $no_of_kids * 150;
    $totalAdult = $no_of_adults * 250;
    $grandTotal = $grandTotal * $noofNights + $totalAdult + $totalKid;
    $grandTotal= $grandTotal + $extrasTotal;
    $paypalAmount = $grandTotal / 2;

    ?>
      <div class="ui green header">
  Amount to be Paid:
  <span class="ui tag labels">
  <a class="ui label teal">
    ₱ <?php echo number_format($paypalAmount, 2); ?>
  </a>
</span>

 </div>
  <form action="<?php print $payment_url;?>" method="post" name="frmPayPal1">
        <input type="hidden" name="cancel_return" value="<?php print $cancel_return;?>">
        <input type="hidden" name="return" value="<?php print $return_url . $_GET['res_id'];?>">
        <input type="hidden" name="business" value="<?php print $merchant_email;?>">
        <input type="hidden" name="cmd" value="_xclick">
        <input type="hidden" name="item_name" value="<?php print $_GET['res_id'];?>">
        <input type="hidden" name="credits" value="510">
        <input type='hidden' name='rm' value='2'>
        <input type="hidden" name="userid" value="1">
        <input type="hidden" name="amount" value="<?php print $paypalAmount;?>">
        <input type="hidden" name="cpp_header_image" value=""> <!-- for header in paypal -->
        <input type="hidden" name="no_shipping" value="1">
        <input type="hidden" name="currency_code" value="PHP">
        <input type="hidden" name="handling" value="0">
        <!-- <span style="color: #424242; font-size: 15px;margin: 25px;"> </span> -->
       <!-- <button class = ""  type = "submit" name = "submit" value = "Pay Now">
       </button> -->

    <div class="ui image">
      <input type="image" alt="PayPal - The safer, easier way to pay online!" name="submit" src="../img/paypal.png" style="width: 200px;"/>

    </div>



  </div>
       </form>
</div>

                <?php } else {?>
  <div class="ui teal header">
  Steps in paying thru BDO Bank
  </div>
  <p>We've Sent you an Email with step by step instruction on how to pay for your reservation</p>
  <!-- <ul class="list">
  <li>Step 1: Go to any BDO branches to deposit half of the payment</li>
  <li>Step 2: Open your email account to see your reservation details</li>
  <li>Step 3: Deposit 50% of the Total to the Bank Account number we sent thru email</li>
  <li>Step 4: After depositing upload a photo of the Receipt issued by  the BDO</li>
  </ul> -->
  Step 1: Review your Reservation Details Below and take note of the Downpayment Amount<br>
  Step 2: Go to any BDO branches to deposit <b>50%</b> of your Payment<br>
  Step 3: Upload your Deposit Receipt <a href="./upload_receipt.php">here</a><br>
  Step 4: Print the Reservation Details. or Take note of your Reference ID<br>
</div>


<?php }?>
</div>
</div>
                <div class="ui segment container">
                  <button class="ui teal button" onclick="printFrame('receipt')">Print Reservation Details</button>
                <iframe id="receipt" class="ui container fluid" src="reservation-details.php?res_id=<?php echo $res_id; ?>" frameborder="0" allow="autoplay" scrolling="no" onload="resizeIframe(this)"></iframe>

  </div>
    </section>
    </div>

    <!-- Back to top -->
    <div id="back-to-top"></div>
    <!-- /Back to top -->

    <!-- Preloader -->
    <div id="preloader">
        <div class="preloader">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
    <script>
        $('.ui.radio.checkbox')
            .checkbox();
    </script>
    <!-- jQuery Plugins -->
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/main.js"></script>
    <script src="ajax/uploadReceipt.js"></script>
<script>

  function printFrame(id) {
            var frm = document.getElementById(id).contentWindow;
            frm.focus();// focus on contentWindow is needed on some ie versions
            frm.print();
            return false;
}
  </script>

</body>

</html

