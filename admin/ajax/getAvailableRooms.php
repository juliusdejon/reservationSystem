<script src="plugins/jquery/jquery.min.js"></script>
<?php
include '../../config/mysqli.php';
$arrival_date = $_POST['start'];
$departure_date = $_POST['end'];

$arrival_date = date_create_from_format('F j, Y', $arrival_date);
$arrival_date = $arrival_date->format('Y-m-d');

$departure_date = date_create_from_format('F j, Y', $departure_date);
$departure_date = $departure_date->format('Y-m-d');

$client_reference_id = $_POST['ref_id'];

// Get the rang of dates
function date_range($first, $last, $step, $output_format)
{
    $dates = array();
    $current = strtotime($first);
    $last = strtotime($last);
    while ($current <= $last) {
        $dates[] = date($output_format, $current);
        $current = strtotime($step, $current);
    }
    return $dates;
}

?>


<table class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                            <thead>
                                                <tr role="row">

                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending">Room Name</th>
                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Room Description</th>
                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Room Price</th>
                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Quantity</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                        <?php
// Select room type eg. Ana Villa, Mike Hotel
$res = $mysqli->query('SELECT * FROM room_type WHERE isDeleted=0');
while ($rows = mysqli_fetch_assoc($res)) {
    $roomType = $rows['room_id'];
    $room_name = $rows['room_name'];
    $room_info = $rows['room_info'];
    $room_price = $rows['room_price'];
    // Get the Input dates provided by the customer at the front end eg. he clicked July 24 - 26
    $range = date_range($arrival_date, $departure_date, "+1 day", "Y-m-d");
    // Slice it to July 24 - 25
    $reserved_period = array_slice($range, 0, -1);

    // Here you are selecting 'RESERVED ROOMS!!' AND ONLY WITH A STATUS THAT HAS BEEN CONFIRMED AND BOOKED REFLECTED by the given Dates on the site
    $s = "SELECT * FROM reserved_rooms JOIN customer ON customer.client_reference_id = reserved_rooms.client_reference_id
        JOIN reservation ON reserved_rooms.client_reference_id = reservation.client_reference_id
        WHERE reserved_rooms.room_type_id = '$roomType' AND (reservation_status='Booked'  OR reservation_status='Checked In')";
    // Selecting own Rooms

    $z = "SELECT * FROM customer JOIN reserved_rooms ON customer.client_reference_id = reserved_rooms.client_reference_id
    JOIN reservation ON reserved_rooms.client_reference_id = reservation.client_reference_id
    WHERE reserved_rooms.room_type_id = '$roomType' AND (reservation_status='Booked'  OR reservation_status='Checked In')  AND reserved_rooms.client_reference_id='$client_reference_id'";
    $r = $mysqli->query($s);
    $rz = $mysqli->query($z);

    $reserved_rooms = 0;
    $my_selected_rooms = 0;
    while ($ro = mysqli_fetch_assoc($r)) {
        $arrival_date_db = $ro['arrival_date'];
        $departure_date_db = $ro['departure_date'];
        // -1 to the Departure date. July 26 becomes July 25... so it will not consider 26 as still Reserved date.
        $departure_date_db = date('Y-m-d', (strtotime('-1 day', strtotime($departure_date_db))));
        // Get the Database Arrival Date and Departure Date of Ana Villa Room type that has been Reserved
        $rangeDb = date_range($arrival_date_db, $departure_date_db, "+1 day", "Y-m-d");

        $roomNotAvailable = 'not existing';
        foreach ($reserved_period as $day) {
            // Check the reserved period above an Array of dates
            // eg. $reservation_period = [2018-07-25,2018-07-25] // These dates will be compared at the Database
            // rangeDb
            if (in_array($day, $rangeDb)) {
                $roomNotAvailable = 'existing';
            }
        }
        if ($roomNotAvailable == 'existing') {
            // array_push($reserved_rooms, 1);
            $reserved_rooms++;
        }
    }

    while ($ros = mysqli_fetch_assoc($rz)) {
        $arrival_date_db = $ros['arrival_date'];
        $departure_date_db = $ros['departure_date'];
        // Get the Database Arrival Date and Departure Date of Ana Villa Room type that has been Reserved
        $rangeDb = date_range($arrival_date_db, $departure_date_db, "+1 day", "Y-m-d");
        $found = 'false';
        foreach ($reserved_period as $day) {
            // Check the reserved period above an Array of dates
            // eg. $reservation_period = [2018-07-25,2018-07-25] // These dates will be compared at the Database
            // rangeDb
            if (in_array($day, $rangeDb)) {
                $found = 'true';
            }
        }
        if ($found == 'true') {
            // array_push($my_selected_rooms, 1);
            $my_selected_rooms++;
        }
    }

    // $reserved_rooms = array_sum($reserved_rooms);
    // $my_selected_rooms = array_sum($my_selected_rooms);

    $sql2 = "SELECT COUNT(*) as stayedRoom FROM reserved_rooms
             JOIN customer ON customer.client_reference_id = reserved_rooms.client_reference_id
             WHERE customer.client_reference_id='$client_reference_id' AND room_type_id='$roomType'";

    // Available Rooms = Total Rooms - Reserved Rooms IN Check In Date
    $hey = "SELECT COUNT(*) as total_room FROM rooms WHERE room_type_id='$roomType'";
    $reshey = $mysqli->query($hey);
    while ($result_n = mysqli_fetch_assoc($reshey)) {
        $res_online = $result_n['total_room'];
    }
    $result = $res_online - $reserved_rooms;

    ?>
      <tr>
        <td><?php echo $room_name; ?></td>
        <td><?php echo $room_info; ?></td>
        <td><?php echo $room_price; ?></td>
        <td>
          <?php

    if ($result == 0 || $result < 0) {
        echo "<a class='ui red right ribbon label'>Not Available</a>";
        $selectQuery = "SELECT COUNT(*) as alreadySelected FROM reserved_rooms WHERE client_reference_id = '$client_reference_id' AND room_type_id='$roomType'";
        $resultQuery = $mysqli->query($selectQuery);
        $rowsQuery = mysqli_fetch_assoc($resultQuery);
        if ($rowsQuery['alreadySelected'] > 0) {
            echo "<script>
        $( document ).ready(function() {
                $('#continuex').val('no');
        });
            </script>";
        } else {
            echo "<script>
        $('#continuex').val('yes');
    </script>";
        }

    } else if ($my_selected_rooms == 1 && $result == 1) {
        echo "<a class='ui green right ribbon label'>Selected</a>";
    } else {
        echo "<script>
        $('#continuex').val('yes');
    </script>";
        ?>
        <input type="hidden" name='room_id<?php echo $roomType; ?>' value="<?php echo $roomType; ?>" />
        <select class="ui teal button" name="quantity<?php echo $roomType; ?>">
                                            <option value="0" selected>0</option>
                                            <?php

        for ($i = 1; $i <= $result; $i++) {
            echo "<option value='$i'>$i</option>";
        }

    }
    ?>
                                        </select>
                           </td>
      </tr>

<?php }?>


</tbody>
</table>




