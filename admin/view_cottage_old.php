<?php
include '../config/mysqli.php';
$cottage_id = $_GET['cottage_id'];
$sql = "SELECT * FROM cottage_type WHERE cottage_type_id = '$cottage_id'";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $cottage_name = $rows['cottage_name'];
    $cottage_img = $rows['cottage_img'];
}
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>Villa Alfredo Admin</title>

        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/adminlte.min.css">
        <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap4.css">
          <!-- Semantic UI -->
    <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />

        
        
    </head>

    <body class="hold-transition sidebar-mini">
        <div class="wrapper">

            <?php
include 'layout/navbar.php';
include 'layout/sidebar.php';
?>
                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <div class="content-header">
                        <div class="container-fluid">
                            <div class="row mb-2">
                                <div class="col-sm-6">
                                    <h3 class="m-0 text-dark">
                                        <?php echo $cottage_name; ?>
                                    </h3>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->
                    </div>
                    <!-- /.content-header -->
                    <script type="text/javascript">
                        function readURL(input) {
                            if (input.files && input.files[0]) {
                                var reader = new FileReader();

                                reader.onload = function(e) {
                                    $('#blah').attr('src', e.target.result);
                                }

                                reader.readAsDataURL(input.files[0]);
                            }
                        }
                    </script>
                    <!-- Main content -->
                    <div class="content">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-header no-border" style="background: url(dist/img/<?php echo $cottage_img; ?>);
                                height: 350px;
                                background-repeat: no-repeat;
                                width: 100%;
                                background-size: cover;
                                background-position: center center;">
                                        <div class="card-tools">
                                        </div>
                                    </div>
                                    <div class="card-body p-0">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card card-teal">
                                    <div class="card-header">
                                        <h3 class="card-title">Add Cottages</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <!-- form start -->
                                    <form role="form" action="" method="post">
                                        <div class="card-body">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Cottage Number</label>
                                                    <input type="text" name="cottage_number" class="form-control" id="exampleInputEmail1" placeholder="101">
                                                    <input type="hidden" name="cottage_id" value="<?php echo $cottage_id; ?>" />
                                                </div>

                                            </div>
                                        </div>
                                        <!-- /.card-body -->

                                        <div class="card-footer">
                                            <button type="submit" class="btn btn-teal" name="submit">Add</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9">
                            <div class="card">
                                <div class="card-header no-border">
                                    <h3 class="card-title">Cottage</h3>
                                    <div class="card-tools">
                                    </div>
                                </div>
                                <div class="card-body p-0">
                                    <table class="table table-valign-middle">
                                        <thead>
                                            <tr>
                                                <th>Cottage Number</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
$res = $mysqli->query("SELECT * FROM cottages WHERE cottage_type_id='$cottage_id'");
while ($rows = mysqli_fetch_assoc($res)) {
    ?>
                                                <tr>
                                        <td><?php echo $rows['cottage_number']; ?></td>
                                                    <td>
                                                        <div>
                                                            <a href="view_cottage.php?cottage_id=<?php echo $rows['cottage_id']; ?>" class="text-muted">
                                                                <buton class="btn btn-sm bg-info"><i class="fa fa-search"></i></button>
                                                            </a>
                                                            <a href="edit_cottage.php" class="text-muted">
                                                                <buton class="btn btn-sm text-muted"><i class="fa fa-pencil"></i></button>
                                                            </a>
                                                    </td>
                                                </tr>
                                                <?php
}
?>

                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- /.content -->
                </div>
                <!-- /.content-wrapper -->

                <!-- Main Footer -->
                <?php
include 'layout/footer.php';
?>
                    <!-- ./wrapper -->

                    <!-- REQUIRED SCRIPTS -->
                    <!-- jQuery -->
                    <script src="plugins/jquery/jquery.min.js"></script>
                    <script src="plugins/datatables/jquery.dataTables.js"></script>
                    <script src="plugins/datatables/dataTables.bootstrap4.js"></script>
                    <!-- Bootstrap 4 -->
                    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
                    <!-- AdminLTE App -->
                    <script src="dist/js/adminlte.min.js"></script>
    </body>

    </html>

    <?php

if (isset($_POST['submit'])) {
    $cottage_id = $_POST['cottage_id'];
    $cottage_number = $_POST['cottage_number'];

    $sql = "SELECT cottage_number FROM cottages WHERE cottage_type_id='$cottage_id'";
    $res = $mysqli->query($sql);
    $row = mysqli_fetch_assoc($res);
    $cottage_number_db = $row['cottage_number'];
    if ($cottage_number == '') {
        echo "<script>alert('Please enter a Room Number');</script>";
    } else {
        if ($cottage_number == $cottage_number_db) {
            echo "<script>alert('Room Number already Exists');</script>";
        } else {
            $sql = "INSERT INTO cottages (cottage_type_id,cottage_number) VALUES('$cottage_id','$cottage_number')";
            if ($mysqli->query($sql)) {
                echo "<script>alert('Sucessfully Added');</script>";

                echo "<script>
                window.location.href('view_rooms.php?<?php echo $cottage_id;?>')
                if ( window.history.replaceState ) {
                    window.history.replaceState( null, null, window.location.href('view_rooms.php?<?php echo $cottage_id;?>') );
                }
            </script>";
            }
        }
    }
}