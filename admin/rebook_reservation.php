<?php
$rebook_id = $_GET['res_id'];
@$no_room = $_GET['no_room'];
include '../config/mysqli.php';

$sql = "SELECT * FROM reservation
        JOIN customer on reservation.client_reference_id = customer.client_reference_id
        WHERE reservation.client_reference_id = '$rebook_id'";
$res = $mysqli->query($sql);
while ($rows = $res->fetch_assoc()) {
    $reservation_status = $rows['reservation_status'];
    $first_name = $rows['first_name'];
    $last_name = $rows['last_name'];
    $email = $rows['email_address'];
    $contact = $rows['contact_number'];
    $no_of_kids = $rows['no_of_kids'];
    $no_of_adults = $rows['no_of_adults'];
    $arrival_date = $rows['arrival_date'];
    $departure_date = $rows['departure_date'];
}
if ($reservation_status != 'Booked') {
    header('Location: rebooking.php?rebooking=false');
}

// Get the rang of dates
function date_range($first, $last, $step, $output_format)
{
    $dates = array();
    $current = strtotime($first);
    $last = strtotime($last);
    while ($current <= $last) {
        $dates[] = date($output_format, $current);
        $current = strtotime($step, $current);
    }
    return $dates;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Villa Alfredo Admin</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Semantic UI -->
  <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />

         <!-- Initialize Semantic UI and JQuery to load Calendar Datepicker-->
         <script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" src="../js/semantic.min.js"></script>

        <!-- Calendar -->
        <link type="text/css" rel="stylesheet" href="../css/calendar.min.css" />
        <script src="../js/calendar.min.js"></script>

        <!-- Icons -->
        <link rel="stylesheet" href="../css/icon.min.css">



</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

<?php
include 'layout/navbar.php';
include 'layout/sidebar.php';
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Rebook Reservation: <?php echo $rebook_id; ?></h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
    <div class="row">
                            <div class="col-md-8">
                            <?php if ($no_room == true) {
    echo "<div class='callout callout-danger'>
                            <h5>Please Select A Room</h5>
                          </div>";
}?>
                                    <form action="rebook_post.php" name="rebookForm" method="post" onsubmit="return validateForm();">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Rebooking Form </h3>
                                    </div>
                                    <!-- /.card-header -->

                                    <div class="card-body">
                                          <input type="hidden" name="ref_id" id="ref_id" value="<?php echo $rebook_id; ?>">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label>First Name</label>
                                                            <input type="text" name="first_name" class="form-control" disabled value="<?php echo $first_name; ?>">
                                                            <input type="hidden" name="first_name_post" value="<?php echo $first_name; ?>" />
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label>Last Name</label>
                                                            <input type="text" name="last_name" class="form-control" disabled value="<?php echo $last_name; ?>">
                                                            <input type="hidden" name="last_name_post" value="<?php echo $last_name; ?>" />
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label>Contact Number</label>
                                                            <input type="number" name="contact" class="form-control" disabled value="<?php echo $contact; ?>">
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label>Email Address</label>
                                                            <input type="email" name="email" class="form-control" disabled value="<?php echo $email; ?>">
                                                            <input type="hidden" name="email_address" value="<?php echo $email; ?>" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="ui form">
                                                        <div class="two fields">
                                                          <div class="field">
                                                            <label>Arrival Date</label>
                                                            <div class="ui calendar" id="rangestart">
                                                                <div class="ui input left icon">
                                                                    <i class="calendar icon"></i>
                                                                    <input type="text" id="start" placeholder="Start" name="arrival_date_post" autocomplete="off" value="<?php echo $arrival_date; ?>">
                                                                </div>
                                                            </div>
                                                          </div>
                                                          <div class="field">
                                                            <label>Departure Date</label>
                                                            <div class="ui calendar" id="rangeend">
                                                                <div class="ui input left icon">
                                                                    <i class="calendar icon"></i>
                                                                    <input type="text" id="end" placeholder="End" name="departure_date_post" autocomplete="off" value="<?php echo $departure_date; ?>">
                                                                </div>
                                                            </div>
                                                          </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                Adults <input type="number" name="no_of_adults_post" min="0" value="<?php echo $no_of_adults; ?>" />
                                                            </div>
                                                            <div class="col-md-3">
                                                                Kids <input type="number" name="no_of_kids_post" min="0" value="<?php echo $no_of_kids; ?>" />
                                                            </div>
                                                            <div class="col-md-6">
                                                                &nbsp;
                                                                <input type="submit" name="rebook" class="ui fluid teal button" value="Rebook">
                                                            </div>
                                                        </div>
                                        </div>

                                        </div>

                                        </div>

                                    </div>
                                    <!-- /.card-body -->
                                </div>


<div class="card">
<div id="available-rooms">
<table class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                            <thead>
                                                <tr role="row">

                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending">Room Name</th>
                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Room Description</th>
                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Room Price</th>
                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Quantity</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                        <?php
// Select room type eg. Ana Villa, Mike Hotel
$res = $mysqli->query('SELECT * FROM room_type WHERE isDeleted=0');
while ($rows = mysqli_fetch_assoc($res)) {
    $roomType = $rows['room_id'];
    $room_name = $rows['room_name'];
    $room_info = $rows['room_info'];
    $room_price = $rows['room_price'];
    // Get the Input dates provided by the customer at the front end eg. he clicked July 24 - 26
    $range = date_range($arrival_date, $departure_date, "+1 day", "Y-m-d");
    // Slice it to July 24 - 25
    $reserved_period = array_slice($range, 0, -1);

    // Here you are selecting 'RESERVED ROOMS!!' AND ONLY WITH A STATUS THAT HAS BEEN CONFIRMED AND BOOKED REFLECTED by the given Dates on the site
    $s = "SELECT * FROM reserved_rooms JOIN customer ON customer.client_reference_id = reserved_rooms.client_reference_id
        JOIN reservation ON reserved_rooms.client_reference_id = reservation.client_reference_id
        WHERE reserved_rooms.room_type_id = '$roomType' AND (reservation_status='Booked'  OR reservation_status='Checked In')";
    // Selecting own Rooms

    $z = "SELECT * FROM customer JOIN reserved_rooms ON customer.client_reference_id = reserved_rooms.client_reference_id
    JOIN reservation ON reserved_rooms.client_reference_id = reservation.client_reference_id
    WHERE reserved_rooms.room_type_id = '$roomType' AND (reservation_status='Booked'  OR reservation_status='Checked In') AND reserved_rooms.client_reference_id='$rebook_id'";

    $r = $mysqli->query($s);
    $rz = $mysqli->query($z);

    $reserved_rooms = 0;
    $my_selected_rooms = 0;
    while ($ro = mysqli_fetch_assoc($r)) {
        $arrival_date_db = $ro['arrival_date'];
        $departure_date_db = $ro['departure_date'];
        // Get the Database Arrival Date and Departure Date of Ana Villa Room type that has been Reserved
        // -1 to the Departure date. July 26 becomes July 25... so it will not consider 26 as still Reserved date.
        $departure_date_db = date('Y-m-d', (strtotime('-1 day', strtotime($departure_date_db))));
        $rangeDb = date_range($arrival_date_db, $departure_date_db, "+1 day", "Y-m-d");

        $roomNotAvailable = 'not existing';
        foreach ($reserved_period as $day) {
            // Check the reserved period above an Array of dates
            // eg. $reservation_period = [2018-07-25,2018-07-25] // These dates will be compared at the Database
            // rangeDb
            if (in_array($day, $rangeDb)) {
                $roomNotAvailable = 'existing';
            }
        }
        if ($roomNotAvailable == 'existing') {
            // array_push($reserved_rooms, 1);
            $reserved_rooms++;
        }
    }

    while ($ros = mysqli_fetch_assoc($rz)) {
        $arrival_date_db = $ros['arrival_date'];
        $departure_date_db = $ros['departure_date'];
        // Get the Database Arrival Date and Departure Date of Ana Villa Room type that has been Reserved
        $rangeDb = date_range($arrival_date_db, $departure_date_db, "+1 day", "Y-m-d");
        $found = 'false';
        foreach ($reserved_period as $day) {
            // Check the reserved period above an Array of dates
            // eg. $reservation_period = [2018-07-25,2018-07-25] // These dates will be compared at the Database
            // rangeDb
            if (in_array($day, $rangeDb)) {
                $found = 'true';
            }
        }
        if ($found == 'true') {
            // array_push($my_selected_rooms, 1);
            $my_selected_rooms++;
        }
    }

    // $reserved_rooms = array_sum($reserved_rooms);
    // $my_selected_rooms = array_sum($my_selected_rooms);

    $sql2 = "SELECT COUNT(*) as stayedRoom FROM reserved_rooms
             JOIN customer ON customer.client_reference_id = reserved_rooms.client_reference_id
             WHERE customer.client_reference_id='$rebook_id' AND room_type_id='$roomType'";

    // Available Rooms = Total Rooms - Reserved Rooms IN Check In Date
    $sqlAvailableRoom = "SELECT COUNT(*) as TotalRoom FROM rooms WHERE room_type_id='$roomType'";
    $resAvailableRoom = $mysqli->query($sqlAvailableRoom);
    $result = mysqli_fetch_assoc($resAvailableRoom);
    $result = $result['TotalRoom'] - $reserved_rooms;

    ?>
      <tr>
        <td><?php echo $room_name; ?></td>
        <td><?php echo $room_info; ?></td>
        <td>₱<?php echo number_format($room_price, 2); ?></td>
        <td>
          <?php
    if ($result == 0 || $result < 0) {
        echo "<a class='ui red right ribbon label'>Not Available</a>";
    } else if ($my_selected_rooms == 1 && $result == 1) {
        echo "<a class='ui green right ribbon label'>Selected</a>";
    } else {
        ?>
        <input type="hidden" name='room_id<?php echo $roomType; ?>' value="<?php echo $roomType; ?>" />
        <select class="ui teal button" name="quantity<?php echo $roomType; ?>">
                                            <option value="0" selected>0</option>
                                            <?php

        for ($i = 1; $i <= $result; $i++) {
            echo "<option value='$i'>$i</option>";
        }

    }
    ?>
                                        </select>
                           </td>
      </tr>

<?php }?>


</tbody>
</table>

</div>
</div>


                            </div>

                            <div class="col-md-4">
        <div class="card">
<h1 align="center">Rooms Reserved</h1>
<?php
$sql = "SELECT * FROM reserved_rooms JOIN room_type ON reserved_rooms.room_type_id=room_type.room_id WHERE client_reference_id='$rebook_id'
 GROUP BY room_type_id";
$res = $mysqli->query($sql);
$rowCount = mysqli_num_rows($res);
if ($rowCount == 0) {
    echo '<div class="alert alert-danger alert-dismissible">
                  <h5><i class="fa fa-ban"></i> Alert!</h5>
                 No Rooms Found on this Reservation
                 <p>Add a Room immediately</p>
                </div>';
} else {?>
<table border="1" align="center">
  <th style="padding:6px; color: #424242;">Rooms</th>
  <th style="padding:6px; color: #424242;">Quantity</th>
  <th style="padding:6px; color: #424242;">Price </th>
  <th style="padding:6px; color: #424242;">Remove</th>
  <tbody

<?php
while ($rows = mysqli_fetch_assoc($res)) {
    $room_id = $rows['room_id'];
    ?>
      <tr>
        <td><?php echo $rows['room_name']; ?></td>
        <td align="center">x
          <?php
$sql1 = "SELECT COUNT(*) as totalRoom FROM reserved_rooms WHERE room_type_id ='$room_id' AND client_reference_id ='$rebook_id'";
    $res1 = $mysqli->query($sql1);
    $row = mysqli_fetch_assoc($res1);
    $countedRoom = $row['totalRoom'];
    echo $countedRoom;
    ?>
      </td>
      <td align="center">₱ <?php echo number_format($rows['room_price'] * $countedRoom, 2); ?></td>
      <td align="center"><a href="delete_reserved_rooms.php?room_id=<?php echo $room_id; ?>&res_id=<?php echo $rebook_id; ?>"  onclick="return confirm('Are you sure you want to Remove this Room?');" class="ui tiny red button"><i class="fa fa-close"></i><a/>
      </tr>
    <?php
}
    ?>
</tbody>
</table>

<?php }?>
<div id="reserved-rooms">

</div>
      </div>
</div>
                        </div>
                    </div>
                    <input type="hidden" name="continuex" id="continuex" value="yes">
                    </form>

    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
<?php
include 'layout/footer.php';
?>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- Arrival Date -->



<script>
                        const today = new Date();
                        $('#rangestart').calendar({
                            type: 'date',
                            endCalendar: $('#rangeend'),
                            onChange: function (date, text, mode) {
                              $("#available-rooms").html('<img src="ajax/loading-spinner.gif" width="100px">')
                              var start = $("#start").val();
                              var end = $("#end").val();
                              var ref_id = $("#ref_id").val();
                              $.post( "ajax/getAvailableRooms.php", {start:start ,end: end, ref_id: ref_id},function(data) {
                              $( "#available-rooms").html(data);
                                });

                          },
                        });
                        $('#rangeend').calendar({
                            type: 'date',
                            startCalendar: $('#rangestart'),
                            onChange: function (date, text, mode) {
                              $("#available-rooms").html('<img src="ajax/loading-spinner.gif" width="100px">')
                              var start = $("#start").val();
                              var end = $("#end").val();
                              var ref_id = $("#ref_id").val();
                              var jqxhr =$.post( "ajax/getAvailableRooms.php", {start:start ,end: end,ref_id:ref_id},function(data) {
                              $("#available-rooms").html(data);
                            });
                          },
                          onHidden: function () {
                                $.post( "ajax/getAvailableRooms.php", {start:start ,end: end,ref_id:ref_id},function(data) {
                              $("#available-rooms").html(data);
                            });
                            },
                        });
                    </script>



<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>

<script>

                        // Validation Payment form
                        function validateForm() {
                            var continuex = document.forms["rebookForm"]["continuex"].value;
                            var arrivalDate = document.forms["rebookForm"]["arrival_date_post"].value;
                            var departureDate = document.forms["rebookForm"]["departure_date_post"].value;
                            if (continuex == 'no') {
                                alert("Unable to Transfer Dates.The Rooms Reserved has no Available Rooms.");
                                return false;
                            }
                            if(arrivalDate == "") {
                            alert("Select Arrival Date");
                            return false;
                                }
                            if(departureDate =="") {
                                alert("Select Departure Date");
                                return false;
                            }
                            if(arrivalDate == departureDate) {
                                alert("Enter range of Dates");
                                return false;
                            }
                        }
                    </script>


<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
</body>
</html>

