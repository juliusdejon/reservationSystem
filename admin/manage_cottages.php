<?php
@$deleted = $_GET['deleted'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Villa Alfredo Admin</title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
    <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap4.css">
 <!-- Semantic UI -->
 <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />

</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <?php
include 'layout/navbar.php';
include 'layout/sidebar.php';
?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h3 class="m-0 text-dark">Manage Cottages</h3>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.content-header -->
                <script type="text/javascript">
                    function readURL(input) {
                        if (input.files && input.files[0]) {
                            var reader = new FileReader();

                            reader.onload = function(e) {
                                $('#blah').attr('src', e.target.result);
                            }

                            reader.readAsDataURL(input.files[0]);
                        }
                    }
                </script>
                <!-- Main content -->
                <div class="content">
                <?php if ($deleted == 'false') {
    echo '<div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        Something went wrong on Admin Side. Contact the Administrator
                                        </div>';
} else {

}?>
        <?php if ($deleted == 'true') {
    echo '<div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        Successfully Deleted Cottages!
                                        </div>';
} else {

}?>
                    <div class="row">
                        <div class="col-md-9">
                            <div class="card">
                                <div class="card-header no-border">
                                    <h3 class="card-title">Cottages</h3>
                                    <div class="card-tools">
                                    </div>
                                </div>
                                <div class="card-body p-0">
                                    <table class="table table-valign-middle">
                                        <thead>
                                            <tr>
                                                <th>Cottage Name</th>
                                                <th>Price</th>
                                                <th>Info</th>
                                                <th>Total</th>
                                                <th>More</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
$res = $mysqli->query('SELECT * FROM cottage_type');
while ($rows = mysqli_fetch_assoc($res)) {
    $cottage_type_id = $rows['cottage_type_id'];
    ?>
                                                <tr>
                                                    <td>
                                                        <img src="../img/<?php echo $rows['cottage_img']; ?>"  class="img-size-64 mr-1">
                                                        <?php echo $rows['cottage_name']; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo '₱' . number_format($rows['cottage_price'], 2) ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $rows['cottage_info']; ?>
                                                    </td>
                                                    <td>
                                                        <?php
$count = "SELECT COUNT(*) as totalCottages FROM cottages WHERE cottage_type_id='$cottage_type_id'";
    $countRes = $mysqli->query($count);
    $row = mysqli_fetch_assoc($countRes);
    echo $row['totalCottages'] ? $row['totalCottages'] : 0;
    ?>

                                                    </td>
                                                    <td>
                                                        <div>
                                                            <a href="view_cottage.php?cottage_id=<?php echo $cottage_type_id; ?>" class="text-muted">
                                                                <buton class="btn btn-sm bg-info"><i class="fa fa-search"></i></button>
                                                            </a>
                                                            <!-- <a href="delete_cottage_type.php?cottage_id=<?php echo $cottage_type_id; ?>" class="text-muted">
                                                                <buton class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                                            </a> -->
                                                    </td>
                                                </tr>
                                                <?php
}
?>


                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card card-teal">
                                    <div class="card-header">
                                        <h3 class="card-title">Add Cottage Type</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <!-- form start -->
                                    <form role="form" action="" method="post" enctype="multipart/form-data">
                                        <div class="card-body">
                                            <div class="form-group">
                                                <div class="div-image">
                                                    <?php
echo "<img width='100' height='100' src='../img/default.png' alt='Default Profile Pic' id='blah'>";
?>
                                                </div>
                                                <label for="exampleInputFile">Image</label>
                                                <div class="input-group">
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" name="file" onchange="readURL(this);">
                                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                                    </div>
                                                    <div class="input-group-append">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Cottage Name</label>
                                                    <input type="text" name="cottage_name" class="form-control alpha-numeric-only" id="exampleInputEmail1" placeholder="eg. BC Open Cottage">
                                                </div>
                                                <div class="form-group">
                                                    <label>Cottage Info</label>
                                                    <textarea class="form-control" rows="3" placeholder="Enter ..." name="cottage_info"></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Price</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">₱</span>
                                                        </div>
                                                        <input type="number" name="cottage_price" class="form-control">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">.00</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.card-body -->

                                        <div class="card-footer">
                                            <button type="submit" class="btn btn-teal" name="submit">Add</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.content -->
                </div>
                <!-- /.content-wrapper -->

                <!-- Main Footer -->
                <?php
include 'layout/footer.php';
?>
                    <!-- ./wrapper -->

                    <!-- REQUIRED SCRIPTS -->
                    <!-- jQuery -->
                    <script src="plugins/jquery/jquery.min.js"></script>
                    <!-- JQuery Validation source -->
                    <script type="text/javascript" src="plugins/jquery/jquery-key-restrictions.min.js"></script>
                    <!-- Actual Validation calling class -->
                    <script type="text/javascript">
                    $(document).ready(function () {
                        $(".letters-only").lettersOnly();
                        $(".numbers-only").numbersOnly();
                        $(".alpha-numeric-only").alphaNumericOnly();
                    });
                    </script>
                    <script src="plugins/datatables/jquery.dataTables.js"></script>
                    <script src="plugins/datatables/dataTables.bootstrap4.js"></script>
                    <!-- Bootstrap 4 -->
                    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
                    <!-- AdminLTE App -->
                    <script src="dist/js/adminlte.min.js"></script>
</body>

</html>

<?php

if (isset($_POST['submit'])) {
    // Enable in Production

    $cottage_name = $_POST['cottage_name'];
    $cottage_price = $_POST['cottage_price'];
    $cottage_info = $_POST['cottage_info'];
    $image = $_FILES['file']['name'];

    if ($cottage_name == '' || $cottage_price == '') {
        echo "<script>alert('Fill up all the Required Fields');</script>";
    } else {

        $sql = "INSERT INTO cottage_type (cottage_name, cottage_info,cottage_price) VALUES ('$cottage_name', '$cottage_info', '$cottage_price')";
        if ($mysqli->query($sql)) {
            // upload
            $target_dir = "../img/";
            $target_file = $target_dir . basename($_FILES["file"]["name"]);
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
            // Check file size
            if ($_FILES["file"]["size"] > 500000) {
                echo "Sorry, your file is too large.";
                $uploadOk = 0;
            }
            // Allow certain file formats
            if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                && $imageFileType != "gif") {
                echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = 0;
            }

            move_uploaded_file($_FILES["file"]["tmp_name"], $target_file);
            $sql = "SELECT MAX(cottage_type_id) AS cottage_type_id FROM cottage_type";
            $res = $mysqli->query($sql);
            $rows = mysqli_fetch_assoc($res);
            $cottageID = $rows['cottage_type_id'];
            $sql = "UPDATE cottage_type SET cottage_img = '$image' WHERE cottage_type_id='$cottageID'";

            if ($mysqli->query($sql)) {
                echo "<script>alert('done');</script>";
            } else {
                echo "<script>alert('error');</script>";
            }
        } else {
            echo "<script>alert('error');</script>";
        }

    }

}