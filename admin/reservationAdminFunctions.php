<?php

include '../config/mysqli.php';

if (isset($_POST['submitBooking'])) {
    if ($_POST['arrivalDate'] == $_POST['departureDate']) {
        echo "<script>alert('Reservation period is per Night, Enter range of Dates');</script>";
        return;
    }
    else {
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $arrival_date_post = $_POST['arrivalDate'];
    $departure_date_post = $_POST['departureDate'];
    $contact = $_POST['contact'];
    $email = $_POST['email'];
    $no_of_adults = $_POST['no_of_adults'];
    $no_of_kids = $_POST['no_of_kids'];
    $no_of_senior_pwd = $_POST['no_of_senior_pwd'];
    // Random Numbers
    $random = substr(str_shuffle(str_repeat("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", 5)), 0, 5);
    // Date format dmy append to refernece ID
    $date = date("dmY");
    $first_name = strtolower($first_name);
    $last_name = strtolower($last_name);
    $first_name = ucfirst($first_name);
    $last_name = ucfirst($last_name);
    $client_reference_id = $first_name[0] . $last_name[0] . $date . $random;

    // Validate First The Room
    $sql = "SELECT * FROM room_type WHERE isDeleted=0";
    $res = $mysqli->query($sql);
    $hasSelectedRoom = [];
    $room = [];
    while ($rows = mysqli_fetch_assoc($res)) {
        $roomType = $rows['room_id'];
        @$_POST['room_id' . $roomType];
        @$isSelected = $_POST['quantity' . $roomType];
        array_push($hasSelectedRoom, $isSelected);
        if ($isSelected != 0) {
            array_push($room, $roomType);
            array_push($room, $isSelected);
        }
    }
    // Validate if a Room has been Selected. If not do not proceed
    $count = 0;
    for ($i = 0; $i < count($hasSelectedRoom); $i++) {
        if ($hasSelectedRoom[$i] != 0) {

        } else {
            $count++;
        }
    }
    if ($count == count($hasSelectedRoom)) {
        echo "<script>alert('Please select a room');</script>";
    } else {
        $arrival_date_post = date_create_from_format('F j, Y', $arrival_date_post);
        $arrival_date_post = $arrival_date_post->format('Y-m-d');

        $departure_date_post = date_create_from_format('F j, Y', $departure_date_post);
        $departure_date_post = $departure_date_post->format('Y-m-d');

        $sql = "INSERT INTO customer (client_reference_id,first_name,last_name,contact_number,email_address,arrival_date,departure_date,no_of_adults,no_of_kids,no_of_senior_pwd)
             VALUES('$client_reference_id', '$first_name', '$last_name', '$contact','$email', '$arrival_date_post','$departure_date_post','$no_of_adults','$no_of_kids','$no_of_senior_pwd')";
        $res = $mysqli->query($sql);

        if ($res) {
            $created_at = date('Y-m-d H:i:s');
            $expires_at = date('Y-m-d', strtotime("+3 days"));
            $time = date("H:i:s");
            $expires_at = $expires_at . ' ' . $time;

            $sql = "INSERT INTO
        reservation (client_reference_id,reservation_type,reservation_status,created_at,expires_at)
        VALUES('$client_reference_id','Walk In', 'Booked','$created_at','$expires_at')";
            $res = $mysqli->query($sql);

            $rooms = array_chunk($room, 2);

            if ($res) {
                for ($i = 0; $i < count($rooms); $i++) {
                    $room_id = $rooms[$i][0];
                    $room_qty = $rooms[$i][1];
                    for ($j = 0; $j < $room_qty; $j++) {
                        $sql1 = "INSERT INTO reserved_rooms (room_type_id,client_reference_id) VALUES('$room_id','$client_reference_id')";
                        $mysqli->query($sql1);
                    }
                }
                header('Location: reservation_payment.php?res_id=' . $client_reference_id);
            } else {
                echo "lol";
            }

        } else {
            echo "lol";
        }
    }
}
}
