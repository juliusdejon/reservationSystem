<?php

class User
{
    /* Properties */
    private $conn;

    /* Get database access */
    public function __construct(\PDO $pdo)
    {
        $this->conn = $pdo;
    }

    /* Login */
    public function login($loginData)
    {
        $stmt = $this->conn->prepare('SELECT 1 FROM admin WHERE username = ? AND password=?');
        $stmt->execute([$loginData['username'], $loginData['password']]);
        return $user = $stmt->fetch();
    }

    /* Logout */
    public function logout()
    {
        session_start();
        session_destroy();
    }
}
