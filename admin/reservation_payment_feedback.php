<?php
include '../config/mysqli.php';
// --Mailing Requirements--
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;

require '../src/Exception.php';
require '../src/PHPMailer.php';
require '../src/SMTP.php';


if (isset($_POST['pay'])) {
    $ref_id = $_POST['ref_id'];
    $downpayment = $_POST['downpayment'];
    $payment_total = $_POST['total'];
    $half_payment = $payment_total / 2;
    $balance = $payment_total - $downpayment;

    $sql = "SELECT * FROM customer WHERE client_reference_id = '$ref_id'";
    $res = $mysqli->query($sql);
    while ($rows = mysqli_fetch_assoc($res)) {
        $first_name = $rows['first_name'];
        $last_name = $rows['last_name'];
        $email_address = $rows['email_address'];
        $arrival_date = $rows['arrival_date'];
        $departure_date = $rows['departure_date'];
        $additional_details = $rows['additional_details'];
        $no_of_adults = $rows['no_of_adults'];
        $no_of_kids = $rows['no_of_kids'];
        $no_of_senior_pwd = $rows['no_of_senior_pwd'];
    }
       // get no of Nights
       $departure = strtotime($departure_date);
       $arrival = strtotime($arrival_date);
       $datediff = $departure - $arrival;

       $noofNights = round($datediff / (60 * 60 * 24));

if($mailing == true) {
    
    if($deployedMailing == true) {
    $from = "villa_alfredo_email@villaalfredo.x10host.com"; // This is an Email Account coming from the x10hos that has been setup on hosting. server only.
    $to = "$email_address,$first_name.' '.$last_name"; // Recepient
    $baseurl = $_SERVER['SERVER_NAME'];

    if ($downpayment >= $half_payment) {
        $mailbody = "
            <div id=':14u' class='ii gt'><div id=':14t' class='a3s aXjCH '><div id='m_-852930379096079456page-wrapper' style='background-color:#ffffff;padding:0px'><div class='adM'>
</div><table style='font-family:'Open Sans',sans-serif;font-size:12px;margin:0px' width='100%' border='0' cellspacing='0'>
<tbody>
<tr>
<td>
<p style='font-size:20px'><span class='il'>Villa</span> <span class='il'>Alfredo</span> Resort</p>
</td>
</tr>
<tr>
<td class='m_-852930379096079456box-grey' style='background-color:#eeeeee;padding:15px 15px 15px 15px;border-bottom-left-radius:0;border-bottom-right-radius:0' valign='top'>
<table width='100%' border='0' cellpadding='0'>
<tbody>
<tr valign='top'>
<td width='65%'><span style='font-size:12px'><strong>Thank you, " . $first_name . " Your booking is Booked.</strong></span></td>
<td style='font-size:12px' width='35%'>
<table width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td align='right'><strong>Reference Number:</strong>" . $ref_id . "</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td class='m_-852930379096079456box-grey' style='background-color:#f8f8f8;padding:0 15px 15px 15px;border-top-left-radius:0;border-top-right-radius:0' valign='top'>
<table width='100%' border='0' cellpadding='0'>
<tbody>
<tr valign='top'>
<td width='65%'>
<ul style='padding:15px 0 0 15px;margin:0'>
<li style='line-height:20px;padding:0;margin:0'>For booking enquires, cancellations or amendments please contact us directly at <a href='mailto:test.villaalfredo@gmail.com' target='_blank'>test.villaalfredo@gmail.com</a> or (045) 455 1397.</li>
</ul>
</td>
<td style='font-size:12px' width='35%'>
<table width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td align='right'>&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td>
<div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Your Booking</strong></div>
<table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td style='padding:1px 10px 1px 20px' valign='top' width='20%'><strong>Guest:</strong></td>
<td style='padding:1px 20px' width='80%'>" . $first_name . " " . $last_name . " </td>
</tr>
<tr>
<td style='padding:1px 10px 1px 20px' valign='top' width='20%'><strong>Details:</strong></td>
<td style='padding:1px 0px 1px 20px' width='80%'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                    <tbody><tr>
                        <td width='75%'>
                        ";
        $sql1 = "SELECT * FROM reserved_rooms
            JOIN customer ON
            reserved_rooms.client_reference_id = customer.client_reference_id
            JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
            WHERE customer.client_reference_id ='$ref_id'
            GROUP BY reserved_rooms.room_type_id";
        $res1 = $mysqli->query($sql1);
        $room_total_1 = 0;
        $room_subtotal_1 = [];
        while ($row1 = mysqli_fetch_assoc($res1)) {
            $room_type_id_mail_1 = $row1['room_type_id'];
            $c1 = "SELECT COUNT(*) as quantity FROM reserved_rooms
              JOIN customer ON
              reserved_rooms.client_reference_id = customer.client_reference_id
              JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
              WHERE customer.client_reference_id ='$ref_id' AND room_type_id ='$room_type_id_mail_1'";
            $cres1 = $mysqli->query($c1);
            $mq1 = mysqli_fetch_assoc($cres1);
            $room_total_1 = $mq1['quantity'] * $mailRrow['room_price'];

            $mailbody .= "
                                " . $row1['room_name'] . " x
                                " . $mq1['quantity'] . ",
                            ";

            $count++;
            array_push($room_subtotal_1, $room_total_1);
        }

        function multiplyDays($room_total_1)
        {
            global $noofNights;
            return ($room_total_1 * $noofNights);
        }
        $room_grandTotal_1 = array_sum(array_map("multiplyDays", $room_subtotal_1));
        $totalAdult = $no_of_adults * 250;
        $totalKid = $no_of_kids * 150;
        $room_grandTotal_1 = $room_grandTotal_1 + $totalAdult + $totalKid;
        $grandestTotal_1 = number_format($room_grandTotal_1, 2);
        $down_1 = number_format($room_grandTotal_1 / 2, 2);

        $mailbody .= "Adult x " . $no_of_adults . ",
            Child x " . $no_of_kids . "";

        $mailbody .= "</td>
                        <td width='25%' valign='top' align='right' style='padding:1px 15px'>PHP " . $grandestTotal_1 . "</td>
                    </tr>
                </tbody></table></td>
</tr>
<tr>
<td style='padding:1px 10px 1px 20px' width='20%'><strong>Check-in:</strong></td>
<td style='padding:1px 20px' width='80%'>" . $arrival_date . " from 14:00</td>
</tr>
<tr>
<td style='padding:1px 10px 1px 20px' width='20%'><strong>Check-out:</strong></td>
<td style='padding:1px 20px' width='80%'>" . $departure_date . " until 12:00</td>
</tr>
</tbody>
</table>
</td>
</tr>

<tr>
<td>
<div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Additional Details</strong></div>
<table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td><table width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody><tr>
<td width='20%' style='padding:1px 10px 1px 20px'><strong>Additional comments:</strong></td>
<td width='80%' style='padding:1px 20px'>" . $additional_details . "</td>
</tr>
</tbody></table></td>
</tr>
</tbody>
</table>
</td>
</tr>

<tr>
<td>
<div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Payment Details</strong></div>
<table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td style='padding:1px 20px' width='50%'><strong>Total Amount Due: ".$grandestTotal."</strong></td>
<td style='padding:1px 15px' align='right' width='50'>PHP " . $grandestTotal_1 . "</td>
</tr>

<tr>
<td style='padding:1px 20px'><strong>Downpayment: ".$down."</strong></td>
<td style='padding:1px 15px;vertical-align:top' align='right'>PHP " . $down_1 . "</td>
</tr>

<tr>
<td colspan='2'></td>
</tr>
</tbody>
</table>
</td>
</tr>

<tr>
<td>
<div style='font-size:14px;margin-top:20px'><strong>Booking Policies</strong></div>
<table style='margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody><tr>
<td><table width='100%' border='0' cellpadding='0' cellspacing='0'>
                    <tbody><tr>
                        <td style='border-bottom:2px solid #eeeeee;border-top:2px solid #eeeeee;padding:10px 20px!important'>
                        <span style='font-size:11px'>
                            <strong>Cancellation:</strong> If cancelled, modified or in case of no-show, no penalty   will be charged.<br>
                           <strong>Other policies:  </strong>Please call 63 045 455-0789 or send an email to <a href='mailto:test.villaalfredo@gmail.com' target='_blank'>test.villaalfredo@gmail.com</a><br> Hotel requires an 'Incidental Deposit' at check-in, which is fully refundable at check-out.<br>
                        </span>
                        </td>
                    </tr>

                </tbody></table></td>
</tr>
</tbody></table></td></tr></tbody>
</table>



<table><tbody><tr>
<td>
<div style='font-size:14px;margin-top:20px'><strong>Villa Alfredos</strong></div>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>

</tbody></table><div class='yj6qo'></div><div class='adL'>
</div></div></div></div>";
        $mailbody .= "
<br/>
<h3>House Rules</h3>
<ol>
<li>  No hard liquor </li>
<li> No eating near the pool. </li>
<li> No firearms allowed. </li>
<li> No littering. </li>
<li> No picking of fruits and flowers. </li>
<li> Wearing sando, t-shirt & pants will not be allowed to swim. </li>
<li> Videoke & swimming are allowed until 12:00 o’clock midnight only. </li>
<li> Loud music is not allowed between 12 o’clock midnight up to 8 o’clock in the morning. </li>
<li> Management provides 24 hours security, we are not responsible for item lost inside the car, please lock your vehicle and secure your entire valuable. </li>
<li> Please pay particular attention to your entire valuable and secure them at all times. 11. No running, diving, shoving or pushing within the pool area. </li>
<li> Right Conduct. We reserve the right to evict/eject any guest or group of guest should we find them engaging in fights, or generally disrupting the peace at  the resort </li>
</ol>
<i>Not allowed: Radio, Component, Speaker, DVD Player, Magic Sing, Electric Stove, Gas Stove, Butane Stove. </i>";
        $subject = "Your Booking is Booked";

        $headers = "From: $from" . "\r\n";
        $headers .= "Reply-To: $from" . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        $resp = mail($to, $subject, $mailbody, $headers);

        $transaction_status = 'Full Payment';

    } else {
        $mailbody = "
            <div id=':14u' class='ii gt'><div id=':14t' class='a3s aXjCH '><div id='m_-852930379096079456page-wrapper' style='background-color:#ffffff;padding:0px'><div class='adM'>
</div><table style='font-family:'Open Sans',sans-serif;font-size:12px;margin:0px' width='100%' border='0' cellspacing='0'>
<tbody>
<tr>
<td>
<p style='font-size:20px'><span class='il'>Villa</span> <span class='il'>Alfredo</span> Resort</p>
</td>
</tr>
<tr>
<td class='m_-852930379096079456box-grey' style='background-color:#eeeeee;padding:15px 15px 15px 15px;border-bottom-left-radius:0;border-bottom-right-radius:0' valign='top'>
<table width='100%' border='0' cellpadding='0'>
<tbody>
<tr valign='top'>
<td width='65%'><span style='font-size:12px'><strong>Thank you, " . $first_name . " Your booking is pending.</strong></span></td>
<td style='font-size:12px' width='35%'>
<table width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td align='right'><strong>Reference Number:</strong>" . $ref_id . "</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td class='m_-852930379096079456box-grey' style='background-color:#f8f8f8;padding:0 15px 15px 15px;border-top-left-radius:0;border-top-right-radius:0' valign='top'>
<table width='100%' border='0' cellpadding='0'>
<tbody>
<tr valign='top'>
<td width='65%'>
<ul style='padding:15px 0 0 15px;margin:0'>
<li style='line-height:20px;padding:0;margin:0'>To secure your reservation, Please pay the stated amount below on our Primary account BDO Account Number: 00-131-067-3959 and upload the receipt here <a href='" . $baseurl . "/reservation/upload_receipt.php' target='_blank'>Upload Receipt</a></li>
<li style='line-height:20px;padding:0;margin:0'>For booking enquires, cancellations or amendments please contact us directly at <a href='mailto:test.villaalfredo@gmail.com' target='_blank'>test.villaalfredo@gmail.com</a> or (045) 455 1397.</li>
</ul>
</td>
<td style='font-size:12px' width='35%'>
<table width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td align='right'>&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td>
<div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Your Booking</strong></div>
<table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td style='padding:1px 10px 1px 20px' valign='top' width='20%'><strong>Guest:</strong></td>
<td style='padding:1px 20px' width='80%'>" . $first_name . " " . $last_name . " </td>
</tr>
<tr>
<td style='padding:1px 10px 1px 20px' valign='top' width='20%'><strong>Details:</strong></td>
<td style='padding:1px 0px 1px 20px' width='80%'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                    <tbody><tr>
                        <td width='75%'>
                        ";
        $sqlD = "SELECT * FROM reserved_rooms
            JOIN customer ON
            reserved_rooms.client_reference_id = customer.client_reference_id
            JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
            WHERE customer.client_reference_id ='$ref_id'
            GROUP BY reserved_rooms.room_type_id";
        $resD = $mysqli->query($sqlD);
        $room_total_d = 0;
        $room_subtotal_d = [];
        while ($rowD = mysqli_fetch_assoc($resD)) {
            $room_type_id_d = $rowD['room_type_id'];
            $csqlD = "SELECT COUNT(*) as quantity FROM reserved_rooms
              JOIN customer ON
              reserved_rooms.client_reference_id = customer.client_reference_id
              JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
              WHERE customer.client_reference_id ='$ref_id' AND room_type_id ='$room_type_id_d'";
            $resresD = $mysqli->query($csqlD);
            $rowrowD = mysqli_fetch_assoc($resresD);
            $room_total_d = $rowrowD['quantity'] * $rowD['room_price'];

            $mailbody .= "
                                " . $rowD['room_name'] . " x
                                " . $rowrowD['quantity'] . ",
                            ";

            $count++;
            array_push($room_subtotal_d, $room_total_d);
        }

        function multiplyDays($room_total_d)
        {
            global $noofNights;
            return ($room_total_d * $noofNights);
        }
        $room_grandTotal_d = array_sum(array_map("multiplyDays", $room_subtotal_d));
        $totalAdult = $no_of_adults * 250;
        $totalKid = $no_of_kids * 150;
        $grandTotal_d = $room_grandTotal_d + $totalAdult + $totalKid;
        $grandestTotal_d = number_format($grandTotal_d, 2);
        $down_d = number_format($grandTotal_d / 2, 2);

        $mailbody .= "Adult x " . $no_of_adults . ",
            Child x " . $no_of_kids . "";

        $mailbody .= "</td>
                        <td width='25%' valign='top' align='right' style='padding:1px 15px'>PHP " . $grandestTotal_d . "</td>
                    </tr>
                </tbody></table></td>
</tr>
<tr>
<td style='padding:1px 10px 1px 20px' width='20%'><strong>Check-in:</strong></td>
<td style='padding:1px 20px' width='80%'>" . $arrival_date . " from 14:00</td>
</tr>
<tr>
<td style='padding:1px 10px 1px 20px' width='20%'><strong>Check-out:</strong></td>
<td style='padding:1px 20px' width='80%'>" . $departure_date . " until 12:00</td>
</tr>
</tbody>
</table>
</td>
</tr>

<tr>
<td>
<div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Additional Details</strong></div>
<table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td><table width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody><tr>
<td width='20%' style='padding:1px 10px 1px 20px'><strong>Additional comments:</strong></td>
<td width='80%' style='padding:1px 20px'>" . $additional_details . "</td>
</tr>
</tbody></table></td>
</tr>
</tbody>
</table>
</td>
</tr>

<tr>
<td>
<div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Payment Details</strong></div>
<table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td style='padding:1px 20px' width='50%'><strong>Total Amount Due: ".$grandestTotal."</strong></td>
<td style='padding:1px 15px' align='right' width='50'>PHP " . $grandestTotal_d . "</td>
</tr>

<tr>
<td style='padding:1px 20px'><strong>Downpayment: ".$down."</strong></td>
<td style='padding:1px 15px;vertical-align:top' align='right'>PHP " . $down_d . "</td>
</tr>

<tr>
<td colspan='2'></td>
</tr>
</tbody>
</table>
</td>
</tr>

<tr>
<td>
<div style='font-size:14px;margin-top:20px'><strong>Booking Policies</strong></div>
<table style='margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody><tr>
<td><table width='100%' border='0' cellpadding='0' cellspacing='0'>
                    <tbody><tr>
                        <td style='border-bottom:2px solid #eeeeee;border-top:2px solid #eeeeee;padding:10px 20px!important'>
                        <span style='font-size:11px'>
                            <strong>Cancellation:</strong> If cancelled, modified or in case of no-show, no penalty   will be charged.<br>
                           <strong>Other policies:  </strong>Please call 63 045 455-0789 or send an email to <a href='mailto:test.villaalfredo@gmail.com' target='_blank'>test.villaalfredo@gmail.com</a><br> Hotel requires an 'Incidental Deposit' at check-in, which is fully refundable at check-out.<br>
                        </span>
                        </td>
                    </tr>

                </tbody></table></td>
</tr>
</tbody></table></td></tr></tbody>
</table>



<table><tbody><tr>
<td>
<div style='font-size:14px;margin-top:20px'><strong>Villa Alfredos</strong></div>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>

</tbody></table><div class='yj6qo'></div><div class='adL'>
</div></div></div></div>";
        $subject = "Reservation Request Step 1";

        $headers = "From: $from" . "\r\n";
        $headers .= "Reply-To: $from" . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        $resp = mail($to, $subject, $mailbody, $headers);

        $transaction_status = 'Partial';
        $mysqli->query("UPDATE reservation SET reservation_status='Pending' WHERE client_reference_id ='$ref_id'");

    }
    $date = date('Y-m-d H:i:s');
    $sql = "INSERT INTO transaction (downpayment,total,transaction_type,transaction_status,client_reference_id,check_in_date,balance)
                            VALUES('$downpayment', '$payment_total','Reservation','$transaction_status','$ref_id','$date','$balance')";
    $res = $mysqli->query($sql);
    if ($res) {
        header('Location: reservation_payment_receipt.php?res_id=' . $ref_id);
    }
} else {
    $mail = new PHPMailer(true); // Passing `true` enables exceptions
    //Server settings
    $mail->SMTPDebug = 0; // Enable verbose debug output
    $mail->isSMTP(); // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com'; // Specify main and backup SMTP servers
    $mail->SMTPAuth = true; // Enable SMTP authentication
    $mail->Username = 'test.villaalfredo@gmail.com'; // SMTP username
    $mail->Password = 'reservationsystem'; // SMTP password
    $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587; // TCP port to connect to
    $mail->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true,
        ),
    );
    //Recipients
    $mail->setFrom('test.villaalfredo@gmail.com', 'Villa Alfredo');
    $mail->addAddress($email_address, $first_name . ' ' . $last_name); // Add a recipient
    $baseurl = $_SERVER['SERVER_NAME'];


    if ($downpayment >= $half_payment) {
        $mailbody = "
            <div id=':14u' class='ii gt'><div id=':14t' class='a3s aXjCH '><div id='m_-852930379096079456page-wrapper' style='background-color:#ffffff;padding:0px'><div class='adM'>
</div><table style='font-family:'Open Sans',sans-serif;font-size:12px;margin:0px' width='100%' border='0' cellspacing='0'>
<tbody>
<tr>
<td>
<p style='font-size:20px'><span class='il'>Villa</span> <span class='il'>Alfredo</span> Resort</p>
</td>
</tr>
<tr>
<td class='m_-852930379096079456box-grey' style='background-color:#eeeeee;padding:15px 15px 15px 15px;border-bottom-left-radius:0;border-bottom-right-radius:0' valign='top'>
<table width='100%' border='0' cellpadding='0'>
<tbody>
<tr valign='top'>
<td width='65%'><span style='font-size:12px'><strong>Thank you, " . $first_name . " Your booking is Booked.</strong></span></td>
<td style='font-size:12px' width='35%'>
<table width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td align='right'><strong>Reference Number:</strong>" . $ref_id . "</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td class='m_-852930379096079456box-grey' style='background-color:#f8f8f8;padding:0 15px 15px 15px;border-top-left-radius:0;border-top-right-radius:0' valign='top'>
<table width='100%' border='0' cellpadding='0'>
<tbody>
<tr valign='top'>
<td width='65%'>
<ul style='padding:15px 0 0 15px;margin:0'>
<li style='line-height:20px;padding:0;margin:0'>For booking enquires, cancellations or amendments please contact us directly at <a href='mailto:test.villaalfredo@gmail.com' target='_blank'>test.villaalfredo@gmail.com</a> or (045) 455 1397.</li>
</ul>
</td>
<td style='font-size:12px' width='35%'>
<table width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td align='right'>&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td>
<div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Your Booking</strong></div>
<table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td style='padding:1px 10px 1px 20px' valign='top' width='20%'><strong>Guest:</strong></td>
<td style='padding:1px 20px' width='80%'>" . $first_name . " " . $last_name . " </td>
</tr>
<tr>
<td style='padding:1px 10px 1px 20px' valign='top' width='20%'><strong>Details:</strong></td>
<td style='padding:1px 0px 1px 20px' width='80%'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                    <tbody><tr>
                        <td width='75%'>
                        ";
        $s2 = "SELECT * FROM reserved_rooms
            JOIN customer ON
            reserved_rooms.client_reference_id = customer.client_reference_id
            JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
            WHERE customer.client_reference_id ='$ref_id'
            GROUP BY reserved_rooms.room_type_id";
        $res2 = $mysqli->query($s2);
        $room_total_2 = 0;
        $room_subtotal_2 = [];
        $count_2 = 0;
        while ($row2 = mysqli_fetch_assoc($res2)) {
            $room_type_id_mail_2 = $row2['room_type_id'];
            $c2 = "SELECT COUNT(*) as quantity FROM reserved_rooms
              JOIN customer ON
              reserved_rooms.client_reference_id = customer.client_reference_id
              JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
              WHERE customer.client_reference_id ='$ref_id' AND room_type_id ='$room_type_id_mail_2'";
            $cr2 = $mysqli->query($c2);
            $mq = mysqli_fetch_assoc($cr2);
            $room_total_2 = $mq['quantity'] * $row2['room_price'];

            $mailbody .= "
                                " . $row2['room_name'] . " x
                                " . $mq['quantity'] . ",
                            ";

            $count_2++;
            array_push($room_subtotal_2, $room_total_2);
        }

        function multiplyDays($room_total_2)
        {
            global $noofNights;
            return ($room_total_2 * $noofNights);
        }
        $room_grandTotal_2 = array_sum(array_map("multiplyDays", $room_subtotal_2));
        $totalAdult = $no_of_adults * 250;
        $totalKid = $no_of_kids * 150;
        $room_grandTotal_2 = $room_grandTotal_2 + $totalAdult + $totalKid;
        $grandestTotal_2 = number_format($room_grandTotal_2, 2);
        $down_2 = number_format($room_grandTotal_2 / 2, 2);

        $mailbody .= "Adult x " . $no_of_adults . ",
            Child x " . $no_of_kids . "";

        $mailbody .= "</td>
                        <td width='25%' valign='top' align='right' style='padding:1px 15px'>PHP " . $grandestTotal_2 . "</td>
                    </tr>
                </tbody></table></td>
</tr>
<tr>
<td style='padding:1px 10px 1px 20px' width='20%'><strong>Check-in:</strong></td>
<td style='padding:1px 20px' width='80%'>" . $arrival_date . " from 14:00</td>
</tr>
<tr>
<td style='padding:1px 10px 1px 20px' width='20%'><strong>Check-out:</strong></td>
<td style='padding:1px 20px' width='80%'>" . $departure_date . " until 12:00</td>
</tr>
</tbody>
</table>
</td>
</tr>

<tr>
<td>
<div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Additional Details</strong></div>
<table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td><table width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody><tr>
<td width='20%' style='padding:1px 10px 1px 20px'><strong>Additional comments:</strong></td>
<td width='80%' style='padding:1px 20px'>" . $additional_details . "</td>
</tr>
</tbody></table></td>
</tr>
</tbody>
</table>
</td>
</tr>

<tr>
<td>
<div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Payment Details</strong></div>
<table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td style='padding:1px 20px' width='50%'><strong>Total Amount Due: ".$grandestTotal_2."</strong></td>
</tr>

<tr>
<td style='padding:1px 20px'><strong>Downpayment: ".$down_2."</strong></td>
</tr>

<tr>
<td colspan='2'></td>
</tr>
</tbody>
</table>
</td>
</tr>

<tr>
<td>
<div style='font-size:14px;margin-top:20px'><strong>Booking Policies</strong></div>
<table style='margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody><tr>
<td><table width='100%' border='0' cellpadding='0' cellspacing='0'>
                    <tbody><tr>
                        <td style='border-bottom:2px solid #eeeeee;border-top:2px solid #eeeeee;padding:10px 20px!important'>
                        <span style='font-size:11px'>
                            <strong>Cancellation:</strong> If cancelled, modified or in case of no-show, no penalty   will be charged.<br>
                           <strong>Other policies:  </strong>Please call 63 045 455-0789 or send an email to <a href='mailto:test.villaalfredo@gmail.com' target='_blank'>test.villaalfredo@gmail.com</a><br> Hotel requires an 'Incidental Deposit' at check-in, which is fully refundable at check-out.<br>
                        </span>
                        </td>
                    </tr>

                </tbody></table></td>
</tr>
</tbody></table></td></tr></tbody>
</table>



<table><tbody><tr>
<td>
<div style='font-size:14px;margin-top:20px'><strong>Villa Alfredos</strong></div>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>

</tbody></table><div class='yj6qo'></div><div class='adL'>
</div></div></div></div>";
        $mailbody .= "
<br/>
<h3>House Rules</h3>
<ol>
<li>  No hard liquor </li>
<li> No eating near the pool. </li>
<li> No firearms allowed. </li>
<li> No littering. </li>
<li> No picking of fruits and flowers. </li>
<li> Wearing sando, t-shirt & pants will not be allowed to swim. </li>
<li> Videoke & swimming are allowed until 12:00 o’clock midnight only. </li>
<li> Loud music is not allowed between 12 o’clock midnight up to 8 o’clock in the morning. </li>
<li> Management provides 24 hours security, we are not responsible for item lost inside the car, please lock your vehicle and secure your entire valuable. </li>
<li> Please pay particular attention to your entire valuable and secure them at all times. 11. No running, diving, shoving or pushing within the pool area. </li>
<li> Right Conduct. We reserve the right to evict/eject any guest or group of guest should we find them engaging in fights, or generally disrupting the peace at  the resort </li>
</ol>
<i>Not allowed: Radio, Component, Speaker, DVD Player, Magic Sing, Electric Stove, Gas Stove, Butane Stove. </i>";
         //Content
         $mail->isHTML(true); // Set email format to HTML
         $mail->Subject = 'Your Booking is Booked';
         $mail->Body = $mailbody;
         $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
         $mail->send();
        $transaction_status = 'Full Payment';

    } else {
        $mailbody = "
            <div id=':14u' class='ii gt'><div id=':14t' class='a3s aXjCH '><div id='m_-852930379096079456page-wrapper' style='background-color:#ffffff;padding:0px'><div class='adM'>
</div><table style='font-family:'Open Sans',sans-serif;font-size:12px;margin:0px' width='100%' border='0' cellspacing='0'>
<tbody>
<tr>
<td>
<p style='font-size:20px'><span class='il'>Villa</span> <span class='il'>Alfredo</span> Resort</p>
</td>
</tr>
<tr>
<td class='m_-852930379096079456box-grey' style='background-color:#eeeeee;padding:15px 15px 15px 15px;border-bottom-left-radius:0;border-bottom-right-radius:0' valign='top'>
<table width='100%' border='0' cellpadding='0'>
<tbody>
<tr valign='top'>
<td width='65%'><span style='font-size:12px'><strong>Thank you, " . $first_name . " Your booking is pending.</strong></span></td>
<td style='font-size:12px' width='35%'>
<table width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td align='right'><strong>Reference Number:</strong>" . $ref_id . "</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td class='m_-852930379096079456box-grey' style='background-color:#f8f8f8;padding:0 15px 15px 15px;border-top-left-radius:0;border-top-right-radius:0' valign='top'>
<table width='100%' border='0' cellpadding='0'>
<tbody>
<tr valign='top'>
<td width='65%'>
<ul style='padding:15px 0 0 15px;margin:0'>
<li style='line-height:20px;padding:0;margin:0'>To secure your reservation, Please pay the stated amount below on our Primary account BDO Account Number: 00-131-067-3959 and upload the receipt here <a href='" . $baseurl . "/reservation/upload_receipt.php' target='_blank'>Upload Receipt</a></li>
<li style='line-height:20px;padding:0;margin:0'>For booking enquires, cancellations or amendments please contact us directly at <a href='mailto:test.villaalfredo@gmail.com' target='_blank'>test.villaalfredo@gmail.com</a> or (045) 455 1397.</li>
</ul>
</td>
<td style='font-size:12px' width='35%'>
<table width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td align='right'>&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td>
<div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Your Booking</strong></div>
<table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td style='padding:1px 10px 1px 20px' valign='top' width='20%'><strong>Guest:</strong></td>
<td style='padding:1px 20px' width='80%'>" . $first_name . " " . $last_name . " </td>
</tr>
<tr>
<td style='padding:1px 10px 1px 20px' valign='top' width='20%'><strong>Details:</strong></td>
<td style='padding:1px 0px 1px 20px' width='80%'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                    <tbody><tr>
                        <td width='75%'>
                        ";
        $s3 = "SELECT * FROM reserved_rooms
            JOIN customer ON
            reserved_rooms.client_reference_id = customer.client_reference_id
            JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
            WHERE customer.client_reference_id ='$ref_id'
            GROUP BY reserved_rooms.room_type_id";
        $res3 = $mysqli->query($s3);
        $room_total_3 = 0;
        $room_subtotal_3 = [];
        $count_3 = 0;
        while ($row3 = mysqli_fetch_assoc($res3)) {
            $room_type_id_3 = $row3['room_type_id'];
            $c3 = "SELECT COUNT(*) as quantity FROM reserved_rooms
              JOIN customer ON
              reserved_rooms.client_reference_id = customer.client_reference_id
              JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
              WHERE customer.client_reference_id ='$ref_id' AND room_type_id ='$room_type_id_3'";
            $r3 = $mysqli->query($c3);
            $rowrow3 = mysqli_fetch_assoc($r3);
            $room_total_3 = $rowrow3['quantity'] * $row3['room_price'];

            $mailbody .= "
                                " . $row3['room_name'] . " x
                                " . $rowrow3['quantity'] . ",
                            ";

            $count_3++;
            array_push($room_subtotal_3, $room_total_3);
        }

        function multiplyDays($room_total_3)
        {
            global $noofNights;
            return ($room_total_3 * $noofNights);
        }
        $room_grandTotal_3 = array_sum(array_map("multiplyDays", $room_subtotal_3));
        $totalAdult = $no_of_adults * 250;
        $totalKid = $no_of_kids * 150;
        $grandTotal_3 = $room_grandTotal_3 + $totalAdult + $totalKid;
        $grandestTotal_3 = number_format($grandTotal_3, 2);
        $down_3 = number_format($grandTotal_3 / 2, 2);

        $mailbody .= "Adult x " . $no_of_adults . ",
            Child x " . $no_of_kids . "";

        $mailbody .= "</td>
                        <td width='25%' valign='top' align='right' style='padding:1px 15px'>PHP " . $grandestTotal_3 . "</td>
                    </tr>
                </tbody></table></td>
</tr>
<tr>
<td style='padding:1px 10px 1px 20px' width='20%'><strong>Check-in:</strong></td>
<td style='padding:1px 20px' width='80%'>" . $arrival_date . " from 14:00</td>
</tr>
<tr>
<td style='padding:1px 10px 1px 20px' width='20%'><strong>Check-out:</strong></td>
<td style='padding:1px 20px' width='80%'>" . $departure_date . " until 12:00</td>
</tr>
</tbody>
</table>
</td>
</tr>

<tr>
<td>
<div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Additional Details</strong></div>
<table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td><table width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody><tr>
<td width='20%' style='padding:1px 10px 1px 20px'><strong>Additional comments:</strong></td>
<td width='80%' style='padding:1px 20px'>" . $additional_details . "</td>
</tr>
</tbody></table></td>
</tr>
</tbody>
</table>
</td>
</tr>

<tr>
<td>
<div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Payment Details</strong></div>
<table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td style='padding:1px 20px' width='50%'><strong>Total Amount Due: ".$grandestTotal_3."</strong></td>
</tr>

<tr>
<td style='padding:1px 20px'><strong>Downpayment: ".$down_3."</strong></td>
</tr>

<tr>
<td colspan='2'></td>
</tr>
</tbody>
</table>
</td>
</tr>

<tr>
<td>
<div style='font-size:14px;margin-top:20px'><strong>Booking Policies</strong></div>
<table style='margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody><tr>
<td><table width='100%' border='0' cellpadding='0' cellspacing='0'>
                    <tbody><tr>
                        <td style='border-bottom:2px solid #eeeeee;border-top:2px solid #eeeeee;padding:10px 20px!important'>
                        <span style='font-size:11px'>
                            <strong>Cancellation:</strong> If cancelled, modified or in case of no-show, no penalty   will be charged.<br>
                           <strong>Other policies:  </strong>Please call 63 045 455-0789 or send an email to <a href='mailto:test.villaalfredo@gmail.com' target='_blank'>test.villaalfredo@gmail.com</a><br> Hotel requires an 'Incidental Deposit' at check-in, which is fully refundable at check-out.<br>
                        </span>
                        </td>
                    </tr>

                </tbody></table></td>
</tr>
</tbody></table></td></tr></tbody>
</table>



<table><tbody><tr>
<td>
<div style='font-size:14px;margin-top:20px'><strong>Villa Alfredos</strong></div>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>

</tbody></table><div class='yj6qo'></div><div class='adL'>
</div></div></div></div>";
      //Content
      $mail->isHTML(true); // Set email format to HTML
      $mail->Subject = 'Reservation Request Step 1';
      $mail->Body = $mailbody;
      $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
      $mail->send();

        $transaction_status = 'Partial';
        $mysqli->query("UPDATE reservation SET reservation_status='Pending' WHERE client_reference_id ='$ref_id'");

}
$date = date('Y-m-d H:i:s');
$check = "SELECT * FROM transaction WHERE client_reference_id ='$ref_id'";
$resultcheck= $mysqli->query($check);       
$resultcheck = mysqli_num_rows($resultcheck);
if($resultcheck > 0) {
    $sql_sample = "UPDATE transaction SET downpayment='$downpayment',total='$payment_total',balance='$balance' WHERE client_reference_id='$ref_id'";
} else{
    $sql_sample= "INSERT INTO transaction (downpayment,total,transaction_type,transaction_status,client_reference_id,balance)
    VALUES('$downpayment', '$payment_total','Reservation','$transaction_status','$ref_id','$balance')";
}

$res = $mysqli->query($sql_sample);
if ($res) {
    header('Location: reservation_payment_receipt.php?res_id=' . $ref_id);
}
}
}
 else {
    $date = date('Y-m-d H:i:s');
    $check = "SELECT * FROM transaction WHERE client_reference_id ='$ref_id'";
    $resultcheck = $mysqli->query($check);       
    $resultcheck = mysqli_num_rows($resultcheck);
    if($resultcheck > 0) {
        $sql = "UPDATE transaction SET downpayment='$downpayment',total='$payment_total',balance='$balance' WHERE client_reference_id='$ref_id'";
    } else{
        $sql = "INSERT INTO transaction (downpayment,total,transaction_type,transaction_status,client_reference_id,balance)
        VALUES('$downpayment', '$payment_total','Reservation','$transaction_status','$ref_id','$balance')";
    }
    $res = $mysqli->query($sql);
    if ($res) {
        header('Location: reservation_payment_receipt.php?res_id=' . $ref_id);
    }
}

}