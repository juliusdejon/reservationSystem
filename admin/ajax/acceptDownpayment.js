function foo(id) {
    $("#accept" + id).submit(function(e) {
        e.preventDefault();
        $("#button" + id).html('<img src="ajax/loading-spinner.gif" width="25px">')
        var form = $(this);
        var post_url = form.attr("action");
        var post_data = form.serialize();
        $.ajax({
            type: "POST",
            url: post_url,
            data: post_data,
            cache: false,
            success: function(info) {
                $("#result" + id).html(info);
                $("#button" + id).text('Accept Downpayment');
            },
        });
    });
}