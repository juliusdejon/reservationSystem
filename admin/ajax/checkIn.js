$(document).ready(function() {
    $("input#reference_id").keyup(function() {
        var ref = $("input#reference_id").val();
        if ($.trim(ref) != '') {
            $('#check-in-Form').html('<img src="ajax/loading.gif" width="200px">');
            if (ref != '') {
                $.post('ajax/check-in-Form.php', { variable: ref },
                    function(data) {
                        $('#check-in-Form').html(data);
                    });
            } else {

                $('#check-in-Form').html('');
            }
        } else {
            $('#check-in-Form').html(
                "<div class='bs-example' data-example-id='simple-jumbotron'><div class='jumbotron'><h3>Please Enter Reference ID.</h3><p></p></div></div>");
        }

    });
});