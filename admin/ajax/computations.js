Number.prototype.format = function(n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};


$(document).ready(function() {
    // computation entrance Fee

    var seniorInput = $('#no-of-senior-pwd').val();
    var discount = 250 * 0.20;
    seniorComputation = seniorInput * (250 - discount);
 var entrance = (($('#no-of-adult').val() * 250) + ($('#extra-adults').val() * 250)) + (($('input#no-of-kids').val() * 150) + ($('#extra-kids').val() * 150)) + seniorComputation;
    $('#entrance').text('₱ ' + entrance.format(2));
    $('#entranceTotal').val(entrance);

    // deposit
    var depositTotalInput = $("#depositTotalInput").val() + parseInt($('#roomKey').val());


    // extras

    var extras = $('#extrasHiddenInput').val();

    // computation subtotal
    var subtotal = entrance + parseInt($("#roomTotal").val()) + parseInt(depositTotalInput) + parseInt(extras);
    $("#subtotal").text('₱ ' + subtotal.format(2));
    $("#subTotal").val(subtotal);

    // computated downpayment 
    var downpayment = $("#downpaymentTotal").val();


    // computation balance
    if (downpayment > subtotal) {
        var balance = 0
    } else {
        var balance = subtotal - downpayment;
    }
    $("#balance").text('₱ ' + balance.format(2));
    $("#balanceTotal").val(balance);

});


$('#no-of-senior-pwd').change(function() {
    // computation entrance Fee

    var seniorInput = $('#no-of-senior-pwd').val();
    var discount = 250 * 0.20;
    seniorComputation = seniorInput * (250 - discount);
 var entrance = (($('#no-of-adult').val() * 250) + ($('#extra-adults').val() * 250)) + (($('input#no-of-kids').val() * 150) + ($('#extra-kids').val() * 150)) + seniorComputation;
    $('#entrance').text('₱ ' + entrance.format(2));
    $('#entranceTotal').val(entrance);

    // deposit
    var depositTotalInput = $("#depositTotalInput").val();



    // extras

    var extras = $('#extrasHiddenInput').val();
    // computation subtotal
    var subtotal = entrance + parseInt($("#roomTotal").val()) + parseInt(depositTotalInput) + parseInt(extras);
    $("#subtotal").text('₱ ' + subtotal.format(2));
    $("#subTotal").val(subtotal);

    // computated downpayment 
    var downpayment = $("#downpaymentTotal").val();


    // computation balance
    if (downpayment > subtotal) {
        var balance = 0
    } else {
        var balance = subtotal - downpayment;
    }
    $("#balance").text('₱ ' + balance.format(2));
    $("#balanceTotal").val(balance);

});

$('#no-of-senior-pwd').blur(function() {
    // computation entrance Fee

    var seniorInput = $('#no-of-senior-pwd').val();
    var discount = 250 * 0.20;
    seniorComputation = seniorInput * (250 - discount);
 var entrance = (($('#no-of-adult').val() * 250) + ($('#extra-adults').val() * 250)) + (($('input#no-of-kids').val() * 150) + ($('#extra-kids').val() * 150)) + seniorComputation;
    $('#entrance').text('₱ ' + entrance.format(2));
    $('#entranceTotal').val(entrance);

    // deposit
    var depositTotalInput = $("#depositTotalInput").val();



    // extras

    var extras = $('#extrasHiddenInput').val();
    // computation subtotal
    var subtotal = entrance + parseInt($("#roomTotal").val()) + parseInt(depositTotalInput) + parseInt(extras);
    $("#subtotal").text('₱ ' + subtotal.format(2));
    $("#subTotal").val(subtotal);

    // computated downpayment 
    var downpayment = $("#downpaymentTotal").val();


    // computation balance
    if (downpayment > subtotal) {
        var balance = 0
    } else {
        var balance = subtotal - downpayment;
    }
    $("#balance").text('₱ ' + balance.format(2));
    $("#balanceTotal").val(balance);

});

$('#no-of-senior-pwd').keydown(function() {
    // computation entrance Fee

    var seniorInput = $('#no-of-senior-pwd').val();
    var discount = 250 * 0.20;
    seniorComputation = seniorInput * (250 - discount);
 var entrance = (($('#no-of-adult').val() * 250) + ($('#extra-adults').val() * 250)) + (($('input#no-of-kids').val() * 150) + ($('#extra-kids').val() * 150)) + seniorComputation;
    $('#entrance').text('₱ ' + entrance.format(2));
    $('#entranceTotal').val(entrance);

    // deposit
    var depositTotalInput = $("#depositTotalInput").val();



    // extras

    var extras = $('#extrasHiddenInput').val();
    // computation subtotal
    var subtotal = entrance + parseInt($("#roomTotal").val()) + parseInt(depositTotalInput) + parseInt(extras);
    $("#subtotal").text('₱ ' + subtotal.format(2));
    $("#subTotal").val(subtotal);

    // computated downpayment 
    var downpayment = $("#downpaymentTotal").val();


    // computation balance
    if (downpayment > subtotal) {
        var balance = 0
    } else {
        var balance = subtotal - downpayment;
    }
    $("#balance").text('₱ ' + balance.format(2));
    $("#balanceTotal").val(balance);

});


$('#no-of-senior-pwd').keyup(function() {
    // computation entrance Fee

    var seniorInput = $('#no-of-senior-pwd').val();
    var discount = 250 * 0.20;
    seniorComputation = seniorInput * (250 - discount);
 var entrance = (($('#no-of-adult').val() * 250) + ($('#extra-adults').val() * 250)) + (($('input#no-of-kids').val() * 150) + ($('#extra-kids').val() * 150)) + seniorComputation;
    $('#entrance').text('₱ ' + entrance.format(2));
    $('#entranceTotal').val(entrance);

    // deposit
    var depositTotalInput = $("#depositTotalInput").val();



    // extras

    var extras = $('#extrasHiddenInput').val();
    // computation subtotal
    var subtotal = entrance + parseInt($("#roomTotal").val()) + parseInt(depositTotalInput) + parseInt(extras);
    $("#subtotal").text('₱ ' + subtotal.format(2));
    $("#subTotal").val(subtotal);

    // computated downpayment 
    var downpayment = $("#downpaymentTotal").val();


    // computation balance
    if (downpayment > subtotal) {
        var balance = 0
    } else {
        var balance = subtotal - downpayment;
    }
    $("#balance").text('₱ ' + balance.format(2));
    $("#balanceTotal").val(balance);

});


$('#extra-adults').keydown(function() {
    // computation entrance Fee

    var seniorInput = $('#no-of-senior-pwd').val();
    var discount = 250 * 0.20;
    seniorComputation = seniorInput * (250 - discount);
 var entrance = (($('#no-of-adult').val() * 250) + ($('#extra-adults').val() * 250)) + (($('input#no-of-kids').val() * 150) + ($('#extra-kids').val() * 150)) + seniorComputation;
    $('#entrance').text('₱ ' + entrance.format(2));
    $('#entranceTotal').val(entrance);

    // deposit
    var depositTotalInput = $("#depositTotalInput").val();



    // extras

    var extras = $('#extrasHiddenInput').val();
    // computation subtotal
    var subtotal = entrance + parseInt($("#roomTotal").val()) + parseInt(depositTotalInput) + parseInt(extras);
    $("#subtotal").text('₱ ' + subtotal.format(2));
    $("#subTotal").val(subtotal);

    // computated downpayment 
    var downpayment = $("#downpaymentTotal").val();


    // computation balance
    if (downpayment > subtotal) {
        var balance = 0
    } else {
        var balance = subtotal - downpayment;
    }
    $("#balance").text('₱ ' + balance.format(2));
    $("#balanceTotal").val(balance);

});
$('#extra-adults').change(function() {
    // computation entrance Fee

    var seniorInput = $('#no-of-senior-pwd').val();
    var discount = 250 * 0.20;
    seniorComputation = seniorInput * (250 - discount);
 var entrance = (($('#no-of-adult').val() * 250) + ($('#extra-adults').val() * 250)) + (($('input#no-of-kids').val() * 150) + ($('#extra-kids').val() * 150)) + seniorComputation;
    $('#entrance').text('₱ ' + entrance.format(2));
    $('#entranceTotal').val(entrance);

    // deposit
    var depositTotalInput = $("#depositTotalInput").val();



    // extras

    var extras = $('#extrasHiddenInput').val();
    // computation subtotal
    var subtotal = entrance + parseInt($("#roomTotal").val()) + parseInt(depositTotalInput) + parseInt(extras);
    $("#subtotal").text('₱ ' + subtotal.format(2));
    $("#subTotal").val(subtotal);

    // computated downpayment 
    var downpayment = $("#downpaymentTotal").val();


    // computation balance
    if (downpayment > subtotal) {
        var balance = 0
    } else {
        var balance = subtotal - downpayment;
    }
    $("#balance").text('₱ ' + balance.format(2));
    $("#balanceTotal").val(balance);

});
$('#extra-adults').keyup(function() {
    // computation entrance Fee

    var seniorInput = $('#no-of-senior-pwd').val();
    var discount = 250 * 0.20;
    seniorComputation = seniorInput * (250 - discount);
 var entrance = (($('#no-of-adult').val() * 250) + ($('#extra-adults').val() * 250)) + (($('input#no-of-kids').val() * 150) + ($('#extra-kids').val() * 150)) + seniorComputation;
    $('#entrance').text('₱ ' + entrance.format(2));
    $('#entranceTotal').val(entrance);

    // deposit
    var depositTotalInput = $("#depositTotalInput").val();



    // extras

    var extras = $('#extrasHiddenInput').val();
    // computation subtotal
    var subtotal = entrance + parseInt($("#roomTotal").val()) + parseInt(depositTotalInput) + parseInt(extras);
    $("#subtotal").text('₱ ' + subtotal.format(2));
    $("#subTotal").val(subtotal);

    // computated downpayment 
    var downpayment = $("#downpaymentTotal").val();


    // computation balance
    if (downpayment > subtotal) {
        var balance = 0
    } else {
        var balance = subtotal - downpayment;
    }
    $("#balance").text('₱ ' + balance.format(2));
    $("#balanceTotal").val(balance);

});

$('#extra-kids').keydown(function() {
    // computation entrance Fee

    var seniorInput = $('#no-of-senior-pwd').val();
    var discount = 250 * 0.20;
    seniorComputation = seniorInput * (250 - discount);
 var entrance = (($('#no-of-adult').val() * 250) + ($('#extra-adults').val() * 250)) + (($('input#no-of-kids').val() * 150) + ($('#extra-kids').val() * 150)) + seniorComputation;
    $('#entrance').text('₱ ' + entrance.format(2));
    $('#entranceTotal').val(entrance);

    // deposit
    var depositTotalInput = $("#depositTotalInput").val();



    // extras

    var extras = $('#extrasHiddenInput').val();
    // computation subtotal
    var subtotal = entrance + parseInt($("#roomTotal").val()) + parseInt(depositTotalInput) + parseInt(extras);
    $("#subtotal").text('₱ ' + subtotal.format(2));
    $("#subTotal").val(subtotal);

    // computated downpayment 
    var downpayment = $("#downpaymentTotal").val();


    // computation balance
    if (downpayment > subtotal) {
        var balance = 0
    } else {
        var balance = subtotal - downpayment;
    }
    $("#balance").text('₱ ' + balance.format(2));
    $("#balanceTotal").val(balance);

});
$('#extra-kids').change(function() {
    // computation entrance Fee

    var seniorInput = $('#no-of-senior-pwd').val();
    var discount = 250 * 0.20;
    seniorComputation = seniorInput * (250 - discount);
 var entrance = (($('#no-of-adult').val() * 250) + ($('#extra-adults').val() * 250)) + (($('input#no-of-kids').val() * 150) + ($('#extra-kids').val() * 150)) + seniorComputation;
    $('#entrance').text('₱ ' + entrance.format(2));
    $('#entranceTotal').val(entrance);

    // deposit
    var depositTotalInput = $("#depositTotalInput").val();



    // extras

    var extras = $('#extrasHiddenInput').val();
    // computation subtotal
    var subtotal = entrance + parseInt($("#roomTotal").val()) + parseInt(depositTotalInput) + parseInt(extras);
    $("#subtotal").text('₱ ' + subtotal.format(2));
    $("#subTotal").val(subtotal);

    // computated downpayment 
    var downpayment = $("#downpaymentTotal").val();


    // computation balance
    if (downpayment > subtotal) {
        var balance = 0
    } else {
        var balance = subtotal - downpayment;
    }
    $("#balance").text('₱ ' + balance.format(2));
    $("#balanceTotal").val(balance);

});
$('#extra-kids').keyup(function() {
    // computation entrance Fee

    var seniorInput = $('#no-of-senior-pwd').val();
    var discount = 250 * 0.20;
    seniorComputation = seniorInput * (250 - discount);
 var entrance = (($('#no-of-adult').val() * 250) + ($('#extra-adults').val() * 250)) + (($('input#no-of-kids').val() * 150) + ($('#extra-kids').val() * 150)) + seniorComputation;
    $('#entrance').text('₱ ' + entrance.format(2));
    $('#entranceTotal').val(entrance);

    // deposit
    var depositTotalInput = $("#depositTotalInput").val();



    // extras

    var extras = $('#extrasHiddenInput').val();
    // computation subtotal
    var subtotal = entrance + parseInt($("#roomTotal").val()) + parseInt(depositTotalInput) + parseInt(extras);
    $("#subtotal").text('₱ ' + subtotal.format(2));
    $("#subTotal").val(subtotal);

    // computated downpayment 
    var downpayment = $("#downpaymentTotal").val();


    // computation balance
    if (downpayment > subtotal) {
        var balance = 0
    } else {
        var balance = subtotal - downpayment;
    }
    $("#balance").text('₱ ' + balance.format(2));
    $("#balanceTotal").val(balance);

});

$('#no-of-adult').keydown(function() {
    // computation entrance Fee
    var seniorInput = $('#no-of-senior-pwd').val();
    var discount = 250 * 0.20;
    seniorComputation = seniorInput * (250 - discount);
 var entrance = (($('#no-of-adult').val() * 250) + ($('#extra-adults').val() * 250)) + (($('input#no-of-kids').val() * 150) + ($('#extra-kids').val() * 150)) + seniorComputation;
    $('#entrance').text('₱ ' + entrance.format(2));
    $('#entranceTotal').val(entrance);

    // deposit
    var depositTotalInput = $("#depositTotalInput").val();



    // extras

    var extras = $('#extrasHiddenInput').val();
    // computation subtotal
    var subtotal = entrance + parseInt($("#roomTotal").val()) + parseInt(depositTotalInput) + parseInt(extras);
    $("#subtotal").text('₱ ' + subtotal.format(2));
    $("#subTotal").val(subtotal);

    // computated downpayment 
    var downpayment = $("#downpaymentTotal").val();


    // computation balance
    if (downpayment > subtotal) {
        var balance = 0
    } else {
        var balance = subtotal - downpayment;
    }
    $("#balance").text('₱ ' + balance.format(2));
    $("#balanceTotal").val(balance);
});

$('#no-of-kids').keydown(function() {
    // computation entrance Fee
    var seniorInput = $('#no-of-senior-pwd').val();
    var discount = 250 * 0.20;
    seniorComputation = seniorInput * (250 - discount);
     var entrance = (($('#no-of-adult').val() * 250) + ($('#extra-adults').val() * 250)) + (($('input#no-of-kids').val() * 150) + ($('#extra-kids').val() * 150)) + seniorComputation;
    $('#entranceTotal').val(entrance);

    // deposit
    var depositTotalInput = $("#depositTotalInput").val();

    // extras

    var extras = $('#extrasHiddenInput').val();
    // computation subtotal
    var subtotal = entrance + parseInt($("#roomTotal").val()) + parseInt(depositTotalInput) + parseInt(extras);
    $("#subtotal").text('₱ ' + subtotal.format(2));
    $("#subTotal").val(subtotal);


    // computated downpayment 
    var downpayment = $("#downpaymentTotal").val();


    // computation balance
    if (downpayment > subtotal) {
        var balance = 0
    } else {
        var balance = subtotal - downpayment;
    }
    $("#balance").text('₱ ' + balance.format(2));
    $("#balanceTotal").val(balance);

});


$('#no-of-kids').keyup(function() {
    // computation entrance Fee
    var seniorInput = $('#no-of-senior-pwd').val();
    var discount = 250 * 0.20;
    seniorComputation = seniorInput * (250 - discount);
    var entrance = (($('#no-of-adult').val() * 250) + ($('#extra-adults').val() * 250)) + (($('input#no-of-kids').val() * 150) + ($('#extra-kids').val() * 150)) + seniorComputation;
    $('#entranceTotal').val(entrance);

    // deposit
    var depositTotalInput = $("#depositTotalInput").val();

    // extras

    var extras = $('#extrasHiddenInput').val();
    // computation subtotal
    var subtotal = entrance + parseInt($("#roomTotal").val()) + parseInt(depositTotalInput) + parseInt(extras);
    $("#subtotal").text('₱ ' + subtotal.format(2));
    $("#subTotal").val(subtotal);


    // computated downpayment 
    var downpayment = $("#downpaymentTotal").val();


    // computation balance
    if (downpayment > subtotal) {
        var balance = 0
    } else {
        var balance = subtotal - downpayment;
    }
    $("#balance").text('₱ ' + balance.format(2));
    $("#balanceTotal").val(balance);

});


$('#no-of-kids').keydown(function() {
    // computation entrance Fee
    var seniorInput = $('#no-of-senior-pwd').val();
    var discount = 250 * 0.20;
    seniorComputation = seniorInput * (250 - discount);
     var entrance = (($('#no-of-adult').val() * 250) + ($('#extra-adults').val() * 250)) + (($('input#no-of-kids').val() * 150) + ($('#extra-kids').val() * 150)) + seniorComputation;
    $('#entranceTotal').val(entrance);

    // deposit
    var depositTotalInput = $("#depositTotalInput").val();

    // extras

    var extras = $('#extrasHiddenInput').val();
    // computation subtotal
    var subtotal = entrance + parseInt($("#roomTotal").val()) + parseInt(depositTotalInput) + parseInt(extras);
    $("#subtotal").text('₱ ' + subtotal.format(2));
    $("#subTotal").val(subtotal);


    // computated downpayment 
    var downpayment = $("#downpaymentTotal").val();


    // computation balance
    if (downpayment > subtotal) {
        var balance = 0
    } else {
        var balance = subtotal - downpayment;
    }
    $("#balance").text('₱ ' + balance.format(2));
    $("#balanceTotal").val(balance);

});


$('#no-of-adult').change(function() {
    // computation entrance Fee
    var countNights = $("#no-of-nights").val();
    var seniorInput = $('#no-of-senior-pwd').val();
    var discount = 250 * 0.20;
    seniorComputation = seniorInput * (250 - discount);
     var entrance = (($('#no-of-adult').val() * 250) + ($('#extra-adults').val() * 250)) + (($('input#no-of-kids').val() * 150) + ($('#extra-kids').val() * 150)) + seniorComputation;
    $('#entrance').text('₱ ' + entrance.format(2));
    $('#entranceTotal').val(entrance);
    // deposit
    var depositTotalInput = $("#depositTotalInput").val();



    // extras

    var extras = $('#extrasHiddenInput').val();
    // computation subtotal
    var subtotal = entrance + parseInt($("#roomTotal").val()) + parseInt(depositTotalInput) + parseInt(extras);
    $("#subtotal").text('₱ ' + subtotal.format(2));
    $("#subTotal").val(subtotal);

    // computated downpayment 
    var downpayment = $("#downpaymentTotal").val();


    // computation balance
    var balance = subtotal - parseInt($("#downpaymentTotal").val());
    $("#balance").text('₱ ' + balance.format(2));
    $("#balanceTotal").val(balance);
});

$('#no-of-kids').change(function() {
    // computation entrance Fee
    var seniorInput = $('#no-of-senior-pwd').val();
    var discount = 250 * 0.20;
    seniorComputation = seniorInput * (250 - discount);
     var entrance = (($('#no-of-adult').val() * 250) + ($('#extra-adults').val() * 250)) + (($('input#no-of-kids').val() * 150) + ($('#extra-kids').val() * 150)) + seniorComputation;
    $('#entrance').text('₱ ' + entrance.format(2));
    $('#entranceTotal').val(entrance);

    // deposit
    var depositTotalInput = $("#depositTotalInput").val();

    // extras

    var extras = $('#extrasHiddenInput').val();
    // computation subtotal
    var subtotal = entrance + parseInt($("#roomTotal").val()) + parseInt(depositTotalInput) + parseInt(extras);
    $("#subtotal").text('₱ ' + subtotal.format(2));
    $("#subTotal").val(subtotal);


    // computated downpayment 
    var downpayment = $("#downpaymentTotal").val();


    // computation balance
    if (downpayment > subtotal) {
        var balance = 0
    } else {
        var balance = subtotal - downpayment;
    }
    $("#balance").text('₱ ' + balance.format(2));
    $("#balanceTotal").val(balance);

});

$('#no-of-senior-pwd').change(function() {
    // computation entrance Fee
    var seniorInput = $('#no-of-senior-pwd').val();
    var discount = 250 * 0.20;
    seniorComputation = seniorInput * (250 - discount);
     var entrance = (($('#no-of-adult').val() * 250) + ($('#extra-adults').val() * 250)) + (($('input#no-of-kids').val() * 150) + ($('#extra-kids').val() * 150)) + seniorComputation;
    $('#entrance').text('₱ ' + entrance.format(2));
    $('#entranceTotal').val(entrance);

    // deposit
    var depositTotalInput = $("#depositTotalInput").val();

    // extras

    var extras = $('#extrasHiddenInput').val();
    // computation subtotal
    var subtotal = entrance + parseInt($("#roomTotal").val()) + parseInt(depositTotalInput) + parseInt(extras);
    $("#subtotal").text('₱ ' + subtotal.format(2));
    $("#subTotal").val(subtotal);


    // computated downpayment 
    var downpayment = $("#downpaymentTotal").val();


    // computation balance
    if (downpayment > subtotal) {
        var balance = 0
    } else {
        var balance = subtotal - downpayment;
    }
    $("#balance").text('₱ ' + balance.format(2));
    $("#balanceTotal").val(balance);

});

$('#no-of-adult').blur(function() {

    // computation entrance Fee
    var seniorInput = $('#no-of-senior-pwd').val();
    var discount = 250 * 0.20;
    seniorComputation = seniorInput * (250 - discount);
     var entrance = (($('#no-of-adult').val() * 250) + ($('#extra-adults').val() * 250)) + (($('input#no-of-kids').val() * 150) + ($('#extra-kids').val() * 150)) + seniorComputation;
    $('#entrance').text('₱ ' + entrance.format(2));
    $('#entranceTotal').val(entrance);

    // deposit
    var depositTotalInput = $("#depositTotalInput").val();

    // extras

    var extras = $('#extrasHiddenInput').val();
    // computation subtotal
    var subtotal = entrance + parseInt($("#roomTotal").val()) + parseInt(depositTotalInput) + parseInt(extras);
    $("#subtotal").text('₱ ' + subtotal.format(2));
    $("#subTotal").val(subtotal);


    // computated downpayment 
    var downpayment = $("#downpaymentTotal").val();


    // computation balance
    if (downpayment > subtotal) {
        var balance = 0
    } else {
        var balance = subtotal - downpayment;
    }
    $("#balance").text('₱ ' + balance.format(2));
    $("#balanceTotal").val(balance);

});


$('#no-of-senior-pwd').blur(function() {

    // computation entrance Fee
    var seniorInput = $('#no-of-senior-pwd').val();
    var discount = 250 * 0.20;
    seniorComputation = seniorInput * (250 - discount);
     var entrance = (($('#no-of-adult').val() * 250) + ($('#extra-adults').val() * 250)) + (($('input#no-of-kids').val() * 150) + ($('#extra-kids').val() * 150)) + seniorComputation;
    $('#entrance').text('₱ ' + entrance.format(2));
    $('#entranceTotal').val(entrance);

    // deposit
    var depositTotalInput = $("#depositTotalInput").val();

    // extras

    var extras = $('#extrasHiddenInput').val();
    // computation subtotal
    var subtotal = entrance + parseInt($("#roomTotal").val()) + parseInt(depositTotalInput) + parseInt(extras);
    $("#subtotal").text('₱ ' + subtotal.format(2));
    $("#subTotal").val(subtotal);


    // computated downpayment 
    var downpayment = $("#downpaymentTotal").val();


    // computation balance
    if (downpayment > subtotal) {
        var balance = 0
    } else {
        var balance = subtotal - downpayment;
    }
    $("#balance").text('₱ ' + balance.format(2));
    $("#balanceTotal").val(balance);

});


$('#no-of-kids').blur(function() {

    // computation entrance Fee
    var seniorInput = $('#no-of-senior-pwd').val();
    var discount = 250 * 0.20;
    seniorComputation = seniorInput * (250 - discount);
     var entrance = (($('#no-of-adult').val() * 250) + ($('#extra-adults').val() * 250)) + (($('input#no-of-kids').val() * 150) + ($('#extra-kids').val() * 150)) + seniorComputation;
    $('#entrance').text('₱ ' + entrance.format(2));
    $('#entranceTotal').val(entrance);

    // deposit
    var depositTotalInput = $("#depositTotalInput").val();

    // extras

    var extras = $('#extrasHiddenInput').val();
    // computation subtotal
    var subtotal = entrance + parseInt($("#roomTotal").val()) + parseInt(depositTotalInput) + parseInt(extras);
    $("#subtotal").text('₱ ' + subtotal.format(2));
    $("#subTotal").val(subtotal);


    // computated downpayment 
    var downpayment = $("#downpaymentTotal").val();


    // computation balance
    if (downpayment > subtotal) {
        var balance = 0
    } else {
        var balance = subtotal - downpayment;
    }
    $("#balance").text('₱ ' + balance.format(2));
    $("#balanceTotal").val(balance);

});
