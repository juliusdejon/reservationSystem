<?php
session_start();
require_once '../config/db.php';
require_once '../config/User.php';

$user = new User($pdo);
// Prevent anyone to Access Routes without Session
if (!isset($_SESSION['id'])) {
    header('Location: index.php');
}
?>

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
            </li>
            <li class="nav-item">
<?php
$link = $_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING'];
?>
<button  onclick="refresh('<?php echo $link ?>')" class="btn btn-success"><i class="fa fa-refresh"></i></button>
<script>
function refresh(query) {
    window.location.href = query;
}
</script>
            </li>
            <!-- <li class="nav-item">
            <a href="clear_database.php" onclick="return confirm('Development Tool - Are you sure you want to Erase Transaction, Reservation , Reserved Rooms?');" class="btn btn-warning">Development - Clear Transaction, Reservation</a>
            </li> -->
        </ul>


        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" data-slide="true">
                    <?php
echo ($_SESSION['id']);
?>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-slide="true" href="logout.php">Logout</a>
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->
