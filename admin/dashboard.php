    <?php

// Get the rang of dates
function date_range($first, $last, $step, $output_format)
{
    $dates = array();
    $current = strtotime($first);
    $last = strtotime($last);
    while ($current <= $last) {
        $dates[] = date($output_format, $current);
        $current = strtotime($step, $current);
    }
    return $dates;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Villa Alfredo Admin</title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
    <!-- Semantic UI -->
    <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />

    <!-- Initialize Semantic UI and JQuery to load Calendar Datepicker-->
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/semantic.min.js"></script>

    <!-- Calendar -->
    <link type="text/css" rel="stylesheet" href="../css/calendar.min.css" />
    <script src="../js/calendar.min.js"></script>

    <!-- Icons -->
    <link rel="stylesheet" href="../css/icon.min.css">

</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <?php
include 'layout/navbar.php';
include 'layout/sidebar.php';
?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1 class="m-0 text-dark"></h1>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.content-header -->

                <!-- Main content -->
                <div class="content">
                <div class="row">
                        <div class="col-lg-3 col-6">
                            <!-- small box -->
                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3>
                                        <?php
$sql = "SELECT COUNT(*) as Pending FROM reservation WHERE reservation_status='Pending'";
$res = $mysqli->query($sql);
$rows = mysqli_fetch_assoc($res);
$pending = $rows['Pending'];

echo $pending;
?>
                                    </h3>
                                    <p>Pending Reservations</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-bag"></i>
                                </div>
                                <a href="reservations.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-6">
                            <!-- small box -->
                            <div class="small-box bg-success">
                                <div class="inner">
                                    <h3>
                                        <?php
$total = [];
$sql = "SELECT * FROM customer JOIN transaction ON customer.client_reference_id = transaction.client_reference_id
                WHERE transaction_status='checked_in' OR transaction_status='In'";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $pax = $rows['no_of_adults'] + $rows['no_of_kids'];
    array_push($total, $pax);
}
$total = array_sum($total);
echo $total;
?>
                                            <sup style="font-size: 20px"></sup></h3>

                                    <p>Total Visitors</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-stats-bars"></i>
                                </div>
                                <a href="checked_in.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->

                        <!-- ./col -->
                        <?php if ($user_role == 'frontdesk') {} else {?>
                        <div class="col-lg-3 col-6">
                            <!-- small box -->
                            <div class="small-box bg-danger">
                                <div class="inner">
                                    <h3>
                                        <!-- <?php
$sales = [];
    $sql = "SELECT * FROM transaction WHERE transaction_status='Cleared'";
    $res = $mysqli->query($sql);
    while ($rows = mysqli_fetch_assoc($res)) {
        array_push($sales, $rows['total']);

    }
    $sales = array_sum($sales);
    echo $sales;

    ?> -->

                                    </h3>

                                    <p>Revenue</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-pie-graph"></i>
                                </div>
                                <a href="reports.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <?php }?>
                        <!-- ./col -->
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <small class="lead text-center">Arrivals</small>
                                <table id="example1" class="display p-0 m-0" style="width:100%" role="grid" aria-describedby="example1_info">
                                    <thead>
                                        <tr role="row">
                                            <th>Reference ID</th>
                                            <th>Customer Name</th>
                                            <th>Check In</th>
                                            <th>Check Out</th>
                                            <th>Pax </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
include '../config/mysqli.php';
$date = date("Y-m-d");
$sql = "SELECT * FROM customer
 JOIN reservation on customer.client_reference_id=reservation.client_reference_id
 JOIN transaction on transaction.client_reference_id = reservation.client_reference_id
WHERE arrival_date = '$date' AND reservation_status ='Booked'";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $client_reference_id = $rows['client_reference_id'];
    $first_name = $rows['first_name'];
    $last_name = $rows['last_name'];
    $arrival_date = $rows['arrival_date'];
    $departure_date = $rows['departure_date'];
    $no_of_kids = $rows['no_of_kids'];
    $no_of_adults = $rows['no_of_adults'];
    $pax = $no_of_adults + $no_of_kids;
    ?>
                                            <tr>
                                                <td>
                                                    <?php echo $client_reference_id; ?>
                                                </td>
                                                <td>
                                                    <?php echo $first_name . ' ' . $last_name; ?>
                                                </td>
                                                <td>
                                                    <?php echo $arrival_date; ?>
                                                </td>
                                                <td>
                                                    <?php echo $departure_date; ?>
                                                </td>
                                                <td>
                                                    <?php echo $pax; ?>
                                                </td>
                                            </tr>
                                            <?php }?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card">
                                <small class="lead text-center">Departure</small>
                                <table id="example1" class="display p-0 m-0" style="width:100%" role="grid" aria-describedby="example1_info">
                                    <thead>
                                        <tr role="row">
                                            <th>Reference ID</th>
                                            <th>Customer Name</th>
                                            <th>Check In</th>
                                            <th>Check Out</th>
                                            <th>Pax </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
include '../config/mysqli.php';
$date = date("Y-m-d");
$sql = "SELECT * FROM customer
 JOIN reservation on customer.client_reference_id=reservation.client_reference_id
 JOIN transaction on transaction.client_reference_id = reservation.client_reference_id
WHERE departure_date = '$date' AND transaction_status='checked_in'";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $client_reference_id = $rows['client_reference_id'];
    $first_name = $rows['first_name'];
    $last_name = $rows['last_name'];
    $arrival_date = $rows['arrival_date'];
    $departure_date = $rows['departure_date'];
    $no_of_kids = $rows['no_of_kids'];
    $no_of_adults = $rows['no_of_adults'];
    $pax = $no_of_adults + $no_of_kids;
    ?>
                                            <tr>
                                                <td>
                                                    <?php echo $client_reference_id; ?>
                                                </td>
                                                <td>
                                                    <?php echo $first_name . ' ' . $last_name; ?>
                                                </td>
                                                <td>
                                                    <?php echo $arrival_date; ?>
                                                </td>
                                                <td>
                                                    <?php echo $departure_date; ?>
                                                </td>
                                                <td>
                                                    <?php echo $pax; ?>
                                                </td>
                                            </tr>
                                            <?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                    <h3>Check Available Rooms</h3>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="field">
                                <label>Arrival Date</label>
                                <div class="ui calendar" id="rangestart">
                                    <div class="ui input left icon">
                                        <i class="calendar icon"></i>
                                        <input type="text" id="start" placeholder="Start" name="arrival_date_post" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="field">
                                <label>Departure Date</label>
                                <div class="ui calendar" id="rangeend">
                                    <div class="ui input left icon">
                                        <i class="calendar icon"></i>
                                        <input type="text" id="end" placeholder="End" name="departure_date_post" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

<div class="ui segment col-md-9">
<h4>List of Rooms</h4>
<div id="available-rooms">
<!-- <table class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                            <thead>
                                                <tr role="row">

                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending">Room Name</th>
                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Room Description</th>
                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Room Price</th>
                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Quantity</th>

                                                </tr>
                                            </thead>
                                            <tbody> -->
                                        <!-- <?php
// Select room type eg. Ana Villa, Mike Hotel
$res = $mysqli->query('SELECT * FROM room_type WHERE isDeleted=0');
while ($rows = mysqli_fetch_assoc($res)) {
    $roomType = $rows['room_id'];
    $room_name = $rows['room_name'];
    $room_info = $rows['room_info'];
    $room_price = $rows['room_price'];
    // Get the Input dates provided by the customer at the front end eg. he clicked July 24 - 26
    @$range = date_range($arrival_date, $departure_date, "+1 day", "Y-m-d");

    // Slice it to July 24 - 25
    $reserved_period = array_slice($range, 0, -1);

    // Here you are selecting 'RESERVED ROOMS!!' AND ONLY WITH A STATUS THAT HAS BEEN CONFIRMED AND BOOKED REFLECTED by the given Dates on the site
    $s = "SELECT * FROM reserved_rooms JOIN customer ON customer.client_reference_id = reserved_rooms.client_reference_id
        JOIN reservation ON reserved_rooms.client_reference_id = reservation.client_reference_id
        WHERE reserved_rooms.room_type_id = '$roomType' AND reservation_status='Booked'  OR reservation_status='Checked In'";

    $r = $mysqli->query($s);
    $reserved_rooms = [];
    $my_selected_rooms = [];
    while ($ro = mysqli_fetch_assoc($r)) {
        $arrival_date_db = $ro['arrival_date'];
        $departure_date_db = $ro['departure_date'];
        // -1 to the Departure date. July 26 becomes July 25... so it will not consider 26 as still Reserved date.
        $departure_date_db = date('Y-m-d', (strtotime('-1 day', strtotime($departure_date_db))));
        // Get the Database Arrival Date and Departure Date of Ana Villa Room type that has been Reserved
        $rangeDb = date_range($arrival_date_db, $departure_date_db, "+1 day", "Y-m-d");

        $roomNotAvailable = 'not existing';
        foreach ($reserved_period as $day) {
            // Check the reserved period above an Array of dates
            // eg. $reservation_period = [2018-07-25,2018-07-25] // These dates will be compared at the Database
            // rangeDb
            if (in_array($day, $rangeDb)) {
                $roomNotAvailable = 'existing';
            }
        }
        if ($roomNotAvailable == 'existing') {
            array_push($reserved_rooms, 1);
        }
    }

    $reserved_rooms = array_sum($reserved_rooms);

    // Available Rooms = Total Rooms - Reserved Rooms IN Check In Date
    $sqlAvailableRoom = "SELECT COUNT(*) as TotalRoom FROM rooms WHERE room_type_id='$roomType'";
    $resAvailableRoom = $mysqli->query($sqlAvailableRoom);
    $result = mysqli_fetch_assoc($resAvailableRoom);
    $result = $result['TotalRoom'] - $reserved_rooms;

    ?>
      <tr>
        <td><?php echo $room_name; ?></td>
        <td><?php echo $room_info; ?></td>
        <td align="center">₱<?php echo number_format($room_price, 2); ?></td>
        <td align="center">
          <?php

    if ($result <= 0) {
        echo "<a class='ui red right ribbon label'>Not Available</a>";
    } else {
        echo $result;
    }
    ?>
                                        </select>
                          </td>
      </tr>

<?php }?> -->


</tbody>
</table>

</div>
</div>


</div>
                </div>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <!-- Main Footer -->
            <?php
include 'layout/footer.php';
?>
                <!-- ./wrapper -->

                <!-- REQUIRED SCRIPTS -->



                <script>
                    const today = new Date();
                    $('#rangestart').calendar({
                        type: 'date',
                        endCalendar: $('#rangeend'),
                        onChange: function(date, text, mode) {
                            $("#available-rooms").html('<img src="ajax/loading-spinner.gif" width="100px">')
                            var start = $("#start").val();
                            var end = $("#end").val();
                            $.post("ajax/searchAvailableRoomsDashboard.php", {
                                start: start,
                                end: end,
                            }, function(data) {
                                $("#available-rooms").html(data);
                            });

                        },
                    });
                    $('#rangeend').calendar({
                        type: 'date',
                        startCalendar: $('#rangestart'),
                        onChange: function(date, text, mode) {
                            $("#available-rooms").html('<img src="ajax/loading-spinner.gif" width="100px">')
                            var start = $("#start").val();
                            var end = $("#end").val();
                            var jqxhr = $.post("ajax/searchAvailableRoomsDashboard.php", {
                                start: start,
                                end: end,
                            }, function(data) {
                                $("#available-rooms").html(data);
                            });
                        },
                        onHidden: function() {
                            $.post("ajax/searchAvailableRoomsDashboard.php", {
                                start: start,
                                end: end,
                            }, function(data) {
                                $("#available-rooms").html(data);
                            });
                        },
                    });
                </script>
                <!-- jQuery -->
                <script src="plugins/jquery/jquery.min.js"></script>
                <!-- Bootstrap 4 -->
                <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
                <!-- AdminLTE App -->
                <script src="dist/js/adminlte.min.js"></script>
</body>

</html>