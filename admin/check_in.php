<?php
include '../config/mysqli.php';
@$success = $_GET['feedback'];
@$receipt = $_GET['receipt'];
if ($receipt) {?>
    <script type="text/javascript">
    window.open("transaction_receipt_print.php?res_id=<?php echo $receipt; ?>", '_blank');
 </script>
 <?php
}
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>Villa Alfredo Admin</title>

        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/adminlte.min.css">
         <!-- Semantic UI -->
        <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />




        <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap4.css">
        <script language="javascript" type="text/javascript">
function removeSpaces(string) {
 return string.split(' ').join('');
}
</script>
    </head>

    <body class="hold-transition sidebar-mini">
        <div class="wrapper">

            <?php
include 'layout/navbar.php';
include 'layout/sidebar.php';
?>
                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <div class="content-header">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6">
                                <?php if ($success) {
    echo '   <div class="alert alert-success alert-dismissible">
                                    <a href="check_in.php" class="btn-button pull-right">×</a>
                                        <h5><i class="fa fa-check"></i> Success!</h5>
                                        Successfully Checked In!
                                    </div>';
} else {

}?>
                                    <div class="card">
                                        <div class="card-body">
                                        <div class="row">
                                            <div class="col-2">
                                            Reference ID:
                                            </div>
                                            <div class="col-10">
                                            <input type="text" class="form-control form-control-sm" id="reference_id" name="reference_id" autofocus onkeydown="this.value=removeSpaces(this.value);"/>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card">

                                        <div class="card-body p-2">
                                            <small class="lead text-center">Expected Guests</small>
                                            <table id="example1" class="display p-0 m-0" style="width:100%" role="grid" aria-describedby="example1_info">
                                                <thead>
                                                    <tr role="row">

                                                        <th>Name</th>
                                                        <th>Reference Id</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
$date = date("Y-m-d");
$sql = "SELECT * FROM customer
 JOIN reservation on customer.client_reference_id=reservation.client_reference_id
 WHERE arrival_date ='$date' AND reservation_status ='Booked'";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $client_reference_id = $rows['client_reference_id'];
    $first_name = $rows['first_name'];
    $last_name = $rows['last_name'];
    $arrival_date = new DateTime($arrival_date = $rows['arrival_date']);
    $created_at = new DateTime($created_at = $rows['created_at']);
    $reservation_status = $rows['reservation_status'];
    $receipt = $rows['receipt'];
    ?>
                                                        <tr>
                                                            <td>
                                                                <?php echo $first_name . ' ' . $last_name; ?>
                                                            </td>
                                                            <td>
                                                                <?php echo $client_reference_id; ?>
                                                            </td>

                                                        </tr>
                                                        <?php
}
?>
                                                </tbody>

                                            </table>
                                            <!-- /.card-header -->
                                        </div>
                                    </div>

                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <!-- /.container-fluid -->
                    </div>
                    <!-- /.content-header -->

                    <!-- Main content -->
                    <div class="content">
                        <div class="row">
                            <div class="col-md-12">
                              <div id="check-in-Form"></div>
</div>
                        </div>
                    </div>
                    <!-- /.content -->
                </div>
                <!-- /.content-wrapper -->

                <!-- Main Footer -->
                <?php
include 'layout/footer.php';
?>
                    <!-- ./wrapper -->

                    <!-- REQUIRED SCRIPTS -->


                    <!-- jQuery -->
                    <script src="plugins/jquery/jquery.min.js"></script>
                    <!-- Live Search CheckIn Form-->
                    <script src="ajax/checkIn.js"></script>
                    <script>
                            function validateForm() {
                        const balance = document.forms["checkForm"]["balance"].value;
                        const input_payment = document.forms["checkForm"]["input_payment"].value;
                        const no_of_adults = document.forms["checkForm"]["no_of_adults"].value;

                        if (no_of_adults == "" || no_of_adult == 0) {
                            alert("an Adult is Required");
                            return false;
                        }
                        if (input_payment > balance) {
                            alert("Please Enter Exact amount of the Balance");
                            return false;
                        }
                        alert('Hello');

                    }
                    </script>
                    <!-- Bootstrap 4 -->
                    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
                    <!-- DataTables -->
                    <script src="plugins/datatables/jquery.dataTables.js"></script>
                    <script src="plugins/datatables/dataTables.bootstrap4.js"></script>
                    <!-- AdminLTE App -->
                    <script src="dist/js/adminlte.min.js"></script>
                    <script>
                        $(function() {

                            $('#example1').DataTable({
                                "paging": true,
                                "pageLength": 10,

                            });
                        });
                    </script>
                     </script>
    <!-- JQuery Validation source -->
    <script type="text/javascript" src="../admin/plugins/jquery/jquery-key-restrictions.min.js"></script>
    <!-- Actual Validation calling class -->
    <script type="text/javascript">
    $(document).ready(function () {
        $(".letters-only").lettersOnly();
        $(".numbers-only").numbersOnly();
        $(".alpha-numeric-only").alphaNumericOnly();
    });
    </script>
    </body>

    </html>
