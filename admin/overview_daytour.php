<?php
include '../config/mysqli.php';
$client_ref_id = $_GET['ref_id'];

$sql = "SELECT * FROM customer
        JOIN transaction ON customer.client_reference_id = transaction.client_reference_id
        WHERE customer.client_reference_id = '$client_ref_id'";

$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $first_name = $rows['first_name'];
    $last_name = $rows['last_name'];
    $contact = $rows['contact_number'];
    $email = $rows['email_address'];
    $arrival_date = $rows['arrival_date'];
    $departure_date = $rows['departure_date'];
    $no_of_adults = $rows['no_of_adults'];
    $no_of_kids = $rows['no_of_kids'];
    $no_of_senior_pwd = $rows['no_of_senior_pwd'];
    $downpayment = $rows['downpayment'];
    $balance = $rows['balance'];
}

?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>Villa Alfredo Admin</title>

        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/adminlte.min.css">
         <!-- Semantic UI -->
        <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />




        <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap4.css">
    </head>

    <body class="hold-transition sidebar-mini">
    <script>
		function printDiv(divName){
			var printContents = document.getElementById(divName).innerHTML;
			var originalContents = document.body.innerHTML;
			document.body.innerHTML = printContents;

			window.print();
			document.body.innerHTML = originalContents;
		}
	</script>
        <div class="wrapper">

            <?php
include 'layout/navbar.php';
include 'layout/sidebar.php';
?>
                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <div class="content-header">
                        <div class="container-fluid">

                        </div>
                        <!-- /.container-fluid -->
                    </div>
                    <!-- /.content-header -->

                    <!-- Main content -->
                    <div class="content">
                    <button class="btn btn-success btn-md pull-right mr-3" onclick="printDiv('printMe')">Print Receipt <i class="fa fa-print"></i></button>
                    <h3>Reference Id: <?php echo $client_ref_id; ?> </h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card" id="printMe">
                                    <!-- /.card-header -->
                                    <div class="ui segment">
                                      <div class="row">
                                      <div class="col-md-6">
            <div class="logo">
                <img src="../img/valogo.png" width="200px">
            </div>
</div>
<div class="col-md-6 mt-3">
                <h3>Villa Alfredo's Resort</h3>

                <br>

                <span>Purok 1 Barangay Baliti, City of San Fernando</span>
                <span>,Pampanga Philippines</span>

                <br>

                <span>Manila Phone Office: +63 02 5844840</span>
                <span>/Resort Phone Office: +63 045 455-1397</span>
                <br>
                <span>villaalfredosresort@yahoo.com</span>
                <span>/villaalfredos@yahoo.com</span>
</div>
</div>
<hr />
<div class="row">
<div class="col-md-6">
<div>Customer Details:</div>
Reference ID: <?php echo $client_ref_id; ?> <br/>
Name: <?php echo $first_name . ' ' . $last_name; ?><br />
Contact Number: <?php echo $contact; ?> <br/>
Email Address: <?php echo $email; ?> <br />
<br />
<?php
$sql = "SELECT transaction_type FROM transaction WHERE client_reference_id='$client_ref_id'";
$res = $mysqli->query($sql);
$row = mysqli_fetch_assoc($res);
$transaction_type = $row['transaction_type'];
?>
<div> <?php echo $transaction_type; ?> Details: </div>
Arrival Date: <?php echo $arrival_date; ?> <br />

 <br/><br />

<table border="1">
  <th style="padding:6px; color: #424242;">Cottages</th>
  <th style="padding:6px; color: #424242;">Quantity</th>
  <th style="padding:6px; color: #424242;">Price </th>
  <tbody>
    <?php
$sql = "SELECT * FROM occupied_cottages JOIN cottage_type ON occupied_cottages.cottage_id=cottage_type.cottage_type_id WHERE occupied_cottages.client_reference_id='$client_ref_id'
 GROUP BY cottage_type_id";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $cottage_type_id = $rows['cottage_type_id'];
    ?>
      <tr>
        <td><?php echo $rows['cottage_name']; ?></td>
        <td align="center">x
          <?php
$sql1 = "SELECT COUNT(*) as totalCottage FROM occupied_cottages WHERE cottage_id ='$cottage_type_id' AND client_reference_id ='$client_ref_id'";
    $res1 = $mysqli->query($sql1);
    $row = mysqli_fetch_assoc($res1);
    $countedCottage = $row['totalCottage'];
    echo $countedCottage;
    ?>
      </td>
      <td>₱ <?php echo number_format($rows['cottage_price'] * $countedCottage, 2); ?></td>
      </tr>
    <?php
}
?>
</tbody>
</table>
<br />
<br />
<div> Transaction Details: </div>
No of Kids: <?php echo $no_of_kids; ?><br />
No of Adults: <?php echo $no_of_adults; ?><br/>
Type: <?php echo $transaction_type; ?>

<br />
</div>
<div class="col-md-6">
<div class="row">
<div class="col-md-6">
<p class="lead">Extras</p>
<table border="1">
<th style="padding:6px; color: #424242;">Item</th>
<th style="padding:6px; color: #424242;">Qty</th>
<th style="padding:6px; color: #424242;">Price</th>
<tbody>
<?php
$totalExtras = [];
$sql = "SELECT * FROM customer_extras
            JOIN extras ON customer_extras.extras_id = extras.extras_id
            WHERE client_reference_id = '$client_ref_id'
            GROUP BY customer_extras.extras_id";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $extras_id = $rows['extras_id'];
    $extras_name = $rows['extras_name'];
    $extras_price = $rows['extras_price'];
    $sqlCount = "SELECT COUNT(*) as extras_qty FROM customer_extras WHERE extras_id='$extras_id' AND client_reference_id='$client_ref_id'";
    $resCount = $mysqli->query($sqlCount);
    $rowCount = mysqli_fetch_array($resCount);
    $extras_qty = $rowCount['extras_qty'];
    array_push($totalExtras, $extras_qty * $extras_price);
    ?>
            <tr>
            <td align="center"><?php echo $extras_name; ?></td>
            <td align="center"><?php echo $extras_qty; ?></td>
            <td align="center">₱ <?php echo number_format($extras_price, 2); ?></td>
            </tr>



<?php
}

$totalExtras = array_sum($totalExtras);
?>
</tbody>
</table>
</div>
<div class="col-md-6">

</div>
</div>
                  <p class="lead">Amount</p>

                  <div class="table-responsive">
                    <table class="table">
                      <tbody>
                      <tr>
                      <th>Cottages Total:  </th>
                      <td>
<?php

$sql = "SELECT * FROM occupied_cottages
                   JOIN customer ON
                   occupied_cottages.client_reference_id = customer.client_reference_id
                   JOIN cottage_type ON occupied_cottages.cottage_id = cottage_type.cottage_type_id
                   WHERE customer.client_reference_id ='$client_ref_id'
                   GROUP BY occupied_cottages.cottage_id";
$res = $mysqli->query($sql);
$count = 1;
$total = 0;
$subtotal = [];
while ($rows = mysqli_fetch_assoc($res)) {
    $cottage_id = $rows['cottage_id'];
    $sql1 = "SELECT COUNT(*) as quantity FROM occupied_cottages
      JOIN customer ON
      occupied_cottages.client_reference_id = customer.client_reference_id
      JOIN cottage_type ON occupied_cottages.cottage_id = cottage_type.cottage_type_id
      WHERE customer.client_reference_id ='$client_ref_id' AND cottage_id ='$cottage_id'";
    $res1 = $mysqli->query($sql1);
    $row1 = mysqli_fetch_assoc($res1);
    $total = $row1['quantity'] * $rows['cottage_price'];
    array_push($subtotal, $total);
}

$grandTotal = array_sum($subtotal);
echo "₱ " . number_format($grandTotal, 2);
?>
                      </td>
            <input type="hidden" name="room_total" id="roomTotal" value="2000">
                      </tr>
                      
                      <tr>
                      <th>Entrance Fees:</th>
                      <td>
                        <span id="entrance"></span>
                        <?php

if ($transaction_type == 'Daytour') {

    $entranceTotal = ($no_of_adults * 230) + ($no_of_kids * 130) + ($no_of_senior_pwd * 184);

} else {
    $entranceTotal = ($no_of_adults * 250) + ($no_of_kids * 150) + ($no_of_senior_pwd * 200);

}
echo "₱ " . number_format($entranceTotal, 2);
?>
                      </td>
                      </tr>
                      <tr>
                      <th>Corkages:</th>
                      <td>
                        <span id="corkages"></span>
                        <?php
    $sql = "SELECT * FROM customer_corkages WHERE client_reference_id = '$client_ref_id'";
    $res = $mysqli->query($sql);
    $row = mysqli_fetch_assoc($res);
    $corkagesTotal = $row['price'];
echo "₱ " . number_format($corkagesTotal, 2);
?>
                      </td>
                      </tr>
                      <tr>
                      <th>Extras</th>
                      <td>
                        <span id="extras"></span>
                    <?php echo "₱ " . number_format($totalExtras, 2); ?>
                      </td>
                      </tr><tr>

                      <th>Grand Total</th>
                      <td>
                      <span id="subtotal"></span>
                     <?php echo "₱ " . number_format($grandTotal = $grandTotal + $entranceTotal + $totalExtras + $corkagesTotal, 2); ?>
                      </td>
                      </tr>

                      <tr>
                        <th style="width:50%">Downpayment</th>
                        <td>            <span id="downpayment">
            <?php echo "₱ " . number_format($downpayment, 2); ?>          </span>
            <input type="hidden" name="downpayment" id="downpaymentTotal" >

            </td>
                      </tr>
                      <tr>
                        <th>Balance </th>
                        <td>
                                                <span id="balance"><?php

if ($balance == 0) {
    echo "₱ " . number_format($balance, 2);
} else {
    echo "₱ " . number_format($grandTotal - $downpayment, 2);
}
?></span>
                        <input type="hidden" name="balance" id="balanceTotal" >
                        </td>
</tr>

                    </tbody></table>
                  </div>
                </div>
</div>


</div>
                                    <!-- /.card-body -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.content -->
                </div>
                <!-- /.content-wrapper -->

                <!-- Main Footer -->
                <?php
include 'layout/footer.php';
?>
                    <!-- ./wrapper -->

                    <!-- REQUIRED SCRIPTS -->


                    <!-- jQuery -->
                    <script src="plugins/jquery/jquery.min.js"></script>
                    <!-- Bootstrap 4 -->
                    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
                    <!-- DataTables -->
                    <script src="plugins/datatables/jquery.dataTables.js"></script>
                    <script src="plugins/datatables/dataTables.bootstrap4.js"></script>
                    <!-- AdminLTE App -->
                    <script src="dist/js/adminlte.min.js"></script>
    </body>

    </html>
