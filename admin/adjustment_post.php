<?php
include '../config/mysqli.php';

if (isset($_POST['rebook'])) {

    $client_reference_id = $_POST['ref_id'];
    $arrival_date = $_POST['arrival_date_post'];
    $departure_date = $_POST['departure_date_post'];
    $no_of_adults = $_POST['no_of_adults_post'];
    $no_of_kids = $_POST['no_of_kids_post'];

    $arrival_date = date_create_from_format('F j, Y', $arrival_date);
    $arrival_date = date_format($arrival_date, "Y-m-d");

    $departure_date = date_create_from_format('F j, Y', $departure_date);
    $departure_date = date_format($departure_date, "Y-m-d");

    $sql = "SELECT * FROM room_type WHERE isDeleted=0";
    $res = $mysqli->query($sql);
    $hasSelectedRoom = [];
    $room = [];
    while ($rows = mysqli_fetch_assoc($res)) {
        $roomType = $rows['room_id'];
        $_POST['room_id' . $roomType];
        @$isSelected = $_POST['quantity' . $roomType];
        array_push($hasSelectedRoom, $isSelected);
        if ($isSelected != 0) {
            array_push($room, $roomType);
            array_push($room, $isSelected);
        }
    }
    // Validate if a Room has been Selected. If not do not proceed
    $count = 0;
    for ($i = 0; $i < count($hasSelectedRoom); $i++) {
        if ($hasSelectedRoom[$i] != 0) {

        } else {
            $count++;
        }
    }

    $date_now = date('Y-m-d');

    $get_check_in_date = "SELECT * FROM transaction WHERE client_reference_id='$client_reference_id'";
    $res_get_check_in_date = $mysqli->query($get_check_in_date);
    $row_get_check_in_date = mysqli_fetch_assoc($res_get_check_in_date);

    $check_in_date = $row_get_check_in_date['check_in_date'];
    $check_in_date = substr($check_in_date, 0, 10);

    if ($date_now == $check_in_date) {
        header('Location: adjustments.php?invalid_date=true&res_id=' . $client_reference_id);
    } else {
        // Validate on the database
        $select_existing_room = "SELECT COUNT(*) as existing_room FROM reserved_rooms WHERE client_reference_id='$client_reference_id'";
        $res_select_existing = $mysqli->query($select_existing_room);
        $row_existing = mysqli_fetch_assoc($res_select_existing);
        $a_room_exists = $row_existing['existing_room'];
        if ($count == count($hasSelectedRoom) && $a_room_exists == 0) {
            header('Location: adjustments.php?no_room=true&res_id=' . $client_reference_id);
        } else {

            $rooms = array_chunk($room, 2);

            $resRebook = $mysqli->query("UPDATE customer SET
    departure_date ='$departure_date',
    no_of_kids = '$no_of_kids',
    no_of_adults = '$no_of_adults' WHERE client_reference_id='$client_reference_id'");
            if ($resRebook) {
                for ($i = 0; $i < count($rooms); $i++) {
                    $room_id = $rooms[$i][0];
                    $room_qty = $rooms[$i][1];
                    for ($j = 0; $j < $room_qty; $j++) {
                        $sql1 = "INSERT INTO reserved_rooms (room_type_id,client_reference_id) VALUES('$room_id','$client_reference_id')";
                        $mysqli->query($sql1);
                    }
                    $mysqli->query("INSERT INTO adjusted_rooms (client_reference_id, room_type_id, adjusted_room_quantity,adjusted_start_date,adjusted_end_date) VALUES ('$client_reference_id','$room_id','$room_qty', '$check_in_date','$date_now')");
                }
                $sql = "UPDATE transaction SET transaction_type='Adjustment' WHERE client_reference_id ='$client_reference_id'";
                $mysqli->query($sql);
                header("Location: overview.php?ref_id=" . $client_reference_id . "&view_by=checked_in&adjustment=true");
            } else {
                header("Location: overview.php?ref_id=$client_reference_id&view_by=checked_in&adjustment=false");
            }
        }
    }
}
