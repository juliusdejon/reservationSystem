<?php
include '../config/mysqli.php';

@$success = $_GET['feedback'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Villa Alfredo Admin</title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
     <!-- Semantic UI -->
  <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />


    <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap4.css">
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <?php
include 'layout/navbar.php';
include 'layout/sidebar.php';
?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">

                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.content-header -->

                <!-- Main content -->
                <div class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Daytour / Overnight </h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                <?php if ($success) {
    echo '   <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h5><i class="fa fa-check"></i> Success!</h5>
                                        Successfully Checked In!
                                    </div>';
} else {

}?>
                                    <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                        <thead>
                                            <tr role="row">
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" style="width: 94px;">Name</th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 110px;">Arrived Date</th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 77px;">Reference Id</th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column descending" aria-sort="ascending" style="width: 50px;">Type</th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column descending" aria-sort="ascending" style="width: 50px;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php
$sql = "SELECT * FROM customer INNER JOIN transaction on customer.client_reference_id = transaction.client_reference_id  WHERE transaction_type='Daytour' OR transaction_type='Overnight'";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $client_reference_id = $rows['client_reference_id'];
    $first_name = $rows['first_name'];
    $last_name = $rows['last_name'];
    $arrival_date = new DateTime($arrival_date = $rows['arrival_date']);
    $transaction_type = $rows['transaction_type'];
    ?>
                                            <tr>
                                              <td><?php echo $first_name . ' ' . $last_name; ?></td>
                                              <td><?php echo $arrival_date->format('F j, Y'); ?></td>
                                              <td><?php echo $client_reference_id; ?></td>
                                              <td><?php echo $transaction_type; ?></td>
                                              <td>
                                            <div class="container">
                                            <a href="delete_room.php?room_id=<?php echo $room_id; ?>" class="text-muted">

<?php
echo '<a class="btn btn-sm bg-primary" href="extras_daytour.php?ref_id=' . $client_reference_id . '">Extras <i class="fa fa-plus"></i></a>';
    ?>

</a>
                                                            <?php
echo '<a class="btn btn-sm bg-info" href="overview_daytour.php?ref_id=' . $client_reference_id . '">Details <i class="fa fa-search"></i></a>';

    ?>



                                                            <a href="delete_room.php?room_id=<?php echo $room_id; ?>" class="text-muted">
                                                                <!-- <a href="check_out.php?res_id=<?php echo $client_reference_id; ?>" class="btn btn-sm bg-danger">Receipt <i class="fa fa-print"></i></a> -->
                                                            </a>
                                            </div>
                                              </td>
                                          </tr>
                                            <?php
}
?>
                                        </tbody>

                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <!-- Main Footer -->
            <?php
include 'layout/footer.php';
?>
                <!-- ./wrapper -->

                <!-- REQUIRED SCRIPTS -->


                <!-- jQuery -->
                <script src="plugins/jquery/jquery.min.js"></script>
                <!-- Bootstrap 4 -->
                <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
                <!-- DataTables -->
                <script src="plugins/datatables/jquery.dataTables.js"></script>
                <script src="plugins/datatables/dataTables.bootstrap4.js"></script>
                <!-- AdminLTE App -->
                <script src="dist/js/adminlte.min.js"></script>
                <script>
                    $(function() {

                        $('#example1').DataTable({
                            "paging": true,
                            "lengthChange": false,
                            "searching": true,
                            "ordering": true,
                            "info": true,
                            "autoWidth": false,
                            "stripeClasses": [ 'odd-row', 'even-row' ]
                        });
                    });
                </script>
</body>

</html>