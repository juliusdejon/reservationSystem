    <?php
// Requires functions.php
include 'functions.php';
// Redirect if Dates are Not Set

if ((empty($_SESSION['arrivalDate'])) || (empty($_SESSION['departureDate']))) {
    header('Location: index.php?dates=false');
}
// $arrival_date = date_create_from_format('F j, Y', $_SESSION['arrivalDate']);
// $arrival_date = $arrival_date->format('Y-m-d');

// $departure_date = date_create_from_format('F j, Y', $_SESSION['departureDate']);
// $departure_date = $departure_date->format('Y-m-d');

$arrival_date = $_SESSION['arrivalDate'];
$departure_date = $_SESSION['departureDate'];

$departure = strtotime($departure_date);
$arrival = strtotime($arrival_date);
$datediff = $departure - $arrival;

$noofNights = round($datediff / (60 * 60 * 24));
// Get the rang of dates
function date_range($first, $last, $step, $output_format)
{
    $dates = array();
    $current = strtotime($first);
    $last = strtotime($last);
    while ($current <= $last) {
        $dates[] = date($output_format, $current);
        $current = strtotime($step, $current);
    }
    return $dates;
}

?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

        <title>Villa Alfredo's Reservation System</title>

        <!-- Google font -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7CVarela+Round" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

        <!-- Bootstrap -->
        <link type="text/css" rel="stylesheet" href="../css/bootstrap.min.css" />

        <!-- Semantic -->
        <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />
        <!-- Initialize Semantic UI and JQuery to load Calendar Datepicker-->
        <script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" src="../js/semantic.min.js"></script>

        <!-- Owl Carousel -->
        <link type="text/css" rel="stylesheet" href="../css/owl.carousel.css" />
        <link type="text/css" rel="stylesheet" href="../css/owl.theme.default.css" />

        <!-- Magnific Popup -->
        <link type="text/css" rel="stylesheet" href="../css/magnific-popup.css" />

        <!-- Font Awesome Icon -->
        <link rel="stylesheet" href="../css/font-awesome.min.css">

        <!-- Icons -->
        <link rel="stylesheet" href="../css/icon.min.css">

        <!-- Custom stlylesheet -->
        <link type="text/css" rel="stylesheet" href="../css/style.css" />


    </head>

    <body>
        <!-- Header -->
        <header>
            <!-- Nav -->
            <nav id="nav" class="navbar">
                <div class="container">

                    <div class="navbar-header">
                        <!-- Logo -->
                        <div class="navbar-brand">
                            <a href="../index.php">
                                <img class="logo" src="../img/valogo-alt.png" alt="logo">
                                <img class="logo-alt" src="../img/valogo-alt.png" alt="logo">
                            </a>
                        </div>
                        <!-- /Logo -->

                        <!-- Collapse nav button -->
                        <div class="nav-collapse">
                            <span></span>
                        </div>
                        <!-- /Collapse nav button -->
                    </div>

                    <!--  Main navigation  -->
                    <ul class="main-nav nav navbar-nav navbar-right">
                        <li><a href="../index.php">Home</a></li>
                        <li><a href="../index.php#accomodation">Accomodation</a></li>
                        <li><a href="../#day-tour-cottages">Cottages</a></li>
                        <li><a href="../#contact">Contact</a></li>
                        <li><a href="./index.php"><button class="secondary-btn">Book Now</button></a></li>
                    </ul>
                    <!-- /Main navigation -->

                </div>
            </nav>
            <!-- /Nav -->
        </header>
        <section class="xs-padding">
            <div class="ui segment container">
                <div class="row">
                    <div class="col-md-9">
                        <div class="ui stackable mini steps">
                            <div class="completed step">
                                <i class="calendar alternate icon"></i>
                                <div class="content">
                                    <div class="title">Select Dates</div>
                                    <div class="description">Choose Date of Stay</div>
                                </div>
                            </div>
                            <div class="active step">
                                <i class="bed icon"></i>
                                <div class="content">
                                    <div class="title">Select Room</div>
                                    <div class="description">Choose Rooms</div>
                                </div>
                            </div>
                                <!-- <div class="disabled step">
                                    <i class="tag icon"></i>
                                    <div class="content">
                                        <div class="title">Amenities</div>
                                        <div class="description">Extras & Amenities</div>
                                    </div>
                                </div> -->
                            <div class="disabled step">
                                <i class="credit card icon"></i>
                                <div class="content">
                                    <div class="title">Payment</div>
                                    <div class="description">Verify reservation details</div>
                                </div>
                            </div>
                        </div>
                        <?php if (isset($_GET['rooms'])) {
    ?>
                        <div class="ui negative message">
                            <div class="header">
                                Room not Selected
                            </div>
                            <p> Please Select Rooms
                            </p>
                        </div>
                        <?php }?>
                        <form action="" method="post">
                            <div class="ui relaxed divided items">
                                <?php
// Select room type eg. Ana Villa, Mike Hotel
$res = $mysqli->query('SELECT * FROM room_type WHERE isDeleted=0');
while ($rows = mysqli_fetch_assoc($res)) {
    $roomType = $rows['room_id'];
    $room_adults_capacity = $rows['room_adults_capacity'];
    $room_child_capacity = $rows['room_child_capacity'];
    // Get the Input dates provided by the customer at the front end eg. he clicked July 24 - 26
    $range = date_range($arrival_date, $departure_date, "+1 day", "Y-m-d");
    // Slice it to July 24 - 25
    $reserved_period = array_slice($range, 0, -1);

    // Here you are selecting 'RESERVED ROOMS!!' AND ONLY WITH A STATUS THAT HAS BEEN CONFIRMED AND BOOKED REFLECTED by the given Dates on the site
    $s = "SELECT * FROM reserved_rooms JOIN customer ON customer.client_reference_id = reserved_rooms.client_reference_id
    JOIN reservation ON reserved_rooms.client_reference_id = reservation.client_reference_id
    WHERE reserved_rooms.room_type_id = '$roomType' AND (reservation_status='Booked' OR reservation_status='Checked In')";

    $r = $mysqli->query($s);

    // Instantiate an array to store reserved rooms
    $reserved_rooms = 0;

    while ($ro = mysqli_fetch_assoc($r)) {
        $arrival_date_db = $ro['arrival_date'];
        $departure_date_db = $ro['departure_date'];
        // -1 to the Departure date. July 26 becomes July 25... so it will not consider 26 as still Reserved date.
        $departure_date_db = date('Y-m-d', (strtotime('-1 day', strtotime($departure_date_db))));
        // Get the Database Arrival Date and Departure Date of Ana Villa Room type that has been Reserved
        $rangeDb = date_range($arrival_date_db, $departure_date_db, "+1 day", "Y-m-d");

        $roomNotAvailable = 'not existing';
        foreach ($reserved_period as $day) {
            // Check the reserved period above an Array of dates
            // eg. $reservation_period = [2018-07-25,2018-07-25] // These dates will be compared at the Database
            // rangeDb
            if (in_array($day, $rangeDb)) {
                $roomNotAvailable = 'existing';
            }
        }
        if ($roomNotAvailable == 'existing') {
            $reserved_rooms++;
        }

    }


    // Available Rooms = Total Rooms - Reserved Rooms IN Check In Date
    $sqlAvailableRoom = "SELECT COUNT(*) as TotalRoom FROM rooms WHERE room_type_id='$roomType'";
    $resAvailableRoom = $mysqli->query($sqlAvailableRoom);
    $results = mysqli_fetch_assoc($resAvailableRoom);
    $result = $results['TotalRoom'] - $reserved_rooms;
    ?>
                                    <input type="hidden" name='room_id<?php echo $roomType; ?>' value="<?php echo $roomType; ?>" />
                                    <div class="item">


                                        <div class="ui medium image">


                                                                   <?php  if ($result == 0 || $result < 0) {
        echo '<div class="ui red ribbon label" style="opacity:0.9;">';
        echo ' <i class="hotel icon"></i> Not Available on Selected Dates';
        echo '</div>';
    } else {

    }
    ?>

                                            <img src="../img/<?php echo $rows['room_img']; ?>">
</div>
                                        <div class="content">
                                            <a class="header">

                                                <?php echo $rows['room_name']; ?>
                                            </a>
                                            <div class="meta">

                                            </div>
                                            <div class="description">
                                                <?php echo $rows['room_info']; ?>
                                            </div>
                                            <table class="ui very basic collapsing celled table">
  <thead>
    <tr>
  </tr></thead>
  <tbody>
  <tr>

    </tbody>
</table>


                                            <div class="extra">


                                                <div class="ui input right floated">

                                                    <select class="ui tiny teal button" name="quantity<?php echo $roomType; ?>">
                                            <option value="0" selected>0</option>
                                            <?php

    for ($i = 1; $i <= $result; $i++) {
        echo "<option value='$i'>$i</option>";
    }
    ?>
                                        </select>

                                                </div>
                                                <div class="ui tiny blue label right floated right pointing">
                                                    Qty
                                                </div>

                                                <div class="ui input right floated">

                                              </div>

                                    

       <div class="ui tiny input right floated">
</div>



                                                <div class="ui-label">₱
                                                    <?php echo number_format($rows['room_price'], 2); ?>/night</div>
                                            </div>

                                        </div>

                                    </div>
                                    <?php }?>
                            </div>
                    </div>
                    <div class="col-md-3">
                        <span class="reservation">Your Reservation</span>
                        <div class="reservation-content">
                            <div class="ui clearing divider"></div>
                            <div class="reservation-title">Room Selections: </div>
                            <div id="room-selected"></div>
                            <div class="reservation-title">Arrival Date:</div>
                            <?php echo $_SESSION['arrivalDate']; ?>
                            <div class="ui hidden divider"></div>
                            <div class="reservation-title">Departure Date</div>
                            <?php echo $_SESSION['departureDate']; ?>
                            <div class="ui hidden divider"></div>
                            <div class="reservation-title">Room Nights</div>
                            <?php echo $noofNights; ?>
                            <div class="ui clearing divider"></div>
                            <div class="reservation-title">Room Total</div>
                            <div class="ui clearing divider"></div>
                            <?php

?>
                        </div>
                        <input type="submit" name="selectRooms" class="fluid huge ui teal button" value="Proceed" />
                        </form>

                    </div>
        </section>
        </div>


        <!-- Back to top -->
        <div id="back-to-top"></div>
        <!-- /Back to top -->

        <!-- Preloader -->
        <div id="preloader">
            <div class="preloader">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>

        <!-- jQuery Plugins -->
        <script type="text/javascript" src="../js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../js/main.js"></script>

    </body>

    </html