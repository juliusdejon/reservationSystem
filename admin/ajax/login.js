    $("#login").submit(function(e) {
        e.preventDefault();
        $("#button").html('<img src="ajax/loading-spinner.gif" width="25px">')
        var form = $(this);
        var post_url = form.attr("action");
        var post_data = form.serialize();
        $.ajax({
            type: "POST",
            url: post_url,
            data: post_data,
            cache: false,
            success: function(info) {
                $("#result").html(info);
                $("#button").text('Submit');
            },
        });
    });