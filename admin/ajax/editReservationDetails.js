$("#form-update-reservation").submit(function(e) {
    e.preventDefault();
    $("#btn-update-reservation").html('<img src="ajax/loading-spinner.gif" width="25px">')
    var form = $(this);
    var post_url = form.attr("action");
    var post_data = form.serialize();
    $.ajax({
        type: "POST",
        url: post_url,
        data: post_data,
        cache: false,
        success: function(info) {
            $("#result-reservation").html(info);
            $("#btn-update-reservation").text('Update');
        },
    });
});