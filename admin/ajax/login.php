<?php
require_once '../../config/db.php';
require_once '../../config/User.php';
session_start();
$username = (isset($_POST['username'])) ? $_POST['username'] : '';
$password = (isset($_POST['password'])) ? $_POST['password'] : '';
$message = '';

if ($username == '' || $password == '') {
    echo $message = '<div class="alert alert-danger mt-2" role="alert">All fields are required</div>';
} else {
    // Get the Login Data from the View
    $loginData = [
        'username' => $username,
        'password' => $password,
    ];
    // Instantiate User to login
    $user = new User($pdo);
    // Check credentials
    $isAdmin = $user->login($loginData);

    if ($isAdmin) {
        $_SESSION['id'] = $_POST["username"];
        echo "<script>window.location='dashboard.php'</script>";
    } else {
        echo $message = '<div class="alert alert-danger mt-2" role="alert">Incorrect Username or Password</div>';
    }

}
