<?php

include '../config/mysqli.php';
@$isDeleted = $_GET['success'];
@$isCancelled = $_GET['cancelled'];
@$isRebooked = $_GET['rebooking'];
@$Insufficient = $_GET['insufficent'];
@$excess = $_GET['excess'];
function multiplyDays($total)
{
    global $noofNights;
    return ($total * $noofNights);
}

?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>Villa Alfredo Admin</title>

        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/adminlte.min.css">
        <!-- Semantic UI -->
        <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />




        <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap4.css">
    </head>

    <body class="hold-transition sidebar-mini">
        <div class="wrapper">

            <?php
include 'layout/navbar.php';
include 'layout/sidebar.php';
$user = $_SESSION['id'];
?>
                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <div class="content-header">
                        <div class="container-fluid">

                        </div>
                        <!-- /.container-fluid -->
                    </div>
                    <!-- /.content-header -->

                    <!-- Main content -->
                    <div class="content">
                        <?php if ($isDeleted == 'true') {
    echo "<div class='callout callout-success'>
                        <h5>Successfully Deleted!</h5>
                      </div>";
}?>
                        <?php if ($isDeleted == 'false') {
    echo "<div class='callout callout-info'>
                        <h5>Failed to Delete</h5>
                        <p>You are trying to delete a Booked or Cancelled reservation</p>
                      </div>";
}?>

                        <?php if ($isCancelled == 'true') {
    echo "<div class='callout callout-success'>
                        <h5>Successfully Cancelled a Reservation</h5>
                      </div>";
}?>
                        <?php if ($isCancelled == 'false') {
    echo "<div class='callout callout-info'>
                        <h5>Failed to Delete</h5>
                        <p>The Reservation Cannot be Cancelled</p>
                      </div>";
}?>

                        <?php if ($isRebooked == 'false') {
    echo "<div class='callout callout-info'>
                        <h5>The Selected Reservation cannot be Re-Book because of the ff.</h5>
                        <ul>
                        <li>The Booked Reservation is already checked in.</li>
                        <li>Pending Reservations needs to be Approved first. Approve first the Pending Reservation and verify the receipt</li>
                        <li>For Cancelled Reservation. Approve first their Reservation.</li>
                        </ul>
                      </div>";
}?>




                        <?php if ($Insufficient) {
    echo "<div class='callout callout-warning'>
                        <h5>Please Enter the right Amount</h5>
                      </div>";
}?>
                        <?php if ($excess) {
    echo "<div class='callout callout-danger'>
                        <h5>Amount is higher than the Grand Total</h5>
                      </div>";
}?>



                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Pending Reservations</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">

                                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                            <thead>
                                                <tr role="row">

                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending">Name</th>
                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Date</th>
                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Arrival Date</th>
                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Departure Date</th>
                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Reference Id</th>
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column descending" aria-sort="ascending">Status</th>
                                                    <th>Receipt</th>
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column descending" aria-sort="ascending">Action</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
$sql = "SELECT * FROM customer JOIN reservation ON customer.client_reference_id=reservation.client_reference_id WHERE reservation_status='Pending'";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $client_reference_id = $rows['client_reference_id'];
    $first_name = $rows['first_name'];
    $last_name = $rows['last_name'];
    $arrival_date = new DateTime($arrival_date = $rows['arrival_date']);
    $departure_date = new DateTime($departure_date = $rows['departure_date']);
    $created_at = new DateTime($created_at = $rows['created_at']);
    $reservation_status = $rows['reservation_status'];
    $receipt = $rows['receipt'];
    // $receipt_2 = $rows['receipt_2'];
    ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $first_name . ' ' . $last_name; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $created_at->format('F j, Y'); ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $arrival_date->format('F j, Y'); ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $departure_date->format('F j, Y'); ?>
                                                        </td>

                                                        <td>
                                                            <?php echo $client_reference_id; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $reservation_status; ?>
                                                        </td>
                                                        <td>

                                                            <?php if ($user_role == 'frontdesk') {} else {?>
                                                            <button type="button" name="approve" class='btn btn-sm btn-success' data-toggle="modal" data-target="#<?php echo $client_reference_id; ?>">Approve</button>
                                                            <a href="cancel_reservation.php?res_id=<?php echo $client_reference_id; ?>" onclick="return confirm('Are you sure you want to Cancel this Reservation?');" class='btn btn-danger btn-sm'>Cancel</a>
                                                            <?php }?>
                                                            <!-- Modal -->
                                                            <div class="modal" id="<?php echo $client_reference_id; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                                <div class="modal-dialog modal-dialog-centered modal-md" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLongTitle">Receipt of
                                                                                <?php echo $first_name . ' ' . $last_name; ?>
                                                                            </h5>

                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div>
                                                                                <div id="result<?php echo $client_reference_id; ?>"></div>
                                                                                <div> 50% of the Total:
                                                                                    <?php
$select = "SELECT * FROM customer JOIN reservation ON customer.client_reference_id = reservation.client_reference_id WHERE reservation.client_reference_id='$client_reference_id'";

    $result = $mysqli->query($select);

    while ($rows = mysqli_fetch_assoc($result)) {
        $arrival_date = $rows['arrival_date'];
        $departure_date = $rows['departure_date'];
        $no_of_adults = $rows['no_of_adults'];
        $no_of_kids = $rows['no_of_kids'];
    }
    $departure = strtotime($departure_date);
    $arrival = strtotime($arrival_date);
    $datediff = $departure - $arrival;

    $noofNights = round($datediff / (60 * 60 * 24));

// Rooms

    $selectRooms = "SELECT * FROM reserved_rooms
            JOIN customer ON
            reserved_rooms.client_reference_id = customer.client_reference_id
            JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
            WHERE customer.client_reference_id ='$client_reference_id'
            GROUP BY reserved_rooms.room_type_id";

    $ResultRooms = $mysqli->query($selectRooms);
    $count = 1;
    $total = 0;
    $subtotal = [];
    while ($rowrow = mysqli_fetch_assoc($ResultRooms)) {
        $room_type_id = $rowrow['room_type_id'];
        $sql1 = "SELECT COUNT(*) as quantity FROM reserved_rooms
        JOIN customer ON
        reserved_rooms.client_reference_id = customer.client_reference_id
        JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
        WHERE customer.client_reference_id ='$client_reference_id' AND room_type_id ='$room_type_id'";
        $res1 = $mysqli->query($sql1);
        $row1 = mysqli_fetch_assoc($res1);
        $total = $row1['quantity'] * $rowrow['room_price'];
        $count++;
        array_push($subtotal, $total);
    }

    $grandTotal = array_sum($newTotal = array_map("multiplyDays", $subtotal));

    $totalAdult = $no_of_adults * 250;
    $totalKid = $no_of_kids * 150;
    $grandTotal = $grandTotal + $totalAdult + $totalKid;
    echo "₱ " . number_format($grandTotal / 2);
    $requiredPayment = $grandTotal / 2;

    ?>
                                                                                    </div>
                                                                                <div class="row">
                                                                                     <div class="col-md-3">
                                                                                        <small>Confirm the amount below</small>
                                                                                    </div>

                                                                                    <div class="col-md-8">
                                                                                        <form action="acceptDownpayment.php" method="post" id="accept<?php echo $client_reference_id; ?>" name="customerForm<?php echo $client_reference_id; ?>" onsubmit="return validateForm(<?php echo $client_reference_id; ?>);">
                                                                                        <input type="hidden" name="requiredPayment" value="<?php echo $requiredPayment; ?>">
                                                                                        <input type="hidden" name="requiredTotal" value="<?php echo $grandTotal; ?>">
                                                                                            <div class="input-group-prepend input-group-sm">
                                                                                                <span class="input-group-text">
                    ₱
                    </span>
                                                                                                <?php $sqlAmt = "SELECT downpayment FROM transaction WHERE client_reference_id ='$client_reference_id'";
    $resAmt = $mysqli->query($sqlAmt);
    $amount = mysqli_fetch_assoc($resAmt);
    $amount = $amount['downpayment'];
    if ($amount) {
        echo "<input type='number' name='amount' class='form-control' value='" . $amount . "' />";
    } else {
        echo "<input type='number' name='amount' class='form-control'/>";
    }
    ?>
    <input type="hidden" name="user" value="<?php echo $user;?>" />
                                                                                                <input type="hidden" name="client_reference_id" value="<?php echo $client_reference_id; ?>" />
                                                                                                <span class="input-group-append">
                    <button type="submit" name="accept" id="button<?php echo $client_reference_id; ?>" class="btn btn-info btn-flat btn-accept" onclick="foo('<?php echo $client_reference_id; ?>')">Accept Downpayment</button>
                  </span>
                                                                                            </div>
                                                                                        </form>

                                                                                    </div>
                                                                                </div>
                                                                                <div>
</div>
                                                                                <?php if ($receipt == '') {
                                                                                    echo "No Receipts Found";
                                                                                }
                                                                                    else {
                                                                                        echo "<a href='dist/img/receipts/" . $receipt . "' target='_blank'>
                                                                                        <img src='dist/img/receipts/" . $receipt . "' width='50%'/></a>";
                                                                                 
                                                                                    }
    ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                        </td>
                                                        <td>
                                                            <div class='container'>

                                                                <input type="hidden" name="client_reference_id" value="<?php echo $client_reference_id; ?>" />
                                                                <a class='btn btn-info btn-sm' href="overview.php?ref_id=<?php echo $client_reference_id; ?>&view_by=reservation">Details <i class='fa fa-search'></i></a>
                                                                <?php if ($user_role == 'frontdesk') {

    } else {
        ?>
                                                                    <a href="delete_reservation.php?res_id=<?php echo $client_reference_id; ?>" onclick="return confirm('Are you sure you want to delete this item?');" class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></a>
                                                                <?php }?>
                                                            </div>

                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php
}
?>
                                            </tbody>

                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.content -->
                </div>
                <!-- /.content-wrapper -->

                <!-- Main Footer -->
                <?php
include 'layout/footer.php';
?>
                    <!-- ./wrapper -->

                    <!-- REQUIRED SCRIPTS -->


                    <!-- jQuery -->
                    <script src="plugins/jquery/jquery.min.js"></script>
                    <!-- Downpayment Ajax -->
                    <!-- <script type="text/javascript" src="ajax/acceptDownpayment.js"></script> -->
                    <!-- Bootstrap 4 -->
                    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
                    <!-- DataTables -->
                    <script src="plugins/datatables/jquery.dataTables.js"></script>
                    <script src="plugins/datatables/dataTables.bootstrap4.js"></script>
                    <!-- AdminLTE App -->
                    <script src="dist/js/adminlte.min.js"></script>
                    <script>
                        // Validation Payment form
                        function validateForm(form) {
                            var amount = document.forms[form]["amount"].value;
                            var requiredPayment = document.forms[form]["requiredPayment"].value;

                            if (amount < requiredPayment ) {
                                alert("Insufficient Amount");
                                return false;
                            }


                        }
                    </script>


                    <script>
                        $(function() {

                            $('#example1').DataTable({
                                "paging": true,
                                "lengthChange": false,
                                "searching": true,
                                "ordering": true,
                                "info": true,
                                "autoWidth": false,
                                "stripeClasses": ['odd-row', 'even-row'],
                                "order": [
                                    [4, "desc"]
                                ]
                            });
                        });
                    </script>
    </body>

    </html>
    <?php
if (isset($_POST['approve'])) {
    $client_reference_id = $_POST['client_reference_id'];

    $sql = "UPDATE reservation SET reservation_status ='Booked' WHERE client_reference_id='$client_reference_id'";
    if ($mysqli->query($sql)) {
        echo "<script>alert('Approved!');</script>";
    } else {
        echo "<script>alert('Denied!');</script>";
    }
}

if (isset($_POST['delete'])) {
    $client_reference_id = $_POST['client_reference_id'];
    $sql = "DELETE FROM reservation WHERE client_reference_id='$client_reference_id'";
    if ($mysqli->query($sql)) {
        $sql = "DELETE FROM reserved_rooms WHERE client_reference_id ='$client_reference_id'";
        if ($mysqli->query($sql)) {
            echo "<script>alert('Deleted!');</script>";
        }
    }
}