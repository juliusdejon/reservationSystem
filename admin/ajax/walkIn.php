<?php
include '../../config/mysqli.php';

// Global Variables
$first_name = $_POST['first_name'];
$last_name = $_POST['last_name'];
$arrival_date = $_POST['arrival_date'];
$contact = $_POST['contact'];
$email = $_POST['email'];
$no_of_adults = $_POST['no_of_adults'];
$no_of_kids = $_POST['no_of_kids'];

// Queries

$sql = "SELECT * FROM cottage_type";
$res = $mysqli->query($sql);
?>
<form action="walkInForm.php" method="post">
<input type="hidden" name="first_name" value="<?php echo $first_name;?>" />
<input type="hidden" name="last_name" value="<?php echo $last_name;?>" />
<input type="hidden" name="arrival_date" value="<?php echo $arrival_date;?>" />
<input type="hidden" name="contact" value="<?php echo $contact;?>" />
<input type="hidden" name="email" value="<?php echo $email;?>" />
<input type="hidden" name="no_of_adults" value="<?php echo $no_of_adults;?>" />
<input type="hidden" name="no_of_kids" value="<?php echo $no_of_kids;?>" />

    <div class="row">
        <!--First Col -->
        <div class="col-md-9">
            <div class="container fluid">
                <div class="ui three cards">
                    <?php while ($rows = mysqli_fetch_assoc($res)) {
    // POST VARIABLES
    $cottage_type_id = $rows['cottage_type_id'];
    $cottage_name = $rows['cottage_name'];
    $cottage_price = $rows['cottage_price'];
    $cottage_info = $rows['cottage_info'];
    $cottage_img = $rows['cottage_img'];

    ?>
                    <div class="card">
                        <div class="image">
                            <img src="dist/img/<?php echo $cottage_img; ?>">
                        </div>
                        <div class="content">
                            <h3>
                                <?php echo $cottage_name; ?>
                            </h3>
                            <div>Price:
                                <?php echo number_format($cottage_price, 2); ?>
                            </div>
                            Quantity:
                            <div class="ui input right floated">
                            <input type="hidden" name="cottage_id<?php echo $cottage_type_id; ?>"  />
                            <select class="ui tiny teal button" name="quantity<?php echo $cottage_type_id; ?>">
                                            <option value="0" selected>0</option>
                                            <?php
// Available Rooms = Total Rooms - Reserved Rooms IN Check In Date
    $sqlAvailableRoom = "SELECT COUNT(*) as TotalCottages FROM cottages WHERE cottage_type_id='$cottage_type_id'";
    $resAvailableRoom = $mysqli->query($sqlAvailableRoom);
    $result = mysqli_fetch_assoc($resAvailableRoom);
    $result = $result['TotalCottages'];
    for ($i = 1; $i <= $result; $i++) {
        echo "<option value='$i'>$i</option>";
    }
    ?>
                                        </select>

                             </div>

                        </div>
                        <div class="extra">
                            <div>
                                <a class="fluid tiny ui teal button" data-toggle="collapse" href="#<?php echo $cottage_type_id; ?>" role="button" aria-expanded="false" aria-controls="collapseExample">
                                    Description
                                </a>
                                <div class="collapse" id="<?php echo $cottage_type_id; ?>">
                                    <?php echo $cottage_info; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="ui segment">
                <button type="submit" name="compute" class="ui fluid massive teal button">Compute</button>
            </div>
        </div>
    </div>
    </form>
    <?php
if (isset($_POST['compute'])) {

    $cottages = [];
    $hasSelectedCottage = [];
    $sql = "SELECT * FROM cottage_type";
    $res = $mysqli->query($sql);
    while ($rows = mysqli_fetch_assoc($res)) {
        $cottageType = $rows['cottage_type_id'];
        $_POST['room_id' . $cottageType];
        $isSelected = $_POST['quantity' . $cottageType];
        array_push($hasSelectedCottage, $isSelected);
        if ($isSelected != 0) {
            array_push($room, $cottageType);
            array_push($room, $isSelected);
        }
    }
    echo $cottages = array_chunk($cottages, 2);

}

    if(isset($_POST['compute'])) {
//    // Random Numbers
//    $random = substr(str_shuffle(str_repeat("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", 5)), 0, 5);
//    // Date format dmy append to refernece ID
//    $date = date("dmY");
//    $first_name = strtolower($first_name);
//    $last_name = strtolower($last_name);
//    $first_name = ucfirst($first_name);
//    $last_name = ucfirst($last_name);
//    $client_reference_id = $first_name[0] . $last_name[0] . $date . $random;

//         $sql = "INSERT INTO customer (client_reference_id,first_name,last_name,contact,email,arrival_date,departure_date)
//                 VALUES('$client_reference_id', '$first_name', '$last_name', '$contact','$email', '$arrival_date','$arrival_date')";
//         $res = $mysqli->query($sql);
//         if($res) {
//             for ($i = 0; $i < count($cottages); $i++) {
//                 $room_id = $cottages[$i][0];
//                 $room_qty = $cottages[$i][1];
//                 for ($j = 0; $j < $room_qty; $j++) {
//                     $sql1 = "INSERT INTO occupied_cottages (cottage_type_id,client_reference_id) VALUES('$room_id','$client_reference_id')";
//                     $mysqli->query($sql1);
//                 }
//         }
//     }
}