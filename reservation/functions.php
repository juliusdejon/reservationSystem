<?php
// Important Connection
include '../config/mysqli.php';
session_start();
// Set Timezone
date_default_timezone_set('Asia/Manila');

// --Mailing Requirements--
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;

require '../src/Exception.php';
require '../src/PHPMailer.php';
require '../src/SMTP.php';

// --Mailing Requirments--

// Paypal Modules
include 'paypalFunctions.php';
include 'paypalConfig.php';
// Set first the Dates

if (isset($_POST['submitDates'])) {
    if ($_POST['arrivalDate'] == $_POST['departureDate']) {
        echo "<script>alert('Reservation period is per Night, Enter range of Dates');</script>";
        return;
    }
    $_SESSION['arrivalDate'] = (isset($_POST['arrivalDate'])) ? $_POST['arrivalDate'] : '';
    $_SESSION['departureDate'] = (isset($_POST['departureDate'])) ? $_POST['departureDate'] : '';
    header('Location: select-room.php');
}

if (isset($_POST['selectRooms'])) {

    $sql = "SELECT * FROM room_type WHERE isDeleted=0";
    $res = $mysqli->query($sql);
    $hasSelectedRoom = [];
    $hasSelectedAdult = [];
    $room = [];

    while ($rows = mysqli_fetch_assoc($res)) {
        $roomType = $rows['room_id'];

        $_POST['room_id' . $roomType];

 

        $isSelected = $_POST['quantity' . $roomType];
        array_push($hasSelectedRoom, $isSelected);
        if ($isSelected != 0) {
            array_push($room, $roomType);
            array_push($room, $isSelected);
        }
    }
    // Validate if a Room has been Selected. If not do not proceed
    $count = 0;
    for ($i = 0; $i < count($hasSelectedRoom); $i++) {
        if ($hasSelectedRoom[$i] != 0) {

        } else {
            $count++;
        }
    }
    if ($count == count($hasSelectedRoom)) {
        echo "<script>alert('Please select a room');</script>";
    } else {
        $_SESSION['rooms'] = $room;
        header('Location: payment.php');
    }

}

// Add Amenities

if (isset($_POST['addAmenities'])) {
    $item = $_POST['item'];
    $quantity = $_POST['quantity'];

    $_SESSION['item'] = $item;
    $_SESSION['quantity'] = $quantity;
    // foreach (array_combine($item, $quantity) as $i => $q) {
    //     echo $i;
    //     echo $q;
    //     // $i = intval($i);
    //     // if ($i == '' || $q == '') {

    //     // } else {
    //     //     for ($a = 0; $a < $q; $a++) {
    //     //         // $sql = "INSERT INTO customer_extras (client_reference_id,extras_id) VALUES('$client_ref_id','$i')";
    //     //         // $res = $mysqli->query($sql);
    //     //     }
    //     // }
    // }
    header('Location: payment.php');
}
// Pay

if (isset($_POST['pay'])) {
    $_SESSION['first_name'] = (isset($_POST['first_name'])) ? $_POST['first_name'] : '';
    $_SESSION['last_name'] = (isset($_POST['last_name'])) ? $_POST['last_name'] : '';
    $_SESSION['contact'] = (isset($_POST['contact'])) ? $_POST['contact'] : '';
    $_SESSION['email'] = (isset($_POST['email'])) ? $_POST['email'] : '';
    $_SESSION['no_of_adults'] = (isset($_POST['no_of_adults'])) ? $_POST['no_of_adults'] : '';
    $_SESSION['no_of_kids'] = (isset($_POST['no_of_kids'])) ? $_POST['no_of_kids'] : '';
    $_SESSION['additional_details'] = (isset($_POST['additional_details'])) ? $_POST['additional_details'] : '';

    $totalAdult = $_SESSION['totalAdult'];
    $totalChild = $_SESSION['totalChild'];

    $no_of_adults = array_sum($totalAdult);
    $no_of_kids = array_sum($totalChild);

    $no_of_adults ? $no_of_adults : '0';
    $no_of_kids ? $no_of_kids : '0';

    // $no_of_adults = $_SESSION['no_of_adults'];
    // $no_of_kids = $_SESSION['no_of_kids'];

    $rooms = $_SESSION['rooms'];
    $rooms = array_chunk($rooms, 2);

    $adult = $_SESSION['adult'];
    $adult = array_chunk($adult, 2);

    $child = $_SESSION['child'];
    $child = array_chunk($child, 2);

    $first_name = $_SESSION['first_name'];
    $last_name = $_SESSION['last_name'];

    $additional_details = $_SESSION['additional_details'];

    // Random Numbers
    $random = substr(str_shuffle(str_repeat("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", 5)), 0, 5);
    // Date format dmy append to refernece ID
    $date = date("dmY");
    $first_name = mysqli_real_escape_string($mysqli, $first_name);
    $last_name = mysqli_real_escape_string($mysqli, $last_name);

    $first_name = strtolower(trim($first_name, " "));
    $last_name = strtolower(trim($last_name, " "));
    $first_name = ucfirst($first_name);
    $last_name = ucfirst($last_name);

    $client_reference_id = $first_name[0] . $last_name[0] . $date . $random;

    $contact_number = $_SESSION['contact'];
    $email_address = $_SESSION['email'];

    $email_address = trim($email_address, " ");
    $email_address = mysqli_real_escape_string($mysqli, $email_address);

    $arrival_date = $_SESSION['arrivalDate'];
    $departure_date = $_SESSION['departureDate'];
    // $arrival_date = date_create_from_format('F j, Y', $_SESSION['arrivalDate']);
    // $arrival_date = $arrival_date->format('Y-m-d');

    // $departure_date = date_create_from_format('F j, Y', $_SESSION['departureDate']);
    // $departure_date = $departure_date->format('Y-m-d');

    $arrival_date = $_SESSION['arrivalDate'];
    $departure_date = $_SESSION['departureDate'];
    $created_at = date('Y-m-d H:i:s');
    $date_now = date('Y-m-d');
    // Paypal
    if ($_POST['method'] == 'paypal') {
        $reservation_status = 'Pending';

        $res = $mysqli->query("INSERT INTO `customer`(`client_reference_id`, `first_name`, `last_name`,`contact_number`, `email_address`, `arrival_date`, `departure_date`,`additional_details`, `no_of_adults`,`no_of_kids`)
      VALUES ('$client_reference_id', '$first_name', '$last_name', '$contact_number', '$email_address', '$arrival_date', '$departure_date','$additional_details','$no_of_adults','$no_of_kids')");
        if ($res) {

            $timestamp = strtotime($date_now);
            $day = date('D', $timestamp);
            if ($day == 'Sat') {
                $expires_at = date('Y-m-d', strtotime("+4 days"));
            } else if ($day == 'Sun') {
                $expires_at = date('Y-m-d', strtotime("+3 days"));
            } else if ($day == 'Fri') {
                $expires_at = date('Y-m-d', strtotime("+4 days"));
            } else {
                $expires_at = date('Y-m-d', strtotime("+2 days"));
            }

            $time = date("H:i:s");
            $expires_at = $expires_at . ' ' . $time;
            $res = $mysqli->query("INSERT INTO `reservation`(`client_reference_id`,`reservation_type`, `reservation_status`,`created_at`,`expires_at`) VALUES ('$client_reference_id','Online','Pending','$created_at','$expires_at')");
            if ($res) {
                for ($i = 0; $i < count($rooms); $i++) {
                    $room_id = $rooms[$i][0];
                    $room_qty = $rooms[$i][1];
                    for ($j = 0; $j < $room_qty; $j++) {
                        $sql1 = "INSERT INTO reserved_rooms (room_type_id,client_reference_id) VALUES('$room_id','$client_reference_id')";
                        $mysqli->query($sql1);
                    }
                }

                $encrypted = base64_encode($client_reference_id);

                if ($mailing == true) {

                    if ($deployedMailing == true) {
                        $from = "villa_alfredo_email@villaalfredo.x10host.com"; // This is an Email Account coming from the x10hos that has been setup on hosting. server only.
                        $to = "$email_address,$first_name.' '.$last_name"; // Recepient

                        $baseurl = $_SERVER['SERVER_NAME'];
                        $mailbody = "
                        <div id=':14u' class='ii gt'><div id=':14t' class='a3s aXjCH '><div id='m_-852930379096079456page-wrapper' style='background-color:#ffffff;padding:0px'><div class='adM'>
</div><table style='font-family:'Open Sans',sans-serif;font-size:12px;margin:0px' width='100%' border='0' cellspacing='0'>
<tbody>
<tr>
<td>
<p style='font-size:20px'><span class='il'>Villa</span> <span class='il'>Alfredo</span> Resort</p>
</td>
</tr>
<tr>
<td class='m_-852930379096079456box-grey' style='background-color:#eeeeee;padding:15px 15px 15px 15px;border-bottom-left-radius:0;border-bottom-right-radius:0' valign='top'>
<table width='100%' border='0' cellpadding='0'>
<tbody>
<tr valign='top'>
<td width='65%'><span style='font-size:12px'><strong>Thank you, ".$first_name." Your booking is pending.</strong></span></td>
<td style='font-size:12px' width='35%'>
<table width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td align='right'><strong>Reference Number:</strong>".$client_reference_id."</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td class='m_-852930379096079456box-grey' style='background-color:#f8f8f8;padding:0 15px 15px 15px;border-top-left-radius:0;border-top-right-radius:0' valign='top'>
<table width='100%' border='0' cellpadding='0'>
<tbody>
<tr valign='top'>
<td width='65%'>
<ul style='padding:15px 0 0 15px;margin:0'>
<li style='line-height:20px;padding:0;margin:0'>To secure your reservation, Please pay the stated amount below on our Primary account BDO Account Number: 00-131-067-3959 and upload the receipt here <a href='".$baseurl."/reservation/upload_receipt.php' target='_blank'>Upload Receipt</a></li>
<li style='line-height:20px;padding:0;margin:0'>For booking enquires or amendments please contact us directly at <a href='mailto:test.villaalfredo@gmail.com' target='_blank'>test.villaalfredo@gmail.com</a> or (045) 455 1397.</li>
<li style='line-height:20px;padding:0;margin:0'>For cancellations <a href='".$baseurl."/reservation/cancel-reservations.php?res_id=".$encrypted."' target='_blank'>Click Here</a></li>
</ul>
</td>
<td style='font-size:12px' width='35%'>
<table width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td align='right'>&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td>
<div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Your Booking</strong></div>
<table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td style='padding:1px 10px 1px 20px' valign='top' width='20%'><strong>Guest:</strong></td>
<td style='padding:1px 20px' width='80%'>".$first_name." ".$last_name." </td>
</tr>
<tr>
<td style='padding:1px 10px 1px 20px' valign='top' width='20%'><strong>Details:</strong></td>
<td style='padding:1px 0px 1px 20px' width='80%'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                                <tbody><tr>
                                    <td width='75%'>
                                    ";
                        $sql = "SELECT * FROM reserved_rooms
                        JOIN customer ON
                        reserved_rooms.client_reference_id = customer.client_reference_id
                        JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
                        WHERE customer.client_reference_id ='$client_reference_id'
                        GROUP BY reserved_rooms.room_type_id";
                         $res = $mysqli->query($sql);
                         $total = 0;
                         $subtotal = [];
                         while ($rows = mysqli_fetch_assoc($res)) {
                            $room_type_id = $rows['room_type_id'];
                            $sql1 = "SELECT COUNT(*) as quantity FROM reserved_rooms
                          JOIN customer ON
                          reserved_rooms.client_reference_id = customer.client_reference_id
                          JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
                          WHERE customer.client_reference_id ='$client_reference_id' AND room_type_id ='$room_type_id'";
                            $res1 = $mysqli->query($sql1);
                            $row1 = mysqli_fetch_assoc($res1);
                            $total = $row1['quantity'] * $rows['room_price'];

                            $mailbody .= "
                                            ".$rows['room_name'] ." x
                                            " . $row1['quantity'] . ",
                                        ";

                            $count++;
                            array_push($subtotal, $total);
                        }
                   
                        function multiplyDays($total)
                        {
                            global $noofNights;
                            return ($total * $noofNights);
                        }
                        $grandTotal = array_sum(array_map("multiplyDays", $subtotal));
                        $totalAdult = $no_of_adults * 250;
                        $totalKid = $no_of_kids * 150;
                        $grandTotal = $grandTotal + $totalAdult + $totalKid;
                        $grandestTotal = number_format($grandTotal, 2);
                        $downTotal = $grandTotal / 2;
                        $vat = $downTotal * 0.12;
                        $vatable = number_format($downTotal - $vat,2); 
                        $vat_display = number_format($vat,2);
                        $down = number_format($grandTotal / 2, 2);



                        $mailbody .="Adult x ".$no_of_adults.",
                        Child x ".$no_of_kids."";

                        $mailbody .="-PHP ".$grandestTotal."</td>
                            
                                </tr>
                            </tbody></table></td>
</tr>
<tr>
<td style='padding:1px 10px 1px 20px' width='20%'><strong>Check-in:</strong></td>
<td style='padding:1px 20px' width='80%'>".$arrival_date." from 14:00</td>
</tr>
<tr>
<td style='padding:1px 10px 1px 20px' width='20%'><strong>Check-out:</strong></td>
<td style='padding:1px 20px' width='80%'>".$departure_date." until 12:00</td>
</tr>
</tbody>
</table>
</td>
</tr>

<tr>
<td>
<div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Additional Details</strong></div>
<table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td><table width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody><tr>
    <td width='20%' style='padding:1px 10px 1px 20px'><strong>Additional comments:</strong></td>
    <td width='80%' style='padding:1px 20px'>".$additional_details."</td>
</tr>
</tbody></table></td>
</tr>
 </tbody>
</table>
</td>
</tr>

<tr>
<td>
<div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Payment Details</strong></div>
<table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td style='padding:1px 20px' width='50%'><strong>Total Amount Due: ".$grandestTotal."</strong></td>

</tr>
 
<tr>
<td style='padding:1px 20px'><strong>Vatable Amount: ".$vatable."</strong></td>
</tr>
<tr>
<td style='padding:1px 20px'><strong>Vat(12%): ".$vat_display."</strong></td>
</tr>
<tr>
<td style='padding:1px 20px'><strong>Downpayment: ".$down."</strong></td>
</tr>
<tr>
<td colspan='2'></td>
</tr>
</tbody>
</table>
</td>
</tr>

<tr>
<td>
<div style='font-size:14px;margin-top:20px'><strong>Booking Policies</strong></div>
<table style='margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody><tr>
<td><table width='100%' border='0' cellpadding='0' cellspacing='0'>
                                <tbody><tr>
                                    <td style='border-bottom:2px solid #eeeeee;border-top:2px solid #eeeeee;padding:10px 20px!important'>
                                    <span style='font-size:11px'>
                                        <strong>Cancellation:</strong> If cancelled, modified or in case of no-show, no penalty   will be charged.<br>
                                       <strong>Other policies:  </strong>Please call 63 045 455-0789 or send an email to <a href='mailto:test.villaalfredo@gmail.com' target='_blank'>test.villaalfredo@gmail.com</a><br> Hotel requires an 'Incidental Deposit' at check-in, which is fully refundable at check-out.<br>
                                    </span>
                                    </td>
                                </tr>

                            </tbody></table></td>
</tr>
</tbody></table></td></tr></tbody>
</table>


 
<table><tbody><tr>
<td>
<div style='font-size:14px;margin-top:20px'><strong>Villa Alfredos</strong></div>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>

</tbody></table><div class='yj6qo'></div><div class='adL'>
</div></div></div></div>";
                        $subject = "Reservation Request Step 1";

                        $headers = "From: $from" . "\r\n";
                        $headers .= "Reply-To: $from" . "\r\n";
                        $headers .= "MIME-Version: 1.0\r\n";
                        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

                        $resp = mail($to, $subject, $mailbody, $headers);

                        if ($resp) {
                            session_destroy();
                            header("Location: message.php?res_id=$encrypted&paypal=true");

                        }
                    } else {
                        $mail = new PHPMailer(true); // Passing `true` enables exceptions
                        //Server settings
                        $mail->SMTPDebug = 0; // Enable verbose debug output
                        $mail->isSMTP(); // Set mailer to use SMTP
                        $mail->Host = 'smtp.gmail.com'; // Specify main and backup SMTP servers
                        $mail->SMTPAuth = true; // Enable SMTP authentication
                        $mail->Username = 'test.villaalfredo@gmail.com'; // SMTP username
                        $mail->Password = 'reservationsystem'; // SMTP password
                        $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
                        $mail->Port = 587; // TCP port to connect to
                        $mail->SMTPOptions = array(
                            'ssl' => array(
                                'verify_peer' => false,
                                'verify_peer_name' => false,
                                'allow_self_signed' => true,
                            ),
                        );
                        //Recipients
                        $mail->setFrom('test.villaalfredo@gmail.com', 'Villa Alfredo');
                        $mail->addAddress($email_address, $first_name . ' ' . $last_name); // Add a recipient
                        $baseurl = $_SERVER['SERVER_NAME'];
                        $mailbody = "
                        <div id=':14u' class='ii gt'><div id=':14t' class='a3s aXjCH '><div id='m_-852930379096079456page-wrapper' style='background-color:#ffffff;padding:0px'><div class='adM'>
</div><table style='font-family:'Open Sans',sans-serif;font-size:12px;margin:0px' width='100%' border='0' cellspacing='0'>
<tbody>
<tr>
<td>
<p style='font-size:20px'><span class='il'>Villa</span> <span class='il'>Alfredo</span> Resort</p>
</td>
</tr>
<tr>
<td class='m_-852930379096079456box-grey' style='background-color:#eeeeee;padding:15px 15px 15px 15px;border-bottom-left-radius:0;border-bottom-right-radius:0' valign='top'>
<table width='100%' border='0' cellpadding='0'>
<tbody>
<tr valign='top'>
<td width='65%'><span style='font-size:12px'><strong>Thank you, ".$first_name." Your booking is pending.</strong></span></td>
<td style='font-size:12px' width='35%'>
<table width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td align='right'><strong>Reference Number:</strong>".$client_reference_id."</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td class='m_-852930379096079456box-grey' style='background-color:#f8f8f8;padding:0 15px 15px 15px;border-top-left-radius:0;border-top-right-radius:0' valign='top'>
<table width='100%' border='0' cellpadding='0'>
<tbody>
<tr valign='top'>
<td width='65%'>
<ul style='padding:15px 0 0 15px;margin:0'>
<li style='line-height:20px;padding:0;margin:0'>To secure your reservation, Please pay the stated amount below on our Primary account BDO Account Number: 00-131-067-3959 and upload the receipt here <a href='".$baseurl."/reservation/upload_receipt.php' target='_blank'>Upload Receipt</a></li>
<li style='line-height:20px;padding:0;margin:0'>For booking enquires or amendments please contact us directly at <a href='mailto:test.villaalfredo@gmail.com' target='_blank'>test.villaalfredo@gmail.com</a> or (045) 455 1397.</li>
<li style='line-height:20px;padding:0;margin:0'>For cancellations <a href='".$baseurl."/reservation/cancel-reservations.php?res_id=".$encrypted."' target='_blank'>Click Here</a></li>
</ul>
</td>
<td style='font-size:12px' width='35%'>
<table width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td align='right'>&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td>
<div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Your Booking</strong></div>
<table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td style='padding:1px 10px 1px 20px' valign='top' width='20%'><strong>Guest:</strong></td>
<td style='padding:1px 20px' width='80%'>".$first_name." ".$last_name." </td>
</tr>
<tr>
<td style='padding:1px 10px 1px 20px' valign='top' width='20%'><strong>Details:</strong></td>
<td style='padding:1px 0px 1px 20px' width='80%'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                                <tbody><tr>
                                    <td width='75%'>
                                    ";
                        $sql = "SELECT * FROM reserved_rooms
                        JOIN customer ON
                        reserved_rooms.client_reference_id = customer.client_reference_id
                        JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
                        WHERE customer.client_reference_id ='$client_reference_id'
                        GROUP BY reserved_rooms.room_type_id";
                         $res = $mysqli->query($sql);
                         $total = 0;
                         $subtotal = [];
                         while ($rows = mysqli_fetch_assoc($res)) {
                            $room_type_id = $rows['room_type_id'];
                            $sql1 = "SELECT COUNT(*) as quantity FROM reserved_rooms
                          JOIN customer ON
                          reserved_rooms.client_reference_id = customer.client_reference_id
                          JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
                          WHERE customer.client_reference_id ='$client_reference_id' AND room_type_id ='$room_type_id'";
                            $res1 = $mysqli->query($sql1);
                            $row1 = mysqli_fetch_assoc($res1);
                            $total = $row1['quantity'] * $rows['room_price'];

                            $mailbody .= "
                                            ".$rows['room_name'] ." x
                                            " . $row1['quantity'] . ",
                                        ";

                            $count++;
                            array_push($subtotal, $total);
                        }
                        
                        function multiplyDays($total)
                        {
                            global $noofNights;
                            return ($total * $noofNights);
                        }
                        $grandTotal = array_sum(array_map("multiplyDays", $subtotal));
                        $totalAdult = $no_of_adults * 250;
                        $totalKid = $no_of_kids * 150;
                        $grandTotal = $grandTotal + $totalAdult + $totalKid;
                        $grandestTotal = number_format($grandTotal, 2);
                        $downTotal = $grandTotal / 2;
                        $vat = $downTotal * 0.12;
                        $vatable = number_format($downTotal - $vat,2); 
                        $vat_display = number_format($vat,2);
                        $down = number_format($grandTotal / 2, 2);
                        

                        $mailbody .="Adult x ".$no_of_adults.",
                        Child x ".$no_of_kids."";

                        $mailbody .="-PHP ".$grandestTotal."</td>
                                </tr>
                            </tbody></table></td>
</tr>
<tr>
<td style='padding:1px 10px 1px 20px' width='20%'><strong>Check-in:</strong></td>
<td style='padding:1px 20px' width='80%'>".$arrival_date." from 14:00</td>
</tr>
<tr>
<td style='padding:1px 10px 1px 20px' width='20%'><strong>Check-out:</strong></td>
<td style='padding:1px 20px' width='80%'>".$departure_date." until 12:00</td>
</tr>
</tbody>
</table>
</td>
</tr>

<tr>
<td>
<div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Additional Details</strong></div>
<table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td><table width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody><tr>
    <td width='20%' style='padding:1px 10px 1px 20px'><strong>Additional comments:</strong></td>
    <td width='80%' style='padding:1px 20px'>".$additional_details."</td>
</tr>
</tbody></table></td>
</tr>
 </tbody>
</table>
</td>
</tr>

<tr>
<td>
<div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Payment Details</strong></div>
<table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td style='padding:1px 20px' width='50%'><strong>Total Amount Due: ".$grandestTotal."</strong></td>

</tr>
<tr>
<td style='padding:1px 20px'><strong>Vatable Amount: ".$vatable."</strong></td>
</tr>
<tr>
<td style='padding:1px 20px'><strong>Vat(12%): ".$vat_display."</strong></td>
</tr>
<tr>
<td style='padding:1px 20px'><strong>Downpayment: ".$down."</strong></td>
</tr>

<tr>
<td colspan='2'></td>
</tr>
</tbody>
</table>
</td>
</tr>

<tr>
<td>
<div style='font-size:14px;margin-top:20px'><strong>Booking Policies</strong></div>
<table style='margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody><tr>
<td><table width='100%' border='0' cellpadding='0' cellspacing='0'>
                                <tbody><tr>
                                    <td style='border-bottom:2px solid #eeeeee;border-top:2px solid #eeeeee;padding:10px 20px!important'>
                                    <span style='font-size:11px'>
                                        <strong>Cancellation:</strong> If cancelled, modified or in case of no-show, no penalty   will be charged.<br>
                                       <strong>Other policies:  </strong>Please call 63 045 455-0789 or send an email to <a href='mailto:test.villaalfredo@gmail.com' target='_blank'>test.villaalfredo@gmail.com</a><br> Hotel requires an 'Incidental Deposit' at check-in, which is fully refundable at check-out.<br>
                                    </span>
                                    </td>
                                </tr>

                            </tbody></table></td>
</tr>
</tbody></table></td></tr></tbody>
</table>


 
<table><tbody><tr>
<td>
<div style='font-size:14px;margin-top:20px'><strong>Villa Alfredos</strong></div>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>

</tbody></table><div class='yj6qo'></div><div class='adL'>
</div></div></div></div>
                        
                        ";

                        //Content
                        $mail->isHTML(true); // Set email format to HTML
                        $mail->Subject = 'Reservation Request Step 1';
                        $mail->Body = $mailbody;
                        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

                        if ($mail->send()) {
                            session_destroy();
                            header("Location: message.php?res_id=$encrypted&paypal=true");
                        }
                    }
                } else {
                    session_destroy();
                    header("Location: message.php?res_id=$encrypted&paypal=true");
                }
            }
        }
    } else {

        $reservation_status = 'Pending';

        $res = $mysqli->query("INSERT INTO `customer`(`client_reference_id`, `first_name`, `last_name`,`contact_number`, `email_address`, `arrival_date`, `departure_date`,`additional_details`, `no_of_adults`,`no_of_kids`)
      VALUES ('$client_reference_id', '$first_name', '$last_name', '$contact_number', '$email_address', '$arrival_date', '$departure_date','$additional_details','$no_of_adults','$no_of_kids')");
        if ($res) {
            
            $timestamp = strtotime($date_now);
            $day = date('D', $timestamp);
            if ($day == 'Sat') {
                $expires_at = date('Y-m-d', strtotime("+4 days"));
            } else if ($day == 'Sun') {
                $expires_at = date('Y-m-d', strtotime("+3 days"));
            } else if ($day == 'Fri') {
                $expires_at = date('Y-m-d', strtotime("+4 days"));
            } else {
                $expires_at = date('Y-m-d', strtotime("+2 days"));
            }

            $time = date("H:i:s");
            $expires_at = $expires_at . ' ' . $time;
            $res = $mysqli->query("INSERT INTO `reservation`(`client_reference_id`,`reservation_type`, `reservation_status`,`created_at`,`expires_at`) VALUES ('$client_reference_id','Online','Pending','$created_at','$expires_at')");
            if ($res) {
                for ($i = 0; $i < count($rooms); $i++) {
                    $room_id = $rooms[$i][0];
                    $room_qty = $rooms[$i][1];
                    for ($j = 0; $j < $room_qty; $j++) {
                        $sql1 = "INSERT INTO reserved_rooms (room_type_id,client_reference_id) VALUES('$room_id','$client_reference_id')";
                        $mysqli->query($sql1);
                    }
                }

                $encrypted = base64_encode($client_reference_id);
                $sql = "SELECT * FROM customer JOIN reservation ON customer.client_reference_id = reservation.client_reference_id WHERE reservation.client_reference_id='$client_reference_id'";

                $res = $mysqli->query($sql);

                while ($rows = mysqli_fetch_assoc($res)) {
                    $first_name = $rows['first_name'];
                    $last_name = $rows['last_name'];
                    $email_address = $rows['email_address'];
                    $arrival_date = $rows['arrival_date'];
                    $departure_date = $rows['departure_date'];
                    $no_of_adults = $rows['no_of_adults'];
                    $no_of_kids = $rows['no_of_kids'];
                    $expires_at = $rows['expires_at'];
                    $additional_details = $rows['additional_details'];
                }

                // get no of Nights
                $departure = strtotime($departure_date);
                $arrival = strtotime($arrival_date);
                $datediff = $departure - $arrival;

                $noofNights = round($datediff / (60 * 60 * 24));

                if ($mailing == true) {

                    if ($deployedMailing == true) {

                        $from = "villa_alfredo_email@villaalfredo.x10host.com"; // This is an Email Account coming from the x10hos that has been setup on hosting. server only.
                        $to = "$email_address,$first_name.' '.$last_name"; // Recepient

                        $baseurl = $_SERVER['SERVER_NAME'];
                        $mailbody = "
                        <div id=':14u' class='ii gt'><div id=':14t' class='a3s aXjCH '><div id='m_-852930379096079456page-wrapper' style='background-color:#ffffff;padding:0px'><div class='adM'>
</div><table style='font-family:'Open Sans',sans-serif;font-size:12px;margin:0px' width='100%' border='0' cellspacing='0'>
<tbody>
<tr>
<td>
<p style='font-size:20px'><span class='il'>Villa</span> <span class='il'>Alfredo</span> Resort</p>
</td>
</tr>
<tr>
<td class='m_-852930379096079456box-grey' style='background-color:#eeeeee;padding:15px 15px 15px 15px;border-bottom-left-radius:0;border-bottom-right-radius:0' valign='top'>
<table width='100%' border='0' cellpadding='0'>
<tbody>
<tr valign='top'>
<td width='65%'><span style='font-size:12px'><strong>Thank you, ".$first_name." Your booking is pending.</strong></span></td>
<td style='font-size:12px' width='35%'>
<table width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td align='right'><strong>Reference Number:</strong>".$client_reference_id."</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td class='m_-852930379096079456box-grey' style='background-color:#f8f8f8;padding:0 15px 15px 15px;border-top-left-radius:0;border-top-right-radius:0' valign='top'>
<table width='100%' border='0' cellpadding='0'>
<tbody>
<tr valign='top'>
<td width='65%'>
<ul style='padding:15px 0 0 15px;margin:0'>
<li style='line-height:20px;padding:0;margin:0'>To secure your reservation, Please pay the stated amount below on our Primary account BDO Account Number: 00-131-067-3959 and upload the receipt here <a href='".$baseurl."/reservation/upload_receipt.php' target='_blank'>Upload Receipt</a></li>
<li style='line-height:20px;padding:0;margin:0'>For booking enquires or amendments please contact us directly at <a href='mailto:test.villaalfredo@gmail.com' target='_blank'>test.villaalfredo@gmail.com</a> or (045) 455 1397.</li>
<li style='line-height:20px;padding:0;margin:0'>For cancellations <a href='".$baseurl."/reservation/cancel-reservations.php?res_id=".$encrypted."' target='_blank'>Click Here</a></li>
</ul>
</td>
<td style='font-size:12px' width='35%'>
<table width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td align='right'>&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td>
<div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Your Booking</strong></div>
<table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td style='padding:1px 10px 1px 20px' valign='top' width='20%'><strong>Guest:</strong></td>
<td style='padding:1px 20px' width='80%'>".$first_name." ".$last_name." </td>
</tr>
<tr>
<td style='padding:1px 10px 1px 20px' valign='top' width='20%'><strong>Details:</strong></td>
<td style='padding:1px 0px 1px 20px' width='80%'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                                <tbody><tr>
                                    <td width='75%'>
                                    ";
                        $sql = "SELECT * FROM reserved_rooms
                        JOIN customer ON
                        reserved_rooms.client_reference_id = customer.client_reference_id
                        JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
                        WHERE customer.client_reference_id ='$client_reference_id'
                        GROUP BY reserved_rooms.room_type_id";
                         $res = $mysqli->query($sql);
                         $total = 0;
                         $subtotal = [];
                         while ($rows = mysqli_fetch_assoc($res)) {
                            $room_type_id = $rows['room_type_id'];
                            $sql1 = "SELECT COUNT(*) as quantity FROM reserved_rooms
                          JOIN customer ON
                          reserved_rooms.client_reference_id = customer.client_reference_id
                          JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
                          WHERE customer.client_reference_id ='$client_reference_id' AND room_type_id ='$room_type_id'";
                            $res1 = $mysqli->query($sql1);
                            $row1 = mysqli_fetch_assoc($res1);
                            $total = $row1['quantity'] * $rows['room_price'];

                            $mailbody .= "
                                            ".$rows['room_name'] ." x
                                            " . $row1['quantity'] . ",
                                        ";

                            $count++;
                            array_push($subtotal, $total);
                        }
                        
                        function multiplyDays($total)
                        {
                            global $noofNights;
                            return ($total * $noofNights);
                        }
                        $grandTotal = array_sum(array_map("multiplyDays", $subtotal));
                        $totalAdult = $no_of_adults * 250;
                        $totalKid = $no_of_kids * 150;
                        $grandTotal = $grandTotal + $totalAdult + $totalKid;
                        $grandestTotal = number_format($grandTotal, 2);
                        $downTotal = $grandTotal / 2;
                        $vat = $downTotal * 0.12;
                        $vatable = number_format($downTotal - $vat,2); 
                        $vat_display = number_format($vat,2);
                        $down = number_format($grandTotal / 2, 2);

                        $mailbody .="Adult x ".$no_of_adults.",
                        Child x ".$no_of_kids."";

                        $mailbody .="-PHP ".$grandestTotal."</td>
                            
                                </tr>
                            </tbody></table></td>
</tr>
<tr>
<td style='padding:1px 10px 1px 20px' width='20%'><strong>Check-in:</strong></td>
<td style='padding:1px 20px' width='80%'>".$arrival_date." from 14:00</td>
</tr>
<tr>
<td style='padding:1px 10px 1px 20px' width='20%'><strong>Check-out:</strong></td>
<td style='padding:1px 20px' width='80%'>".$departure_date." until 12:00</td>
</tr>
</tbody>
</table>
</td>
</tr>

<tr>
<td>
<div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Additional Details</strong></div>
<table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td><table width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody><tr>
    <td width='20%' style='padding:1px 10px 1px 20px'><strong>Additional comments:</strong></td>
    <td width='80%' style='padding:1px 20px'>".$additional_details."</td>
</tr>
</tbody></table></td>
</tr>
 </tbody>
</table>
</td>
</tr>

<tr>
<td>
<div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Payment Details</strong></div>
<table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td style='padding:1px 20px' width='50%'><strong>Total Amount Due: ".$grandestTotal."</strong></td>

</tr>
 
<tr>
<td style='padding:1px 20px'><strong>Vatable Amount: ".$vatable."</strong></td>
</tr>
<tr>
<td style='padding:1px 20px'><strong>Vat(12%): ".$vat_display."</strong></td>
</tr>
<tr>
<td style='padding:1px 20px'><strong>Downpayment: ".$down."</strong></td>
</tr>

<tr>
<td colspan='2'></td>
</tr>
</tbody>
</table>
</td>
</tr>

<tr>
<td>
<div style='font-size:14px;margin-top:20px'><strong>Booking Policies</strong></div>
<table style='margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody><tr>
<td><table width='100%' border='0' cellpadding='0' cellspacing='0'>
                                <tbody><tr>
                                    <td style='border-bottom:2px solid #eeeeee;border-top:2px solid #eeeeee;padding:10px 20px!important'>
                                    <span style='font-size:11px'>
                                        <strong>Cancellation:</strong> If cancelled, modified or in case of no-show, no penalty   will be charged.<br>
                                       <strong>Other policies:  </strong>Please call 63 045 455-0789 or send an email to <a href='mailto:test.villaalfredo@gmail.com' target='_blank'>test.villaalfredo@gmail.com</a><br> Hotel requires an 'Incidental Deposit' at check-in, which is fully refundable at check-out.<br>
                                    </span>
                                    </td>
                                </tr>

                            </tbody></table></td>
</tr>
</tbody></table></td></tr></tbody>
</table>


 
<table><tbody><tr>
<td>
<div style='font-size:14px;margin-top:20px'><strong>Villa Alfredos</strong></div>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>

</tbody></table><div class='yj6qo'></div><div class='adL'>
</div></div></div></div>";
                        $subject = "Reservation Request Step 1";

                        $headers = "From: $from" . "\r\n";
                        $headers .= "Reply-To: $from" . "\r\n";
                        $headers .= "MIME-Version: 1.0\r\n";
                        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

                        $resp = mail($to, $subject, $mailbody, $headers);

                        if ($resp) {
                            echo "<script>window.location = 'reservation-details.php?res_id=" . $encrypted . "';</script>";
                            session_destroy();
                            header("Location: message.php?res_id=$encrypted");

                        }

                    } else {
                        $mail = new PHPMailer(true); // Passing `true` enables exceptions
                        //Server settings
                        $mail->SMTPDebug = 0; // Enable verbose debug output
                        $mail->isSMTP(); // Set mailer to use SMTP
                        $mail->Host = 'smtp.gmail.com'; // Specify main and backup SMTP servers
                        $mail->SMTPAuth = true; // Enable SMTP authentication
                        $mail->Username = 'test.villaalfredo@gmail.com'; // SMTP username
                        $mail->Password = 'reservationsystem'; // SMTP password
                        $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
                        $mail->Port = 587; // TCP port to connect to
                        $mail->SMTPOptions = array(
                            'ssl' => array(
                                'verify_peer' => false,
                                'verify_peer_name' => false,
                                'allow_self_signed' => true,
                            ),
                        );
                        //Recipients
                        $mail->setFrom('test.villaalfredo@gmail.com', 'Villa Alfredo');
                        $mail->addAddress($email_address, $first_name . ' ' . $last_name); // Add a recipient
                        $baseurl = $_SERVER['SERVER_NAME'];
                        $mailbody = "
                        <div id=':14u' class='ii gt'><div id=':14t' class='a3s aXjCH '><div id='m_-852930379096079456page-wrapper' style='background-color:#ffffff;padding:0px'><div class='adM'>
</div><table style='font-family:'Open Sans',sans-serif;font-size:12px;margin:0px' width='100%' border='0' cellspacing='0'>
<tbody>
<tr>
<td>
<p style='font-size:20px'><span class='il'>Villa</span> <span class='il'>Alfredo</span> Resort</p>
</td>
</tr>
<tr>
<td class='m_-852930379096079456box-grey' style='background-color:#eeeeee;padding:15px 15px 15px 15px;border-bottom-left-radius:0;border-bottom-right-radius:0' valign='top'>
<table width='100%' border='0' cellpadding='0'>
<tbody>
<tr valign='top'>
<td width='65%'><span style='font-size:12px'><strong>Thank you, ".$first_name." Your booking is pending.</strong></span></td>
<td style='font-size:12px' width='35%'>
<table width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td align='right'><strong>Reference Number:</strong>".$client_reference_id."</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td class='m_-852930379096079456box-grey' style='background-color:#f8f8f8;padding:0 15px 15px 15px;border-top-left-radius:0;border-top-right-radius:0' valign='top'>
<table width='100%' border='0' cellpadding='0'>
<tbody>
<tr valign='top'>
<td width='65%'>
<ul style='padding:15px 0 0 15px;margin:0'>
<li style='line-height:20px;padding:0;margin:0'>To secure your reservation, Please pay the stated amount below on our Primary account BDO Account Number: 00-131-067-3959 and upload the receipt here <a href='".$baseurl."/reservation/upload_receipt.php' target='_blank'>Upload Receipt</a></li>
<li style='line-height:20px;padding:0;margin:0'>For booking enquires or amendments please contact us directly at <a href='mailto:test.villaalfredo@gmail.com' target='_blank'>test.villaalfredo@gmail.com</a> or (045) 455 1397.</li>
<li style='line-height:20px;padding:0;margin:0'>For cancellations <a href='".$baseurl."/reservation/cancel-reservations.php?res_id=".$encrypted."' target='_blank'>Click Here</a></li>
</ul>
</td>
<td style='font-size:12px' width='35%'>
<table width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td align='right'>&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td>
<div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Your Booking</strong></div>
<table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td style='padding:1px 10px 1px 20px' valign='top' width='20%'><strong>Guest:</strong></td>
<td style='padding:1px 20px' width='80%'>".$first_name." ".$last_name." </td>
</tr>
<tr>
<td style='padding:1px 10px 1px 20px' valign='top' width='20%'><strong>Details:</strong></td>
<td style='padding:1px 0px 1px 20px' width='80%'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                                <tbody><tr>
                                    <td width='75%'>
                                    ";
                        $sql = "SELECT * FROM reserved_rooms
                        JOIN customer ON
                        reserved_rooms.client_reference_id = customer.client_reference_id
                        JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
                        WHERE customer.client_reference_id ='$client_reference_id'
                        GROUP BY reserved_rooms.room_type_id";
                         $res = $mysqli->query($sql);
                         $total = 0;
                         $subtotal = [];
                         while ($rows = mysqli_fetch_assoc($res)) {
                            $room_type_id = $rows['room_type_id'];
                            $sql1 = "SELECT COUNT(*) as quantity FROM reserved_rooms
                          JOIN customer ON
                          reserved_rooms.client_reference_id = customer.client_reference_id
                          JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
                          WHERE customer.client_reference_id ='$client_reference_id' AND room_type_id ='$room_type_id'";
                            $res1 = $mysqli->query($sql1);
                            $row1 = mysqli_fetch_assoc($res1);
                            $total = $row1['quantity'] * $rows['room_price'];

                            $mailbody .= "
                                            ".$rows['room_name'] ." x
                                            " . $row1['quantity'] . ",
                                        ";

                            $count++;
                            array_push($subtotal, $total);
                        }
                        
                        function multiplyDays($total)
                        {
                            global $noofNights;
                            return ($total * $noofNights);
                        }
                        $grandTotal = array_sum(array_map("multiplyDays", $subtotal));
                        $totalAdult = $no_of_adults * 250;
                        $totalKid = $no_of_kids * 150;
                        $grandTotal = $grandTotal + $totalAdult + $totalKid;
                        $grandestTotal = number_format($grandTotal, 2);
 $downTotal = $grandTotal / 2;
                        $vat = $downTotal * 0.12;
                        $vat_display = number_format($vat,2);
                        $vatable = number_format($downTotal - $vat,2); 
                        $down = number_format($grandTotal / 2, 2);


                        $mailbody .="Adult x ".$no_of_adults.",
                        Child x ".$no_of_kids."";

                        $mailbody .="-PHP ".$grandestTotal."</td>
                            
                                </tr>
                            </tbody></table></td>
</tr>
<tr>
<td style='padding:1px 10px 1px 20px' width='20%'><strong>Check-in:</strong></td>
<td style='padding:1px 20px' width='80%'>".$arrival_date." from 14:00</td>
</tr>
<tr>
<td style='padding:1px 10px 1px 20px' width='20%'><strong>Check-out:</strong></td>
<td style='padding:1px 20px' width='80%'>".$departure_date." until 12:00</td>
</tr>
</tbody>
</table>
</td>
</tr>

<tr>
<td>
<div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Additional Details</strong></div>
<table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td><table width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody><tr>
    <td width='20%' style='padding:1px 10px 1px 20px'><strong>Additional comments:</strong></td>
    <td width='80%' style='padding:1px 20px'>".$additional_details."</td>
</tr>
</tbody></table></td>
</tr>
 </tbody>
</table>
</td>
</tr>

<tr>
<td>
<div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Payment Details</strong></div>
<table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td style='padding:1px 20px' width='50%'><strong>Total Amount Due: ".$grandestTotal."</strong></td>

</tr>
 
<tr>
<td style='padding:1px 20px'><strong>Vatable Amount: ".$vatable."</strong></td>
</tr>
<tr>
<td style='padding:1px 20px'><strong>Vat(12%): ".$vat_display."</strong></td>
</tr>
<tr>
<td style='padding:1px 20px'><strong>Downpayment: ".$down."</strong></td>
</tr>

<tr>
<td colspan='2'></td>
</tr>
</tbody>
</table>
</td>
</tr>

<tr>
<td>
<div style='font-size:14px;margin-top:20px'><strong>Booking Policies</strong></div>
<table style='margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody><tr>
<td><table width='100%' border='0' cellpadding='0' cellspacing='0'>
                                <tbody><tr>
                                    <td style='border-bottom:2px solid #eeeeee;border-top:2px solid #eeeeee;padding:10px 20px!important'>
                                    <span style='font-size:11px'>
                                        <strong>Cancellation:</strong> If cancelled, modified or in case of no-show, no penalty   will be charged.<br>
                                       <strong>Other policies:  </strong>Please call 63 045 455-0789 or send an email to <a href='mailto:test.villaalfredo@gmail.com' target='_blank'>test.villaalfredo@gmail.com</a><br> Hotel requires an 'Incidental Deposit' at check-in, which is fully refundable at check-out.<br>
                                    </span>
                                    </td>
                                </tr>

                            </tbody></table></td>
</tr>
</tbody></table></td></tr></tbody>
</table>


 
<table><tbody><tr>
<td>
<div style='font-size:14px;margin-top:20px'><strong>Villa Alfredos</strong></div>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>

</tbody></table><div class='yj6qo'></div><div class='adL'>
</div></div></div></div>";

                        //Content
                        $mail->isHTML(true); // Set email format to HTML
                        $mail->Subject = 'Reservation Request Step 1';
                        $mail->Body = $mailbody;
                        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
                        if ($mail->send()) {
                            echo "<script>window.location = 'reservation-details.php?res_id=" . $encrypted . "';</script>";
                            session_destroy();
                            header("Location: message.php?res_id=$encrypted");

                        }

                    }
                } else {
                    session_destroy();
                    header("Location: message.php?res_id=$encrypted");
                }

            } else {
                echo "<script>alert('Error');</script>";
            }
        } else {
            echo "<script>alert('Customer Details arent Accepted');</script>";
        }
    }
}
