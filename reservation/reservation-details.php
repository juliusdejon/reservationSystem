<?php
include 'functions.php';
include '../config/mysqli.php';
$real_res_id = $_GET['res_id'];

// hello
$real_res_id = base64_decode($real_res_id);

$sql = "SELECT * FROM customer JOIN reservation ON customer.client_reference_id = reservation.client_reference_id WHERE reservation.client_reference_id='$real_res_id'";

$res = $mysqli->query($sql);

while ($rows = mysqli_fetch_assoc($res)) {
    $client_reference_id = $rows['client_reference_id'];
    $first_name = $rows['first_name'];
    $last_name = $rows['last_name'];
    $email_address = $rows['email_address'];
    $arrival_date = $rows['arrival_date'];
    $departure_date = $rows['departure_date'];
    $no_of_adults = $rows['no_of_adults'];
    $no_of_kids = $rows['no_of_kids'];
    $expires_at = $rows['expires_at'];
}

// get no of Nights
$departure = strtotime($departure_date);
$arrival = strtotime($arrival_date);
$datediff = $departure - $arrival;

$noofNights = round($datediff / (60 * 60 * 24));

?>
<!DOCTYPE html>
<!--
  Invoice template by invoicebus.com
  To customize this template consider following this guide https://invoicebus.com/how-to-create-invoice-template/
  This template is under Invoicebus Template License, see https://invoicebus.com/templates/license/
-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Villa Alfredo's Resort | Receipt</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- <meta name="description" content="Invoicebus Invoice Template">
    <meta name="author" content="Invoicebus"> -->

    <!-- <meta name="template-hash" content="baadb45704803c2d1a1ac3e569b757d5"> -->

    <link rel="stylesheet" href="../css/template.css">
    <style type="text/css" media="print">
    @page
    {
        size: auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }
</style>
</head>

<body>
    <div id="container">
        <section id="memo">
            <div class="logo">
                <img src="../img/valogo.png" />
            </div>

            <div class="company-info">
                <div>Villa Alfredo's Resort</div>

                <br />

                <span>Purok 1 Barangay Baliti, City of San Fernando</span>
                <span>,Pampanga Philippines</span>

                <br />

                <span>Manila Phone Office: +63 02 5844840</span>
                <span>/Resort Phone Office: +63 045 455-1397</span>
                <br />
                <span>villaalfredosresort@yahoo.com</span>
                <span>/villaalfredos@yahoo.com</span>
            </div>

        </section>

        <section id="invoice-title-number">

            <span id="title">Ref ID: </span>
            <span id="number"><?php echo $real_res_id; ?></span>

        </section>

        <div class="clearfix"></div>

        <section id="client-info">

            <div>
           <span class="bold">Hello, <?php echo $first_name . ' ' . $last_name; ?></span><span>Kindly check your email to push through your reservation at <b><?php echo $email_address; ?></b></span>
            </div>
            <div>
            </div>
            <div>
                <span>Arrival Date: <?php echo $arrival_date; ?> 2:00PM</span>
            </div>

            <div>
                <span>Departure Date: <?php echo $departure_date; ?> 12:00PM</span>
            </div>


        </section>

        <div class="clearfix"></div>

        <section id="items">
            <table cellpadding="0" cellspacing="0">

                <tr>
                    <th></th>
                    <!-- Dummy cell for the row number and row commands -->
                    <th>Room Description</th>
                    <th>Room Name</th>
                    <th>Quantity</th>
                    <th>No. of Nights</th>
                    <th>Price</th>
                    <th>Total</th>
                </tr>
    <?php
$sql = "SELECT * FROM reserved_rooms
    JOIN customer ON
    reserved_rooms.client_reference_id = customer.client_reference_id
    JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
    WHERE customer.client_reference_id ='$real_res_id'
    GROUP BY reserved_rooms.room_type_id";

$res = $mysqli->query($sql);
$count = 1;
$total = 0;
$subtotal = [];
while ($rows = mysqli_fetch_assoc($res)) {
    $room_type_id = $rows['room_type_id'];
    $sql1 = "SELECT COUNT(*) as quantity FROM reserved_rooms
  JOIN customer ON
  reserved_rooms.client_reference_id = customer.client_reference_id
  JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
  WHERE customer.client_reference_id ='$real_res_id' AND room_type_id ='$room_type_id'";
    $res1 = $mysqli->query($sql1);
    $row1 = mysqli_fetch_assoc($res1);
    ?>
                <tr data-iterate="item">
                    <td><?php echo $count; ?></td>
                    <td><?php echo $rows['room_info']; ?></td>
                    <td><?php echo $rows['room_name']; ?> </td>
                    <td><?php echo $row1['quantity']; ?></td>
                    <td><?php echo $noofNights; ?></td>
                    <td>₱ <?php echo number_format($rows['room_price'], 2); ?></td>
                    <td>₱ <?php echo number_format($total = $row1['quantity'] * $rows['room_price'] * $noofNights, 2); ?></td>

                </tr>

    <?php
$count++;
    array_push($subtotal, $total);
}?>

            </table>

        </section>
        <section id="items">
            <table >



</table>

</section>

        <section id="sums">

            <table cellpadding="0" cellspacing="0">
            <tr>
            </tr>
                <tr>
                <th>Subtotal</th>
                <td></td>
                    <td>₱ <?php

$grandTotal = array_sum($subtotal);
echo number_format($grandTotal, 2);?></td>
                </tr>
                <!-- <tr>
                    <th>No of Adults</th>
                    <td> <?php echo $no_of_adults;
echo ' x ₱ 250.00'; ?> </td>
                    <?php
$totalAdult = $no_of_adults * 250
?>
                    <td>₱ <?php echo number_format($totalAdult, 2); ?></td>
                </tr> -->
                <!-- <tr>
                <th>No of Kids</th>
                <td><?php echo $no_of_kids;
echo ' x ₱ 150.00'; ?></td>
                <?php $totalKid = $no_of_kids * 150;?>
                <td>₱ <?php echo number_format($totalKid, 2); ?></td>
                </tr> -->
            
                      <?php
$grandTotal = $grandTotal + $totalAdult + $totalKid;
$vat = $grandTotal * 0.12;
$vattable = $grandTotal - $vat;
?>

                <!-- You can use attribute data-hide-on-quote="true" to hide specific information on quotes.
               For example Invoicebus doesn't need amount paid and amount due on quotes  -->

          <tr>
                    <th>Vatable Amount</th>
                    <td></td>
                    <td>₱
                      <?php
echo number_format($vattable, 2);
?></td>
          <tr>
                    <th>Vat(12%)</th>
                    <td></td>
                    <td>₱
                      <?php
echo number_format($vat,2);
?></td>
    <tr class="amount-total">
                    <th>Total</th>
                    <td></td>
                    <td>₱ <?php echo number_format($grandTotal,2);?></td>
                    </tr>
                <tr data-hide-on-quote="true">
                    <th>Downpayment</th>
                    <td></td>
                    <td>₱ <?php echo number_format($grandTotal / 2, 2); ?></td>
                </tr>

            </table>

            <div class="clearfix"></div>

        </section>

        <div class="clearfix"></div>

        <section id="invoice-info">

            <div>
            <div class="notes">Note* To secure your reservation you must pay atleast 50% within the next 3 days</div>
                <span>Pay until:</span> <span><?php echo $expires_at; ?></span>
            </div>

            <br />


        </section>


        </section>

        <div class="clearfix"></div>

        <div class="clearfix"></div>
    </div>


    <!-- <script src="http://cdn.invoicebus.com/generator/generator.min.js?data=data.js"></script> -->


</body>

</html>
