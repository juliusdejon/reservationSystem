<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Villa Alfredo Admin</title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
    <!-- Semantic UI -->
    <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />

    <!-- Initialize Semantic UI and JQuery to load Calendar Datepicker-->
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/semantic.min.js"></script>

    <!-- Calendar -->
    <link type="text/css" rel="stylesheet" href="../css/calendar.min.css" />
    <script src="../js/calendar.min.js"></script>



    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="../css/font-awesome.min.css">

    <!-- Icons -->
    <link rel="stylesheet" href="../css/icon.min.css">




</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <?php
include 'layout/navbar.php';
include 'layout/sidebar.php';
?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h2><i class="fa fa-file"></i> Report Lists</h2>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.content-header -->

                <!-- Main content -->
                <div class="content">
                    <div class="ui segment">
                        <div class="row">
                            <div class="col-md-4">
                                <form action="" method="post" name="selectDateForm">
                                    <div class="ui form">
                                        <div class="two fields">
                                            <div class="field">
                                                <label>Starting Date</label>
                                                <div class="ui calendar" id="rangestart">
                                                    <div class="ui input left icon">
                                                        <i class="calendar icon"></i>
                                                        <input type="text" placeholder="Start" id="start" name="start" autocomplete="off">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="field">
                                                <label>Ending Date</label>
                                                <div class="ui calendar" id="rangeend">
                                                    <div class="ui input left icon">
                                                        <i class="calendar icon"></i>
                                                        <input type="text" placeholder="End" id="end" name="end" autocomplete="off">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>&nbsp;</label>
                                        <select class="form-control" id="report_type" name="report_type" placeholder="Report Type" />
                                        <option value="Sales">Revenue</option>
                                        <option value="Booked">Confirmed Booking</option>
                                        <option value="ArrivalDeparture">Check In/ Checkout</option>
                                        <option value="Cancelled">Cancelled Booking</option>
                                        <option value="Extensions">Extension/Adjustments</option>
                                        <option value="Rebooking">Rebooking</option>
                                        <option value="Damages">Damages</option>
                                        <option value="Occupancy">Occupancy</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label>&nbsp;</label>
                                        <button type="button" onclick="generateReport()" class="ui button teal form-control">Search</button>
                                    </div>
                                    <div class="col-md-4">
                                        <label>&nbsp;</label>
                                        <button type="button" onclick="printTable()" class="ui button red form-control ">Print <i class="fa fa-print"></i></button>
                                    </div>
                                </div>

                            </div>
                        </div>



                    <div class="report-search">
                    <h1>Revenue</h1>
<table class="ui striped table">
  <thead>
    <tr>
      <th>Reference ID</th>
      <th>Name</th>
      <th>Arrival Date</th>
      <th>Departure Date</th>
      <th>Total</th>
    </tr>
  </thead>
  <tbody>
  <?php
$sql = "SELECT * FROM transaction JOIN customer ON customer.client_reference_id = transaction.client_reference_id
        WHERE transaction_status='cleared'";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    ?>
    <tr>
        <td><?php echo $rows['client_reference_id']; ?></td>
        <td><?php echo $rows['first_name'] . ' ' . $rows['last_name']; ?></td>
        <td><?php echo $rows['arrival_date']; ?></td>
        <td><?php echo $rows['departure_date']; ?></td>
        <td><?php echo $rows['total']; ?></td>
    </tr>
  <?php
}
?>
  </tbody>
</table>


                    </div>


                    </div>
                </div>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <!-- Main Footer -->
            <?php
include 'layout/footer.php';
?>
                <!-- ./wrapper -->

                <!-- REQUIRED SCRIPTS -->
                <script>
                    function printTable() {
                        var start = $("#start").val();
                        var end = $("#end").val();
                        var report_type = $("#report_type").val();
                       var win =  window.open(`report_search.php?type=${report_type}&start=${start}&end=${end}&print`,'_blank');
                        win.focus();
                    }

                    function generateReport()
                    {
                      var start = $("#start").val();
                        var end = $("#end").val();
                        var report_type = $("#report_type").val();
                      $(".report-search").load(`report_search.php?type=${report_type}&start=${start}&end=${end}`);
                    }
                </script>
                <script>

                    const today = new Date();
                    $('#rangestart').calendar({
                        type: 'date',
                        endCalendar: $('#rangeend'),
                        formatter: {
        date: function (date, settings) {
            if (!date) return '';
            var day = date.getDate() + '';
            if (day.length < 2) {
                day = '0' + day;
            }
            var month = (date.getMonth() + 1) + '';
            if (month.length < 2) {
                month = '0' + month;
            }
            var year = date.getFullYear();
            return year + '-' + month + '-' + day;
        }
    }

                    });
                    $('#rangeend').calendar({
                        type: 'date',
                        startCalendar: $('#rangestart'),
                        formatter: {
        date: function (date, settings) {
            if (!date) return '';
            var day = date.getDate() + '';
            if (day.length < 2) {
                day = '0' + day;
            }
            var month = (date.getMonth() + 1) + '';
            if (month.length < 2) {
                month = '0' + month;
            }
            var year = date.getFullYear();
            return year + '-' + month + '-' + day;
        }
    }
                    });
                </script>

                <!-- jQuery -->
                <script src=" plugins/jquery/jquery.min.js "></script>
                <!-- Bootstrap 4 -->
                <script src="plugins/bootstrap/js/bootstrap.bundle.min.js "></script>
                <!-- AdminLTE App -->
                <script src="dist/js/adminlte.min.js "></script>
</body>

</html>