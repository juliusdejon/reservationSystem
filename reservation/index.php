<?php
include_once 'functions.php';
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

        <title>Villa Alfredo's Reservation System</title>

        <!-- Google font -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7CVarela+Round" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

        <!-- Bootstrap -->
        <link type="text/css" rel="stylesheet" href="../css/bootstrap.min.css" />

        <!-- Semantic -->
        <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />
        <!-- Initialize Semantic UI and JQuery to load Calendar Datepicker-->
        <script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" src="../js/semantic.min.js"></script>

        <!-- Calendar -->
        <!-- <link type="text/css" rel="stylesheet" href="../css/calendar.min.css" /> -->
        <!-- <script src="../js/calendar.min.js"></script> -->


        <!-- Font Awesome Icon -->
        <link rel="stylesheet" href="../css/font-awesome.min.css">

        <!-- Icons -->
        <link rel="stylesheet" href="../css/icon.min.css">

        <!-- Custom stlylesheet -->
        <link type="text/css" rel="stylesheet" href="../css/style.css" />
        <link type="text/css" rel="stylesheet" href="../css/jquery-ui.css">
    <style>

   .custom-container{
    position: absolute;
    margin-left: auto;
    margin-right: auto;
    left: 0;
    right: 0;
   }
   #ui-datepicker-div{
    background: white;

   }
   td  {
       padding: 10px;
   }

   .ui-datepicker-title {
    color: black;
    font-weight: 900;
    justify-content: center;
    display: flex;

   }
   .ui-datepicker-next{
       float:right!important;
   }
   th {
       color:teal;
   }
    </style>

    </head>

    <body>
        <!-- Header -->
        <header>
            <!-- Nav -->
            <nav id="nav" class="navbar">
                <div class="container">

                    <div class="navbar-header">
                        <!-- Logo -->
                        <div class="navbar-brand">
                            <a href="../index.php">
                                <img class="logo" src="../img/valogo-alt.png" alt="logo">
                                <img class="logo-alt" src="../img/valogo-alt.png" alt="logo">
                            </a>
                        </div>
                        <!-- /Logo -->

                        <!-- Collapse nav button -->
                        <div class="nav-collapse">
                            <span></span>
                        </div>
                        <!-- /Collapse nav button -->
                    </div>

                    <!--  Main navigation  -->
                    <ul class="main-nav nav navbar-nav navbar-right">
                        <li><a href="../index.php">Home</a></li>
                        <li><a href="../index.php#accomodation">Accomodation</a></li>
                        <li><a href="../#day-tour-cottages">Cottages</a></li>
                        <li><a href="../#contact">Contact</a></li>
                        <li><a href="./index.php"><button class="secondary-btn">Book Now</button></a></li>
                    </ul>
                    <!-- /Main navigation -->

                </div>
            </nav>
            <!-- /Nav -->
        </header>
        <div class="custom-container">
                <div class="ui segment container">
                <div class="row">
                    <div class="col-md-9">
                        <div class="ui stackable mini steps">
                            <div class="active step">
                                <i class="calendar alternate icon"></i>
                                <div class="content">
                                    <div class="title">Select Dates</div>
                                    <div class="description">Choose Date of Stay</div>
                                </div>
                            </div>
                            <div class="disabled step">
                                <i class="bed icon"></i>
                                <div class="content">
                                    <div class="title">Select Room</div>
                                    <div class="description">Choose Rooms</div>
                                </div>
                            </div>
                            <!-- <div class="disabled step">
                                <i class="tag icon"></i>
                                <div class="content">
                                    <div class="title">Amenities</div>
                                    <div class="description">Extras & Amenities</div>
                                </div>
                            </div> -->
                            <div class="disabled step">
                                <i class="credit card icon"></i>
                                <div class="content">
                                    <div class="title">Payment</div>
                                    <div class="description">Verify reservation details</div>
                                </div>
                            </div>
                        </div>
                        <?php if (isset($_GET['dates'])) {
    ?>
                        <div class="ui negative message">
                            <div class="header">
                                Enter range of Dates
                            </div>
                            <p> Please Select Arrival and Departure Dates
                            </p>
                        </div>
                        <?php }?>

                        <form action="" method="post" name="selectDateForm" onsubmit="return validateForm()">
                        <div class="form-group">
                                <div class="ui form">
                                    <div class="two fields">
                                        <div class="field">
                                            <label>Arrival Date</label>
                                            <div>
                                                <div class="ui input left icon">
                                                    <i class="calendar icon"></i>
                                                    <!-- <input type="text" placeholder="Start" name="arrivalDate" autocomplete="off"> -->
            <input type="text" id="checkInDate" name="arrivalDate">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="field">
                                            <label>Departure Date</label>
                                            <div>
                                                <div class="ui input left icon">
                                                    <i class="calendar icon"></i>
                                                    <!-- <input type="text" placeholder="End" name="departureDate" autocomplete="off"> -->
                                                    <input type = "text" id="checkOutDate" name="departureDate" n >
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                            <br/>
                            <input type="submit" name="submitDates" class="ui teal button" value="Check Availability" />
                        </form>
                    </div>
                    <div class="col-md-3">

                    </div>
                    </div>
        </div>

        <!-- Back to top -->
        <div id="back-to-top"></div>
        <!-- /Back to top -->

        <!-- Preloader -->
        <div id="preloader">
            <div class="preloader">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <!-- /Preloader -->
        <!-- Arrival and Departure Init on reservation/index.php -->

        <!-- jQuery Plugins -->
        <script type="text/javascript" src="../js/bootstrap.min.js"></script>
        <!-- <script type="text/javascript" src="../js/bootstrap-datepicker.js"></script> -->

        <!-- <script>

            const today = new Date();
            const todayMutable = new Date();
            let newDate = todayMutable;
            $('#rangestart').calendar({
                minDate: today,
                type: 'date',
                endCalendar: $('#rangeend'),
                onChange: function (date, text, mode) {
                    const selectedDate = date;
                    const tom = date;
                    // const tomorrow = selectedDate.setDate(selectedDate.getDate() + 1);
                    newDate = new Date(tom.setDate(tom.getDate() + 1))
                    // newDate = tomorrow;
                    const mutable = new Date(selectedDate.setDate(selectedDate.getDate() - 1));
                },

            });

            $('#rangeend').calendar({
                minDate: newDate,
                type: 'date',
                startCalendar: $('#rangestart')
            });

        </script> -->


        <script type="text/javascript" src ="../js/jquerydatepicker.js"></script>
        <script type="text/javascript" src="../js/jquery-ui.min.js"></script>
        <script type="text/javascript">
            var max = new Date();
            max.setMonth(max.getMonth() + 6);
            let interval = new Date();

            $("#checkInDate").datepicker({
                dateFormat: "yy-mm-dd",
                minDate: "+0",
                maxDate: max,
                onSelect: function(dateText, inst) {
                    var d = $.datepicker.parseDate(inst.settings.dateFormat, dateText);
                    d.setDate(d.getDate() + 1);
                    $("#checkOutDate").datepicker("option","minDate",
                    $("#checkInDate").datepicker("getDate"));
                    $("#checkOutDate").val($.datepicker.formatDate(inst.settings.dateFormat, d));
                },
            }).datepicker("setDate", "+0");

            $("#checkOutDate").datepicker({
                dateFormat: "yy-mm-dd",
                minDate: "+",
            }).datepicker("setDate", "+1");


      </script>

        <script type="text/javascript" src="../js/main.js"></script>
        <script>

    // Validation Payment form
    function validateForm() {
    var arrivalDate = document.forms["selectDateForm"]["arrivalDate"].value;
    var departureDate = document.forms["selectDateForm"]["departureDate"].value;

    if(arrivalDate == '' || departureDate == '') {

return false;
    } else {
        if (arrivalDate == departureDate ) {
        alert("Enter between dates");
        return false;
    }

    }


}
    </script>
    </body>

    </html