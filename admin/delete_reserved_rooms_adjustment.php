    <?php
include '../config/mysqli.php';

$client_reference_id = $_GET['res_id'];
$room_id = $_GET['room_id'];

$date_now = date('Y-m-d');

$get_check_in_date = "SELECT * FROM transaction WHERE client_reference_id='$client_reference_id'";
$res_get_check_in_date = $mysqli->query($get_check_in_date);
$row_get_check_in_date = mysqli_fetch_assoc($res_get_check_in_date);

$check_in_date = $row_get_check_in_date['check_in_date'];
$check_in_date = substr($check_in_date, 0, 10);

$is_first_time = $row_get_check_in_date['transaction_type'];

if ($date_now == $check_in_date) {
    header('Location: adjustments.php?invalid_date=true&res_id=' . $client_reference_id);
} else {

    if ('Reservation' == $is_first_time) {
        $sql = "SELECT COUNT(*) as quantity FROM reserved_rooms WHERE room_type_id='$room_id' AND client_reference_id ='$client_reference_id'";
        $res = $mysqli->query($sql);
        $rows = mysqli_fetch_assoc($res);
        $quantity = $rows['quantity'];
        $mysqli->query("INSERT INTO adjusted_rooms (client_reference_id, room_type_id, adjusted_room_quantity,adjusted_start_date,adjusted_end_date) VALUES ('$client_reference_id','$room_id','$quantity', '$check_in_date','$date_now')");
    }
    // First Booking
    $sql = "SELECT COUNT(*) as quantity FROM reserved_rooms WHERE room_type_id='$room_id' AND client_reference_id ='$client_reference_id'";
    $res = $mysqli->query($sql);
    $rows = mysqli_fetch_assoc($res);
    $quantity = $rows['quantity'];
    $mysqli->query("INSERT INTO adjusted_rooms (client_reference_id, room_type_id, adjusted_room_quantity,adjusted_start_date,adjusted_end_date) VALUES ('$client_reference_id','$room_id','$quantity', '$check_in_date','$date_now')");
    // Delete to Reserved Room
    $sql = "DELETE FROM reserved_rooms WHERE room_type_id='$room_id' AND client_reference_id='$client_reference_id'";
    $res = $mysqli->query($sql);
    if ($res) {
        header('Location: adjustments.php?res_id=' . $client_reference_id);
    } else {
        echo "error";
    }

    $sql = "DELETE FROM reserved_rooms WHERE room_type_id='$room_id' AND client_reference_id='$client_reference_id'";
    $res = $mysqli->query($sql);
    if ($res) {
        header('Location: adjustments.php?res_id=' . $client_reference_id);
    } else {
        echo "error";
    }

}