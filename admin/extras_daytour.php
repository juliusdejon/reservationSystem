<?php

include '../config/mysqli.php';
$client_ref_id = $_GET['ref_id'];

if (isset($_POST['submit'])) {
    $damages = $_POST['damages'];
    $damagesPrice = $_POST['damagesPrice'];
    foreach (array_combine($damages, $damagesPrice) as $d => $p) {
        if ($d == '' || $p == '') {

        } else {
            $sql = "INSERT INTO extras (extras_name,extras_category,extras_price) VALUES ('$d','Damages','$p')";
            $res = $mysqli->query($sql);
            if ($res) {
                $sql = "SELECT MAX(extras_id) as extra FROM extras";
                $res = $mysqli->query($sql);
                $rows = mysqli_fetch_assoc($res);
                $extras_id = $rows['extra'];
                $sql = "INSERT INTO customer_extras (client_reference_id,extras_id) VALUES ('$client_ref_id','$extras_id')";
                $mysqli->query($sql);
            }

        }
    }

    $item = $_POST['item'];
    $quantity = $_POST['quantity'];
    foreach (array_combine($item, $quantity) as $i => $q) {
        if ($item == '' || $q == '') {

        } else {
            for ($a = 0; $a < $q; $a++) {
                $i = intval($i);
                $sql = "INSERT INTO customer_extras (client_reference_id,extras_id) VALUES('$client_ref_id','$i')";
                $res = $mysqli->query($sql);
            }
            header('Location: overview_daytour.php?ref_id=' . $client_ref_id);
        }
    }

}

$sql = "SELECT * FROM customer JOIN
        reservation ON customer.client_reference_id = reservation.client_reference_id
        JOIN transaction ON customer.client_reference_id = transaction.client_reference_id
        WHERE customer.client_reference_id = '$client_ref_id'";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $client_ref_id = $rows['client_reference_id'];
    $first_name = $rows['first_name'];
    $last_name = $rows['last_name'];
    $contact = $rows['contact_number'];
    $email = $rows['email_address'];
    $arrival_date = $rows['arrival_date'];
    $departure_date = $rows['departure_date'];
    $created_at = $rows['created_at'];
    $no_of_adults = $rows['no_of_adults'];
    $no_of_kids = $rows['no_of_kids'];
    $res_status = $rows['reservation_status'];

}

?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>Villa Alfredo Admin</title>


        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/adminlte.min.css">
        <!-- Semantic UI -->
        <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />

        <!-- Initialize Semantic UI and JQuery to load Calendar Datepicker-->
        <script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" src="../js/semantic.min.js"></script>
        <!-- Calendar -->
        <link type="text/css" rel="stylesheet" href="../css/calendar.min.css" />
        <script src="../js/calendar.min.js"></script>
        <!-- Font Awesome Icon -->
        <link rel="stylesheet" href="../css/font-awesome.min.css">

        <!-- Icons -->
        <link rel="stylesheet" href="../css/icon.min.css">


        <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap4.css">



    </head>

    <body class="hold-transition sidebar-mini">
        <div class="wrapper">

            <?php
include 'layout/navbar.php';
include 'layout/sidebar.php';
?>
                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <div class="content-header">
                        <div class="container-fluid">

                        </div>
                        <!-- /.container-fluid -->
                    </div>
                    <!-- /.content-header -->

                    <!-- Main content -->
                    <div class="content">
                        <h3>Reference Id:
                            <?php echo $client_ref_id; ?>
                        </h3>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="card">
                                    <!-- /.card-header -->
                                    <div class="ui segment">

                                        <div class="row">
                                            <div class="col-md-12">

                                                <!-- <hr /> -->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <h2>Extras</h2>
                                                    </div>

                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h3>Rental eg.(Videoke, Gas Stove, Extra Mattress)</h3>

                                                        <form action="" method="post">
                                                          <input type="hidden" name="client_reference_id" value="<?php echo $client_ref_id; ?>">
                                                        <div id="list">
                                                          <div class="row" id="item">
                                                            <div class="form-group col-md-6" >
                                                                <label for="inputEmail3" class="control-label">Item</label>

                                                                <select id="select" class="form-control" name="item[]">
                                                                <option>---</option>
                                                                <?php
$sql = "SELECT * FROM extras WHERE extras_category='Rental'";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $extras_name = $rows['extras_name'];
    $extras_id = $rows['extras_id'];
    $extras_price = $rows['extras_price'];

    echo "<option value='$extras_id'>$extras_name - ₱$extras_price</option>";
}
?>
                                                                </select>


                                                            </div>
  <div class="form-group col-md-6">
                                                                  <label for="inputEmail3" class="col-sm-4 control-label">Qty</label>

    <input type="number" maxlength="2" min="0" max="30" class="form-control col-sm-4 numbers-only" name="quantity[]" value="0">
</div>
</div>

</div>
<?php
$sql = "SELECT * FROM customer_extras
JOIN extras ON customer_extras.extras_id = extras.extras_id
WHERE client_reference_id = '$client_ref_id' AND extras_category='Rental'
GROUP BY customer_extras.extras_id";

$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $extras_id = $rows['extras_id'];
    $extras_name = $rows['extras_name'];
    $extras_price = $rows['extras_price'];
    $sqlCount = "SELECT COUNT(*) as extras_qty FROM customer_extras WHERE extras_id='$extras_id' AND client_reference_id='$client_ref_id'";
    $resCount = $mysqli->query($sqlCount);
    $rowCount = mysqli_fetch_array($resCount);
    $extras_qty = $rowCount['extras_qty'];
    ?>
    <div class="row">
<input type="text" class="form-control col-md-6" disabled value="<?php echo $extras_name; ?>-<?php echo $extras_qty; ?>"/>
<a type="button" class="ui button red tiny delete-row" href="delete_extras_daytour.php?id=<?php echo $extras_id; ?>&ref_id=<?php echo $client_ref_id; ?>"><i class="fa fa-close"></i></a>
</div>
<?php
}
?>

<div>

<button class="ui button teal tiny" type="button" id="add">More Item <i class="fa fa-plus"></i></button>
</div>






                                                            <script>
                                                                $(document).ready(function() {
                                                                    var a = document.getElementById("item");
                                                                    $("#add").click(function() {
                                                                        $(a).clone().appendTo('#list').find("input[type='number']").val("");
                                                                    });

                                                                });
                                                            </script>

                                                            <h3>Lost/Damages eg.(Lost Key, Damaged Door)</h3>
                                                            <div id="listDamage">

<div class="row" id="itemDamage">
  <div class="form-group col-md-6" >
      <label for="inputEmail3" class="control-label">Item</label>

    <input type="text" class="form-control" name="damages[]" />


  </div>
<div class="form-group col-md-6">
        <label for="inputEmail3" class="col-sm-4 control-label">Price</label>

<input type="number" min="0" max="99999" class="form-control col-sm-4" name="damagesPrice[]">
</div>

</div>

<?php
$sql = "SELECT * FROM customer_extras
JOIN extras ON customer_extras.extras_id = extras.extras_id
WHERE client_reference_id = '$client_ref_id' AND extras_category='Damages'
GROUP BY customer_extras.extras_id";

$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $extras_id = $rows['extras_id'];
    $extras_name = $rows['extras_name'];
    $extras_price = $rows['extras_price'];
    $sqlCount = "SELECT COUNT(*) as extras_qty FROM customer_extras WHERE extras_id='$extras_id'";
    $resCount = $mysqli->query($sqlCount);
    $rowCount = mysqli_fetch_array($resCount);
    $extras_qty = $rowCount['extras_qty'];
    ?>
    <div class="row">
<input type="text" class="form-control col-md-6" disabled value="<?php echo $extras_name; ?>-<?php echo $extras_qty; ?>"/>
<a type="button" class="ui button red tiny delete-row" href="delete_extras_daytour.php?id=<?php echo $extras_id; ?>&ref_id=<?php echo $client_ref_id; ?>"><i class="fa fa-close"></i></a>
</div>
<?php
}
?>

                                                              </div>
<button class="ui button teal tiny" type="button" id="addDamage">More Item <i class="fa fa-plus"></i></button>



                                                                      <script>
                                                                $(document).ready(function() {
                                                                    var a = document.getElementById("itemDamage");
                                                                    $("#addDamage").click(function() {
                                                                        $(a).clone().appendTo('#listDamage').find("input[type='number']").val("");
                                                                    });
                                                                });
                                                            </script>

                                                            </div>

                                                    </div>
                                                </div>
                                        </div>


                                                    <input type="submit" name="submit" class="ui button teal pull-right" value="Submit">
                                            <div class="col-md-6">




                                     </div>
                                            </div>
                                        </div>


                                    </div>
                                    <!-- /.card-body -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.content -->
                </div>
                <!-- /.content-wrapper -->

                <!-- Main Footer -->
                <?php
include 'layout/footer.php';
?>
                    <!-- ./wrapper -->

                    <!-- REQUIRED SCRIPTS -->
                    <!-- DatePicker -->
                    <script>
                        const today = new Date();
                        const reservedDate = new Date(<?php echo $arrival_date; ?>);
                        $('#rangestart').calendar({
                            minDate: reservedDate,
                            type: 'date',
                            endCalendar: $('#rangeend')
                        });
                        $('#rangeend').calendar({
                            type: 'date',
                            startCalendar: $('#rangestart')
                        });
                    </script>
                    <!-- jQuery -->
                    <script src="plugins/jquery/jquery.min.js"></script>


                    <!-- AJAX -->
                    <script src="ajax/editCustomerDetails.js"></script>
                    <script src="ajax/editReservationDetails.js"></script>
                    <!-- Bootstrap 4 -->
                    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
                    <!-- DataTables -->
                    <script src="plugins/datatables/jquery.dataTables.js"></script>
                    <script src="plugins/datatables/dataTables.bootstrap4.js"></script>
                    <!-- AdminLTE App -->
                    <script src="dist/js/adminlte.min.js"></script>
                        <!-- JQuery Validation source -->
    <script type="text/javascript" src="../admin/plugins/jquery/jquery-key-restrictions.min.js"></script>
    <!-- Actual Validation calling class -->
    <script type="text/javascript">
    $(document).ready(function () {
        $(".letters-only").lettersOnly();
        $(".numbers-only").numbersOnly();
        $(".alpha-numeric-only").alphaNumericOnly();
    });
    </script>
    </body>

    </html>


    <?php

?>