<?php
include '../config/mysqli.php';
$client_ref_id = $_GET['ref_id'];

$sql = "SELECT * FROM customer LEFT JOIN
        reservation ON customer.client_reference_id = reservation.client_reference_id
        LEFT JOIN transaction ON customer.client_reference_id = transaction.client_reference_id
        WHERE customer.client_reference_id = '$client_ref_id'";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $client_ref_id = $rows['client_reference_id'];
    $first_name = $rows['first_name'];
    $last_name = $rows['last_name'];
    $contact = $rows['contact_number'];
    $email = $rows['email_address'];
    $arrival_date = $rows['arrival_date'];
    $departure_date = $rows['departure_date'];
    $created_at = $rows['created_at'];
    $no_of_adults = $rows['no_of_adults'];
    $no_of_kids = $rows['no_of_kids'];
    $res_status = $rows['reservation_status'];

}

?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>Villa Alfredo Admin</title>


        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/adminlte.min.css">
         <!-- Semantic UI -->
        <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />

        <!-- Initialize Semantic UI and JQuery to load Calendar Datepicker-->
        <script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" src="../js/semantic.min.js"></script>
          <!-- Calendar -->
          <link type="text/css" rel="stylesheet" href="../css/calendar.min.css" />
        <script src="../js/calendar.min.js"></script>
           <!-- Font Awesome Icon -->
           <link rel="stylesheet" href="../css/font-awesome.min.css">

<!-- Icons -->
<link rel="stylesheet" href="../css/icon.min.css">


        <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap4.css">
    </head>

    <body class="hold-transition sidebar-mini">
        <div class="wrapper">

            <?php
include 'layout/navbar.php';
include 'layout/sidebar.php';
?>
                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <div class="content-header">
                        <div class="container-fluid">

                        </div>
                        <!-- /.container-fluid -->
                    </div>
                    <!-- /.content-header -->

                    <!-- Main content -->
                    <div class="content">
                    <h3>Reference Id: <?php echo $client_ref_id; ?></h3>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <!-- /.card-header -->
                                    <div class="ui segment">
                                      <div class="row">
                                      <div class="col-md-6">
            <div class="logo">
                <img src="../img/valogo.png" width="200px">
            </div>
</div>
<div class="col-md-6 mt-3">
                <h3>Villa Alfredo's Resort</h3>

                <br>

                <span>Purok 1 Barangay Baliti, City of San Fernando</span>
                <span>,Pampanga Philippines</span>

                <br>

                <span>Manila Phone Office: +63 02 5844840</span>
                <span>/Resort Phone Office: +63 045 455-1397</span>
                <br>
                <span>villaalfredosresort@yahoo.com</span>
                <span>/villaalfredos@yahoo.com</span>
</div>
</div>
<hr />
<div class="row">
<div class="col-md-6">

  <?php if ($res_status == 'Pending') {?>
Reservation Status: <span class="badge bg-warning"><h4> <?php echo $res_status; ?></h4></span><br/><br/>
  <?php } else {?>
Reservation Status: <span class="badge bg-success"><h4> <?php echo $res_status; ?></h4></span><br/><br/>
<?php
}?>
<div class="row">
<div class="col-md-6 ui segment">
<h5>Customer Details</h5>
<div id="result-customer"></div>
<form action="ajax/editCustomerDetails.php" id="form-update-customer" method="post">
<div class="row">
<div class="col-md-6">
Reference ID:
</div>
<div class="col-md-6">
<input class="form-control form-control-sm"  type="text" placeholder="reference id" disabled value="<?php echo $client_ref_id; ?>">
<input type="hidden" name="client_reference_id" value="<?php echo $client_ref_id; ?>" />
</div>
</div>

<div class="row">
<div class="col-md-6">
First Name:
</div>
<div class="col-md-6">
<input class="form-control form-control-sm" name="first_name" type="text" placeholder=""  value="<?php echo $first_name; ?>">
</div>
</div>


<div class="row">
<div class="col-md-6">
Last Name:
</div>
<div class="col-md-6">
<input class="form-control form-control-sm" name="last_name" type="text" placeholder=""  value="<?php echo $last_name; ?>">
</div>
</div>

<div class="row">
<div class="col-md-6">
Contact Number:
</div>
<div class="col-md-6">
<input class="form-control form-control-sm" name="contact" type="number" placeholder=""  value="<?php echo $contact; ?>">
</div>
</div>

<div class="row">
  <div class="col-md-6">
  Email Address:
  </div>
  <div class="col-md-6">
    <input class="form-control form-control-sm" name="email" type="email" placeholder=""  value="<?php echo $email; ?>">
  </div>
</div>

<div class="row">
    <button id="btn-update-customer" type="submit" class="ui button teal tiny mt-2" style="margin:auto;">Update </button>
</div>
</form>

</div>
<div class="col-md-6 ui segment mt-0">
<h5> Reservation Details </h5>
<div id="result-reservation"></div>
<form action="ajax/editReservationDetails.php" method="post" id="form-update-reservation">
<div class="row">
<div class="col-md-6">
Arrival Date:
</div>
<div class="col-md-6">
<div class="ui calendar" id="rangestart">
    <div>
        <input type="text" class="form-control form-control sm" placeholder="Start" name="arrivalDate" autocomplete="off" value="<?php echo $arrival_date; ?>">
    </div>
</div>
</div>
</div>
<div class="row">
<div class="col-md-6">
Departure Date:
</div>
<div class="col-md-6">
<div class="ui calendar" id="rangeend">
<div>
    <input type="text" class="form-control form-control sm" placeholder="End" name="departureDate" autocomplete="off" value="<?php echo $departure_date; ?>">
</div>
</div>
</div>
</div>

 <?php
$departure = strtotime($departure_date);
$arrival = strtotime($arrival_date);
$datediff = $departure - $arrival;

$noofNights = round($datediff / (60 * 60 * 24));
?>

<div class="row">
<div class="col-md-6">
No of Nights:
</div>
<div class="col-md-6">
<input class="form-control form-control-sm" type="text" placeholder="" disabled  value="<?php echo $noofNights; ?>">
</div>
</div>
    <div class="row">
    <button id="btn-update-reservation" type="submit" class="ui button teal tiny mt-2" style="margin:auto;">Update </button>
    </div>
</form>
</div>
</div>
<hr />
 <div class="row">
<div class="col-md-6 ui segment">
<div class="row">
<div class="col-md-6">
<h5>Rooms</h5>
</div>
<div class="col-md-6">
<button class="btn btn-sm btn-info">Add Rooms <i class="fa fa-plus"></i></button>
</div>
</div>
<table border="1" class="mt-2">
  <th style="padding:6px; color: #424242;">Rooms</th>
  <th style="padding:6px; color: #424242;">Price </th>
  <th style="padding:6px; color: #424242;">Action</th>
  <tbody>
    <?php
$sql = "SELECT * FROM reserved_rooms JOIN room_type ON reserved_rooms.room_type_id=room_type.room_id WHERE client_reference_id='$client_ref_id'";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $room_id = $rows['room_id'];
    ?>
      <tr>
        <td align="center"><?php echo $rows['room_name']; ?></td>

      <td align="center"><?php echo $rows['room_price'] ?></td>
      <td align="center"><a class="btn btn-sm btn-danger"><i class="fa fa-close" style="color:white;"></i></a></td>
      </tr>
    <?php
}
?>
</tbody>
</table>
</div>

<div class="col-md-6 ui segment mt-0">
<h5> Transaction Details </h5>
<div class="row">
<div class="col-md-6">
No of Kids:
</div>
<div class="col-md-6">
<input class="form-control form-control-sm" type="text" placeholder=""  value="<?php echo $no_of_kids; ?>">
</div>
</div>


<div class="row">
<div class="col-md-6">
No of Adults:
</div>
<div class="col-md-6">
<input class="form-control form-control-sm" type="text" placeholder=""  value="<?php echo $no_of_adults; ?>">
</div>
</div>


</div>
</div>
</div>
<div class="col-md-6">
                  <p class="lead">Amount</p>

                  <div class="table-responsive">
                    <table class="table">
                      <tbody>
                      <tr>
                      <th>Room Total: </th>
                      <td>2000</td>
            <input type="hidden" name="room_total" id="roomTotal" value="2000">
                      </tr>
                      <tr>
                      <th>Entrance Fees:</th>
                      <td>
                        <span id="entrance"></span>
                        <input type="hidden" name="entrance" id="entranceTotal">
                      </td>
                      </tr><tr>
                      <th>Subtotal</th>
                      <td>
                      <span id="subtotal"></span>
                      <input type="hidden" name="subtotal" id="subTotal">
                      </td>
                      </tr>

                      <tr>
                        <th style="width:50%">Downpayment</th>
                        <td>            <span id="downpayment">
            1000            </span>
            <input type="" hidden="downpayment" id="downpaymentTotal" value="1000">

            </td>
                      </tr>
                      <tr>
                        <th>Balance </th>
                        <td>
                                                <span id="balance">1000</span>
                        <input type="hidden" name="balance" id="balanceTotal" value="1000 ">
                        </td>
</tr>

                    </tbody></table>
                  </div>
                </div>
</div>


</div>
                                    <!-- /.card-body -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.content -->
                </div>
                <!-- /.content-wrapper -->

                <!-- Main Footer -->
                <?php
include 'layout/footer.php';
?>
                    <!-- ./wrapper -->

                    <!-- REQUIRED SCRIPTS -->
                    <!-- DatePicker -->
                    <script>
                    const today = new Date();
                    const reservedDate = new Date(<?php echo $arrival_date; ?>);
                    $('#rangestart').calendar({
                        minDate: reservedDate,
                        type: 'date',
                        endCalendar: $('#rangeend')
                    });
                    $('#rangeend').calendar({
                        type: 'date',
                        startCalendar: $('#rangestart')
                    });
                        </script>
                    <!-- jQuery -->
                    <script src="plugins/jquery/jquery.min.js"></script>
                    <!-- AJAX -->
                    <script src="ajax/editCustomerDetails.js"></script>
                    <script src="ajax/editReservationDetails.js"></script>
                    <!-- Bootstrap 4 -->
                    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
                    <!-- DataTables -->
                    <script src="plugins/datatables/jquery.dataTables.js"></script>
                    <script src="plugins/datatables/dataTables.bootstrap4.js"></script>
                    <!-- AdminLTE App -->
                    <script src="dist/js/adminlte.min.js"></script>
    </body>

    </html>
