<?php include '../config/mysqli.php';?>
<?php

$id = $_SESSION['id'];

$sql = "SELECT * FROM admin WHERE username = '$id'";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $user_role = $rows['user_role'];
    $username = $rows['username'];
}
?>
<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="dashboard.php" class="brand-link">
        <img src="dist/img/adminLogo.png" alt="VA Logo" class="brand-image img-circle elevation-3" style="opacity: 1">
        <span class="brand-text font-weight-light"><?php echo $username; ?></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">


            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                    <li class="nav-item">
                        <a href="dashboard.php" class="nav-link">
                            <i class="nav-icon fa fa-th"></i>
                            <p>
                                DashBoard
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="check_in.php" class="nav-link">
                            <i class="nav-icon fa fa-suitcase"></i>
                            <p>
                                Check In
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="walk_in.php" class="nav-link">
                            <i class="nav-icon fa fa-user-plus"></i>
                            <p>
                                Walk In Daytour
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="reservation_admin.php" class="nav-link">
                            <i class="nav-icon fa fa-bed"></i>
                            <p>
                                Walk In Reservation
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="reservations.php" class="nav-link">
                            <i class="nav-icon fa fa-calendar"></i>
                            <p>
                                Pending Reservations
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="rebooking.php" class="nav-link">
                            <i class="nav-icon fa fa-calendar"></i>
                            <p>
                                Booked Reservation
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="checked_in.php" class="nav-link">
                            <i class="nav-icon fa fa-check"></i>
                            <p>
                                Checked In Guest
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="billing.php" class="nav-link">
                            <i class="nav-icon fa fa-home"></i>
                            <p>
                                Day Tour / Overnight
                            </p>
                        </a>
                    </li>
                    <?php if ($user_role == 'admin') {  
    ?>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-cog"></i>
                            <p>
                                Settings
                                <i class="fa fa-angle-right right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="manage_cottages.php" class="nav-link">
                                    <i class="nav-icon fa fa-home"></i>
                                    <p>
                                        Manage Cottages
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="manage_rooms.php" class="nav-link">
                                    <i class="nav-icon fa fa-bed"></i>
                                    <p>
                                        Manage Rooms
                                    </p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="reports.php" class="nav-link">
                            <i class="nav-icon fa fa-file"></i>
                            <p>
                                Reports
                            </p>
                        </a>
                    </li>
                    <?php } else {
                    }?>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
</aside>