<?php
include '../config/mysqli.php';

use PHPMailer\PHPMailer\PHPMailer;

require '../src/Exception.php';
require '../src/PHPMailer.php';
require '../src/SMTP.php';

if (isset($_POST['rebook'])) {

    $client_reference_id = $_POST['ref_id'];
    $encrypted = base64_encode($client_reference_id);
    $arrival_date = $_POST['arrival_date_post'];
    $departure_date = $_POST['departure_date_post'];
    $no_of_adults = $_POST['no_of_adults_post'];
    $no_of_kids = $_POST['no_of_kids_post'];
    $email_address = $_POST['email_address'];
    $first_name = $_POST['first_name_post'];
    $last_name = $_POST['last_name_post'];

    $arrival_date = date_create_from_format('F j, Y', $arrival_date);
    $arrival_date = date_format($arrival_date, "Y-m-d");

    $departure_date = date_create_from_format('F j, Y', $departure_date);
    $departure_date = date_format($departure_date, "Y-m-d");

    $departure = strtotime($departure_date);
    $arrival = strtotime($arrival_date);
    $datediff = $departure - $arrival;

    $noofNights = round($datediff / (60 * 60 * 24));


    $sql = "SELECT * FROM room_type WHERE isDeleted=0";
    $res = $mysqli->query($sql);
    $hasSelectedRoom = [];
    $room = [];
    while ($rows = mysqli_fetch_assoc($res)) {
        $roomType = $rows['room_id'];
        $_POST['room_id' . $roomType];
        @$isSelected = $_POST['quantity' . $roomType];
        array_push($hasSelectedRoom, $isSelected);
        if ($isSelected != 0) {
            array_push($room, $roomType);
            array_push($room, $isSelected);
        }
    }
    $rooms = array_chunk($room, 2);
    if ($mailing == true) {
        if ($deployedMailing == true) {

            $resRebook = $mysqli->query("UPDATE customer SET arrival_date='$arrival_date',
            departure_date ='$departure_date',
            no_of_kids = '$no_of_kids',
            no_of_adults = '$no_of_adults' WHERE client_reference_id='$client_reference_id'");
            if ($resRebook) {
                for ($i = 0; $i < count($rooms); $i++) {
                    $room_id = $rooms[$i][0];
                    $room_qty = $rooms[$i][1];
                    for ($j = 0; $j < $room_qty; $j++) {
                        $sql1 = "INSERT INTO reserved_rooms (room_type_id,client_reference_id) VALUES('$room_id','$client_reference_id')";
                        $mysqli->query($sql1);
                    }
                }
                $sql = "UPDATE reservation SET isRebooked='1' WHERE client_reference_id='$client_reference_id'";
                $mysqli->query($sql);
                $sql_main_select = "SELECT * FROM customer WHERE client_reference_id ='$client_reference_id'";
                $res_main_select = $mysqli->query($sql_main_select);
                while($row_main = mysqli_fetch_assoc($res_main_select)) {
                    $no_of_adults = $row_main['no_of_adults'];
                    $no_of_kids   = $row_main['no_of_kids'];
                }
                $sql = "SELECT * FROM reserved_rooms
                JOIN customer ON
                reserved_rooms.client_reference_id = customer.client_reference_id
                JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
                WHERE customer.client_reference_id ='$client_reference_id'
                GROUP BY reserved_rooms.room_type_id";
                 $res = $mysqli->query($sql);
                 $total = 0;
                 while ($rows = mysqli_fetch_assoc($res)) {
                    $room_type_id = $rows['room_type_id'];
                    $sql1 = "SELECT COUNT(*) as quantity FROM reserved_rooms
                  JOIN customer ON
                  reserved_rooms.client_reference_id = customer.client_reference_id
                  JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
                  WHERE customer.client_reference_id ='$client_reference_id' AND room_type_id ='$room_type_id'";
                    $res1 = $mysqli->query($sql1);
                    $row1 = mysqli_fetch_assoc($res1);
                    $total = $row1['quantity'] * $rows['room_price'];
                    array_push($subtotal, $total);
                }
                
                function multiplyDays($total)
                {
                    global $noofNights;
                    return ($total * $noofNights);
                }
                $grandTotal = array_sum(array_map("multiplyDays", $subtotal));
                $totalAdult = $no_of_adults * 250;
                $totalKid = $no_of_kids * 150;
                $grandTotal = $grandTotal + $totalAdult + $totalKid;

                $sql_get_balance = "SELECT downpayment,balance FROM transaction WHERE client_reference_id='$client_reference_id'";
                $res_balance = $mysqli->query($sql_get_balance);
                $row_balance = mysqli_fetch_assoc($res_balance);
                $transaction_downpayment = $row_balance['downpayment'];
                $transaction_balance     = $row_balance['balance'];
                $balance_now = $grandTotal - $downpayment;
                $sql_update_rebook = "UPDATE transaction SET total='$grandTotal', balance='$balance_now' WHERE client_reference_id='$client_reference_id'";
                $mysqli->query($sql_update_rebook);
            } else {
                echo "<script>alert('Cannot Update Customer');</script>";
            }

            $from = "villa_alfredo_email@villaalfredo.x10host.com";
            $to = "$email_address, $first_name.' '.$last_name";

            $baseurl = $_SERVER['SERVER_NAME'];
            $amount = number_format($amount, 2);
            $mailbody = "
            <div id=':14u' class='ii gt'><div id=':14t' class='a3s aXjCH '><div id='m_-852930379096079456page-wrapper' style='background-color:#ffffff;padding:0px'><div class='adM'>
</div><table style='font-family:'Open Sans',sans-serif;font-size:12px;margin:0px' width='100%' border='0' cellspacing='0'>
<tbody>
<tr>
<td>
<p style='font-size:20px'><span class='il'>Villa</span> <span class='il'>Alfredo</span> Resort</p>
</td>
</tr>
<tr>
<td class='m_-852930379096079456box-grey' style='background-color:#eeeeee;padding:15px 15px 15px 15px;border-bottom-left-radius:0;border-bottom-right-radius:0' valign='top'>
<table width='100%' border='0' cellpadding='0'>
<tbody>
<tr valign='top'>
<td width='65%'><span style='font-size:12px'><strong>Thank you, " . $first_name . " Your booking is now Rebooked!.</strong></span></td>
<td style='font-size:12px' width='35%'>
<table width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td align='right'><strong>Reference Number:</strong>" . $client_reference_id . "</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td class='m_-852930379096079456box-grey' style='background-color:#f8f8f8;padding:0 15px 15px 15px;border-top-left-radius:0;border-top-right-radius:0' valign='top'>
<table width='100%' border='0' cellpadding='0'>
<tbody>
<tr valign='top'>
<td width='65%'>
<ul style='padding:15px 0 0 15px;margin:0'>
<li style='line-height:20px;padding:0;margin:0'>For booking enquires, cancellations or amendments please contact us directly at <a href='mailto:test.villaalfredo@gmail.com' target='_blank'>test.villaalfredo@gmail.com</a> or (045) 455 1397.</li>
</ul>
</td>
<td style='font-size:12px' width='35%'>
<table width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td align='right'>&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td>
<div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Your Booking</strong></div>
<table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td style='padding:1px 10px 1px 20px' valign='top' width='20%'><strong>Guest:</strong></td>
<td style='padding:1px 20px' width='80%'>" . $first_name . " " . $last_name . " </td>
</tr>
<tr>
<td style='padding:1px 10px 1px 20px' valign='top' width='20%'><strong>Details:</strong></td>
<td style='padding:1px 0px 1px 20px' width='80%'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                    <tbody><tr>
                        <td width='75%'>
                        ";
            $sqlMail = "SELECT * FROM reserved_rooms
            JOIN customer ON
            reserved_rooms.client_reference_id = customer.client_reference_id
            JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
            WHERE customer.client_reference_id ='$client_reference_id'
            GROUP BY reserved_rooms.room_type_id";
            $resMail = $mysqli->query($sqlMail);
            $total = 0;
            $subtotal = [];
            while ($rows = mysqli_fetch_assoc($resMail)) {
                $room_type_id = $rows['room_type_id'];
                $sql1 = "SELECT COUNT(*) as quantity FROM reserved_rooms
              JOIN customer ON
              reserved_rooms.client_reference_id = customer.client_reference_id
              JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
              WHERE customer.client_reference_id ='$client_reference_id' AND room_type_id ='$room_type_id'";
                $res1 = $mysqli->query($sql1);
                $row1 = mysqli_fetch_assoc($res1);
                $total = $row1['quantity'] * $rows['room_price'];

                $mailbody .= "
                                " . $rows['room_name'] . " x
                                " . $row1['quantity'] . ",
                            ";

                $count++;
                array_push($subtotal, $total);
            }

            $departure = strtotime($departure_date);
            $arrival = strtotime($arrival_date);
            $datediff = $departure - $arrival;

            $noofNights = round($datediff / (60 * 60 * 24));

            function multiplyDays($total)
            {
                global $noofNights;
                return ($total * $noofNights);
            }
            $grandTotal = array_sum(array_map("multiplyDays", $subtotal));
            $totalAdult = $no_of_adults * 250;
            $totalKid = $no_of_kids * 150;
            $grandTotal = $grandTotal + $totalAdult + $totalKid;
            $grandestTotal = number_format($grandTotal, 2);
            $down = number_format($grandTotal / 2, 2);

            $mailbody .= "Adult x " . $no_of_adults . ",
            Child x " . $no_of_kids . "";

            $mailbody .= "</td>
                        <td width='25%' valign='top' align='right' style='padding:1px 15px'>PHP " . $grandestTotal . "</td>
                    </tr>
                </tbody></table></td>
</tr>
<tr>
<td style='padding:1px 10px 1px 20px' width='20%'><strong>Check-in:</strong></td>
<td style='padding:1px 20px' width='80%'>" . $arrival_date . " from 14:00</td>
</tr>
<tr>
<td style='padding:1px 10px 1px 20px' width='20%'><strong>Check-out:</strong></td>
<td style='padding:1px 20px' width='80%'>" . $departure_date . " until 12:00</td>
</tr>
</tbody>
</table>
</td>
</tr>

<tr>
<td>
<div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Additional Details</strong></div>
<table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td><table width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody><tr>
<td width='20%' style='padding:1px 10px 1px 20px'><strong>Additional comments:</strong></td>
<td width='80%' style='padding:1px 20px'>" . $additional_details . "</td>
</tr>
</tbody></table></td>
</tr>
</tbody>
</table>
</td>
</tr>

<tr>
<td>
<div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Payment Details</strong></div>
<table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td style='padding:1px 20px' width='50%'><strong>Total Amount Due: ".$grandestTotal."</strong></td>
<td style='padding:1px 15px' align='right' width='50'>PHP " . $grandestTotal . "</td>
</tr>

<tr>
<td style='padding:1px 20px'><strong>Downpayment: ".$down."</strong></td>
<td style='padding:1px 15px;vertical-align:top' align='right'>PHP " . $amount . "</td>
</tr>

<tr>
<td colspan='2'></td>
</tr>
</tbody>
</table>
</td>
</tr>

<tr>
<td>
<div style='font-size:14px;margin-top:20px'><strong>Booking Policies</strong></div>
<table style='margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody><tr>
<td><table width='100%' border='0' cellpadding='0' cellspacing='0'>
                    <tbody><tr>
                        <td style='border-bottom:2px solid #eeeeee;border-top:2px solid #eeeeee;padding:10px 20px!important'>
                        <span style='font-size:11px'>
                            <strong>Cancellation:</strong> If cancelled, modified or in case of no-show, no penalty   will be charged.<br>
                           <strong>Other policies:  </strong>Please call 63 045 455-0789 or send an email to <a href='mailto:test.villaalfredo@gmail.com' target='_blank'>test.villaalfredo@gmail.com</a><br> Hotel requires an 'Incidental Deposit' at check-in, which is fully refundable at check-out.<br>
                        </span>
                        </td>
                    </tr>

                </tbody></table></td>
</tr>
</tbody></table></td></tr></tbody>
</table>



<table><tbody><tr>
<td>
<div style='font-size:14px;margin-top:20px'><strong>Villa Alfredos</strong></div>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>

</tbody></table><div class='yj6qo'></div><div class='adL'>
</div></div></div></div>";

            $mailbody .= "<br>
            Check your reservation details here <a href='" . $baseurl . "/reservation/message.php?res_id=$encrypted'>" . $baseurl . "/reservation/message.php?res_id=$encrypted'</a>
            <br/>
            <h3>House Rules</h3>
            <ol>
            <li>  No hard liquor </li>
            <li> No eating near the pool. </li>
            <li> No firearms allowed. </li>
            <li> No littering. </li>
            <li> No picking of fruits and flowers. </li>
            <li> Wearing sando, t-shirt & pants will not be allowed to swim. </li>
            <li> Videoke & swimming are allowed until 12:00 o’clock midnight only. </li>
            <li> Loud music is not allowed between 12 o’clock midnight up to 8 o’clock in the morning. </li>
            <li> Management provides 24 hours security, we are not responsible for item lost inside the car, please lock your vehicle and secure your entire valuable. </li>
            <li> Please pay particular attention to your entire valuable and secure them at all times. 11. No running, diving, shoving or pushing within the pool area. </li>
            <li> Right Conduct. We reserve the right to evict/eject any guest or group of guest should we find them engaging in fights, or generally disrupting the peace at  the resort </li>
            </ol>
            <i>Not allowed: Radio, Component, Speaker, DVD Player, Magic Sing, Electric Stove, Gas Stove, Butane Stove. </i>
            ";
            $subject = "Reservation Complete";

            $headers = "From: $from" . "\r\n";
            $headers .= "Reply-To: $from" . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            $resp = mail($to, $subject, $mailbody, $headers);

            if ($resp) {
                header("Location: overview.php?ref_id=" . $client_reference_id . "&view_by=reservation&rebooked=true");
            } else {
                header("Location: overview.php?ref_id=$client_reference_id&view_by=reservation&rebooked=false");
            }
        } else {
            $resRebook = $mysqli->query("UPDATE customer SET arrival_date='$arrival_date',
            departure_date ='$departure_date',
            no_of_kids = '$no_of_kids',
            no_of_adults = '$no_of_adults' WHERE client_reference_id='$client_reference_id'");
            if ($resRebook) {
                for ($i = 0; $i < count($rooms); $i++) {
                    $room_id = $rooms[$i][0];
                    $room_qty = $rooms[$i][1];
                    for ($j = 0; $j < $room_qty; $j++) {
                        $sql1 = "INSERT INTO reserved_rooms (room_type_id,client_reference_id) VALUES('$room_id','$client_reference_id')";
                        $mysqli->query($sql1);
                    }
                }
                $sql = "UPDATE reservation SET isRebooked='1' WHERE client_reference_id='$client_reference_id'";
                $mysqli->query($sql);
            } else {
                echo "<script>alert('Cannot Update Customer');</script>";
            }

            $mail = new PHPMailer(true); // Passing `true` enables exceptions
            //Server settings
            $mail->SMTPDebug = 0; // Enable verbose debug output
            $mail->isSMTP(); // Set mailer to use SMTP
            $mail->Host = 'smtp.gmail.com'; // Specify main and backup SMTP servers
            $mail->SMTPAuth = true; // Enable SMTP authentication
            $mail->Username = 'test.villaalfredo@gmail.com'; // SMTP username
            $mail->Password = 'reservationsystem'; // SMTP password
            $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587; // TCP port to connect to
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true,
                ),
            );
            //Recipients
            $mail->setFrom('test.villaalfredo@gmail.com', 'Villa Alfredo');
            $mail->addAddress($email_address, $first_name . ' ' . $last_name); // Add a recipient
            $baseurl = $_SERVER['SERVER_NAME'];
            $amount = number_format($amount, 2);
            $mailbody = "
            <div id=':14u' class='ii gt'><div id=':14t' class='a3s aXjCH '><div id='m_-852930379096079456page-wrapper' style='background-color:#ffffff;padding:0px'><div class='adM'>
</div><table style='font-family:'Open Sans',sans-serif;font-size:12px;margin:0px' width='100%' border='0' cellspacing='0'>
<tbody>
<tr>
<td>
<p style='font-size:20px'><span class='il'>Villa</span> <span class='il'>Alfredo</span> Resort</p>
</td>
</tr>
<tr>
<td class='m_-852930379096079456box-grey' style='background-color:#eeeeee;padding:15px 15px 15px 15px;border-bottom-left-radius:0;border-bottom-right-radius:0' valign='top'>
<table width='100%' border='0' cellpadding='0'>
<tbody>
<tr valign='top'>
<td width='65%'><span style='font-size:12px'><strong>Thank you, " . $first_name . " Your booking is now Rebooked!.</strong></span></td>
<td style='font-size:12px' width='35%'>
<table width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td align='right'><strong>Reference Number:</strong>" . $client_reference_id . "</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td class='m_-852930379096079456box-grey' style='background-color:#f8f8f8;padding:0 15px 15px 15px;border-top-left-radius:0;border-top-right-radius:0' valign='top'>
<table width='100%' border='0' cellpadding='0'>
<tbody>
<tr valign='top'>
<td width='65%'>
<ul style='padding:15px 0 0 15px;margin:0'>
<li style='line-height:20px;padding:0;margin:0'>For booking enquires, cancellations or amendments please contact us directly at <a href='mailto:test.villaalfredo@gmail.com' target='_blank'>test.villaalfredo@gmail.com</a> or (045) 455 1397.</li>
</ul>
</td>
<td style='font-size:12px' width='35%'>
<table width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td align='right'>&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td>
<div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Your Booking</strong></div>
<table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td style='padding:1px 10px 1px 20px' valign='top' width='20%'><strong>Guest:</strong></td>
<td style='padding:1px 20px' width='80%'>" . $first_name . " " . $last_name . " </td>
</tr>
<tr>
<td style='padding:1px 10px 1px 20px' valign='top' width='20%'><strong>Details:</strong></td>
<td style='padding:1px 0px 1px 20px' width='80%'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                    <tbody><tr>
                        <td width='75%'>
                        ";
            $sqlMail = "SELECT * FROM reserved_rooms
            JOIN customer ON
            reserved_rooms.client_reference_id = customer.client_reference_id
            JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
            WHERE customer.client_reference_id ='$client_reference_id'
            GROUP BY reserved_rooms.room_type_id";
            $resMail = $mysqli->query($sqlMail);
            $total = 0;
            $subtotal = [];
            while ($rows = mysqli_fetch_assoc($resMail)) {
                $room_type_id = $rows['room_type_id'];
                $sql1 = "SELECT COUNT(*) as quantity FROM reserved_rooms
              JOIN customer ON
              reserved_rooms.client_reference_id = customer.client_reference_id
              JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
              WHERE customer.client_reference_id ='$client_reference_id' AND room_type_id ='$room_type_id'";
                $res1 = $mysqli->query($sql1);
                $row1 = mysqli_fetch_assoc($res1);
                $total = $row1['quantity'] * $rows['room_price'];

                $mailbody .= "
                                " . $rows['room_name'] . " x
                                " . $row1['quantity'] . ",
                            ";

                $count++;
                array_push($subtotal, $total);
            }

            $departure = strtotime($departure_date);
            $arrival = strtotime($arrival_date);
            $datediff = $departure - $arrival;

            $noofNights = round($datediff / (60 * 60 * 24));

            function multiplyDays($total)
            {
                global $noofNights;
                return ($total * $noofNights);
            }
            $grandTotal = array_sum(array_map("multiplyDays", $subtotal));
            $totalAdult = $no_of_adults * 250;
            $totalKid = $no_of_kids * 150;
            $grandTotal = $grandTotal + $totalAdult + $totalKid;
            $grandestTotal = number_format($grandTotal, 2);
            $down = number_format($grandTotal / 2, 2);

            $mailbody .= "Adult x " . $no_of_adults . ",
            Child x " . $no_of_kids . "";

            $mailbody .= "</td>
                        <td width='25%' valign='top' align='right' style='padding:1px 15px'>PHP " . $grandestTotal . "</td>
                    </tr>
                </tbody></table></td>
</tr>
<tr>
<td style='padding:1px 10px 1px 20px' width='20%'><strong>Check-in:</strong></td>
<td style='padding:1px 20px' width='80%'>" . $arrival_date . " from 14:00</td>
</tr>
<tr>
<td style='padding:1px 10px 1px 20px' width='20%'><strong>Check-out:</strong></td>
<td style='padding:1px 20px' width='80%'>" . $departure_date . " until 12:00</td>
</tr>
</tbody>
</table>
</td>
</tr>

<tr>
<td>
<div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Additional Details</strong></div>
<table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td><table width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody><tr>
<td width='20%' style='padding:1px 10px 1px 20px'><strong>Additional comments:</strong></td>
<td width='80%' style='padding:1px 20px'>" . $additional_details . "</td>
</tr>
</tbody></table></td>
</tr>
</tbody>
</table>
</td>
</tr>

<tr>
<td>
<div style='border-bottom:2px solid #eeeeee;padding-bottom:8px;font-size:14px;margin-top:20px'><strong>Payment Details</strong></div>
<table style='border-bottom:2px solid #eeeeee;padding-bottom:8px;margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody>
<tr>
<td style='padding:1px 20px' width='50%'><strong>Total Amount Due: ".$grandestTotal."</strong></td>
<td style='padding:1px 15px' align='right' width='50'>PHP " . $grandestTotal . "</td>
</tr>

<tr>
<td style='padding:1px 20px'><strong>Downpayment: ".$down."</strong></td>
<td style='padding:1px 15px;vertical-align:top' align='right'>PHP " . $amount . "</td>
</tr>

<tr>
<td colspan='2'></td>
</tr>
</tbody>
</table>
</td>
</tr>

<tr>
<td>
<div style='font-size:14px;margin-top:20px'><strong>Booking Policies</strong></div>
<table style='margin-top:8px' width='100%' border='0' cellspacing='0' cellpadding='0'>
<tbody><tr>
<td><table width='100%' border='0' cellpadding='0' cellspacing='0'>
                    <tbody><tr>
                        <td style='border-bottom:2px solid #eeeeee;border-top:2px solid #eeeeee;padding:10px 20px!important'>
                        <span style='font-size:11px'>
                            <strong>Cancellation:</strong> If cancelled, modified or in case of no-show, no penalty   will be charged.<br>
                           <strong>Other policies:  </strong>Please call 63 045 455-0789 or send an email to <a href='mailto:test.villaalfredo@gmail.com' target='_blank'>test.villaalfredo@gmail.com</a><br> Hotel requires an 'Incidental Deposit' at check-in, which is fully refundable at check-out.<br>
                        </span>
                        </td>
                    </tr>

                </tbody></table></td>
</tr>
</tbody></table></td></tr></tbody>
</table>



<table><tbody><tr>
<td>
<div style='font-size:14px;margin-top:20px'><strong>Villa Alfredos</strong></div>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>

</tbody></table><div class='yj6qo'></div><div class='adL'>
</div></div></div></div>";

            $mailbody .= "<br>
            Check your reservation details here <a href='" . $baseurl . "/reservation/message.php?res_id=$encrypted'>" . $baseurl . "/reservation/message.php?res_id=$encrypted'</a>
            <br/>
            <h3>House Rules</h3>
            <ol>
            <li>  No hard liquor </li>
            <li> No eating near the pool. </li>
            <li> No firearms allowed. </li>
            <li> No littering. </li>
            <li> No picking of fruits and flowers. </li>
            <li> Wearing sando, t-shirt & pants will not be allowed to swim. </li>
            <li> Videoke & swimming are allowed until 12:00 o’clock midnight only. </li>
            <li> Loud music is not allowed between 12 o’clock midnight up to 8 o’clock in the morning. </li>
            <li> Management provides 24 hours security, we are not responsible for item lost inside the car, please lock your vehicle and secure your entire valuable. </li>
            <li> Please pay particular attention to your entire valuable and secure them at all times. 11. No running, diving, shoving or pushing within the pool area. </li>
            <li> Right Conduct. We reserve the right to evict/eject any guest or group of guest should we find them engaging in fights, or generally disrupting the peace at  the resort </li>
            </ol>
            <i>Not allowed: Radio, Component, Speaker, DVD Player, Magic Sing, Electric Stove, Gas Stove, Butane Stove. </i>
            ";
            //Content
            $mail->isHTML(true); // Set email format to HTML
            $mail->Subject = 'Reservation Complete';
            $mail->Body = $mailbody;
            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
            if ($mail->send()) {
                header("Location: overview.php?ref_id=" . $client_reference_id . "&view_by=reservation&rebooked=true");
            } else {
                header("Location: overview.php?ref_id=$client_reference_id&view_by=reservation&rebooked=false");
            }

        }
    } else {
        $resRebook = $mysqli->query("UPDATE customer SET arrival_date='$arrival_date',
        departure_date ='$departure_date',
        no_of_kids = '$no_of_kids',
        no_of_adults = '$no_of_adults' WHERE client_reference_id='$client_reference_id'");
        if ($resRebook) {
            for ($i = 0; $i < count($rooms); $i++) {
                $room_id = $rooms[$i][0];
                $room_qty = $rooms[$i][1];
                for ($j = 0; $j < $room_qty; $j++) {
                    $sql1 = "INSERT INTO reserved_rooms (room_type_id,client_reference_id) VALUES('$room_id','$client_reference_id')";
                    $mysqli->query($sql1);
                }
            }
            $sql = "UPDATE reservation SET isRebooked='1' WHERE client_reference_id='$client_reference_id'";
            $mysqli->query($sql);
            header("Location: overview.php?ref_id=" . $client_reference_id . "&view_by=reservation&rebooked=true");
        } else {
            header("Location: overview.php?ref_id=$client_reference_id&view_by=reservation&rebooked=false");
        }
    }

}
