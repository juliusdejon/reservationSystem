<?php

include '../config/mysqli.php';

if (isset($_POST['add_cottage'])) {
    $_SESSION['first_name'] = (isset($_POST['first_name'])) ? $_POST['first_name'] : '';
    $_SESSION['last_name'] = (isset($_POST['last_name'])) ? $_POST['last_name'] : '';
    $_SESSION['arrival_date'] = (isset($_POST['arrival_date'])) ? $_POST['arrival_date'] : '';
    $_SESSION['contact'] = (isset($_POST['contact'])) ? $_POST['contact'] : '';
    $_SESSION['email'] = (isset($_POST['email'])) ? $_POST['email'] : '';
    $_SESSION['no_of_adults'] = (isset($_POST['no_of_adults'])) ? $_POST['no_of_adults'] : '';
    $_SESSION['no_of_kids'] = (isset($_POST['no_of_kids'])) ? $_POST['no_of_kids'] : '';
    $_SESSION['no_of_senior_pwd'] = (isset($_POST['no_of_senior_pwd'])) ? $_POST['no_of_senior_pwd'] : '';
    $_SESSION['type'] = (isset($_POST['type'])) ? $_POST['type'] : '';

    $first_name = $_SESSION['first_name'];
    $last_name = $_SESSION['last_name'];
    $arrival_date = $_SESSION['arrival_date'];
    $contact = $_SESSION['contact'];
    $email = $_SESSION['email'];
    $no_of_adults = $_SESSION['no_of_adults'];
    $no_of_kids = $_SESSION['no_of_kids'];
    $no_of_senior_pwd = $_SESSION['no_of_senior_pwd'];
    $type = $_SESSION['type'];

    // Random Numbers
    $random = substr(str_shuffle(str_repeat("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", 5)), 0, 5);
    // Date format dmy append to refernece ID
    $date = date("dmY");
    $first_name = strtolower($first_name);
    $last_name = strtolower($last_name);
    $first_name = ucfirst($first_name);
    $last_name = ucfirst($last_name);
    $client_reference_id = $first_name[0] . $last_name[0] . $date . $random;

    // $arrival_date = date_create_from_format('F j, Y', $arrival_date);
    // $arrival_date = $arrival_date->format('Y-m-d');

    $sql = "INSERT INTO customer (client_reference_id,first_name,last_name,contact_number,email_address,arrival_date,departure_date,no_of_adults,no_of_kids,no_of_senior_pwd)
             VALUES('$client_reference_id', '$first_name', '$last_name', '$contact','$email', '$arrival_date','$arrival_date','$no_of_adults','$no_of_kids','$no_of_senior_pwd')";
    $res = $mysqli->query($sql);

    if ($res) {
        $check_in_date = date('Y-m-d H:i:s');
        $sql = "INSERT INTO transaction (client_reference_id,transaction_type,check_in_date)
    VALUES('$client_reference_id', '$type', '$check_in_date')";
        $res = $mysqli->query($sql);
        if ($res) {
            header('Location: avail_cottages.php?res_id=' . $client_reference_id);
            session_destroy();
        } else {
            echo "lol";
        }
    } else {
        echo "lol";
    }
}
