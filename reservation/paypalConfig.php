<?php
include '../config/mysqli.php';

// PayPal settings

// paypal_email will receive payment coming from the customers
// $paypal_email = 'test.villaalfredos-facilitator@gmail.com';
// $paypal_email = 'villa-alfredos@gmail.com';

// $return_url = 'localhost/reservationsystem/reservation/payment-success.php';
// $cancel_url = 'localhost/reservationsystem/reservation/payment-cancelled.php';
// $notify_url = 'localhost/reservationsystem/reservation/payments.php';

$payment_url = "https://www.sandbox.paypal.com/cgi-bin/webscr"; // PayPal API URL
/* Please note that after complete development change the above URL and remove ".sandbox" from the URL
or comment the above line and uncomment the below line. */

//$payment_url  = "https://www.paypal.com/cgi-bin/webscr"; // PayPal API URL

$cancel_return = 'http://villaalfredos.x10host.com/reservation/paypalCancelled.php';
$return_url = 'http://villaalfredos.x10host.com/reservation/paypalConfirmed.php?res_id=';
$merchant_email = "test.villaalfredo-facilitator@gmail.com";
// $merchant_email = "villa_alfredo_dev@gmail.com";
// merchant email id
// Optional if you need Seller fresh Account with 0 balance
// email = villa_alfredo_dev@gmail.com
// password = zxczxc123123
