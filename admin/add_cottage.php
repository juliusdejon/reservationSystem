<?php
include '../config/mysqli.php';

$cottage_type_id = $_POST['cottage_type_id'];
$cottage_number = $_POST['cottage_number'];

$sql = "SELECT cottage_number FROM rooms WHERE cottage_type_id='$cottage_type_id' AND cottage_number='$cottage_number'";
$res = $mysqli->query($sql);
$row = mysqli_fetch_assoc($res);
$cottage_number_db = $row['cottage_number'];

if ($cottage_number == '') {
    header('Location: view_cottage.php?cottage_type_id=' . $cottage_type_id . '&added=empty');
} else {
    if ($cottage_number == $cottage_number_db) {
        header('Location: view_cottage.php?cottage_id=' . $cottage_type_id . '&added=exists');
    } else {
        $sql = "INSERT INTO cottages (cottage_type_id,cottage_number) VALUES('$cottage_type_id','$cottage_number')";
        if ($mysqli->query($sql)) {
            header('Location: view_cottage.php?cottage_id=' . $cottage_type_id . '&added=true');

            echo "<script>
              window.location.href('view_cottage.php?<?php echo $cottage_type_id;?>')
              if ( window.history.replaceState ) {
                  window.history.replaceState( null, null, window.location.href('view_cottages.php?<?php echo $cottage_type_id;?>') );
              }
          </script>";
        }
    }
}
