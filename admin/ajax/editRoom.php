<?php
include '../../config/mysqli.php';
$room_id = $_POST['room_id'];
$room_name = $_POST['room_name'];
$room_type = $_POST['room_type'];
$room_price = $_POST['room_price'];
$child_capacity = $_POST['child_capacity'];
$adult_capacity = $_POST['adult_capacity'];
$room_info = $_POST['room_info'];

$image = (isset($_FILES['file']['name'])) ? $_FILES['file']['name'] : $_FILES['file']['name'] = "";

if ($image == '') {
    $sql = "UPDATE room_type SET room_name='$room_name',room_type='$room_type',room_info = '$room_info', room_adults_capacity = '$adult_capacity', room_child_capacity ='$child_capacity',room_price='$room_price'
     WHERE room_id ='$room_id'
";
    if ($res = $mysqli->query($sql)) {
        echo '<div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        Successfully Updated!
        </div>';
        echo "<script type='text/javascript'>

        $('#loadedRoomImage').load('ajax/live/loadedRoomImage.php?room_id=" . $room_id . "');

        </script>";

    } else {
        echo "failed";
    }
} else {
    $newFileName = uniqid('uploaded-', true)
    . '.' . strtolower(pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION));

    $sql = "UPDATE room_type
SET room_name='$room_name',
room_type='$room_type',
room_info = '$room_info',
room_capacity = '$room_capacity',
room_price='$room_price',
room_img = '$newFileName'
WHERE room_id ='$room_id'
";
    $res = $mysqli->query($sql);

    if ($res) {
// upload
        $target_dir = "../../img/";

        $target_file = $target_dir . $newFileName;
        $uploadOk = 1;

        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
// Check file size
        if ($_FILES["file"]["size"] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
// Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif") {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
        move_uploaded_file($_FILES["file"]["tmp_name"], $target_file);
        echo '<div class="alert alert-success alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
Successfully Updated!
</div>';
        echo "<script type='text/javascript'>

$('#loadedRoomImage').load('ajax/live/loadedRoomImage.php?room_id=" . $room_id . "');

</script>";

    }

}
