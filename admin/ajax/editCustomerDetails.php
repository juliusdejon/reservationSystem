<?php
include '../../config/mysqli.php';
$client_reference_id = $_POST['client_reference_id'];
$first_name = $_POST['first_name'];
$last_name = $_POST['last_name'];
$contact = $_POST['contact'];
$email = $_POST['email'];

$client_reference_id = trim($client_reference_id);
$first_name = htmlspecialchars(trim($first_name));
$last_name = htmlspecialchars(trim($last_name));
$contact = htmlspecialchars(trim($contact));
$email = htmlspecialchars(trim($email));

if ($first_name == '') {
    echo '<div class="alert callout callout-warning">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <p>Please enter First Name</p>
                </div>';
} else if ($last_name == '') {
    echo '<div class="alert callout callout-warning">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <p>Please enter Last Name</p>
            </div>';
} else {
    $sql = "UPDATE customer SET first_name='$first_name', last_name ='$last_name', contact_number='$contact', email_address='$email' WHERE client_reference_id ='$client_reference_id'";
    $res = $mysqli->query($sql);
    if ($res) {
        echo '<div class="alert callout callout-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <p>Customer Details Updated</p>
    </div>';
    } else {
        echo '<div class="alert callout callout-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <p>Customer Cannot Update</p>
  </div>';
    }
}
