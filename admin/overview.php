<?php
include '../config/mysqli.php';
$client_ref_id = $_GET['ref_id'];
@$view_by = $_GET['view_by'];
@$rebooked = $_GET['rebooked'];

$sql = "SELECT * FROM customer
        JOIN reservation ON customer.client_reference_id = reservation.client_reference_id
        LEFT JOIN transaction ON customer.client_reference_id = transaction.client_reference_id
        WHERE customer.client_reference_id = '$client_ref_id'";

$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $first_name = $rows['first_name'];
    $last_name = $rows['last_name'];
    $contact = $rows['contact_number'];
    $email = $rows['email_address'];
    $arrival_date = $rows['arrival_date'];
    $departure_date = $rows['departure_date'];
    $created_at = $rows['created_at'];
    $no_of_adults = $rows['no_of_adults'];
    $no_of_kids = $rows['no_of_kids'];
    $no_of_senior_pwd = $rows['no_of_senior_pwd'];
    $res_status = $rows['reservation_status'];
    $downpayment = $rows['downpayment'];
    $balance = $rows['balance'];
}

?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>Villa Alfredo Admin</title>

        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/adminlte.min.css">
         <!-- Semantic UI -->
        <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />




        <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap4.css">
    </head>

    <body class="hold-transition sidebar-mini">
        <div class="wrapper">

            <?php
include 'layout/navbar.php';
include 'layout/sidebar.php';
?>
                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <div class="content-header">
                        <div class="container-fluid">

                        </div>
                        <!-- /.container-fluid -->
                    </div>
                    <!-- /.content-header -->

                    <!-- Main content -->
                    <div class="content">
                        <?php
if ($rebooked == 'true') {

    echo "<div class='callout callout-success'>
                            <h5>Successfully Rebooked this Reservation</h5>
                          </div>";

} else if ($rebooked == 'false') {
    echo "<div class='callout callout-info'>
    <h5>Failed to Delete</h5>
    <p>The Rebooking failed to perform</p>
  </div>";

} else {

    if ($view_by == 'reservation') {
        echo '<a href="reservations.php" class="btn btn-sm btn-primary">Back </a>';
    }
    if ($view_by == 'checked_in') {
        echo '<a href="checked_in.php" class="btn btn-sm btn-primary">Back </a>';

    }
}
?>

                    <h3>Reference Id: <?php echo $client_ref_id; ?></h3>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <!-- /.card-header -->
                                    <div class="ui segment">
                                      <div class="row">
                                      <div class="col-md-6">
            <div class="logo">
                <img src="../img/valogo.png" width="200px">
            </div>
</div>
<div class="col-md-6 mt-3">
                <h3>Villa Alfredo's Resort</h3>

                <br>

                <span>Purok 1 Barangay Baliti, City of San Fernando</span>
                <span>,Pampanga Philippines</span>

                <br>

                <span>Manila Phone Office: +63 02 5844840</span>
                <span>/Resort Phone Office: +63 045 455-1397</span>
                <br>
                <span>villaalfredosresort@yahoo.com</span>
                <span>/villaalfredos@yahoo.com</span>
</div>
</div>
<hr />
<div class="row">
<div class="col-md-6">
  <?php if ($res_status == 'Pending') {?>
Reservation Status: <span class="badge bg-warning"><h4> <?php echo $res_status; ?></h4></span><br/><br/>
  <?php } else if ($res_status == 'Booked') {?>
Reservation Status: <span class="badge bg-success"><h4> <?php echo $res_status; ?></h4></span><br/><br/>
<?php
} else {

}
?>
<div>Customer Details:</div>
Reference ID: <?php echo $client_ref_id; ?> <br/>
Name: <?php echo $first_name . ' ' . $last_name; ?><br />
Contact Number: <?php echo $contact; ?> <br/>
Email Address: <?php echo $email; ?> <br />
<br />
<div> Reservation Details: </div>
Arrival Date: <?php echo $arrival_date; ?> <br />
Departure Date: <?php echo $departure_date; ?><br />
No of Nights: <?php
$departure = strtotime($departure_date);
$arrival = strtotime($arrival_date);
$datediff = $departure - $arrival;

$noofNights = round($datediff / (60 * 60 * 24));
echo $noofNights;

?>
 <br/><br />

<table border="1">
  <th style="padding:6px; color: #424242;">Room Type</th>
  <th style="padding:6px; color: #424242;"># of Rooms</th>
  <th style="padding:6px; color: #424242;">Rate per Night </th>
  <tbody>
    <?php
$sql = "SELECT * FROM reserved_rooms JOIN room_type ON reserved_rooms.room_type_id=room_type.room_id WHERE client_reference_id='$client_ref_id'
 GROUP BY room_type_id";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $room_id = $rows['room_id'];
    ?>
      <tr>
        <td><?php echo $rows['room_name']; ?></td>
        <td align="center">x
          <?php
$sql1 = "SELECT COUNT(*) as totalRoom FROM reserved_rooms WHERE room_type_id ='$room_id' AND client_reference_id ='$client_ref_id'";
    $res1 = $mysqli->query($sql1);
    $row = mysqli_fetch_assoc($res1);
    $countedRoom = $row['totalRoom'];
    echo $countedRoom;
    ?>
      </td>
      <td>₱ <?php echo number_format($rows['room_price'] * $countedRoom, 2); ?></td>
      </tr>
    <?php
}
?>
</tbody>
</table>
<br />
<br />
<div> Transaction Details: </div>
No of Kids: <?php echo $no_of_kids; ?><br />
No of Adults: <?php echo $no_of_adults; ?>
<br />
No of Senior/Pwd: <?php echo $no_of_senior_pwd; ?>
</div>
<div class="col-md-6">
<div class="row">
<div class="col-md-6">
<p class="lead">Extras</p>
<table border="1">
<th style="padding:6px; color: #424242;">Item</th>
<th style="padding:6px; color: #424242;">Qty</th>
<th style="padding:6px; color: #424242;">Price</th>
<tbody>
<?php
$totalExtras = [];
$sql = "SELECT * FROM customer_extras
            JOIN extras ON customer_extras.extras_id = extras.extras_id
            WHERE client_reference_id = '$client_ref_id' AND (extras_category='Rental' OR extras_category='Corkage' OR extras_category='Charges')
            GROUP BY customer_extras.extras_id";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $extras_id = $rows['extras_id'];
    $extras_name = $rows['extras_name'];
    $extras_price = $rows['extras_price'];
    $sqlCount = "SELECT COUNT(*) as extras_qty FROM customer_extras WHERE extras_id='$extras_id' AND client_reference_id='$client_ref_id'";
    $resCount = $mysqli->query($sqlCount);
    $rowCount = mysqli_fetch_array($resCount);
    $extras_qty = $rowCount['extras_qty'];
    array_push($totalExtras, $extras_qty * $extras_price);
    ?>
            <tr>
            <td align="center"><?php echo $extras_name; ?></td>
            <td align="center"><?php echo $extras_qty; ?></td>
            <td align="center">₱ <?php echo number_format($extras_price, 2); ?></td>
            </tr>



<?php
}

$totalExtras = array_sum($totalExtras);
?>
</tbody>
</table>
</div>
<div class="col-md-6">

</div>
</div>
                  <p class="lead">Amount</p>

                  <div class="table-responsive">
                    <table class="table">
                      <tbody>
                      <tr>
                      <th>Room Total:  </th>
                      <td>
<?php
$sql = "SELECT * FROM adjusted_rooms WHERE client_reference_id ='$client_ref_id'";
$res = $mysqli->query($sql);
$result_row = mysqli_num_rows($res);
if ($result_row > 0) {
    $total_rate = 0;
    while ($rows = mysqli_fetch_assoc($res)) {
        $adjusted_start_date = $rows['adjusted_start_date'];
        $total_rate += $rows['rate'];
    }

    $departure = strtotime($departure_date);
    $adjusted_start_date = strtotime($adjusted_start_date);
    $datediff = $departure - $adjusted_start_date;
    $adjusted_nights = round($datediff / (60 * 60 * 24));

    $sql = "SELECT * FROM reserved_rooms
    JOIN customer ON
    reserved_rooms.client_reference_id = customer.client_reference_id
    JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
    WHERE customer.client_reference_id ='$client_ref_id'
    GROUP BY reserved_rooms.room_type_id";
    $res = $mysqli->query($sql);
    $count = 1;
    $total = 0;
    $subtotal = [];
    while ($rows = mysqli_fetch_assoc($res)) {
        $room_type_id = $rows['room_type_id'];
        $sql1 = "SELECT COUNT(*) as quantity FROM reserved_rooms
JOIN customer ON
reserved_rooms.client_reference_id = customer.client_reference_id
JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
WHERE customer.client_reference_id ='$client_ref_id' AND room_type_id ='$room_type_id'";
        $res1 = $mysqli->query($sql1);
        $row1 = mysqli_fetch_assoc($res1);
        $total = $row1['quantity'] * $rows['room_price'];
        array_push($subtotal, $total);
    }
    function multiplyDays($total)
    {
        global $adjusted_nights;
        return ($total * $adjusted_nights);
    }

    $newTotal = array_sum(array_map("multiplyDays", $subtotal));

    $grandTotal = $total_rate + $newTotal;
} else {

    $sql = "SELECT * FROM reserved_rooms
                   JOIN customer ON
                   reserved_rooms.client_reference_id = customer.client_reference_id
                   JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
                   WHERE customer.client_reference_id ='$client_ref_id'
                   GROUP BY reserved_rooms.room_type_id";
    $res = $mysqli->query($sql);
    $count = 1;
    $total = 0;
    $subtotal = [];
    while ($rows = mysqli_fetch_assoc($res)) {
        $room_type_id = $rows['room_type_id'];
        $sql1 = "SELECT COUNT(*) as quantity FROM reserved_rooms
      JOIN customer ON
      reserved_rooms.client_reference_id = customer.client_reference_id
      JOIN room_type ON reserved_rooms.room_type_id = room_type.room_id
      WHERE customer.client_reference_id ='$client_ref_id' AND room_type_id ='$room_type_id'";
        $res1 = $mysqli->query($sql1);
        $row1 = mysqli_fetch_assoc($res1);
        $total = $row1['quantity'] * $rows['room_price'];
        array_push($subtotal, $total);
    }
    function multiplyDays($total)
    {
        global $noofNights;
        return ($total * $noofNights);
    }

    $grandTotal = array_sum($newTotal = array_map("multiplyDays", $subtotal));
}

echo "₱ " . number_format($grandTotal, 2);
?>
                      </td>
            <input type="hidden" name="room_total" id="roomTotal">
                      </tr>
                      <tr>
                      <th>Entrance Fees:</th>
                      <td>
                        <span id="entrance"></span>
                        <?php
$totalAdult = $no_of_adults * 250;
$totalKid = $no_of_kids * 150;
$totalSeniorPwd = $no_of_senior_pwd * 200;
echo "₱ " . number_format($totalAdult + $totalKid, 2);
?>
                      </td>
                      </tr>
                      <tr>
                      <th>Extras</th>
                      <td>
                        <span id="extras"></span>
                    <?php echo "₱ " . number_format($totalExtras, 2); ?>
                      </td>
                      </tr><tr>

                      <th>Grand Total</th>
                      <td>
                      <span id="subtotal"></span>
                     <?php echo "₱ " . number_format($grandTotal = $grandTotal + $totalAdult + $totalKid + $totalExtras, 2); ?>
                      </td>
                      </tr>

                      <tr>
                        <th style="width:50%">Downpayment</th>
                        <td>            <span id="downpayment">
            <?php echo "₱ " . number_format($downpayment, 2); ?>          </span>
            <input type="hidden" name="downpayment" id="downpaymentTotal" >

            </td>
                      </tr>
                      <tr>
                        <th>Balance </th>
                        <td>
                                                <span id="balance"><?php

if ($downpayment > $grandTotal) {
    echo "₱ 0.00";
} else {
    echo "₱ " . number_format($grandTotal - $downpayment, 2);
}
?></span>
                        <input type="hidden" name="balance" id="balanceTotal" value="1000 ">
                        </td>
</tr>

                    </tbody></table>
                  </div>
                </div>
</div>


</div>
                                    <!-- /.card-body -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.content -->
                </div>
                <!-- /.content-wrapper -->

                <!-- Main Footer -->
                <?php
include 'layout/footer.php';
?>
                    <!-- ./wrapper -->

                    <!-- REQUIRED SCRIPTS -->


                    <!-- jQuery -->
                    <script src="plugins/jquery/jquery.min.js"></script>
                    <!-- Bootstrap 4 -->
                    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
                    <!-- DataTables -->
                    <script src="plugins/datatables/jquery.dataTables.js"></script>
                    <script src="plugins/datatables/dataTables.bootstrap4.js"></script>
                    <!-- AdminLTE App -->
                    <script src="dist/js/adminlte.min.js"></script>
    </body>

    </html>
