<?php
include '../config/mysqli.php';

?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>Villa Alfredo Admin</title>

        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/adminlte.min.css">
         <!-- Semantic UI -->
        <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />




        <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap4.css">
        <style>
  body
  {
   margin:0;
   padding:0;
   background-color:#f1f1f1;
  }
  .box
  {
   width:1270px;
   padding:20px;
   background-color:#fff;
   border:1px solid #ccc;
   border-radius:5px;
   margin-top:25px;
   box-sizing:border-box;
  }
  </style>
    </head>

    <body class="hold-transition sidebar-mini">
        <div class="wrapper">

            <?php
include 'layout/navbar.php';
include 'layout/sidebar.php';
?>
                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <div class="content-header">
                        <div class="container-fluid">

                        </div>
                        <!-- /.container-fluid -->
                    </div>
                    <!-- /.content-header -->

                    <!-- Main content -->
                    <div class="content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Reservations</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">








  <div class="container box">
   <h1 align="center">Live Add Edit Delete Datatables Records using PHP Ajax</h1>
   <br />
   <div class="table-responsive">
   <br />
    <div align="right">
     <button type="button" name="add" id="add" class="btn btn-info">Add</button>
    </div>
    <br />
    <div id="alert_message"></div>
    <table id="user_data" class="table table-bordered table-striped dataTable">
     <thead>
      <tr>
       <th>Frist Name</th>
       <th>Last Name</th>
       <th></th>
      </tr>
     </thead>
    </table>
   </div>
  </div>




                                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                            <thead>
                                                <tr role="row">

                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending">Name</th>
                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Date</th>
                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Arrival Date</th>
                                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Reference Id</th>
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column descending" aria-sort="ascending">Status</th>
                                                    <th>Receipt</th>
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column descending" aria-sort="ascending">Action</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
$sql = "SELECT * FROM customer as c INNER JOIN reservation as r ON c.client_reference_id=r.client_reference_id";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $client_reference_id = $rows['client_reference_id'];
    $first_name = $rows['first_name'];
    $last_name = $rows['last_name'];
    $arrival_date = new DateTime($arrival_date = $rows['arrival_date']);
    $created_at = new DateTime($created_at = $rows['created_at']);
    $reservation_status = $rows['reservation_status'];
    $receipt = $rows['receipt'];
    ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $first_name . ' ' . $last_name; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $created_at->format('F j, Y'); ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $arrival_date->format('F j, Y'); ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $client_reference_id; ?>
                                                        </td>
                                                        <td>
                                                            <?php echo $reservation_status; ?>
                                                        </td>
                                                        <td>
                                                            <?php if ($receipt != '') {
        echo '<a href="dist/img/receipts/' . $receipt . '" target="_blank"><button type="button" class="btn btn-info btn-sm">Receipt</button></a>';

    } else {
        echo '<button class="btn btn-sm">No Receipt</button>';
    }?>
                                                                <button type="button" name="approve" class='btn btn-sm btn-success' data-toggle="modal" data-target="#<?php echo $client_reference_id; ?>"><i class='fa fa-thumbs-up'></i></button>
                                                            <!-- Modal -->
                                                            <div class="modal" id="<?php echo $client_reference_id; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                                <div class="modal-dialog modal-dialog-centered modal-md" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLongTitle">Receipt of
                                                                                <?php echo $first_name . ' ' . $last_name; ?>
                                                                            </h5>

                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div>
                                                                                <div id="result<?php echo $client_reference_id; ?>"></div>
                                                                                <div class="row">
                                                                                    <div class="col-md-3">
                                                                                        <small>Confirm the amount below or check your Email</small>
                                                                                    </div>
                                                                                    <div class="col-md-8">
                                                                                        <form action="ajax/acceptDownpayment.php" method="post" id="accept<?php echo $client_reference_id; ?>">
                                                                                            <div class="input-group-prepend input-group-sm">
                                                                                                <span class="input-group-text">
                    ₱
                    </span>
                    <?php $sqlAmt = "SELECT downpayment FROM transaction WHERE client_reference_id ='$client_reference_id'";
    $resAmt = $mysqli->query($sqlAmt);
    $amount = mysqli_fetch_assoc($resAmt);
    $amount = $amount['downpayment'];

    if ($amount) {
        echo "<input type='number' name='amount' class='form-control' value='" . $amount . "' />";
    } else {
        echo "<input type='number' name='amount' class='form-control'/>";
    }
    ?>

                                                                                                <input type="hidden" name="client_reference_id" value="<?php echo $client_reference_id; ?>"/>
                                                                                                <span class="input-group-append">
                    <button type="submit" name="accept" id="button<?php echo $client_reference_id; ?>" class="btn btn-info btn-flat btn-accept" onclick="foo('<?php echo $client_reference_id; ?>')">Accept Downpayment</button>
                  </span>
                                                                                            </div>
                                                                                        </form>

                                                                                    </div>
                                                                                </div>
                                                                                <?php if ($receipt) {
        echo "<img src='dist/img/receipts/" . $receipt . "' width='100%'/>";
    } else {
        echo "No Receipts Found";
    }
    ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                        </td>
                                                        <td>
                                                            <div class='container'>
                                                                <form action="" method="post">
                                                                    <input type="hidden" name="client_reference_id" value="<?php echo $client_reference_id; ?>" />
                                                                    <a class='btn btn-info btn-sm' href="overview.php?ref_id=<?php echo $client_reference_id; ?>"><i class='fa fa-info'></i></a>
                                                                    <button class='btn btn-warning btn-sm'><i class='fa fa-edit'></i></button>
                                                                    <button class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>
                                                                </form>

                                                            </div>

                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php
}
?>
                                            </tbody>

                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.content -->
                </div>
                <!-- /.content-wrapper -->

                <!-- Main Footer -->
                <?php
include 'layout/footer.php';
?>
                    <!-- ./wrapper -->

                    <!-- REQUIRED SCRIPTS -->


                    <!-- jQuery -->
                    <script src="plugins/jquery/jquery.min.js"></script>

                    <script type="text/javascript" language="javascript" >
 $(document).ready(function(){

  fetch_data();

  function fetch_data()
  {
   var dataTable = $('#user_data').DataTable({
    "processing" : true,
    "serverSide" : true,
    "order" : [],
    "ajax" : {
     url:"fetch.php",
     type:"POST"
    }
   });
  }

  function update_data(id, column_name, value)
  {
   $.ajax({
    url:"update.php",
    method:"POST",
    data:{id:id, column_name:column_name, value:value},
    success:function(data)
    {
     $('#alert_message').html('<div class="alert alert-success">'+data+'</div>');
     $('#user_data').DataTable().destroy();
     fetch_data();
    }
   });
   setInterval(function(){
    $('#alert_message').html('');
   }, 5000);
  }

  $(document).on('blur', '.update', function(){
   var id = $(this).data("id");
   var column_name = $(this).data("column");
   var value = $(this).text();
   update_data(id, column_name, value);
  });

  $('#add').click(function(){
   var html = '<tr>';
   html += '<td contenteditable id="data1"></td>';
   html += '<td contenteditable id="data2"></td>';
   html += '<td><button type="button" name="insert" id="insert" class="btn btn-success btn-xs">Insert</button></td>';
   html += '</tr>';
   $('#user_data tbody').prepend(html);
  });

  $(document).on('click', '#insert', function(){
   var first_name = $('#data1').text();
   var last_name = $('#data2').text();
   if(first_name != '' && last_name != '')
   {
    $.ajax({
     url:"insert.php",
     method:"POST",
     data:{first_name:first_name, last_name:last_name},
     success:function(data)
     {
      $('#alert_message').html('<div class="alert alert-success">'+data+'</div>');
      $('#user_data').DataTable().destroy();
      fetch_data();
     }
    });
    setInterval(function(){
     $('#alert_message').html('');
    }, 5000);
   }
   else
   {
    alert("Both Fields is required");
   }
  });

  $(document).on('click', '.delete', function(){
   var id = $(this).attr("id");
   if(confirm("Are you sure you want to remove this?"))
   {
    $.ajax({
     url:"delete.php",
     method:"POST",
     data:{id:id},
     success:function(data){
      $('#alert_message').html('<div class="alert alert-success">'+data+'</div>');
      $('#user_data').DataTable().destroy();
      fetch_data();
     }
    });
    setInterval(function(){
     $('#alert_message').html('');
    }, 5000);
   }
  });
 });
</script>

                    <!-- Downpayment Ajax -->
                    <script type="text/javascript" src="ajax/acceptDownpayment.js"></script>
                    <!-- Bootstrap 4 -->
                    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
                    <!-- DataTables -->
                    <script src="plugins/datatables/jquery.dataTables.js"></script>
                    <script src="plugins/datatables/dataTables.bootstrap4.js"></script>
                    <!-- AdminLTE App -->
                    <script src="dist/js/adminlte.min.js"></script>
                    <script>
                        $(function() {

                            $('#example1').DataTable({
                                "paging": true,
                                "lengthChange": false,
                                "searching": true,
                                "ordering": true,
                                "info": true,
                                "autoWidth": false,
                                "stripeClasses": ['odd-row', 'even-row'],
                                "order": [[ 4, "desc" ]]
                            });
                        });
                    </script>
    </body>

    </html>
    <?php
