<?php
$res_id = $_GET['res_id'];

?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>Villa Alfredo Admin</title>

        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/adminlte.min.css">
        <!-- Semantic UI -->
        <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />

        <script>
            function resizeIframe(obj) {
                obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
            }
        </script>

    </head>

    <body class="hold-transition sidebar-mini">
        <div class="wrapper">

            <?php
include 'layout/navbar.php';
include 'layout/sidebar.php';
?>
                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <div class="content-header">
                        <div class="container-fluid">
                            <div class="row mb-2">
                                <div class="col-sm-6">
                                    <h3>Check Out</h3>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->
                    </div>
                    <!-- /.content-header -->

                    <!-- Main content -->
                    <div class="content">
                        <div class="ui segment">
                          <div class="row">
                            <div class="col-md-6">
                <h3> Deposited items:</h3>
                <?php $sql = "SELECT * FROM customer_extras JOIN
                              extras ON extras.extras_id = customer_extras.extras_id WHERE client_reference_id ='$res_id' AND extras_category='Deposit'";
$res = $mysqli->query($sql);
$depositTotal = [];
while ($rows = mysqli_fetch_assoc($res)) {
    array_push($depositTotal, $rows['extras_price']);
    ?>
  <div class="ui checkbox">
  <input type="checkbox" class="check<?php echo $rows['extras_id']; ?>" onclick="lessDeposit(<?php echo $rows['extras_id']; ?>,<?php echo $rows['extras_price']; ?>)">
  <label><?php echo $rows['extras_name']; ?></label>
</div>
<?php
}
$depositTotal = array_sum($depositTotal);
?>
<?php
$sql = "SELECT * FROM customer WHERE client_reference_id='$res_id'";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $arrival_date = $rows['arrival_date'];
}

$sql = "SELECT * FROM transaction WHERE client_reference_id='$res_id'";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $check_in_date = $rows['check_in_date'];
}
$sql = "SELECT * FROM customer WHERE client_reference_id='$res_id'";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $departure_date = $rows['departure_date'];
}
?>
<br/>
Reservation Date: <?php echo date("F d, Y", strtotime($arrival_date)) . " 02:00 PM"; ?> - <?php echo date("F d, Y", strtotime($departure_date)) . " 12:00 PM" ?><br/>
Check In Time: <?php
echo date("F d, Y h:i A", strtotime($check_in_date)); ?><br/>
Check Out Time: <?php
echo date('F d, Y h:i A');
?>
<br>
<?php

$earlyTotal = [];
$exceedTotal = [];
// Early Check In and Extension
// Check In Date - Arrival Date
$arrival_date = $arrival_date . ' 14:00:00';

$arrival_date = new DateTime($arrival_date);
$check_in_date = new DateTime($check_in_date);
if ($arrival_date > $check_in_date) {
    $interval = $check_in_date->diff($arrival_date);
    // Get Early Stay Hours
    $interval = $interval->format('%h');
} else {
    $interval = 0;
}

// Extension
$now = date('Y-m-d H:i:s');
$departure_date = $departure_date . ' 12:00:00';
$departure_date = new DateTime($departure_date);
$now = new DateTime($now);
if ($now > $departure_date) {
    $extension = $departure_date->diff($now);
    $extension = $extension->format('%h');
} else {
    $extension = 0;
}
$sql = "SELECT * FROM room_type JOIN reserved_rooms
        ON reserved_rooms.room_type_id = room_type.room_id
        WHERE reserved_rooms.client_reference_id='$res_id'
        GROUP BY room_id";
$res = $mysqli->query($sql);
echo "<table border='1'>";
echo "<tr>";
echo "<th>Room</th>";
echo "<th>Qty</th>";
echo "<th>Hourly Rates</th>";
echo "<th>Early</th>";
echo "<th>Exceed</th>";
echo "<th>Total</th>";
echo "</tr>";

while ($rows = mysqli_fetch_assoc($res)) {
    // echo "<div>";
    $room_id = $rows['room_id'];
    $room_name = $rows['room_name'];
    $room_hourly_rate = $rows['room_hourly_rate'];
    $sql1 = "SELECT COUNT(*) as reservedRoomQuantity FROM reserved_rooms WHERE client_reference_id='$res_id' AND room_type_id = '$room_id'";
    $res1 = $mysqli->query($sql1);
    $row1 = mysqli_fetch_assoc($res1);
    $quantity = $row1['reservedRoomQuantity'];
    $total = $quantity * ($room_hourly_rate * ($interval + $extension));
    $early = $quantity * ($room_hourly_rate * $interval);
    $exceed = $quantity * ($room_hourly_rate * $extension);
    echo "<tr>";
    echo "<td>" . $room_name . "</td>";
    echo "<td align='center'>" . $quantity . "</td>";
    echo "<td align='center'>₱ " . number_format($room_hourly_rate, 2) . "</td>";
    echo "<td align='center'>" . $interval . "</td>";
    echo "<td align='center'>" . $extension . "</td>";
    echo "<td>₱ " . number_format($total, 2) . "</td>";
    echo "</tr>";
    array_push($earlyTotal, $early);
    array_push($exceedTotal, $exceed);
}
echo "</table>"
?>
                              </div>
                <div class="col-md-6">

      <?php

$totalExtras = [];
$sql = "SELECT * FROM customer_extras
            JOIN extras ON customer_extras.extras_id = extras.extras_id
            WHERE client_reference_id = '$res_id' AND extras_category='Rental' OR extras_category='Corkage' OR extras_category='Charges'
            GROUP BY customer_extras.extras_id";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $extras_id = $rows['extras_id'];
    $extras_name = $rows['extras_name'];
    $extras_price = $rows['extras_price'];
    $sqlCount = "SELECT COUNT(*) as extras_qty FROM customer_extras WHERE extras_id='$extras_id'";
    $resCount = $mysqli->query($sqlCount);
    $rowCount = mysqli_fetch_array($resCount);
    $extras_qty = $rowCount['extras_qty'];
    array_push($totalExtras, $extras_qty * $extras_price);
}
$totalExtras = array_sum($totalExtras);

$sql = "SELECT * FROM transaction WHERE client_reference_id='$res_id'";
$res = $mysqli->query($sql);
$rows = mysqli_fetch_assoc($res);
$total = $rows['total'] + $totalExtras;
$downpayment = $rows['downpayment'];
$balance = $rows['balance'] + $totalExtras;

?>
<div class="row">
  <div class="col-md-3">
  <label>Early CheckIn</label>
</div>
<div class="col-md-3">
<div class="ui input">₱
<?php
$earlyTotal = array_sum($earlyTotal);
echo number_format($earlyTotal, 2);
?>
</div>
</div>
</div>

<div class="row">
  <div class="col-md-3">
  <label>Extension</label>
</div>
<div class="col-md-3">
<div class="ui input">₱
<?php
$exceedTotal = array_sum($exceedTotal);
echo number_format($exceedTotal, 2);
?>
</div>
</div>
</div>

<div class="row">
  <div class="col-md-3">
  <label>Extras</label>
</div>
<div class="col-md-3">
<div class="ui input">
<span>₱ <?php echo number_format($totalExtras, 2); ?></span>
</div>
</div>
</div>

<div class="row">
  <div class="col-md-3">
  <label>Grand Total</label>
</div>
<div class="col-md-3">
<div class="ui input">
<span>₱ <?php echo number_format($total, 2); ?></span>
</div>
</div>
</div>


<div class="row">
  <div class="col-md-3">
  <label>Less Downpayment</label>
</div>
<div class="col-md-3">
<div class="ui input">
  <span>₱ <?php echo number_format($downpayment, 2); ?></span>
</div>
</div>
</div>

<div class="row">
  <div class="col-md-3">
  <label>Less Deposits</label>
</div>
<div class="col-md-3">
<div class="ui input">
<span id="depositTotalInputTxt">₱ <?php echo number_format($depositTotal, 2); ?></span>
<input type="hidden" placeholder="Deposit" id="depositTotalInput" value="<?php echo $depositTotal; ?>">
</div>
</div>
</div>

<?php
$balance = $balance;
?>
<div class="row">
  <div class="col-md-3">
  <label>Balance</label>
</div>
<div class="col-md-3">

<form action="check_out_feedback.php" name="checkoutForm" method="post" onsubmit="return validateForm();">
<input type="hidden" name="client_ref_id" value="<?php echo $res_id; ?>">
<input type="hidden" name="extra" value="<?php echo $totalExtras; ?>">
<div class="ui input">
<span id="balanceTotalTxt">₱ <?php echo number_format($balance, 2); ?></span>
<input type="hidden" id="balanceTotal" placeholder="Total" value="<?php echo $balance; ?>" name="balance">
</div>
</div>
</div>
<div class="row">
  <div class="col-md-3">
  <label>Enter Amount</label>
</div>
<div class="col-md-3">
<div class="ui input">
<input type="number" name="amount" min="0" oninput="validity.valid||(value='');" placeholder="Enter Amount">

</div>
</div>
</div>



<button name="submit" class="ui pull-right button teal">Checkout</button>
</form>
                </div>

                </div>
                        </div>
                    </div>
                    <div class="ui segment container">
                        <button class="ui teal button" onclick="printFrame('receipt')">Print Transaction Details</button>
                        <iframe id="receipt" class="ui container fluid" src="transaction_receipt.php?res_id=<?php echo $res_id; ?>" frameborder="0" allow="autoplay" scrolling="no" onload="resizeIframe(this)"></iframe>

                    </div>


                </div>
                <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Main Footer -->
        <?php
include 'layout/footer.php';
?>
            <!-- ./wrapper -->

            <!-- REQUIRED SCRIPTS -->
            <script>
              function lessDeposit(id,price) {
              var status = $('.check' + id)[0].checked;
                if(status == true) {
                  var dep = $("#depositTotalInput").val();

                  var less = parseInt(dep) - parseInt(price);
                  $("#depositTotalInput").val(less);

                  var formattedDep = (less).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                  $("#depositTotalInputTxt").text(formattedDep);


                  var bal = $("#balanceTotal").val();
                  var newBal = parseInt(bal) - price;
                  $("#balanceTotal").val(newBal);
                  var formattedBal = (newBal).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                  $("#balanceTotalTxt").text(formattedBal);

                } else {
                  var dep = $("#depositTotalInput").val();
                  var less = parseInt(dep) + parseInt(price);
                  $("#depositTotalInput").val(less);

                  var formattedDep = (less).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                  $("#depositTotalInputTxt").text(formattedDep);

                  var bal = $("#balanceTotal").val();
                  var newBal = parseInt(bal) + price;
                  $("#balanceTotal").val(newBal);
                  var formattedBal = (newBal).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                  $("#balanceTotalTxt").text(formattedBal);

                }

              }
              </script>
               <script>
                        // Validation Payment form
                        function validateForm() {
                            var balance = document.forms["checkoutForm"]["balance"].value;
                            var amount = document.forms["checkoutForm"]["amount"].value;

                            if(amount == '') {
                              alert('Enter amount');
                              return false;
                            }
                            if(balance > amount) {
                              alert('Insufficient Amount given');
                              return false;
                            }
                            if(amount > balance) {
                                alert('Amount Entered is Higher than the balance');
                                return false;
                            }

                        }
                    </script>
              <script>

              function printFrame(id) {
                        var frm = document.getElementById(id).contentWindow;
                        frm.focus();// focus on contentWindow is needed on some ie versions
                        frm.print();
                        return false;
              }
              </script>
            <!-- jQuery -->
            <script src="plugins/jquery/jquery.min.js"></script>
            <!-- Bootstrap 4 -->
            <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
            <!-- AdminLTE App -->
            <script src="dist/js/adminlte.min.js"></script>
    </body>

    </html>