<?php
include '../config/mysqli.php';
include 'daytourFunctions.php';

$client_ref_id = $_GET['res_id'];

$sql = "SELECT * FROM customer WHERE client_reference_id= '$client_ref_id'";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    $client_ref_id = $rows['client_reference_id'];
    $first_name = $rows['first_name'];
    $last_name = $rows['last_name'];
    $contact = $rows['contact_number'];
    $email = $rows['email_address'];
    $arrival_date = $rows['arrival_date'];
    $departure_date = $rows['departure_date'];
    $no_of_kids = $rows['no_of_kids'];
    $no_of_adults = $rows['no_of_adults'];
    $no_of_senior_pwd = $rows['no_of_senior_pwd'];
}
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>Villa Alfredo Admin</title>

        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/adminlte.min.css">
        <!-- Semantic UI -->
        <link type="text/css" rel="stylesheet" href="../css/semantic.min.css" />
             <!-- Initialize Semantic UI and JQuery to load Calendar Datepicker-->
        <script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" src="../js/semantic.min.js"></script>

        <!-- Calendar -->
        <link type="text/css" rel="stylesheet" href="../css/calendar.min.css" />
        <script src="../js/calendar.min.js"></script>

        <!-- Icons -->
        <link rel="stylesheet" href="../css/icon.min.css">

        <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap4.css">
    </head>

    <body class="hold-transition sidebar-mini">
        <div class="wrapper">

            <?php
include 'layout/navbar.php';
include 'layout/sidebar.php';
?>
                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <div class="content-header">
                        <div class="container-fluid">

                        </div>
                        <!-- /.container-fluid -->
                    </div>
                    <!-- /.content-header -->

                    <!-- Main content -->
                    <div class="content">
                        <div class="row">
                            <div class="col-md-12">
                                    <!-- /.card-header -->
                                    <div class="ui segment">
<?php $sql = "SELECT transaction_type FROM transaction WHERE client_reference_id ='$client_ref_id'";
$res = $mysqli->query($sql);
$rows = mysqli_fetch_assoc($res);
$type = $rows['transaction_type'];
?>
                                        <div class="row">
                                            <div class="col-md-6">
                                            <span><label class="button button-success"><h3><?php echo $type; ?> Walkin</h3></label></span>
    <div class="row">

<div class="col-md-12 mb-0">
<h5>Customer Details</h5>
<div class="row">
<div class="col-md-6">
Reference ID:
</div>
<div class="col-md-6">
    <?php echo $client_ref_id; ?>
<input type="hidden" name="client_reference_id" value="<?php echo $client_ref_id; ?>" />
</div>
</div>

<div class="row">
<div class="col-md-6">
First Name:
</div>
<div class="col-md-6">
<?php echo $first_name; ?>
</div>
</div>


<div class="row">
<div class="col-md-6">
Last Name:
</div>
<div class="col-md-6">
<?php echo $last_name; ?>
</div>
</div>

<div class="row">
<div class="col-md-6">
Contact Number:
</div>
<div class="col-md-6">
    <?php echo $contact; ?>
</div>
</div>

<div class="row">
  <div class="col-md-6">
  Email Address:
  </div>
  <div class="col-md-6">
      <?php echo $email; ?>
  </div>
</div>


</form>


<div class="row">
<div class="col-md-6">
Arrival Date
</div>
<div class="col-md-6">
    <?php echo $arrival_date; ?>
<!-- <div class="ui calendar" id="rangestart">
<input type="text" class="form-control form-control-sm" placeholder="Start" name="arrival_date" autocomplete="off" value="<?php echo $arrival_date; ?>">
</div> -->
</div>
</div>


<?php
$departure = strtotime($departure_date);
$arrival = strtotime($arrival_date);
$datediff = $departure - $arrival;

$noofNights = round($datediff / (60 * 60 * 24));
$noofNights;

?>


<div class="row">
<div class="col-md-6">
No of Adults
</div>
<div class="col-md-6">
<?php echo $no_of_adults; ?>

</div>
</div>
<div class="row">
<div class="col-md-6">
    No of Kids below 3 feet
</div>
<div class="col-md-6">
<?php echo $no_of_kids; ?>
</div>
</div>

<div class="row">
<div class="col-md-6">
    No of Senior/Pwd
</div>
<div class="col-md-6">
<?php echo $no_of_senior_pwd; ?>
</div>
</div>





</div>
</div>

    </div>


                                            <div class="col-md-6">
                                                <form action="walkInForm.php" name="checkoutForm" method="post" onsubmit="return validateForm();">
                                                <input type="hidden" name="client_reference_id" value="<?php echo $client_ref_id; ?>" />
                                                <p class="lead">Amount</p>

                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <tbody>
                                                        <tr>
                                                                <th>Corkages</th>
                                                                <td>
                                                                <div class="input-group">
                                                                        <div class="input-group-prepend"> <span class="input-group-text">Php</span></div>
                                                                    <input type="text" class="form-control numbers-only" name="corkages" id="corkage" placeholder="Corkage Fee" value="0" maxlength="5"/>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>

                                                            <tr>
                                                                <th>Entrance Fees:</th>
                                                                <td>
                                                                <?php
if ($type == 'Daytour') {

    $entranceTotal = ($no_of_adults * 230) + ($no_of_kids * 130) + ($no_of_senior_pwd * 184);

} else {
    $entranceTotal = ($no_of_adults * 250) + ($no_of_kids * 150) + ($no_of_senior_pwd * 200);

}

?>
                                                                    <input type="hidden" id="entrance" value="<?php echo $entranceTotal; ?>" />
                                                                    <span id="entranceText">₱
                                                                    <?php echo number_format($entranceTotal, 2); ?>
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>Extras(eg. Cottages, Tables)</th>
                                                                <td>
                                                                    <span id="cottagesText">₱ 0.00</span>
                                                                    <input type="hidden" id="cottagesValue">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th>Total</th>
                                                                <td>
                                                                    <span id="totalText">₱ <?php echo number_format($entranceTotal, 2); ?></span>
                                                                    <input type="hidden" name="total" id="total" value="<?php echo $entranceTotal; ?>" >
                                                                </td>
                                                            </tr>


                                                            <tr>
                                                                <th style="width:50%">Payment</th>
                                                                <td>
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend"> <span class="input-group-text">Php</span></div>
                                                                    <input type="text" class="form-control numbers-only" name="downpayment" placeholder="Payment in Peso" maxlength="5"/>
                                                                    </div>
                                                                </td>
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">


                                        <button class="ui button teal pull-right" name="compute">Print Receipt <i class="fa fa-print"></i></button>
                                        </div>
                                        </div>

                                    </div>

                                <a class="ui button teal mb-3">Select Cottages <i class="fa fa-arrow-down"></i></a> * Available Cottages below
                                <div class="ui four cards">
                    <?php
$sql = "SELECT * FROM cottage_type";
$res = $mysqli->query($sql);
while ($rows = mysqli_fetch_assoc($res)) {
    // POST VARIABLES
    $cottage_type_id = $rows['cottage_type_id'];
    $cottage_name = $rows['cottage_name'];
    $cottage_price = $rows['cottage_price'];
    $cottage_info = $rows['cottage_info'];
    $cottage_img = $rows['cottage_img'];

    ?>
                    <div class="card">
                        <div class="image">
                            <img src="../img/<?php echo $cottage_img; ?>">
                        </div>
                        <div class="content">
                            <h3>
                                <?php echo $cottage_name; ?>

                            </h3>
                            <div>Price:
                                <?php echo number_format($cottage_price, 2); ?>
                            </div>
                            Quantity:
                            <div class="ui input right floated">
                            <input type="hidden" name="cottage_id<?php echo $cottage_type_id; ?>"  />
                            <select class="ui tiny teal button quantity" name="quantity<?php echo $cottage_type_id; ?>" onchange="computeCottage(this.value,<?php echo $cottage_price; ?>,<?php echo $cottage_type_id; ?>);">
                                            <option value="0" selected>0</option>
                                            <?php
// Available Rooms = Total Rooms - Reserved Rooms IN Check In Date
    $sqlAvailableRoom = "SELECT COUNT(*) as TotalCottages FROM cottages WHERE cottage_type_id='$cottage_type_id'";
    $resAvailableRoom = $mysqli->query($sqlAvailableRoom);
    $result = mysqli_fetch_assoc($resAvailableRoom);
    $result = $result['TotalCottages'];
    for ($i = 1; $i <= $result; $i++) {
        echo "<option value='$i'>$i</option>";
    }
    ?>
                                        </select>

                             </div>

                        </div>
                        <div class="extra">
                            <div>
                                <a class="fluid tiny ui teal button" data-toggle="collapse" href="#<?php echo $cottage_type_id; ?>" role="button" aria-expanded="false" aria-controls="collapseExample">
                                    Description
                                </a>
                                <div class="collapse" id="<?php echo $cottage_type_id; ?>">
                                    <?php echo $cottage_info; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php }?>
</form>






                                <!-- /.card-body -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Main Footer -->
        <?php
include 'layout/footer.php';
?>
            <!-- ./wrapper -->

            <!-- REQUIRED SCRIPTS -->
            <script>
            const today = new Date();
            $('#rangestart').calendar({
                minDate: today,
                type: 'date',
                endCalendar: $('#rangeEnd')
            });
            $('#rangeEnd').calendar({
                minDate: today,
                type: 'date',
                startCalendar: $('#rangestart')
            });
            </script>

            <!-- jQuery -->
            <script src="plugins/jquery/jquery.min.js"></script>
            <script>

                 Number.prototype.format = function(n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};


                        // Validation Payment form
                        function validateForm() {
                            var total = document.forms["checkoutForm"]["total"].value;
                            var downpayment = document.forms["checkoutForm"]["downpayment"].value;

                            if(downpayment == '') {
                              alert('Enter amount');
                              return false;
                            }
                            if(downpayment < total) {
                              alert('Insufficient Amount given');
                              return false;
                            }
                            if(downpayment > total) {
                                alert('Amount entered is too high');
                                return false;
                            }

                        }
                    </script>
            <script>
            $('#corkage').keydown(function() {
                var corkage = $("#corkage").val();
                var entrance = $("#entrance").val();
                if(corkage == '') {
                        corkage = 0;
                    }
                var cottage = $("#cottagesValue").val();
                if(cottage =='') {
                    cottage = 0;
                }
                var total = parseInt(cottage) + parseInt(entrance) + parseInt(corkage);
                $("#totalText").text('₱ ' + total.format(2));
                $("#total").val(parseInt(cottage) + parseInt(entrance) + parseInt(corkage));
            });

            $('#corkage').keyup(function() {
                var corkage = $("#corkage").val();
                var entrance = $("#entrance").val();
                if(corkage == '') {
                        corkage = 0;
                    }
                var cottage = $("#cottagesValue").val();
                if(cottage =='') {
                    cottage = 0;
                }
                var total = parseInt(cottage) + parseInt(entrance) + parseInt(corkage);
                $("#totalText").text('₱ ' + total.format(2));
                $("#total").val(parseInt(cottage) + parseInt(entrance) + parseInt(corkage));
            });
                let cottages = [];
                function computeCottage(quantity,price,cottageId) {
                        if(cottages.length == 0) {
                        cottages.push([quantity,price,cottageId]);
                        } else {
                        let checker = 0;
                        for (let i =0; i < cottages.length; i++) {
                            console.log(cottages[i][2])
                            if(cottageId == cottages[i][2]) {
                                cottages.splice([i],1);
                                cottages.push([quantity,price,cottageId]);
                                checker = 1;
                            }
                            else {
                                // console.log('your loop is not working');
                            }
                        }

                        if(checker == 1) {
                            // console.log('Already Existing');
                        } else {
                    cottages.push([quantity,price,cottageId]);

                        }
                    }
                    let total = [];
                    for(let i = 0; i < cottages.length; i++) {
                        total.push(parseInt(cottages[i][0] * cottages[i][1]));
                    }
                    console.log(total);
                    var sum = 0;
                    for (var i = 0; i < total.length; i++) {
                        sum += total[i]
                    }

                    function getSum(total, num) {
                     return total + num;
                    }

                    total.reduce(getSum);
                    console.log(cottages);
                    var corkage = $("#corkage").val();
                    $("#cottagesText").text('₱ '+sum.format(2));
                    $("#cottagesValue").val(sum);
                    var entrance = $("#entrance").val();
                    if(corkage == '') {
                        corkage = 0;
                    }
                    sum = sum + parseInt(entrance) + parseInt(corkage)
                    $("#totalText").text('₱ '+sum.format(2));
                    $("#total").val(sum);
                }
            </script>
            <!-- jQuery -->
            <script src="plugins/jquery/jquery.min.js "></script>
            <!-- JQuery Validation source -->
            <script type="text/javascript" src="plugins/jquery/jquery-key-restrictions.min.js"></script>
            <!-- Actual Validation calling class -->
            <script type="text/javascript">
            $(document).ready(function () {
                $(".letters-only").lettersOnly();
                $(".numbers-only").numbersOnly();
                $(".alpha-numeric-only").alphaNumericOnly();
            });
            </script>
            <!-- Bootstrap 4 -->
            <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
            <!-- DataTables -->
            <script src="plugins/datatables/jquery.dataTables.js"></script>
            <script src="plugins/datatables/dataTables.bootstrap4.js"></script>
            <!-- AdminLTE App -->
            <script src="dist/js/adminlte.min.js"></script>
    </body>

    </html>